//
//  API.h
//  CL Panic
//
//  Created by INT MAC 2015 on 27/06/16.
//  Copyright © 2016 IntotalityInc. All rights reserved.
//

#ifndef API_h
#define API_h

#define TimeSlotValue  [[NSUserDefaults standardUserDefaults] valueForKey:@"TimeSlot"]

#define apiURL @"https://clapptest.cimconlms.com/SLCScannerV2/api-bct/"

//#define apiURL @"https://releaseserver.cimconlms.com/SLCScannerV2/api-bct/"

//------------------------------------------------------------------//

// --------  Web Services URLS  --------  //


// [ 1 ] Post Data of Security Code

#define checkuniquecodeURL [NSString stringWithFormat:@"%@checkuniquecode" , apiURL]
// e.g http://199.199.50.48/SLCScanner/api/checkuniquecode


// [ 2 ] userLogin URL

#define userLoginURL [NSString stringWithFormat:@"%@userLogin" , apiURL]
// e.g http://199.199.50.48/SLCScanner/api/userLogin

// [ 3 ] Get Other Data

#define GetOtherDataURL [NSString stringWithFormat:@"%@setting" , apiURL]
// e.g http://199.199.50.48/SLCScanner/api/setting

// [ 4 ] Post OtherData

#define PostOtherDataURL [NSString stringWithFormat:@"%@saveslcdata" , apiURL]
// e.g http://199.199.50.48/SLCScanner/api/saveslcdata

#define getSLCUsingLatLong [NSString stringWithFormat:@"%@getSlcListUsingLatLong" , apiURL]


#define nodeType [NSString stringWithFormat:@"http://203.88.128.141/LGwebAPI/LightingGale/GetClientType"]

// [ 4 ] Get / Search SLC List

#define slclistURL [NSString stringWithFormat:@"%@slclist" , apiURL]
// e.g http://199.199.50.48/SLCScanner/api/slclist/13

// [ 5 ] Get SLC Detail

#define slcDetailURL [NSString stringWithFormat:@"%@slc" , apiURL]
// e.g http://199.199.50.48/SLCScanner/api/slc/1

// [ 6 ] Edit SLC Detail

#define EditSlcDetailURL [NSString stringWithFormat:@"%@editslcdata" , apiURL]
// e.g http://199.199.50.48/SLCScanner/api/editslcdata

#define checkmacandslcURL [NSString stringWithFormat:@"%@checkMacAddressSlcIdbeforeSave",apiURL];

#define checkInternalUniqueMacAddressURL [NSString stringWithFormat:@"%@checkInternalUniqueMacAddressAPI" , apiURL]

// [ 7 ] checkmacaddress

#define checkInternalUniqueEDITURL [NSString stringWithFormat:@"%@checkInternalUniqueMacAddressEditAPI", apiURL]

#define checkInternalSLCIDEDITURL [NSString stringWithFormat:@"%@checkInternalUniqueSLCIDEditAPI", apiURL]


#define checkmacaddressURL [NSString stringWithFormat:@"%@checkmacaddress" , apiURL]
// e.g http://199.199.50.48/SLCScanner/api/checkmacaddress

// [ 8 ] checkslcid

#define checkslcidURL [NSString stringWithFormat:@"%@checkslcid" , apiURL]
// e.g http://199.199.50.48/SLCScanner/api/checkslcid/123446

// [ 9 ] addotherdata

#define addotherdataURL [NSString stringWithFormat:@"%@addotherdata" , apiURL]
// e.g http://199.199.50.48/SLCScanner/api/addotherdata

// [ 10 ] Dynamic UIData

#define DynamicUIdataURL [NSString stringWithFormat:@"%@assets" , apiURL]
// e.g http://199.199.50.48/SLCScanner/api/assets/13

// [ 11 ] address Data POst

#define addressURL [NSString stringWithFormat:@"%@address" , apiURL]
// e.g http://199.199.50.48/SLCScanner/api/address

// [ 11 ] sync

#define syncURL [NSString stringWithFormat:@"%@sync" , apiURL]
// e.g http://199.199.50.48/SLCScanner/api/sync

// [ 12 ] checkpoleid

#define checkpoleidURL [NSString stringWithFormat:@"%@checkpoleid" , apiURL]
// e.g http://199.199.50.48/SLCScanner/api/checkpoleid

// [ 13 ] Api for Dynamic Setting View

#define UImanageFlagURL [NSString stringWithFormat:@"%@setting" , apiURL]
// e.g http://199.199.50.48/SLCScanner/api/setting/4


#define PostOtherDataURL [NSString stringWithFormat:@"%@saveslcdata" , apiURL]

#define getOptionDataURL [NSString stringWithFormat:@"%@getpoleoptions" , apiURL]

#define LanguageSetup [NSString stringWithFormat:@"%@change-language" , apiURL]
// e.g http://199.199.50.48/SLCScannerV2/api/change-language/1/pt

#endif /* API_h */
