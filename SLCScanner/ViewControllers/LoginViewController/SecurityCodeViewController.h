//
//  SecurityCodeViewController.h
//  SLCScanner
//
//  Created by INT MAC 2015 on 19/03/18.
//  Copyright © 2018 INT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SQLManager.h"
#import "ViewController.h"
#import "ASIFormDataRequest.h"
#import "GlobalAssistant.h"
#import "API.h"

@interface SecurityCodeViewController : UIViewController <UITextFieldDelegate,ASIHTTPRequestDelegate>

//------------assign property--------------//

//-------- SqlManager Obj -------//

@property (strong , nonatomic) SQLManager *sqlObj ;

@property (strong , nonatomic) GlobalAssistant *globalObject ;

@property (strong , nonatomic) IBOutlet UITextField *txtSecurityCode ;

@property (strong , nonatomic) IBOutlet UIButton    *btnEye;

-(IBAction)okAction:(id)sender ;

@end
