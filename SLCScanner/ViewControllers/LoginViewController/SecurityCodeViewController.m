//
//  SecurityCodeViewController.m
//  SLCScanner
//
//  Created by INT MAC 2015 on 19/03/18.
//  Copyright © 2018 INT. All rights reserved.
//

#import "SecurityCodeViewController.h"

#import "LocalizeHelper.h"

#import <FirebaseAnalytics/FirebaseAnalytics.h>

@interface SecurityCodeViewController ()

@end

@implementation SecurityCodeViewController {
    IBOutlet UIButton *btnOk;
    IBOutlet UILabel  *lblSecurity;
    NSString *strLanguage;
}
@synthesize txtSecurityCode , globalObject, sqlObj;

- (void)viewDidLoad {
    [super viewDidLoad];
    _btnEye.selected = false;
    [_btnEye setImage:[UIImage imageNamed:@"eye_invisible"] forState:UIControlStateNormal];
    [txtSecurityCode setSecureTextEntry:true];
    [self.navigationController setNavigationBarHidden:YES];
    
    // --------- Initialize GlobalAssistant
    globalObject = [[GlobalAssistant alloc] init] ;
    [globalObject buttonWithCornerRadious:22.0 btnView:btnOk];
}
//
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    NSString *language          = [[NSLocale preferredLanguages] objectAtIndex:0];
    NSDictionary *languageDic   = [NSLocale componentsFromLocaleIdentifier:language];
    strLanguage  = [languageDic objectForKey:@"kCFLocaleLanguageCodeKey"];
    
    LocalizationSetLanguage(strLanguage);
    
    [[NSUserDefaults standardUserDefaults] setObject:strLanguage forKey:@"appLanguage"];
    
    lblSecurity.text = LocalizedString(@"SECURITY CODE");
    self.title = LocalizedString(@"");
    txtSecurityCode.placeholder = LocalizedString(@"Enter security text");
    
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"SECURITY"] != nil) {
        txtSecurityCode.text = [[NSUserDefaults standardUserDefaults] valueForKey:@"SECURITY"];
    }
}

-(IBAction)okAction:(id)sender
{
    if ([txtSecurityCode.text isEqualToString:@""]) {
        [globalObject showAlertWithTitle:LocalizedString(@"Please enter security code") message:@""];
    } else {
        [self sendRequestForSecurityCode];
    }
    
}
-(IBAction)btnEye_Click:(UIButton *)sender {
    [txtSecurityCode setSecureTextEntry:sender.selected];
    if ([sender isSelected]) {
        [sender setImage:[UIImage imageNamed:@"eye_invisible"] forState:UIControlStateNormal];
    } else {
        [sender setImage:[UIImage imageNamed:@"eye"] forState:UIControlStateNormal];
    }
    sender.selected = !sender.isSelected;
}

#pragma mark - Send Security Code / ASIHttpRequest

-(void)sendRequestForSecurityCode
{
    if ([globalObject checkInternetConnection]) {
        [globalObject removeActivityIndicator];
        [globalObject showActivityIndicator];
        
        NSString *str = checkuniquecodeURL ;
        NSString *urlString = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *url = [NSURL URLWithString:urlString];
        
        ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
        
        [request setRequestMethod:@"POST"] ;
        [request setDelegate:self] ;
        
        //------- Post Data
        
        [request setPostValue:txtSecurityCode.text forKey:@"code"] ;
        //
        [request setPostValue:strLanguage forKey:@"language"];
        
        [request setPostValue:@"IOS" forKey:@"source"] ;
        
        [request setPostValue:[[[UIDevice currentDevice] identifierForVendor] UUIDString] forKey:@"udid"] ;
        
        //-------
        
        [request startAsynchronous] ;
    } else {
        [globalObject removeActivityIndicator];
    }
    
}

-(void)requestFailed:(ASIHTTPRequest *)request {
    NSError *error = [request error];
    NSLog(@"error : %@" , error.localizedDescription);
    [globalObject removeActivityIndicator];
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:error.localizedDescription
                                  message:@""
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"OK"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action)
                                {
    }];
    
    [alert addAction:yesButton];
    
    [self presentViewController:alert animated:YES completion:nil];
    
}

-(void)requestFinished:(ASIHTTPRequest *)request
{
    NSString *responseString = [request responseString];
    NSMutableDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:[responseString dataUsingEncoding:NSUTF8StringEncoding] options:0 error:nil];
    NSLog(@"Responce Dictionary : %@", responseDict);
    
    [globalObject removeActivityIndicator];
    
    if ([[responseDict objectForKey:@"status"] isEqualToString:@"success"])
    {
        [FIRAnalytics logEventWithName:kFIREventSelectContent
                            parameters:@{
                                         kFIRParameterItemName:@"securityCode",
                                         }];
        
        sqlObj = [[SQLManager alloc] init] ;
        
        [sqlObj createAndOpenDatabase] ;
        
        [sqlObj insertKeyToDatabase:@"clientID" value:[responseDict objectForKey:@"ClientID"]];
        [sqlObj insertKeyToDatabase:@"userid" value:[responseDict objectForKey:@"userid"]];
        [sqlObj insertKeyToDatabase:@"ScanLBL" value:[responseDict objectForKey:@"ScanLBL"]];
        [sqlObj insertKeyToDatabase:@"ScanPH" value:[responseDict objectForKey:@"ScanPH"]];
        
//        [sqlObj insertKeyToDatabase:@"PoleID" value:@"Yes"];
        [sqlObj insertKeyToDatabase:@"MeasurmentUnit" value:@"Metric"];
        
        if ([[[responseDict objectForKey:@"setting"] valueForKey:@"client_slc_pole_id"] isEqualToString:@"Yes"])
        {
            [sqlObj insertKeyToDatabase:@"PoleID" value:@"Yes"];
            [sqlObj insertKeyToDatabase:@"slc_pole_id" value:@"Yes"];
        }
        else
        {
            [sqlObj insertKeyToDatabase:@"PoleID" value:@"No"];
            [sqlObj insertKeyToDatabase:@"slc_pole_id" value:@"No"];
        }
        
        if ([[[responseDict objectForKey:@"setting"] valueForKey:@"client_slc_pole_assets_view"] isEqualToString:@"No"])
        {
            [sqlObj insertKeyToDatabase:@"OtherData" value:@"No"];
        }
        else
        {
            [sqlObj insertKeyToDatabase:@"OtherData" value:@"Yes"];
        }
        [sqlObj insertKeyToDatabase:@"ImageView" value:[[responseDict objectForKey:@"setting"] valueForKey:@"client_slc_pole_image_view"]];
        
        [sqlObj insertKeyToDatabase:@"assetsView" value:[[responseDict objectForKey:@"setting"] valueForKey:@"client_slc_pole_assets_view"]];
        
        UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        ViewController *ViewController = [story instantiateViewControllerWithIdentifier:@"ViewController"];
        ViewController.strSecurity = txtSecurityCode.text;
        [self.navigationController pushViewController:ViewController animated:YES];
    } else if ([[responseDict objectForKey:@"status"] isEqualToString:@"error"]) {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:[responseDict objectForKey:@"msg"]
                                      message:@""
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
        }];
        
        [alert addAction:yesButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    } else if ([[responseDict objectForKey:@"status"] isEqualToString:@"0"]) {
        
        NSString *strMsg = [responseDict objectForKey:@"msg"];
        
        if (strMsg == nil || [strMsg  isEqualToString: @""]) {
            strMsg = @"Oops! Something went wrong. Try again later";
        }
        
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:LocalizedString(strMsg)
                                      message:@""
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
        {
        }];
        
        [alert addAction:yesButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    } else {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:LocalizedString(@"Oops! Something went wrong. Try again later")
                                      message:@""
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
        }];
        [alert addAction:yesButton];
        [self presentViewController:alert animated:YES completion:nil];
    }
}


#pragma mark - TextField Delegate

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}

-(BOOL)textFieldShouldReturn:(UITextField*)textField
{
    [textField resignFirstResponder];
    
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    if(textField == txtSecurityCode)
    {
        if([textField.text length]==0)
        {
            if([string isEqualToString:@" "])
            {
                return NO;
            }
        }
    }
    
    /*  if(textField == txtSecurityCode)
     {
     NSString *finalString = [textField.text stringByReplacingCharactersInRange:range withString:string];
     
     if (finalString.length > 6) {
     return NO ;
     }
     
     [self.okAction setEnabled:(finalString.length == 6)];
     }*/
    
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
