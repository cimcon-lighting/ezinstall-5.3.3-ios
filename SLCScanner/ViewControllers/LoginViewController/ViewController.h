//
//  ViewController.h
//  SLCScanner
//
//  Created by INT MAC 2015 on 23/02/18.
//  Copyright © 2018 INT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SQLManager.h"
#import "ViewController.h"
#import "ASIFormDataRequest.h"
#import "GlobalAssistant.h"
#import "API.h"
#import <CoreLocation/CoreLocation.h>

@interface ViewController : UIViewController <ASIHTTPRequestDelegate,UITextFieldDelegate,CLLocationManagerDelegate>

// --------- Initialize GlobalAssistant

@property (strong , nonatomic) GlobalAssistant *globalObject ;

//------------assign property--------------//

//-------- SqlManager Obj -------//

@property (strong , nonatomic) SQLManager *sqlObj ;

@property (strong,nonatomic) IBOutlet UITextField *txtUsername ;

@property (strong,nonatomic) IBOutlet UITextField *txtPassword ;

@property (strong,nonatomic) IBOutlet NSString *strSecurity ;

-(IBAction)login:(id)sender ;

@end

