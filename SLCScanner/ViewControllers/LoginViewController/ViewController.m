//
//  ViewController.m
//  SLCScanner
//
//  Created by INT MAC 2015 on 23/02/18.
//  Copyright © 2018 INT. All rights reserved.
//

#import "ViewController.h"

#import <objc/runtime.h>
#import "LocalizeHelper.h"
#import "ScanViewController.h"
#import "PoleViewController.h"
#import <LocalAuthentication/LocalAuthentication.h>

#import <FirebaseAnalytics/FirebaseAnalytics.h>

@interface ViewController () {
    CLLocationManager *locationManager;
    IBOutlet UIButton *btnLogin;
    IBOutlet UIButton *btnTouchID;
    IBOutlet UIButton *btnCheck;
}

@end

@implementation ViewController

@synthesize txtUsername, txtPassword ;
@synthesize globalObject, sqlObj ;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationController setNavigationBarHidden:YES];
    
    /*  [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:0.0/255.0 green:107.0/255.0 blue:186.0/255.0 alpha:1.0]];
     
     [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];*/
    
    //Lat long code
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self ;
    [locationManager requestWhenInUseAuthorization] ;
    locationManager.distanceFilter = kCLDistanceFilterNone; // whenever we move
    locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation ;
    
    [locationManager startUpdatingLocation];
    
    // --------- Initialize GlobalAssistant
    
    globalObject = [[GlobalAssistant alloc] init] ;
    [globalObject buttonWithCornerRadious:22.0 btnView:btnLogin];
    
    UIColor *color = [[UIColor alloc] initWithRed:55.0/255.0 green:52.0/255.0 blue:53.0/255.0 alpha:1];
    
    Ivar ivar =  class_getInstanceVariable([UITextField class], "_placeholderLabel");
    UILabel *placeholderLabel = object_getIvar(txtUsername, ivar);
    placeholderLabel.textColor = color;
    
    placeholderLabel = object_getIvar(txtPassword, ivar);
    placeholderLabel.textColor = color;
    [btnCheck setSelected:true];
}

-(void)viewWillAppear:(BOOL)animated
{
    //LocalizationSetLanguage(@"es");//pt//es//en
    [txtUsername setPlaceholder:LocalizedString(@"Username")];
    [txtPassword setPlaceholder:LocalizedString(@"Password")];
    [btnLogin setTitle:LocalizedString(@"LOGIN") forState:UIControlStateNormal];
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"UNAME"] != nil) {
        txtUsername.text = [[NSUserDefaults standardUserDefaults] valueForKey:@"UNAME"];
    }
    //[txtUsername setPlaceholder:NSLocalizedString(@"Username", @"")];
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    if(textField == txtPassword || textField == txtUsername)
    {
        if([textField.text length]==0)
        {
            if([string isEqualToString:@" "])
            {
                return NO;
            }
        }
    }
    
    return YES;
}
-(IBAction)btnEye_Click:(UIButton *)sender {
    [txtPassword setSecureTextEntry:sender.selected];
    if ([sender isSelected]) {
        [sender setImage:[UIImage imageNamed:@"eye_invisible"] forState:UIControlStateNormal];
    } else {
        [sender setImage:[UIImage imageNamed:@"eye"] forState:UIControlStateNormal];
    }
    sender.selected = !sender.isSelected;
}
-(IBAction)btnBack:(id)sender
{
    [[self navigationController ] popViewControllerAnimated:true];
}

-(IBAction)btnCheck_Click:(UIButton *)sender {
    sender.selected = !sender.selected;
}

-(IBAction)login:(id)sender
{
    if ([txtUsername.text isEqualToString:@""]) {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:LocalizedString(@"Please enter username")
                                      message:@""
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {}];
        
        [alert addAction:yesButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    } else if ([txtPassword.text isEqualToString:@""]) {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:LocalizedString(@"Please enter password")
                                      message:@""
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {}];
        
        [alert addAction:yesButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
    else
    {
        [FIRAnalytics logEventWithName:kFIREventSelectContent
                            parameters:@{
                                         kFIRParameterItemName:@"login",
                                         }];
        [self loginRequest];
    }
}

-(IBAction)btnTouch_Click:(UIButton *)sender  {
    [btnCheck setSelected:TRUE];
    btnCheck.selected = true;
    if (([[NSUserDefaults standardUserDefaults] valueForKey:@"UNAME"] != nil) && [txtUsername.text isEqualToString:@""]){
        txtUsername.text = [[NSUserDefaults standardUserDefaults] valueForKey:@"UNAME"];
    }
    if (([[NSUserDefaults standardUserDefaults] valueForKey:@"PWD"] != nil) && [txtPassword.text isEqualToString:@""]){
        txtPassword.text = [[NSUserDefaults standardUserDefaults] valueForKey:@"PWD"];
    }
    if ([txtUsername.text isEqualToString:@""]) {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:LocalizedString(@"Please enter username")
                                      message:@""
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {}];
        
        [alert addAction:yesButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    } else if ([txtPassword.text isEqualToString:@""]) {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:LocalizedString(@"Please enter password")
                                      message:@""
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {}];
        
        [alert addAction:yesButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
    else
    {
        LAContext *myContext = [[LAContext alloc] init];
        NSError *authError = nil;
        NSString *myLocalizedReasonString = LocalizedString(@"Enter Username/Password");
        
        if ([myContext canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&authError]) {
            [myContext evaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics
                      localizedReason:myLocalizedReasonString
                                reply:^(BOOL success, NSError *error) {
                if (success) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [FIRAnalytics logEventWithName:kFIREventSelectContent
                                            parameters:@{
                                                         kFIRParameterItemName:@"touchID",
                                                         }];
                        [self loginRequest];
                    });
                } else {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        self.txtPassword.text = @"";
                        UIAlertController * alert=   [UIAlertController
                                                      alertControllerWithTitle:LocalizedString(@"Please enter your password to continue")
                                                      message:@""
                                                      preferredStyle:UIAlertControllerStyleAlert];
                        
                        UIAlertAction* yesButton = [UIAlertAction
                                                    actionWithTitle:@"OK"
                                                    style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction * action)
                                                    {
                            [self.txtPassword becomeFirstResponder];
                        }];
                        
                        [alert addAction:yesButton];
                        
                        [self presentViewController:alert animated:YES completion:nil];
                    });
                }
            }];
        } else {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                UIAlertController * alert=   [UIAlertController
                                              alertControllerWithTitle:LocalizedString(@"Passcode or FaceID is not set on the device")
                                              message:@""
                                              preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* yesButton = [UIAlertAction
                                            actionWithTitle:@"OK"
                                            style:UIAlertActionStyleDefault
                                            handler:^(UIAlertAction * action)
                                            {
                    self.txtPassword.text = @"";
                }];
                
                [alert addAction:yesButton];
                [self presentViewController:alert animated:YES completion:nil];
            
            });
        }
    }
}
#pragma mark - Data Post

-(void)loginRequest
{
    if ([globalObject checkInternetConnection]) {
        [globalObject showActivityIndicator];
        
        NSString *str = userLoginURL ;
        NSString *urlString = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *url = [NSURL URLWithString:urlString];
        
        ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
        
        [request setRequestMethod:@"POST"] ;
        [request setDelegate:self] ;
        
        //------- Post Data
        
        // ---------- Initialize SqlObject
        
        sqlObj = [[SQLManager alloc] init] ;
        
        [sqlObj createAndOpenDatabase] ;
        
        NSLog(@"%@",[[sqlObj getAllUserInfo] valueForKey:@"clientID"]);
        
        NSLog(@"%@",[[sqlObj getAllUserInfo] valueForKey:@"userid"]);
        
        NSLog(@"%@",[[UIDevice currentDevice] identifierForVendor]);
        
        NSLog(@"%f",locationManager.location.coordinate.latitude);
        
        NSLog(@"%f",locationManager.location.coordinate.longitude);
        
        // 65D78ADF-32BC-4A73-B7BC-C9ADBC4C9F55
        
        NSString *strLat = [NSString stringWithFormat:@"%f",locationManager.location.coordinate.latitude];
        
        NSString *strLong = [NSString stringWithFormat:@"%f",locationManager.location.coordinate.longitude];
        
        [request setPostValue:[[sqlObj getAllUserInfo] valueForKey:@"clientID"] forKey:@"client_id"] ;
        
        [request setPostValue:[[sqlObj getAllUserInfo] valueForKey:@"userid"] forKey:@"userid"] ;
        
        [request setPostValue:self.txtUsername.text forKey:@"username"] ;
        
        [request setPostValue:self.txtPassword.text forKey:@"password"] ;
        
        [request setPostValue:strLat forKey:@"lat"] ;
        
        [request setPostValue:strLong forKey:@"long"] ;
        
        // [request setPostValue:[[[UIDevice currentDevice] identifierForVendor] UUIDString] forKey:@"DeviceID"] ;
        
        NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
        
        [request setPostValue:version forKey:@"Version"] ;
        
        [request setPostValue:@"IOS" forKey:@"source"] ;
        
        [request setTimeOutSeconds:180] ;
        
        //-------
        
        [request startAsynchronous] ;
    } else {
        [globalObject removeActivityIndicator];
    }
}

-(void)requestFailed:(ASIHTTPRequest *)request {
    NSError *error = [request error];
    NSLog(@"error : %@" , error.localizedDescription);
    [globalObject removeActivityIndicator];
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:error.localizedDescription
                                  message:@""
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"OK"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action)
                                {
    }];
    
    [alert addAction:yesButton];
    
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)requestFinished:(ASIHTTPRequest *)request
{
    NSString *responseString = [request responseString];
    NSMutableDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:[responseString dataUsingEncoding:NSUTF8StringEncoding] options:0 error:nil];
    NSLog(@"Responce Dictionary : %@", responseDict);
    
    [globalObject removeActivityIndicator];
    
    if ([[responseDict objectForKey:@"status"] isEqualToString:@"success"])
    {

        NSString *strToken = [responseDict objectForKey:@"token"];
        
        [[NSUserDefaults standardUserDefaults] setValue:strToken forKey:@"TOKEN"];

        if (btnCheck.isSelected) {
            [[NSUserDefaults standardUserDefaults] setValue:_strSecurity forKey:@"SECURITY"];
            [[NSUserDefaults standardUserDefaults] setValue:self.txtUsername.text forKey:@"UNAME"];
            [[NSUserDefaults standardUserDefaults] setValue:self.txtPassword.text forKey:@"PWD"];
        } else {
            [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"SECURITY"];
            [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"UNAME"];
            [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"PWD"];
        }
        
        [[NSUserDefaults standardUserDefaults] setValue:[responseDict objectForKey:@"node_type_lg"] forKey:@"NODETYPE"];
        
        sqlObj = [[SQLManager alloc] init] ;
        
        [sqlObj createAndOpenDatabase] ;
        
        [sqlObj insertKeyToDatabase:@"user_logged_in" value:@"Yes"];
        [sqlObj insertKeyToDatabase:@"token" value:[responseDict objectForKey:@"token"]];
        [sqlObj insertKeyToDatabase:@"CUSTOMER_NAME" value:[responseDict objectForKey:@"customer_name"]];
        
        [self.view endEditing:YES];
        
        UIStoryboard *story = [UIStoryboard storyboardWithName:@"TabBarView" bundle:nil];
        
        PoleViewController *PoleViewController = [story instantiateViewControllerWithIdentifier:@"TabBarView"];
        
        [[UITabBarItem appearance] setTitleTextAttributes:@{ NSForegroundColorAttributeName : [UIColor whiteColor] }
                                                 forState:UIControlStateNormal];
        [[UITabBarItem appearance] setTitleTextAttributes:@{ NSForegroundColorAttributeName : [UIColor whiteColor] }
                                                 forState:UIControlStateSelected];
        
        
        // [self.navigationController pushViewController:ScanViewController animated:YES];
        
        [[UIApplication sharedApplication].keyWindow setRootViewController:PoleViewController];
        
    } else if ([[responseDict objectForKey:@"status"] isEqualToString:@"error"]) {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:[responseDict objectForKey:@"msg"]                                      message:@""
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
            
        }];
        
        [alert addAction:yesButton];
        
        [self presentViewController:alert animated:YES completion:nil];/*
                                                                        
                                                                        {
                                                                        let responseDict = responseData.result.value as! [String : AnyObject]
                                                                        if let status = responseDict[STATUS] as? AnyObject {
                                                                        if status as? String == "-1" {
                                                                        AlertView().showAlert(strMsg: responseDict[MSG] as! String, btntext: OK) { (str) in
                                                                        completion([:], false)
                                                                        }
                                                                        } else {
                                                                        AlertView().showAlert(strMsg: responseDict[MSG] as! String, btntext: OK) { (str) in
                                                                        let navigation = appDelegate.window?.rootViewController as! UINavigationController
                                                                        navigation.popToRootViewController(animated: true)
                                                                        completion([:], false)
                                                                        }
                                                                        }
                                                                        } else {
                                                                        AlertView().showAlert(strMsg: responseDict[MSG] as! String, btntext: OK) { (str) in
                                                                        let navigation = appDelegate.window?.rootViewController as! UINavigationController
                                                                        navigation.popToRootViewController(animated: true)
                                                                        completion([:], false)
                                                                        }
                                                                        }
                                                                        }
                                                                        */
    }  else if ([[responseDict objectForKey:@"status"] isEqualToString:@"-1"]) {
        
        NSString *strMsg = [responseDict objectForKey:@"msg"];
        
        NSArray *items = [strMsg componentsSeparatedByString:@"| "];
        
        UIAlertController *alert = [UIAlertController
                                      alertControllerWithTitle:items[0]
                                      message:@""
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
            if (items.count > 1) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:items[1]] options:@{} completionHandler:nil];
            }
        }];
        
        [alert addAction:yesButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    }  else if ([[responseDict objectForKey:@"status"] isEqualToString:@"0"]) {
        
        NSString *strMsg = [responseDict objectForKey:@"msg"];
        
        if (strMsg == nil || [strMsg  isEqualToString: @""]) {
            strMsg = @"Oops! Something went wrong. Try again later";
        }
        
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:LocalizedString(strMsg)
                                      message:@""
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
            [[self navigationController] popToRootViewControllerAnimated:true];
        }];
        
        [alert addAction:yesButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    }  else {
        NSString *strMsg = [responseDict objectForKey:@"msg"];
        
        if (strMsg == nil || [strMsg  isEqualToString: @""]) {
            strMsg = @"Oops! Something went wrong. Try again later";
        }
        
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:LocalizedString(strMsg)
                                      message:@""
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
        }];
        [alert addAction:yesButton];
        [self presentViewController:alert animated:YES completion:nil];
    }
}


#pragma mark - TextField Delegate

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}

/*
 -(void)textFieldDidBeginEditing:(UITextField *)textField
 {
 if(textField == txtPassword || textField == txtUsername)
 {
 [self animateTextField:textField up:YES];
 }
 }
 
 - (void)textFieldDidEndEditing:(UITextField *)textField
 {
 if(textField == txtPassword || textField == txtUsername)
 {
 [self animateTextField:textField up:NO];
 }
 }
 
 -(void)animateTextField:(UITextField*)textField up:(BOOL)up
 {
 if(textField == txtPassword || textField == txtUsername)
 {
 const int movementDistance = -180; // tweak as needed
 const float movementDuration = 0.3f; // tweak as needed
 
 int movement = (up ? movementDistance : -movementDistance);
 
 [UIView beginAnimations: @"animateTextField" context: nil];
 [UIView setAnimationBeginsFromCurrentState: YES];
 [UIView setAnimationDuration: movementDuration];
 self.view.frame = CGRectOffset(self.view.frame, 0, movement);
 [UIView commitAnimations];
 }
 
 }*/

-(BOOL)textFieldShouldReturn:(UITextField*)textField
{
    [textField resignFirstResponder];
    
    return YES;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
