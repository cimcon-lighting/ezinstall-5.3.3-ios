//
//  CellMapSelection.m
//  SLCScanner
//
//  Created by INT MAC 2015 on 05/10/18.
//  Copyright © 2018 INT. All rights reserved.
//

#import "CellMapSelection.h"

@implementation CellMapSelection

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
