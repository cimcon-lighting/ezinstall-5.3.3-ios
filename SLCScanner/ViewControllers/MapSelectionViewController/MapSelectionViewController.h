//
//  MapSelectionViewController.h
//  SLCScanner
//
//  Created by INT MAC 2015 on 05/10/18.
//  Copyright © 2018 INT. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "GlobalAssistant.h"
#import "AppDelegate.h"
#import "CellMapSelection.h"

NS_ASSUME_NONNULL_BEGIN

@interface MapSelectionViewController : UIViewController

@end

NS_ASSUME_NONNULL_END
