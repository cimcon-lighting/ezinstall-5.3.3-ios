//
//  CellMapSelection.h
//  SLCScanner
//
//  Created by INT MAC 2015 on 05/10/18.
//  Copyright © 2018 INT. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CellMapSelection : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *lblNm;

@property (nonatomic, weak) IBOutlet UIImageView *imgArrow;


@end

NS_ASSUME_NONNULL_END
