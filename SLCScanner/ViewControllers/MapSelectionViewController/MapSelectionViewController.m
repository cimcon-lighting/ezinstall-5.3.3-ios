//
//  MapSelectionViewController.m
//  SLCScanner
//
//  Created by INT MAC 2015 on 05/10/18.
//  Copyright © 2018 INT. All rights reserved.
//

#import "MapSelectionViewController.h"

@interface MapSelectionViewController () <UITableViewDelegate, UITableViewDataSource> {
    IBOutlet UITableView *tblView;
}

@end

@implementation MapSelectionViewController {
    NSMutableArray  *arrTblData;
    GlobalAssistant *globalObject;
    AppDelegate     *appDelegate;
}

#pragma mark :- viewDidLoad
- (void)viewDidLoad {
    [super viewDidLoad];
    globalObject = [[GlobalAssistant alloc] init] ;
    arrTblData = globalObject.arrMaps;
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    tblView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [tblView reloadData];
    
    UIButton *alertButton = [[UIButton alloc] initWithFrame:CGRectMake(0,0,25,25)];
    [alertButton setTitle:@"DONE" forState:UIControlStateNormal];
    alertButton.titleLabel.font = [UIFont systemFontOfSize:14.0];
    [alertButton addTarget:self action:@selector(AlertAction)
          forControlEvents:UIControlEventTouchUpInside];
    [alertButton setShowsTouchWhenHighlighted:YES];
    
    UIBarButtonItem *notificationbutton =[[UIBarButtonItem alloc] initWithCustomView:alertButton];
    self.navigationItem.rightBarButtonItem = notificationbutton;
}

-(void)AlertAction {
    NSLog(@"%d",appDelegate.selectedTabIndex);
    [self.tabBarController setSelectedIndex:appDelegate.selectedTabIndex];
    //NSLog(@"RIGHT BUTTON CLICK");
}

#pragma mark - UItableViewDelegate, DataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return  [arrTblData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CellMapSelection *cell = (CellMapSelection *)[tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell = [[CellMapSelection alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    if (appDelegate.strSelectedMap == indexPath.row) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    else
    {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    cell.lblNm.text = arrTblData[indexPath.row];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    appDelegate.strSelectedMap = (int)indexPath.row;
    [tblView reloadData];
}
@end
