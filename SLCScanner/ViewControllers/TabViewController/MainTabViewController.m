//
//  MainTabViewController.m
//  SLCScanner
//
//  Created by INT MAC 2015 on 08/10/18.
//  Copyright © 2018 INT. All rights reserved.
//

#import "MainTabViewController.h"

#import "AppDelegate.h"
#import "LocalizeHelper.h"

@interface MainTabViewController () {
    AppDelegate *appDelegate;
}

@end

@implementation MainTabViewController

#pragma mark :- viewDidLoad
- (void)viewDidLoad {
    [super viewDidLoad];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.viewControllers[0].title = LocalizedString(@"LIST");
    self.viewControllers[1].title = [NSString stringWithFormat:@"%@ UID",LocalizedString(@"SCAN")];//LocalizedString(@"SCAN UID");
    self.viewControllers[2].title = LocalizedString(@"MAP");
    self.viewControllers[3].title = LocalizedString(@"SETTINGS");
}

- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item {
    if (item.tag <= 1) {
        appDelegate.selectedTabIndex = (int)item.tag;
    }
}
@end
