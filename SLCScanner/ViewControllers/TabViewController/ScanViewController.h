//
//  ScanViewController.h
//  SLCScanner
//
//  Created by INT MAC 2015 on 23/02/18.
//  Copyright © 2018 INT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ZXingObjC/ZXingObjC.h>

#import "SQLManager.h"
#import "GlobalAssistant.h"
#import "ASIFormDataRequest.h"


@interface ScanViewController : UIViewController <ZXCaptureDelegate,ASIHTTPRequestDelegate>


//------------assign property--------------//

// --------- Initialize GlobalAssistant

@property (strong , nonatomic) GlobalAssistant *globalObject ;

//-------- SqlManager Obj -------//

@property (strong , nonatomic) SQLManager *sqlObj ;

@end
