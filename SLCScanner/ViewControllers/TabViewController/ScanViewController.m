//
//  ScanViewController.m
//  SLCScanner
//
//  Created by INT MAC 2015 on 23/02/18.
//  Copyright © 2018 INT. All rights reserved.
//

#import "ScanViewController.h"

#import "API.h"
#import "LocalizeHelper.h"
#import "MapViewController.h"
#import "SlcIDViewController.h"
#import "PoleImageViewController.h"
#import "SecurityCodeViewController.h"

#import <AudioToolbox/AudioToolbox.h>
#import <FirebaseAnalytics/FirebaseAnalytics.h>

@interface ScanViewController () <UITextFieldDelegate,CLLocationManagerDelegate>
{
    UIButton            *alertButton  ;
    
    NSString            *scannedCode ;
    NSString            *strFinamMacAdd ;
    NSString            *strRequestFor ;
    NSString            *strUsername ;
    NSString            *strPassword ;
    NSString            *strIDForUpdateData ;
    
    UIView              *laserView ;
    
    CLLocationManager   *locationManager;
    
    NSMutableArray *arrNodetype ;
    
    IBOutlet UILabel *lblNodeHeading;
    IBOutlet UILabel *lblCustomerName;
    IBOutlet UIView *vwCustomerName;
}

@property (nonatomic, strong) ZXCapture         *capture;

@property (nonatomic, weak) IBOutlet UIButton *btnNodetype;

@property (nonatomic, weak) IBOutlet UIView     *scanRectView;

@property (nonatomic, weak) IBOutlet UILabel    *decodedLabel;

@property (nonatomic, weak) IBOutlet UIView *nodeTypeView;

@end

@implementation ScanViewController{
    CGAffineTransform _captureSizeTransform;
}

@synthesize globalObject, sqlObj ;

#pragma mark :- viewDidLoad
- (void)viewDidLoad {
    [super viewDidLoad];
    
    // --------- Initialize GlobalAssistant
    
    globalObject    = [[GlobalAssistant alloc] init] ;
    
    sqlObj          = [[SQLManager alloc] init] ;
    [sqlObj createAndOpenDatabase] ;
    
    //--------------------------------
    
    self.capture = [[ZXCapture alloc] init];
    self.capture.camera = self.capture.back;
    self.capture.focusMode = AVCaptureFocusModeContinuousAutoFocus;
    
    [self.view.layer addSublayer:self.capture.layer];
    
    [self.view bringSubviewToFront:self.scanRectView];
    [self.view bringSubviewToFront:self.nodeTypeView];
    [self.view bringSubviewToFront:self.decodedLabel];
    [self.view bringSubviewToFront:vwCustomerName];
    
    //-----------------------------------------------
    
    //-----------------------------------------------
    
    //Lat long code
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self ;
    [locationManager requestWhenInUseAuthorization] ;
    locationManager.distanceFilter = kCLDistanceFilterNone; // whenever we move
    locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation ;
    
    [locationManager startUpdatingLocation];
    
    
    CGRect frameimg = CGRectMake(0,0,25,25);
    
    UIButton *alertButton = [[UIButton alloc] initWithFrame:frameimg];
    [alertButton setImage:[UIImage imageNamed:@"finger"] forState:UIControlStateNormal];
    //[alertButton setTitle:@"ADD" forState:UIControlStateNormal];
    alertButton.titleLabel.font = [UIFont systemFontOfSize:14.0];
    [alertButton addTarget:self action:@selector(AlertAction)
          forControlEvents:UIControlEventTouchUpInside];
    [alertButton setShowsTouchWhenHighlighted:YES];
    
    UIBarButtonItem *notificationbutton =[[UIBarButtonItem alloc] initWithCustomView:alertButton];
    self.navigationItem.rightBarButtonItem = notificationbutton;
    
    /* UIStoryboard *story = [UIStoryboard storyboardWithName:@"PoleImageViewController" bundle:nil];
     
     PoleImageViewController *PoleImageViewController = [story instantiateViewControllerWithIdentifier:@"PoleImageViewController"];
     
     [self.navigationController pushViewController:PoleImageViewController animated:YES];*/
    
    arrNodetype = [@[ @{ @"clientType" : @"Unknown",
                         @"value" : @""
    }] mutableCopy];
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithDictionary:[[NSUserDefaults standardUserDefaults] valueForKey:@"NODETYPE"]];
    
    [arrNodetype addObjectsFromArray:dict[@"data"]];
    
    
    [_btnNodetype setTitle:arrNodetype[1][@"clientType"] forState:UIControlStateNormal];
    
    //[[NSUserDefaults standardUserDefaults] setValue:arrNodetype[1][@"clientType"] forKey:@"SELECTDODETYPE"];
    
    [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"SELECTDODETYPE"];
}

-(void)AlertAction
{
    [self.capture stop];
    
    [self AlertForConfirm] ;
}

-(void)redLineShow
{
    laserView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.scanRectView.frame.size.width, 2)];
    laserView.backgroundColor = [UIColor redColor];
    //  laserView.layer.shadowColor = CFBridgingRetain([UIColor redColor]);
    laserView.layer.shadowOffset = CGSizeMake(0.5, 0.5);
    laserView.layer.shadowOpacity = 0.6;
    laserView.layer.shadowRadius = 1.5;
    laserView.alpha = 0.0;
    [self.scanRectView addSubview:laserView];
    
    // Add the line
    [UIView animateWithDuration:0.2 animations:^{
        laserView.alpha = 1.0;
    }];
    
    [UIView animateWithDuration:4.0 delay:0.0 options:UIViewAnimationOptionAutoreverse | UIViewAnimationOptionRepeat | UIViewAnimationOptionCurveEaseInOut animations:^{
        laserView.frame = CGRectMake(0, self.scanRectView.frame.size.height, self.scanRectView.frame.size.width, 2);
    } completion:nil];
    
}

- (IBAction)nodeType_Click:(UIButton *)sender {
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle: @"Select node type"
                                                                         message:nil
                                                                  preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:LocalizedString(@"Cancel") style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    
    for (NSMutableDictionary *dictData in arrNodetype) {
        
        NSString *strClientNme = dictData[@"clientType"];
        
        if ([strClientNme isEqualToString: _btnNodetype.titleLabel.text])
        {
            [actionSheet addAction:[UIAlertAction actionWithTitle:strClientNme style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
                [self dismissViewControllerAnimated:YES completion:^{
                }];
            }]];
        }
        else
        {
            [actionSheet addAction:[UIAlertAction actionWithTitle:strClientNme style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                
                
                [self->_btnNodetype setTitle:action.title forState:UIControlStateNormal];
                
                if (![action.title isEqualToString:@"Unknown"]) {
                    [[NSUserDefaults standardUserDefaults] setValue:action.title forKey:@"SELECTDODETYPE"];
                } else {
                    [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"SELECTDODETYPE"];
                }
                
                [self dismissViewControllerAnimated:YES completion:^{
                }];
            }]];
        }
        
    }
    
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
        [actionSheet setModalPresentationStyle:UIModalPresentationPopover];
        
        UIPopoverPresentationController *popPresenter = [actionSheet
                                                         popoverPresentationController];
        popPresenter.sourceView = _btnNodetype;
        popPresenter.sourceRect = _btnNodetype.bounds;
        
    }
    
    [self presentViewController:actionSheet animated:YES completion:nil];
    
}

-(void)viewDidDisappear:(BOOL)animated
{
    [laserView removeFromSuperview];
    
    self.capture.delegate = nil ;
    
}



#pragma mark - View Controller Methods

- (void)dealloc {
    [self.capture.layer removeFromSuperlayer];
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    lblCustomerName.text = [[sqlObj getAllUserInfo] valueForKey:@"CUSTOMER_NAME"];
    
    [self cameraPermission];
    
    scannedCode = @"" ;
    
    [self redLineShow];
    
    self.capture.delegate = self;
    
    [self.capture start];
    
    [self applyOrientation];
    
    [self localizationSetup];
}

-(void)cameraPermission {
    AVAuthorizationStatus status = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    
    if(status == AVAuthorizationStatusDenied) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:LocalizedString(@"Please enter the code manually by tapping on hand icon on top right corner") delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
    }
}

-(void)localizationSetup {
    self.title      = [NSString stringWithFormat:@"%@ UID",LocalizedString(@"SCAN")];//LocalizedString(@"SCAN UID");
    _decodedLabel.text = LocalizedString(@"Position Code in Box Above");
    lblNodeHeading.text = LocalizedString(@"NODE TYPE");
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
    return toInterfaceOrientation == UIInterfaceOrientationPortrait;
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
    [self applyOrientation];
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id <UIViewControllerTransitionCoordinator>)coordinator {
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    [coordinator animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext> context) {
    } completion:^(id<UIViewControllerTransitionCoordinatorContext> context)
     {
        [self applyOrientation];
    }];
}

#pragma mark - Private

- (void)applyOrientation {
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    float scanRectRotation;
    float captureRotation;
    
    switch (orientation) {
        case UIInterfaceOrientationPortrait:
            captureRotation = 0;
            scanRectRotation = 90;
            break;
        case UIInterfaceOrientationLandscapeLeft:
            captureRotation = 90;
            scanRectRotation = 180;
            break;
        case UIInterfaceOrientationLandscapeRight:
            captureRotation = 270;
            scanRectRotation = 0;
            break;
        case UIInterfaceOrientationPortraitUpsideDown:
            captureRotation = 180;
            scanRectRotation = 270;
            break;
        default:
            captureRotation = 0;
            scanRectRotation = 90;
            break;
    }
    [self applyRectOfInterest:orientation];
    CGAffineTransform transform = CGAffineTransformMakeRotation((CGFloat) (captureRotation / 180 * M_PI));
    [self.capture setTransform:transform];
    [self.capture setRotation:scanRectRotation];
    
    self.capture.layer.frame = CGRectMake(self.view.frame.origin.x, 0, self.view.frame.size.width, self.view.frame.size.height) ;
    
    // self.capture.layer.frame = self.view.frame;
}

- (void)applyRectOfInterest:(UIInterfaceOrientation)orientation {
    CGFloat scaleVideo, scaleVideoX, scaleVideoY;
    CGFloat videoSizeX, videoSizeY;
    CGRect transformedVideoRect = self.scanRectView.frame;
    if([self.capture.sessionPreset isEqualToString:AVCaptureSessionPreset1920x1080]) {
        videoSizeX = 1080;
        videoSizeY = 1920;
    } else {
        videoSizeX = 720;
        videoSizeY = 1280;
    }
    if(UIInterfaceOrientationIsPortrait(orientation)) {
        scaleVideoX = self.view.frame.size.width / videoSizeX;
        scaleVideoY = self.view.frame.size.height / videoSizeY;
        scaleVideo = MAX(scaleVideoX, scaleVideoY);
        if(scaleVideoX > scaleVideoY) {
            transformedVideoRect.origin.y += (scaleVideo * videoSizeY - self.view.frame.size.height) / 2;
        } else {
            transformedVideoRect.origin.x += (scaleVideo * videoSizeX - self.view.frame.size.width) / 2;
        }
    } else {
        scaleVideoX = self.view.frame.size.width / videoSizeY;
        scaleVideoY = self.view.frame.size.height / videoSizeX;
        scaleVideo = MAX(scaleVideoX, scaleVideoY);
        if(scaleVideoX > scaleVideoY) {
            transformedVideoRect.origin.y += (scaleVideo * videoSizeX - self.view.frame.size.height) / 2;
        } else {
            transformedVideoRect.origin.x += (scaleVideo * videoSizeY - self.view.frame.size.width) / 2;
        }
    }
    _captureSizeTransform = CGAffineTransformMakeScale(1/scaleVideo, 1/scaleVideo);
    self.capture.scanRect = CGRectApplyAffineTransform(transformedVideoRect, _captureSizeTransform);
    
    
}

#pragma mark - Private Methods

- (NSString *)barcodeFormatToString:(ZXBarcodeFormat)format {
    switch (format) {
        case kBarcodeFormatAztec:
            return @"Aztec";
            
        case kBarcodeFormatCodabar:
            return @"CODABAR";
            
        case kBarcodeFormatCode39:
            return @"Code 39";
            
        case kBarcodeFormatCode93:
            return @"Code 93";
            
        case kBarcodeFormatCode128:
            return @"Code 128";
            
        case kBarcodeFormatDataMatrix:
            return @"Data Matrix";
            
        case kBarcodeFormatEan8:
            return @"EAN-8";
            
        case kBarcodeFormatEan13:
            return @"EAN-13";
            
        case kBarcodeFormatITF:
            return @"ITF";
            
        case kBarcodeFormatPDF417:
            return @"PDF417";
            
        case kBarcodeFormatQRCode:
            return @"QR Code";
            
        case kBarcodeFormatRSS14:
            return @"RSS 14";
            
        case kBarcodeFormatRSSExpanded:
            return @"RSS Expanded";
            
        case kBarcodeFormatUPCA:
            return @"UPCA";
            
        case kBarcodeFormatUPCE:
            return @"UPCE";
            
        case kBarcodeFormatUPCEANExtension:
            return @"UPC/EAN extension";
            
        default:
            return @"Unknown";
    }
}

#pragma mark - ZXCaptureDelegate Methods

- (void)captureResult:(ZXCapture *)capture result:(ZXResult *)result {
    if (!result) return;
    
    CGAffineTransform inverse = CGAffineTransformInvert(_captureSizeTransform);
    NSMutableArray *points = [[NSMutableArray alloc] init];
    NSString *location = @"";
    for (ZXResultPoint *resultPoint in result.resultPoints) {
        CGPoint cgPoint = CGPointMake(resultPoint.x, resultPoint.y);
        CGPoint transformedPoint = CGPointApplyAffineTransform(cgPoint, inverse);
        transformedPoint = [self.scanRectView convertPoint:transformedPoint toView:self.scanRectView.window];
        NSValue* windowPointValue = [NSValue valueWithCGPoint:transformedPoint];
        location = [NSString stringWithFormat:@"%@ (%f, %f)", location, transformedPoint.x, transformedPoint.y];
        [points addObject:windowPointValue];
    }
    
    
    // We got a result. Display information about the result onscreen.
    NSString *formatString = [self barcodeFormatToString:result.barcodeFormat];
    NSString *display = [NSString stringWithFormat:@"Scanned!\n\nFormat: %@\n\nContents:\n%@\nLocation: %@", formatString, result.text, location];
    NSLog(@"%@",display);
    
    // Vibrate
    AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
    
    scannedCode = result.text ;
    
    [self.capture stop];
    
    [self eventof:@"ScanNavigationClick"];
    
    [self AlertForConfirm] ;
    
    //  [self.decodedLabel performSelectorOnMainThread:@selector(setText:) withObject:display waitUntilDone:YES];
    
    /*
     dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
     [self.capture start];
     });*/
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)AlertForConfirm
{
    NSLog(@"%@",scannedCode) ;
    
    // ---------- Initialize SqlObject
    
    sqlObj = [[SQLManager alloc] init] ;
    
    [sqlObj createAndOpenDatabase] ;
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:LocalizedString([[sqlObj getAllUserInfo] valueForKey:@"ScanLBL"])
                                                                   message:nil
                                                            preferredStyle:UIAlertControllerStyleAlert];
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        // optionally configure the text field
        textField.text = scannedCode ;
        textField.placeholder = [[sqlObj getAllUserInfo] valueForKey:@"ScanPH"] ;
        textField.borderStyle = UITextBorderStyleRoundedRect ;
        textField.autocapitalizationType = UITextAutocapitalizationTypeAllCharacters;
        textField.font = [UIFont systemFontOfSize:16.0] ;
        textField.delegate = self;
        //textField.keyboardType = UIKeyboardTypeAlphabet;
        textField.returnKeyType = UIReturnKeySend ;
        textField.delegate = self ;
        
    }];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:LocalizedString(@"Confirm")
                                                       style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction *action) {
        UITextField *textField = [alert.textFields firstObject];
        
        NSString *emailRegEx = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
        NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
        
        scannedCode = textField.text ;
        
        strFinamMacAdd = textField.text ;
        
        if ([textField.text isEqualToString:@""]) {
            NSString *strPl = LocalizedString(@"Please enter");
            NSString *strTmp = [NSString stringWithFormat:@"%@ %@",strPl, [[self->sqlObj getAllUserInfo] valueForKey:@"ScanPH"]];
            [self eventof:@"ScanUIDConfirm"];
            [self->globalObject showAlertWithTitle:strTmp message:@""];
        } else {
            [self checkmacaddressInternal];
            //                                                             [self checkmacaddress] ;
        }
    }];
    
    UIAlertAction *CancelAction = [UIAlertAction actionWithTitle:LocalizedString(@"Cancel")
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction *action) {
        [alertButton setTitle:LocalizedString(@"Stop") forState:UIControlStateNormal];
        [self eventof:@"ScanUIDCancel"];
        [self.capture start];
    }];
    
    [alert addAction:CancelAction];
    [alert addAction:okAction];
    
    [self presentViewController:alert animated:YES completion:nil];
    
}


/*
 
 New API ->
 
 Name -> checkInternalUniqueMacAddressAPI
 Parmas -> user_id, client_id, mac_address, source
 
 
 
 Listing Flag
 mac_address_type = internal | external
 
 */

#pragma mark - checkmacaddressInternal
-(void)checkmacaddressInternal {
    if ([globalObject checkInternetConnection]) {
        [globalObject showActivityIndicator];
        
        strRequestFor = @"checkmacInternal" ;
        
        NSString *str =  checkInternalUniqueMacAddressURL;
        NSString *urlString = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *url = [NSURL URLWithString:urlString];
        
        ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
        
        [request setRequestMethod:@"POST"] ;
        [request setDelegate:self] ;
        
        
        sqlObj = [[SQLManager alloc] init] ;
        
        [sqlObj createAndOpenDatabase] ;
        
        [request setPostValue:[[sqlObj getAllUserInfo] valueForKey:@"clientID"] forKey:@"client_id"] ;
        
        [request setPostValue:[[sqlObj getAllUserInfo] valueForKey:@"userid"] forKey:@"user_id"] ;
        
        [request setPostValue:strFinamMacAdd forKey:@"mac_address"] ;
        [request setPostValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"SELECTDODETYPE"] forKey:@"node_type"] ;
        
        [request setPostValue:@"IOS" forKey:@"source"];
        
        [request setTimeOutSeconds:180] ;
        
        [request startAsynchronous] ;
        
    } else {
        [globalObject removeActivityIndicator];
    }
}


#pragma mark - Data Post

-(void)checkmacaddress
{
    if ([globalObject checkInternetConnection]) {
        [globalObject showActivityIndicator];
        
        strRequestFor = @"checkmacaddress" ;
        
        NSString *str =  checkmacaddressURL;
        NSString *urlString = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *url = [NSURL URLWithString:urlString];
        
        ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
        
        [request setRequestMethod:@"POST"] ;
        [request setDelegate:self] ;
        
        // ---------- Initialize SqlObject
        
        sqlObj = [[SQLManager alloc] init] ;
        
        [sqlObj createAndOpenDatabase] ;
        
        //------- Post Data
        
        // ---------- Initialize SqlObject
        
        [request setPostValue:[[sqlObj getAllUserInfo] valueForKey:@"clientID"] forKey:@"client_id"] ;
        
        [request setPostValue:[[sqlObj getAllUserInfo] valueForKey:@"userid"] forKey:@"user_id"] ;
        
        [request setPostValue:strFinamMacAdd forKey:@"macaddress"] ;
        
        [request setPostValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"SELECTDODETYPE"] forKey:@"node_type"] ;
        
        [request setPostValue:@"IOS" forKey:@"source"];
        
        [request setTimeOutSeconds:180] ;
        
        //-------
        
        [request startAsynchronous] ;
    } else {
        [globalObject removeActivityIndicator];
    }
}

-(void)requestFailed:(ASIHTTPRequest *)request
{
    NSError *error = [request error];
    NSLog(@"error : %@" , error.localizedDescription);
    [globalObject removeActivityIndicator];
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:error.localizedDescription
                                  message:@""
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"OK"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action)
                                {
    }];
    
    [alert addAction:yesButton];
    
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)requestFinished:(ASIHTTPRequest *)request
{
    NSString *responseString = [request responseString];
    NSMutableDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:[responseString dataUsingEncoding:NSUTF8StringEncoding] options:0 error:nil];
    NSLog(@"Responce Dictionary : %@", responseDict);
    
    [globalObject removeActivityIndicator];
    
    if([[responseDict valueForKey:@"status"] isEqualToString:@"success"])
    {
        if ([strRequestFor isEqualToString:@"Login"]) {
            [self.capture start];
            
            [sqlObj insertKeyToDatabase:@"user_logged_in" value:@"yes"];
            
            [sqlObj insertKeyToDatabase:@"token" value:[responseDict objectForKey:@"token"]];
        } else if ([strRequestFor isEqualToString:@"checkmacInternal"]) {
            if ([[responseDict objectForKey:@"mac_address_type"] isEqualToString:@"internal"]) {
                UIStoryboard *story = [UIStoryboard storyboardWithName:@"MapViewController" bundle:nil];
                MapViewController *MapViewController = [story instantiateViewControllerWithIdentifier:@"MapViewController"];
                MapViewController.strMacAddress = strFinamMacAdd ;
                MapViewController.strSLCID = [responseDict valueForKey:@"slc_id"];
                [self.navigationController pushViewController:MapViewController animated:YES];
            } else {
                [self checkmacaddress];
            }
        } else {
            UIStoryboard *story = [UIStoryboard storyboardWithName:@"SlcIDViewController" bundle:nil];
            
            SlcIDViewController *SlcIDViewController = [story instantiateViewControllerWithIdentifier:@"SlcIDViewController"];
            
            SlcIDViewController.strMacAddress = strFinamMacAdd ;
            
            [self.navigationController pushViewController:SlcIDViewController animated:YES];
            
        }
    } else if ([[responseDict objectForKey:@"status"] isEqualToString:@"continue"]) {
        
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:[responseDict objectForKey:@"msg"]                                      message:@""
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:LocalizedString(@"No")
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {}];
        
        UIAlertAction* nobutton = [UIAlertAction
                                   actionWithTitle:LocalizedString(@"Yes")
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action)
                                   {
            if ([[responseDict objectForKey:@"mac_address_type"] isEqualToString:@"internal"]) {
                UIStoryboard *story = [UIStoryboard storyboardWithName:@"MapViewController" bundle:nil];
                MapViewController *MapViewController = [story instantiateViewControllerWithIdentifier:@"MapViewController"];
                MapViewController.strMacAddress = strFinamMacAdd ;
                MapViewController.strSLCID = [responseDict valueForKey:@"slc_id"];
                [self.navigationController pushViewController:MapViewController animated:YES];
            } else {
                [self checkmacaddress];
            }
        }];
        
        [alert addAction:yesButton];
        [alert addAction:nobutton];
        
        [self presentViewController:alert animated:YES completion:nil];
        
        
    } else if ([[responseDict objectForKey:@"status"] isEqualToString:@"logout"]) {
        // strIDForUpdateData = [responseDict objectForKey:@"SLCID"] ;
        
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:[responseDict objectForKey:@"msg"]                                      message:@""
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
            sqlObj = [[SQLManager alloc] init] ;
            [sqlObj createAndOpenDatabase] ;
            
            NSString *strClientID = [[sqlObj getAllUserInfo] valueForKey:@"clientID"] ;
            NSString *strUserID = [[sqlObj getAllUserInfo] valueForKey:@"userid"] ;
            NSString *strScanLBL = [[sqlObj getAllUserInfo] valueForKey:@"ScanLBL"] ;
            NSString *strScanPH = [[sqlObj getAllUserInfo] valueForKey:@"ScanPH"] ;
            
            NSString *strPoleID = [[sqlObj getAllUserInfo] valueForKey:@"PoleID"] ;
            NSString *strMeasurmentUnit = [[sqlObj getAllUserInfo] valueForKey:@"MeasurmentUnit"] ;
            NSString *strOtherData = [[sqlObj getAllUserInfo] valueForKey:@"OtherData"] ;
            
            [sqlObj DeleteData] ;
            
            [sqlObj createAndOpenDatabase] ;
            
            [sqlObj insertKeyToDatabase:@"clientID" value:strClientID];
            [sqlObj insertKeyToDatabase:@"userid" value:strUserID];
            [sqlObj insertKeyToDatabase:@"ScanLBL" value:strScanLBL];
            [sqlObj insertKeyToDatabase:@"ScanPH" value:strScanPH];
            [sqlObj insertKeyToDatabase:@"PoleID" value:strPoleID];
            [sqlObj insertKeyToDatabase:@"MeasurmentUnit" value:strMeasurmentUnit];
            [sqlObj insertKeyToDatabase:@"OtherData" value:strOtherData];
            
            [self AlertForLogin] ;
            
        }];
        
        [alert addAction:yesButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    } else if ([[responseDict objectForKey:@"status"] isEqualToString:@"error"]) {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:[responseDict objectForKey:@"msg"]                                      message:@""
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
            [self.capture start];
            
        }];
        
        [alert addAction:yesButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    } else if ([[responseDict objectForKey:@"status"] isEqualToString:@"internal"]) {
        /*UIStoryboard *story = [UIStoryboard storyboardWithName:@"MapViewController" bundle:nil];
         
         MapViewController *MapViewController = [story instantiateViewControllerWithIdentifier:@"MapViewController"];
         
         MapViewController.strMacAddress = strFinamMacAdd ;
         
         MapViewController.strSLCID = [responseDict valueForKey:@"slc_id"];
         
         [self.navigationController pushViewController:MapViewController animated:YES];*/
    } else if ([[responseDict objectForKey:@"status"] isEqualToString:@"external"]) {
        //        [self checkmacaddress];
    } else if ([[responseDict objectForKey:@"status"] isEqualToString:@"0"]) {
        
        NSString *strMsg = [responseDict objectForKey:@"msg"];
        
        if (strMsg == nil || [strMsg  isEqualToString: @""]) {
            strMsg = @"Oops! Something went wrong. Try again later";
        }
        
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:LocalizedString(strMsg)
                                      message:@""
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
            sqlObj = [[SQLManager alloc] init] ;
            [sqlObj createAndOpenDatabase] ;
            
            [sqlObj DeleteData] ;
            
            UIStoryboard *story = [UIStoryboard storyboardWithName:@"SecurityCodeViewController" bundle:nil];
            
            SecurityCodeViewController *SecurityCodeViewController = [story instantiateViewControllerWithIdentifier:@"SecurityCodeViewController"];
            
            UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:SecurityCodeViewController];
            
            [[UIApplication sharedApplication].keyWindow setRootViewController:navController];
        }];
        
        [alert addAction:yesButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    }else {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:LocalizedString(@"Oops! Something went wrong. Try again later")
                                      message:@""
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
        }];
        [alert addAction:yesButton];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

-(void)AlertForLogin
{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:LocalizedString(@"Login")
                                                                   message:nil
                                                            preferredStyle:UIAlertControllerStyleAlert];
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        // optionally configure the text field
        
        textField.placeholder = LocalizedString(@"Username") ;
        textField.borderStyle = UITextBorderStyleRoundedRect ;
        textField.font = [UIFont systemFontOfSize:16.0] ;
        textField.keyboardType = UIKeyboardTypeAlphabet;
        textField.returnKeyType = UIReturnKeySend ;
        textField.delegate = self ;
        
    }];
    
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textFieldPass) {
        // optionally configure the text field
        
        textFieldPass.placeholder = LocalizedString(@"Password") ;
        textFieldPass.secureTextEntry = YES;
        textFieldPass.borderStyle = UITextBorderStyleRoundedRect ;
        textFieldPass.font = [UIFont systemFontOfSize:16.0] ;
        textFieldPass.keyboardType = UIKeyboardTypeAlphabet;
        textFieldPass.returnKeyType = UIReturnKeySend ;
        textFieldPass.delegate = self ;
        
    }];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Login"
                                                       style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction *action) {
        NSArray * textfields = alert.textFields;
        UITextField * namefield = textfields[0];
        UITextField * passfield = textfields[1];
        
        strUsername = namefield.text ;
        strPassword = passfield.text ;
        
        [self loginAction];
        
    }];
    
    
    [alert addAction:okAction];
    
    [self presentViewController:alert animated:YES completion:nil];
    
    
}

-(void)loginAction
{
    if ([strUsername isEqualToString:@""] || [strPassword isEqualToString:@""])
    {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:@"Please Enter Valid Username and Password."                                     message:@""
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
            [self AlertForLogin] ;
            
        }];
        
        [alert addAction:yesButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
    else
    {
        [self loginRequest];
    }
}


#pragma mark - Data Post

-(void)loginRequest
{
    if ([globalObject checkInternetConnection]) {
        [globalObject showActivityIndicator];
        
        strRequestFor = @"Login" ;
        
        NSString *str = userLoginURL ;
        NSString *urlString = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *url = [NSURL URLWithString:urlString];
        
        ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
        
        [request setRequestMethod:@"POST"] ;
        [request setDelegate:self] ;
        
        //------- Post Data
        
        // ---------- Initialize SqlObject
        
        sqlObj = [[SQLManager alloc] init] ;
        
        [sqlObj createAndOpenDatabase] ;
        
        NSLog(@"%@",[[sqlObj getAllUserInfo] valueForKey:@"clientID"]);
        
        NSLog(@"%@",[[sqlObj getAllUserInfo] valueForKey:@"userid"]);
        
        NSLog(@"%@",[[UIDevice currentDevice] identifierForVendor]);
        
        NSLog(@"%f",locationManager.location.coordinate.latitude);
        
        NSLog(@"%f",locationManager.location.coordinate.longitude);
        
        // 65D78ADF-32BC-4A73-B7BC-C9ADBC4C9F55
        
        NSString *strLat = [NSString stringWithFormat:@"%f",locationManager.location.coordinate.latitude];
        
        NSString *strLong = [NSString stringWithFormat:@"%f",locationManager.location.coordinate.longitude];
        
        [request setPostValue:[[sqlObj getAllUserInfo] valueForKey:@"clientID"] forKey:@"client_id"] ;
        
        [request setPostValue:[[sqlObj getAllUserInfo] valueForKey:@"userid"] forKey:@"userid"] ;
        
        [request setPostValue:strUsername forKey:@"username"] ;
        
        [request setPostValue:strPassword forKey:@"password"] ;
        
        [request setPostValue:strLat forKey:@"lat"] ;
        
        [request setPostValue:strLong forKey:@"long"] ;
        
        // [request setPostValue:[[[UIDevice currentDevice] identifierForVendor] UUIDString] forKey:@"DeviceID"] ;
        
        [request setPostValue:@"IOS" forKey:@"source"] ;
        
        [request setTimeOutSeconds:180] ;
        
        //-------
        
        [request startAsynchronous] ;
    } else {
        [globalObject removeActivityIndicator];
    }
}


#pragma mark - Textfield Delegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSRange lowercaseCharRange = [string rangeOfCharacterFromSet:[NSCharacterSet lowercaseLetterCharacterSet]];
    
    if (lowercaseCharRange.location != NSNotFound) {
        textField.text = [textField.text stringByReplacingCharactersInRange:range
                                                                 withString:[string uppercaseString]];
        return NO;
    }
    
    NSCharacterSet *numbersOnly = [NSCharacterSet characterSetWithCharactersInString:@"0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"];
    NSCharacterSet *characterSetFromTextField = [NSCharacterSet characterSetWithCharactersInString:string];
    
    BOOL stringIsValid = [numbersOnly isSupersetOfSet:characterSetFromTextField];
    
    return stringIsValid;
    
    /*if([textField.text length]==0)
     {
     if([string isEqualToString:@" "])
     {
     return NO;
     }
     }
     
     
     return YES;*/
}

-(void)eventof:(NSString *)strEventName {
    dispatch_async(dispatch_get_main_queue(), ^{
        NSString *strClientID = [[sqlObj getAllUserInfo] valueForKey:@"clientID"];
        NSString *strUserID = [[sqlObj getAllUserInfo] valueForKey:@"userid"];
        
        [FIRAnalytics logEventWithName:kFIREventSelectContent
                            parameters:@{
                                kFIRParameterItemName:[NSString stringWithFormat:@"%@_%@_%@",strClientID, strUserID, strEventName],
                            }];
    });
}
@end
