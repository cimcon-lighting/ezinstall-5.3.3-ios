//
//  PoleViewController.h
//  SLCScanner
//
//  Created by INT MAC 2015 on 27/02/18.
//  Copyright © 2018 INT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NSURLConnection_Class.h"
#import "SQLManager.h"
#import "GlobalAssistant.h"
#import "ASIFormDataRequest.h"

@interface PoleViewController : UIViewController <NSURLConnection_CustomDelegate,ASIHTTPRequestDelegate,UISearchBarDelegate>


//------------assign property--------------//

// --------- Initialize GlobalAssistant

@property (strong , nonatomic) GlobalAssistant *globalObject ;

//-------- SqlManager Obj -------//

@property (strong , nonatomic) SQLManager *sqlObj ;

@property (strong, nonatomic) IBOutlet UITableView *tblObj ;

@property (strong, nonatomic) IBOutlet UISearchBar *serchObj ;

@property (retain, nonatomic) IBOutlet UIButton *btnSearch ;

@property (retain, nonatomic) IBOutlet UIButton *btnClear ;

@end
