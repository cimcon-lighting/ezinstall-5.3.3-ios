//
//  HelpViewController.h
//  SLCScanner
//
//  Created by INT MAC 2015 on 08/03/18.
//  Copyright © 2018 INT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SQLManager.h"
#import <MessageUI/MessageUI.h>

@interface HelpViewController : UIViewController <MFMailComposeViewControllerDelegate>

//------------assign property--------------//

//-------- SqlManager Obj -------//

@property (strong , nonatomic) SQLManager *sqlObj ;

- (IBAction)CallButton:(id)sender;
- (IBAction)EmailButton:(id)sender;


@end
