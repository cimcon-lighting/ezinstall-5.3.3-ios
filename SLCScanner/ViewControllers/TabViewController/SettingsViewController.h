//
//  SettingsViewController.h
//  SLCScanner
//
//  Created by INT MAC 2015 on 08/03/18.
//  Copyright © 2018 INT. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SQLManager.h"
#import "GlobalAssistant.h"
#import "NSURLConnection_Class.h"

@interface SettingsViewController : UIViewController <NSURLConnection_CustomDelegate>

//------------assign property--------------//

// --------- Initialize GlobalAssistant

@property (strong , nonatomic) GlobalAssistant *globalObject ;


//-------- SqlManager Obj -------//

@property (strong , nonatomic) SQLManager *sqlObj ;

@property (strong, nonatomic) IBOutlet UISwitch *PoleIDswitch ;

@property (strong, nonatomic) IBOutlet UISwitch *OtherDataswitch ;

@property (strong, nonatomic) IBOutlet UISegmentedControl *MeasurmentDataSegment ;

- (IBAction)LogoutButton:(id)sender;

@end
