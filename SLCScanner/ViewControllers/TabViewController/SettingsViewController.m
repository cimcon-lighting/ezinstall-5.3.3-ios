//
//  SettingsViewController.m
//  SLCScanner
//
//  Created by INT MAC 2015 on 08/03/18.
//  Copyright © 2018 INT. All rights reserved.
//

#import "SettingsViewController.h"

#import "API.h"
#import "AppDelegate.h"
#import "LocalizeHelper.h"
#import "CellMapSelection.h"
#import "SecurityCodeViewController.h"

#import <FirebaseAnalytics/FirebaseAnalytics.h>

@interface SettingsViewController () <UITableViewDelegate, UITableViewDataSource> {
    IBOutlet UITableView    *tblView;
    
    IBOutlet UIButton       *btnLogout;
    IBOutlet UIButton       *btnLanguage;
    
    IBOutlet UILabel        *lblVersion;
    IBOutlet UILabel        *lblCapture;
    IBOutlet UILabel        *lblMeasure;
    IBOutlet UILabel        *lblPoleDetails;
    IBOutlet UILabel        *lblLanguage;
    IBOutlet UILabel        *lblMaptype;
    
    NSMutableArray          *arrTblData;
    AppDelegate             *appDelegate;
    
    bool                    isLanSelected;
    NSString                *strSeelctedLanguage;
}

@end

@implementation SettingsViewController
@synthesize PoleIDswitch, OtherDataswitch, MeasurmentDataSegment ;
@synthesize sqlObj, globalObject ;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self eventof:@"Settings"];
    
    globalObject = [[GlobalAssistant alloc] init] ;
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    MeasurmentDataSegment.layer.cornerRadius = 15.0;
    MeasurmentDataSegment.layer.borderColor = [UIColor colorWithRed:80.0/255.0 green:184.0/255.0 blue:72.0/255.0 alpha:1.0].CGColor ;
    MeasurmentDataSegment.layer.borderWidth = 1.0f;
    MeasurmentDataSegment.layer.masksToBounds = YES;
    NSString *ver = [[UIDevice currentDevice] systemVersion];
    float ver_float = [ver floatValue];
    if (ver_float > 13.0) {
        [MeasurmentDataSegment setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]} forState:UIControlStateSelected];
        
        UIColor *clr = [[UIColor alloc] initWithRed:71.0/255.0 green:192.0/255.0 blue:85.0/255.0 alpha:1.0];
        [MeasurmentDataSegment setTitleTextAttributes:@{NSForegroundColorAttributeName : clr} forState:UIControlStateNormal];
    } 
    PoleIDswitch.layer.cornerRadius = 16.0;
    
    OtherDataswitch.layer.cornerRadius = 16.0;
    
    sqlObj = [[SQLManager alloc] init] ;
    
    [sqlObj createAndOpenDatabase] ;
    
    /*[PoleIDswitch addTarget:self
                     action:@selector(PoleIDstateChanged:) forControlEvents:UIControlEventValueChanged];*/
    
    /*[OtherDataswitch addTarget:self
                        action:@selector(OtherDatastateChanged:) forControlEvents:UIControlEventValueChanged];*/
    
    [MeasurmentDataSegment addTarget:self action:@selector(SegmentChangeViewValueChanged:) forControlEvents:UIControlEventValueChanged];
    
    [globalObject buttonWithCornerRadious:26.0 btnView:btnLogout];
}

-(void)viewWillAppear:(BOOL)animated {
    
    sqlObj = [[SQLManager alloc] init] ;
    
    [sqlObj createAndOpenDatabase] ;
    
    if ([[[sqlObj getAllUserInfo] valueForKey:@"MeasurmentUnit"] isEqualToString:@"Metric" ])
    {
        MeasurmentDataSegment.selectedSegmentIndex = 0 ;
    }
    else
    {
        MeasurmentDataSegment.selectedSegmentIndex = 1 ;
    }
    
    //********************************************
    
    if ([[[sqlObj getAllUserInfo] valueForKey:@"PoleID"] isEqualToString:@"Yes"])
    {
        [PoleIDswitch setOn:TRUE];
    }
    else
    {
        [PoleIDswitch setOn:FALSE];
    }
    
    //********************************************
    
    if ([[[sqlObj getAllUserInfo] valueForKey:@"OtherData"] isEqualToString:@"Yes"])
    {
        [OtherDataswitch setOn:TRUE];
    }
    else
    {
        [OtherDataswitch setOn:FALSE];
    }
    
    [self getdata] ;
    
    [self localizationSetup];
}

-(void)localizationSetup {
    
    
    
    NSString *strSelectedlang = [[NSUserDefaults standardUserDefaults] stringForKey:@"appLanguage"];
    
    if ([strSelectedlang isEqualToString:@"en"]) {
        [btnLanguage setTitle:@"English" forState:UIControlStateNormal];
    } else if ([strSelectedlang isEqualToString:@"es"]) {
        [btnLanguage setTitle:@"Spanish" forState:UIControlStateNormal];
    } else {
        [btnLanguage setTitle:@"Portuguese" forState:UIControlStateNormal];
    }
    
    /*if ([strSelectedlang isEqualToString:@"en"]) {
        [btnLanguage setTitle:LocalizedString(@"English") forState:UIControlStateNormal];
    } else if ([strSelectedlang isEqualToString:@"es"]) {
        [btnLanguage setTitle:LocalizedString(@"Spanish") forState:UIControlStateNormal];
    } else {
        [btnLanguage setTitle:LocalizedString(@"Portuguese") forState:UIControlStateNormal];
    }*/
    
    self.title              = LocalizedString(@"SETTINGS");
    
    lblCapture.text         = LocalizedString(@"Capture Pole ID");
    lblMeasure.text         = LocalizedString(@"Measurement Units");
    lblPoleDetails.text     = LocalizedString(@"Pole Details");
    lblLanguage.text        = LocalizedString(@"Language");
    lblMaptype.text         = LocalizedString(@"Map Types");
    
    [btnLogout setTitle:LocalizedString(@"LOGOUT") forState:UIControlStateNormal];
    
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    NSString *build = [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString *)kCFBundleVersionKey];
    lblVersion.text = [NSString stringWithFormat:@"%@: %@", LocalizedString(@"Version"),version];
    arrTblData = [[NSMutableArray alloc] initWithObjects:
                  LocalizedString(@"Apple Satellite"),
                  LocalizedString(@"Apple Standard"),
                  LocalizedString(@"Apple Hybrid"),
                  LocalizedString(@"Google Map Satellite"),
                  LocalizedString(@"Google Map Standard"),
                  LocalizedString(@"Google Map Hybrid"),
                  LocalizedString(@"Open Street Standard"),nil];
    
    self.tabBarController.viewControllers[0].title = LocalizedString(@"LIST");
    self.tabBarController.viewControllers[1].title = [NSString stringWithFormat:@"%@ UID",LocalizedString(@"SCAN")];//LocalizedString(@"SCAN");
    self.tabBarController.viewControllers[2].title = LocalizedString(@"MAP");
    self.tabBarController.viewControllers[3].title = LocalizedString(@"SETTINGS");
    
    [tblView reloadData];
}

- (void)PoleIDstateChanged:(UISwitch *)switchState
{
    if ([switchState isOn]) {
        NSLog(@"on");
        
        [sqlObj updateDATA:@"Yes" :@"PoleID"];
        
    } else {
        NSLog(@"off");
        
        [sqlObj updateDATA:@"No" :@"PoleID"];
    }
}

- (void)OtherDatastateChanged:(UISwitch *)switchState
{
    if ([switchState isOn]) {
        NSLog(@"on");
        [sqlObj updateDATA:@"Yes" :@"OtherData"];
    } else {
        NSLog(@"Off");
        
        [sqlObj updateDATA:@"No" :@"OtherData"];
    }
}

-(IBAction)SegmentChangeViewValueChanged:(UISegmentedControl *)SControl
{
    if (SControl.selectedSegmentIndex==0)
    {
        NSLog(@"Metric");
        
        [sqlObj updateDATA:@"Metric" :@"MeasurmentUnit"];
    }
    else
    {
        NSLog(@"English");
        
        [sqlObj updateDATA:@"English" :@"MeasurmentUnit"];
    }
    
}

- (IBAction)LogoutButton:(id)sender
{
    [self eventof:@"SettingLogout"];
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:LocalizedString(@"Do you want to logout?")
                                  message:@""
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"YES"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action)
                                {
        sqlObj = [[SQLManager alloc] init] ;
        [sqlObj createAndOpenDatabase] ;
        
        [sqlObj DeleteData] ;
        
        UIStoryboard *story = [UIStoryboard storyboardWithName:@"SecurityCodeViewController" bundle:nil];
        
        SecurityCodeViewController *SecurityCodeViewController = [story instantiateViewControllerWithIdentifier:@"SecurityCodeViewController"];
        
        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:SecurityCodeViewController];
        
        [[UIApplication sharedApplication].keyWindow setRootViewController:navController];
        
    }];
    
    UIAlertAction* noButton = [UIAlertAction
                               actionWithTitle:@"NO"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
        
    }];
    
    [alert addAction:noButton];
    [alert addAction:yesButton];
    
    
    [self presentViewController:alert animated:YES completion:nil];
}



#pragma mark - Data Get

-(void)setlanguage {
    if ([globalObject checkInternetConnection]) {
        [globalObject showActivityIndicator];
        
        sqlObj = [[SQLManager alloc] init] ;
        
        [sqlObj createAndOpenDatabase] ;
        
        //    NSString *strURL = [NSString stringWithFormat:@"%@/%@/%@",LanguageSetup,[[sqlObj getAllUserInfo] valueForKey:@"userid"],strSeelctedLanguage] ;
        
        NSString *strURL = [NSString stringWithFormat:@"%@/%@/%@/%@",LanguageSetup,[[sqlObj getAllUserInfo] valueForKey:@"userid"],strSeelctedLanguage,[[sqlObj getAllUserInfo] valueForKey:@"clientID"]] ;
        
        NSURLConnection_Class *object = [[NSURLConnection_Class alloc] init] ;
        [object RequestForURL:strURL];
        object.delegate = self ;
    } else {
        [globalObject removeActivityIndicator];
    }
}

#pragma mark - Data Get

-(void)getdata
{
    //---------------------- Request for Getting Client data List  ----------------------//
    
    // --------- Initialize GlobalAssistant
    if ([globalObject checkInternetConnection]) {
        
        [globalObject showActivityIndicator];
        isLanSelected = false;
        // ---------- Initialize SqlObject
        
        sqlObj = [[SQLManager alloc] init] ;
        
        [sqlObj createAndOpenDatabase] ;
        
        NSString *strURL = [NSString stringWithFormat:@"%@/%@",UImanageFlagURL,[[sqlObj getAllUserInfo] valueForKey:@"clientID"]] ;
        
        NSURLConnection_Class *object = [[NSURLConnection_Class alloc] init] ;
        [object RequestForURL:strURL];
        object.delegate = self ;
    } else {
        [globalObject removeActivityIndicator];
    }
    
}

#pragma mark - Custom Delegate [ NSURLConnection ]

-(void)connectionFailWithError:(NSError *)error
{
    NSLog(@"Error: %@",error.localizedDescription) ;
    [globalObject showAlertWithTitle:@"Error" message:LocalizedString(@"No Internet Connection")];
    [globalObject removeActivityIndicator] ;
}

-(void)connectionFinishWithResponse:(NSString *)responseString
{
    NSMutableDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:[responseString dataUsingEncoding:NSUTF8StringEncoding] options:0 error:nil] ;
    
    NSLog(@"%@",responseDict);
    
    [globalObject removeActivityIndicator] ;
    
    if([[responseDict valueForKey:@"status"] isEqualToString:@"success"]) {
        if (isLanSelected) {
            sqlObj = [[SQLManager alloc] init] ;
            
            [sqlObj createAndOpenDatabase] ;
            
            [sqlObj insertKeyToDatabase:@"ScanLBL" value:[responseDict objectForKey:@"ScanLBL"]];
            [sqlObj insertKeyToDatabase:@"ScanPH" value:[responseDict objectForKey:@"ScanPH"]];
        } else {
            
            if ([[[responseDict valueForKey:@"data"] valueForKey:@"client_slc_pole_id"] isEqualToString:@"No"])
            {
                [PoleIDswitch setOn:false];
                [sqlObj updateDATA:@"No" :@"PoleID"];
//                PoleIDswitch.userInteractionEnabled = NO ;
            }
            else
            {
                [PoleIDswitch setOn:true];
                [sqlObj updateDATA:@"Yes" :@"PoleID"];
//                PoleIDswitch.userInteractionEnabled = YES ;
            }
            
            if ([[[responseDict valueForKey:@"data"] valueForKey:@"client_slc_pole_assets_view"] isEqualToString:@"No"])
            {
                [sqlObj updateDATA:@"No" :@"OtherData"];
                //OtherDataswitch.userInteractionEnabled = NO ;
                MeasurmentDataSegment.userInteractionEnabled = NO ;
            }
            else
            {
                [sqlObj updateDATA:@"Yes" :@"OtherData"];
                //OtherDataswitch.userInteractionEnabled = YES ;
                MeasurmentDataSegment.userInteractionEnabled = YES ;
            }
        }
    } else if ([[responseDict objectForKey:@"status"] isEqualToString:@"error"]) {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:[responseDict objectForKey:@"msg"]                                      message:@""
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
            
        }];
        
        [alert addAction:yesButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    } else if ([[responseDict objectForKey:@"status"] isEqualToString:@"0"]) {
        
        NSString *strMsg = [responseDict objectForKey:@"msg"];
        
        if (strMsg == nil || [strMsg  isEqualToString: @""]) {
            strMsg = @"Oops! Something went wrong. Try again later";
        }
        
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:LocalizedString(strMsg)
                                      message:@""
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
            sqlObj = [[SQLManager alloc] init] ;
            [sqlObj createAndOpenDatabase] ;
            
            [sqlObj DeleteData] ;
            
            UIStoryboard *story = [UIStoryboard storyboardWithName:@"SecurityCodeViewController" bundle:nil];
            
            SecurityCodeViewController *SecurityCodeViewController = [story instantiateViewControllerWithIdentifier:@"SecurityCodeViewController"];
            
            UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:SecurityCodeViewController];
            
            [[UIApplication sharedApplication].keyWindow setRootViewController:navController];
        }];
        
        [alert addAction:yesButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    } else {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:LocalizedString(@"Oops! Something went wrong. Try again later")
                                      message:@""
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
        }];
        [alert addAction:yesButton];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

-(IBAction)btnLanguage_Selection:(id)sender {
    [self.view endEditing:YES];
    
    [self eventof:@"SettingLanguage"];
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle: LocalizedString(@"Select Language") message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:LocalizedString(@"Cancel") style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    for(NSString *strTitle in @[@"English",@"Spanish",@"Portuguese"]) {
        if (btnLanguage.titleLabel.text == strTitle) {
            [actionSheet addAction:[UIAlertAction actionWithTitle:strTitle style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
                [self dismissViewControllerAnimated:YES completion:^{
                }];
            }]];
        } else {
            [actionSheet addAction:[UIAlertAction actionWithTitle:strTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                if ([strTitle isEqualToString:@"English"]) {
                    strSeelctedLanguage = @"en";
                } else if ([strTitle isEqualToString:@"Spanish"]) {
                    strSeelctedLanguage = @"es";
                } else {
                    strSeelctedLanguage = @"pt";
                }
                [[NSUserDefaults standardUserDefaults] setObject:strSeelctedLanguage forKey:@"appLanguage"];
                LocalizationSetLanguage(strSeelctedLanguage);
                [btnLanguage setTitle:LocalizedString(strTitle) forState:UIControlStateNormal];
                isLanSelected = true;
                [self setlanguage];
                [self localizationSetup];
                [self dismissViewControllerAnimated:YES completion:^{
                }];
            }]];
        }
    }
    
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
        [actionSheet setModalPresentationStyle:UIModalPresentationPopover];
        
        UIPopoverPresentationController *popPresenter = [actionSheet
                                                         popoverPresentationController];
        popPresenter.sourceView = tblView;
        popPresenter.sourceRect = tblView.bounds;
    }
    
    [self presentViewController:actionSheet animated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Action sheet
-(void)actionSheet {
    [self.view endEditing:YES];
    
    [self eventof:@"SettingMapType"];
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:LocalizedString(@"Select Map Type") message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:LocalizedString(@"Cancel") style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    arrTblData = [[NSMutableArray alloc] initWithObjects:
                  LocalizedString(@"Apple Satellite"),
                  LocalizedString(@"Apple Standard"),
                  LocalizedString(@"Apple Hybrid"),
                  LocalizedString(@"Google Map Satellite"),
                  LocalizedString(@"Google Map Standard"),
                  LocalizedString(@"Google Map Hybrid"),
                  LocalizedString(@"Open Street Standard"),nil];
    for(NSString *strTitle in arrTblData) {
        if (appDelegate.strSelectedMap == [arrTblData indexOfObject:strTitle]){
            [actionSheet addAction:[UIAlertAction actionWithTitle:strTitle style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
                [self dismissViewControllerAnimated:YES completion:^{
                }];
            }]];
        } else {
            [actionSheet addAction:[UIAlertAction actionWithTitle:strTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                appDelegate.strSelectedMap = (int)[arrTblData indexOfObject:strTitle];
                [tblView reloadData];
                [self dismissViewControllerAnimated:YES completion:^{
                }];
            }]];
        }
    }
    
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
        [actionSheet setModalPresentationStyle:UIModalPresentationPopover];
        
        UIPopoverPresentationController *popPresenter = [actionSheet
                                                         popoverPresentationController];
        popPresenter.sourceView = tblView;
        popPresenter.sourceRect = tblView.bounds;
    }
    
    [self presentViewController:actionSheet animated:YES completion:nil];
    
}

#pragma mark - UItableViewDelegate, DataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return  1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CellMapSelection *cell = (CellMapSelection *)[tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell = [[CellMapSelection alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    cell.lblNm.text = LocalizedString(arrTblData[appDelegate.strSelectedMap]);
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self actionSheet];
}
-(void)eventof:(NSString *)strEventName {
    dispatch_async(dispatch_get_main_queue(), ^{
    NSString *strClientID = [[sqlObj getAllUserInfo] valueForKey:@"clientID"];
    NSString *strUserID = [[sqlObj getAllUserInfo] valueForKey:@"userid"];
    
    [FIRAnalytics logEventWithName:kFIREventSelectContent
                        parameters:@{
                                     kFIRParameterItemName:[NSString stringWithFormat:@"%@_%@_%@",strClientID, strUserID, strEventName],
                                     }];
    });
}

@end
