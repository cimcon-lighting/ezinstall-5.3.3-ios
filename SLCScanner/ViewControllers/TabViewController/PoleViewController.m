//
//  PoleViewController.m
//  SLCScanner
//
//  Created by INT MAC 2015 on 27/02/18.
//  Copyright © 2018 INT. All rights reserved.
//

#import "PoleViewController.h"

#import "API.h"
#import "LocalizeHelper.h"
#import "SlcDataTableViewCell.h"
#import "MapSLCDataViewController.h"
#import "EditDataViewController.h"
#import "SecurityCodeViewController.h"
#import <FirebaseAnalytics/FirebaseAnalytics.h>

@interface PoleViewController () <UITextFieldDelegate, CLLocationManagerDelegate, UIPickerViewDelegate, UIPickerViewDataSource>
{
    NSMutableArray *IDarray ;
    
    NSMutableArray *SlcIDarray ;
    
    NSMutableArray *PoleIDarray ;
    
    NSMutableArray *Addedarray ;
    NSMutableArray *Addressarray ;
    NSMutableArray *arrNodetype ;
    
    NSMutableArray *arrSLCType ;
    
    UIRefreshControl *refreshControl ;
    
    NSString *strListView ;
    NSString *strEditView ;
    
    IBOutlet UILabel *lblNodeHeading;
    
    int pageNo;
    int totalRecords;
    
    CLLocationManager   *locationManager;
    UIToolbar *toolBar;
    
    IBOutlet NSLayoutConstraint *constViewHeight;
    IBOutlet UITextField *txtSLCId;
    IBOutlet UITextField *txtFrmDate;
    IBOutlet UITextField *txtToDate;
    IBOutlet UITextField *txtLSCType;
    
    IBOutlet UILabel     *lblCustomerName;
    
    BOOL isFrmDate ;
    
    UIDatePicker *datePicker;
    
    UIPickerView *pickerView;
    
    
}

@property (nonatomic, weak) IBOutlet UIButton *btnNodetype;

@end

@implementation PoleViewController
@synthesize sqlObj, globalObject, tblObj, serchObj ;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    constViewHeight.constant = 0;
    
    self.tblObj.rowHeight=UITableViewAutomaticDimension;
    
    arrSLCType = @[@"All", @"Internal", @"External"] ;
    
    Class CTTelephonyNetworkInfo = NSClassFromString(@"CTTelephonyNetworkInfo");
    id si = [[CTTelephonyNetworkInfo alloc] init];
    
    [self startData];
    
    globalObject = [[GlobalAssistant alloc] init] ;
    
    // ---------- Initialize SqlObject
    
    sqlObj = [[SQLManager alloc] init] ;
    
    [sqlObj createAndOpenDatabase] ;
    
    refreshControl = [[UIRefreshControl alloc]init];
    [tblObj addSubview:refreshControl];
    [refreshControl addTarget:self action:@selector(refreshTable) forControlEvents:UIControlEventValueChanged];
    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [locationManager startUpdatingLocation];
    
    NSLog(@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"NODETYPE"]);
    
    arrNodetype = [@[@{
                         @"clientType" : @"ALL",
                         @"value" : @""
    },
                     @{ @"clientType" : @"Unknown",
                        @"value" : @""
                     }] mutableCopy];
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithDictionary:[[NSUserDefaults standardUserDefaults] valueForKey:@"NODETYPE"]];
    
    [arrNodetype addObjectsFromArray:dict[@"data"]];
    
    [_btnNodetype setTitle:arrNodetype[0][@"clientType"] forState:UIControlStateNormal];
}


-(void)cancelNumberPad{
    [serchObj resignFirstResponder];
    pageNo = 1;
    totalRecords = 1;
    [IDarray        removeAllObjects];
    [SlcIDarray     removeAllObjects];
    [PoleIDarray    removeAllObjects];
    [Addedarray     removeAllObjects];
    [Addressarray   removeAllObjects];
    [tblObj reloadData];
    [self getSLCData];
    serchObj.text = @"";
}

-(void)doneWithNumberPad{
    [serchObj resignFirstResponder];
    
    pageNo = 1;
    totalRecords = 1;
    
    [IDarray        removeAllObjects];
    [SlcIDarray     removeAllObjects];
    [PoleIDarray    removeAllObjects];
    [Addedarray     removeAllObjects];
    [Addressarray   removeAllObjects];
    [tblObj reloadData];
    
    [self PostSearchData];
    
    NSLog(@"search") ;
}

-(void)startData {
    txtSLCId.text = @"";
    txtLSCType.text = @"All";
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MM-dd-yyyy"];
    txtToDate.text = [formatter stringFromDate:[NSDate date]];
    
    NSCalendar * calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [[NSDateComponents alloc] init];
    components.day = -6;
    NSDate *earlierDate = [calendar dateByAddingComponents:components
                                                    toDate:[NSDate date] options:0];
    
    txtFrmDate.text = [formatter stringFromDate:earlierDate];
}

-(void)viewWillAppear:(BOOL)animated {
    
    lblCustomerName.text = [[sqlObj getAllUserInfo] valueForKey:@"CUSTOMER_NAME"];
    
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    
    NSString *strCancel = LocalizedString(@"Cancel");
    NSString *strSearch = LocalizedString(@"Search");
    
    numberToolbar.items = @[[[UIBarButtonItem alloc]initWithTitle:strCancel style:UIBarButtonItemStylePlain target:self action:@selector(cancelNumberPad)],
                            [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                            [[UIBarButtonItem alloc]initWithTitle:strSearch style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)]];
    [numberToolbar sizeToFit];
    serchObj.inputAccessoryView = numberToolbar;
    
    [globalObject buttonWithCornerRadious:14.0 btnView:_btnSearch];
    [globalObject buttonWithCornerRadious:14.0 btnView:_btnClear];
    
    pageNo = 1;
    totalRecords = 1;
    
    IDarray         = [[NSMutableArray alloc] init];
    SlcIDarray      = [[NSMutableArray alloc] init];
    PoleIDarray     = [[NSMutableArray alloc] init];
    Addedarray      = [[NSMutableArray alloc] init];
    Addressarray    = [[NSMutableArray alloc] init];
    
    [self PostSearchData];
    
    [self localizationSetup];
    
    [self showDatePicker];
}

-(void)showDatePicker {
    datePicker=[[UIDatePicker alloc]init];
    datePicker.datePickerMode=UIDatePickerModeDate;
    if (@available(iOS 13.4, *)) {
        datePicker.preferredDatePickerStyle = UIDatePickerStyleWheels;
    }
    datePicker.maximumDate = [NSDate date];
    [txtFrmDate setInputView:datePicker];
    [txtToDate setInputView:datePicker];
    
    NSString *strTitle = @"";
    if (isFrmDate) {
        strTitle = @"From Date";
    } else {
        strTitle = @"To Date";
    }
    
    UIToolbar *toolBar=[[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 44)];
    [toolBar setTintColor:[UIColor grayColor]];
    UIBarButtonItem *cancelBtn = [[UIBarButtonItem alloc]initWithTitle:LocalizedString(@"Cancel") style:UIBarButtonItemStylePlain target:self action:@selector(cancelButton)];
    UIBarButtonItem *space1 = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *center = [[UIBarButtonItem alloc]initWithTitle:LocalizedString(strTitle) style:UIBarButtonItemStylePlain target:self action:@selector(cancelType)];
    UIBarButtonItem *space = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *doneBtn = [[UIBarButtonItem alloc]initWithTitle:LocalizedString(@"Done") style:UIBarButtonItemStylePlain target:self action:@selector(ShowSelectedDate)];
    [toolBar setItems:[NSArray arrayWithObjects:cancelBtn, space1, center, space, doneBtn, nil]];
    [txtFrmDate setInputAccessoryView:toolBar];
    [txtToDate setInputAccessoryView:toolBar];
}

-(void)cancelButton {
    [txtFrmDate resignFirstResponder];
    [txtToDate resignFirstResponder];
}

-(void)ShowSelectedDate {
    NSDateFormatter *formatter=[[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"MM-dd-yyyy"];
    if (isFrmDate) {
        txtFrmDate.text=[NSString stringWithFormat:@"%@",[formatter stringFromDate:datePicker.date]];
        [txtFrmDate resignFirstResponder];
    } else {
        txtToDate.text=[NSString stringWithFormat:@"%@",[formatter stringFromDate:datePicker.date]];
        [txtToDate resignFirstResponder];
    }
}
-(void)typePickerView {
    pickerView=[[UIPickerView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height-200, self.view.bounds.size.width, 200)];
    pickerView.delegate = self;
    pickerView.dataSource = self;
    pickerView.showsSelectionIndicator = YES;
    pickerView.backgroundColor = [UIColor whiteColor];
    [pickerView selectRow:0 inComponent:0 animated:YES];
    toolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height-240, self.view.bounds.size.width, 44)];
    [toolBar setTintColor:[UIColor grayColor]];
    UIBarButtonItem *cancelBtn = [[UIBarButtonItem alloc]initWithTitle:LocalizedString(@"Cancel") style:UIBarButtonItemStylePlain target:self action:@selector(cancelType)];
    UIBarButtonItem *space1 = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *center = [[UIBarButtonItem alloc]initWithTitle:LocalizedString(@"SLC Type") style:UIBarButtonItemStylePlain target:self action:@selector(cancelType)];
    UIBarButtonItem *space = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *doneBtn = [[UIBarButtonItem alloc]initWithTitle:LocalizedString(@"Done") style:UIBarButtonItemStylePlain target:self action:@selector(doneType)];
    [toolBar setItems:[NSArray arrayWithObjects:cancelBtn, space1 ,center, space,doneBtn, nil]];
    [txtLSCType setInputView:pickerView];
    [self.view addSubview:pickerView];
    [self.view addSubview:toolBar];
}

-(void)doneType {
    txtLSCType.text = arrSLCType[[pickerView selectedRowInComponent:0]];
    [toolBar removeFromSuperview];
    [txtLSCType resignFirstResponder];
    [pickerView removeFromSuperview];
}

-(void)cancelType {
    [toolBar removeFromSuperview];
    [txtLSCType resignFirstResponder];
    [pickerView removeFromSuperview];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return [arrSLCType count];
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return [arrSLCType objectAtIndex:row];
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    txtLSCType.text = [arrSLCType objectAtIndex:row];
}

-(void)localizationSetup {
    self.title                  = LocalizedString(@"LIST");
    
    lblNodeHeading.text         = LocalizedString(@"NODE TYPE");
    
    self.serchObj.placeholder   = LocalizedString(@"Search by SLC ID");
    
    txtSLCId.placeholder        =  LocalizedString(@"Search by SLC ID");
    
    [_btnSearch setTitle:LocalizedString(@"Search") forState:UIControlStateNormal];
    [_btnClear setTitle:LocalizedString(@"Clear") forState:UIControlStateNormal];
}

- (void)refreshTable {
    
    pageNo = 1 ;
    totalRecords = 1;
    
    [self startData];
    
    [IDarray        removeAllObjects];
    [SlcIDarray     removeAllObjects];
    [PoleIDarray    removeAllObjects];
    [Addedarray     removeAllObjects];
    [Addressarray    removeAllObjects];
    [tblObj reloadData];
    [self PostSearchData];
}

-(void)AlertAction
{
    UIStoryboard *story = [UIStoryboard storyboardWithName:@"MapSLCDataViewController" bundle:nil];
    
    MapSLCDataViewController *MapSLCDataViewController = [story instantiateViewControllerWithIdentifier:@"MapSLCDataViewController"];
    
    MapSLCDataViewController.strSearchValue = serchObj.text ;
    
    MapSLCDataViewController.strEditView = strEditView ;
    
    MapSLCDataViewController.strListView = strListView ;
    
    [self.navigationController pushViewController:MapSLCDataViewController animated:YES];
}

- (IBAction)searchBarbutton_Click:(UIBarButtonItem *)sender {
    
    [self eventof:@"ListNavigationSearch"];
       
    [[sqlObj getAllUserInfo] valueForKey:@"userid"];
    if (constViewHeight.constant == 0) {
        constViewHeight.constant = 174;
    } else {
        constViewHeight.constant = 0;
    }
    [UIView animateWithDuration:0.1
                     animations:^{
        [self.view layoutIfNeeded];
    }];
    
}

- (IBAction)btnSearch_Click:(UIButton *)sender {
    
    [self eventof:@"SearchFilter"];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MM-dd-yyyy"];
    NSDate * dateOne = [formatter dateFromString:txtFrmDate.text];
    NSDate * dateTwo = [formatter dateFromString:txtToDate.text];
    
    if([dateOne compare:dateTwo] == NSOrderedAscending) {
        [txtSLCId resignFirstResponder];
        [txtFrmDate resignFirstResponder];
        [txtLSCType resignFirstResponder];
        [txtToDate resignFirstResponder];
        
        pageNo = 1 ;
        totalRecords = 1;
            
        [IDarray        removeAllObjects];
        [SlcIDarray     removeAllObjects];
        [PoleIDarray    removeAllObjects];
        [Addedarray     removeAllObjects];
        [Addressarray    removeAllObjects];
        [tblObj reloadData];
        [self PostSearchData];
    } else {
        [globalObject showAlertWithTitle:@"" message:LocalizedString(@"From date should not be greater than To date")];
        return;
    }
    
    
}

- (IBAction)btnclear_Click:(UIButton *)sender {
    [self eventof:@"SearchClear"];
    
    [self refreshTable];
}

- (IBAction)nodeType_Click:(UIButton *)sender {
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle: @"Select Node Type"
                                                                         message:nil
                                                                  preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:LocalizedString(@"Cancel") style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    
    for (NSMutableDictionary *dictData in arrNodetype) {
        
        NSString *strClientNme = dictData[@"clientType"];
        
        if ([strClientNme isEqualToString: _btnNodetype.titleLabel.text])
        {
            [actionSheet addAction:[UIAlertAction actionWithTitle:strClientNme style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
                [self dismissViewControllerAnimated:YES completion:^{
                }];
            }]];
        }
        else
        {
            [actionSheet addAction:[UIAlertAction actionWithTitle:strClientNme style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                
                [self->_btnNodetype setTitle:action.title forState:UIControlStateNormal];;
                
                [self dismissViewControllerAnimated:YES completion:^{
                    [self refreshTable];
                }];
            }]];
        }
    }
    
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )  {
        [actionSheet setModalPresentationStyle:UIModalPresentationPopover];
        
        UIPopoverPresentationController *popPresenter = [actionSheet
                                                         popoverPresentationController];
        popPresenter.sourceView = _btnNodetype;
        popPresenter.sourceRect = _btnNodetype.bounds;
    }
    
    [self presentViewController:actionSheet animated:YES completion:nil];
    
}

#pragma Mark - Data Get

-(void)getSLCData
{
    //---------------------- Request for Getting Client data List  ----------------------//
    if (IDarray.count != totalRecords) {
        if ([globalObject checkInternetConnection]) {
            [globalObject showActivityIndicator];
            globalObject.appDelegate.mpType = 2;
            NSString *strURL = [NSString stringWithFormat:@"%@/%@/%d/%f/%f",slclistURL,[[sqlObj getAllUserInfo] valueForKey:@"userid"],pageNo, locationManager.location.coordinate.latitude,locationManager.location.coordinate.longitude];
            NSLog(@"%@",strURL);
            NSURLConnection_Class *object = [[NSURLConnection_Class alloc] init] ;
            [object RequestForURL:strURL];
            object.delegate = self ;
        } else {
            [refreshControl endRefreshing];
            [globalObject removeActivityIndicator];
        }
    }
}

#pragma mark - Custom Delegate [ NSURLConnection ]

-(void)connectionFailWithError:(NSError *)error
{
    NSLog(@"Error: %@",error.localizedDescription) ;
    [globalObject showAlertWithTitle:@"Error" message:LocalizedString(@"No Internet Connection")];
    [globalObject removeActivityIndicator] ;
}

-(void)connectionFinishWithResponse:(NSString *)responseString
{
    NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:[responseString dataUsingEncoding:NSUTF8StringEncoding] options:0 error:nil] ;
    
    NSLog(@"%@",responseDict);
    
    /*[IDarray        removeAllObjects];
     [SlcIDarray     removeAllObjects];
     [PoleIDarray    removeAllObjects];*/
    
    if([[responseDict valueForKey:@"status"] isEqualToString:@"1"])
    {
        
        pageNo += 1;
        totalRecords = [[[responseDict objectForKey:@"data"] objectForKey: @"tot_no_of_records"] intValue];
        
        NSMutableArray *dataarray = [[responseDict objectForKey:@"data"] objectForKey: @"List"];
        
        strListView = [[responseDict objectForKey:@"setting"] valueForKey:@"client_slc_list_view"] ;
        
        strEditView = [[responseDict objectForKey:@"setting"] valueForKey:@"client_slc_edit_view"] ;
        
        for (int i = 0 ; i < dataarray.count ; i++)
        {
            NSDictionary *subDic = [dataarray objectAtIndex:i] ;
            
            [IDarray        addObject:[subDic objectForKey:@"ID"]] ;
            [SlcIDarray     addObject:[subDic objectForKey:@"slc_id"]] ;
            [PoleIDarray    addObject:[subDic objectForKey:@"pole_id"]] ;
            [Addedarray     addObject:[subDic objectForKey:@"created"]];
            [Addressarray   addObject:[subDic objectForKey:@"address"]];
        }
        
        [tblObj reloadData];
    }
    else if ([[responseDict objectForKey:@"status"] isEqualToString:@"error"]) {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:[responseDict objectForKey:@"msg"]
                                      message:@""
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
            //Handel your yes please button action here
            
        }];
        
        [alert addAction:yesButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    } else if ([[responseDict objectForKey:@"status"] isEqualToString:@"0"]) {
        
        NSString *strMsg = [responseDict objectForKey:@"msg"];
        
        if (strMsg == nil || [strMsg  isEqualToString: @""]) {
            strMsg = @"Oops! Something went wrong. Try again later";
        }
        
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:LocalizedString(strMsg)
                                      message:@""
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
            [[self navigationController] popToRootViewControllerAnimated:true];
        }];
        
        [alert addAction:yesButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    } else {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:LocalizedString(@"Oops! Something went wrong. Try again later")
                                      message:@""
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
        }];
        [alert addAction:yesButton];
        [self presentViewController:alert animated:YES completion:nil];
    }
    
    [refreshControl endRefreshing];
    
    [globalObject removeActivityIndicator] ;
    
}

#pragma mark - MENU TABLE VIEW

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [IDarray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SlcDataTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (!cell) {
        cell = [[SlcDataTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    }
    
    if ([[[sqlObj getAllUserInfo] valueForKey:@"PoleID"] isEqualToString:@"No"]) {
        cell.consPoleIdheight.constant = 0.0;
        cell.consPoleValueheight.constant = 0.0;
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    //LocalizedString(@"SLC ID")
    //LocalizedString(@"POLE ID")
    
    cell.lblSlcCap.text     = [NSString stringWithFormat:@"%@"  ,LocalizedString(@"SLC ID")];
    cell.lblPoleCap.text    = [NSString stringWithFormat:@"%@"  ,LocalizedString(@"POLE ID")];
    cell.lblAddedCap.text   = [NSString stringWithFormat:@"%@"  ,LocalizedString(@"ADDED ON")];
    cell.lblAddressCap.text = [NSString stringWithFormat:@"%@"  ,LocalizedString(@"ADDRESS")];
    
    cell.lblSlcID.text      = [NSString stringWithFormat:@": %@",[SlcIDarray objectAtIndex:indexPath.row]];
    
    cell.lblPoleID.text     = [NSString stringWithFormat:@": %@", [PoleIDarray objectAtIndex:indexPath.row]];
    
    cell.lblTime.text       =  [NSString stringWithFormat:@": %@", [Addedarray objectAtIndex:indexPath.row]];
    
    cell.lblAddress.text    = [NSString stringWithFormat:@": %@", [Addressarray objectAtIndex:indexPath.row]];
    //[NSString stringWithFormat:@"%@ : %@",LocalizedString(@"ADDED ON"), [PoleIDarray objectAtIndex:indexPath.row]];
    
    if ([strListView isEqualToString:@"No"])
    {
        cell.img.hidden = YES ;
    }
    
    if (indexPath.row == IDarray.count-1){
        /*if ([serchObj.text isEqualToString:@""]) {
         //            [self getSLCData];
         [self PostSearchData:serchObj.text]
         } else {*/
        [self PostSearchData];
        // }
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (![strListView isEqualToString:@"No"])
    {
        EditDataViewController *EditView = [[EditDataViewController alloc] initWithNibName:@"EditDataViewController" bundle:nil] ;
        
        EditView.strSelecctedDataSlcID = [IDarray objectAtIndex:indexPath.row];
        
        EditView.strEditView = strEditView ;
        
        [self.navigationController pushViewController:EditView animated:YES];
    }
}

#pragma mark - SearchBar Method
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    if ([searchText length] == 0) {
        [self performSelector:@selector(hideKeyboardWithSearchBar:) withObject:searchBar afterDelay:0];
    }
}

- (void)hideKeyboardWithSearchBar:(UISearchBar *)searchBar{
    [self cancelNumberPad];
    [searchBar resignFirstResponder];
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
    
    [self PostSearchData];
}

#pragma mark - Data Post

/*
 
 func getClientType() {
 
 var dictHeader : [String : String] = [:]
 dictHeader[AUTHORIZATION]   = serviceHandler.objUserModel.strTokenType!+" "+serviceHandler.objUserModel.strAccessToken!
 dictHeader[CONTENT_TYPE]    = "application/json"
 
 ProfileServices().getClientType(headerParams: dictHeader, showLoader: true) { (responseData, isSuccess) in
 if isSuccess {
 var arrClientType = [["clientType" : "ALL".localized(), "value" : ""], ["clientType" : "Unknown", "value" : "0"]]
 let arrReposnedata = responseData[DATA] as! [AnyObject]
 for dict in arrReposnedata {
 arrClientType.append(dict as! [String : String])
 }
 if arrClientType.count > 0 {
 print(arrClientType)
 
 UserDefault.set(arrClientType, forKey: "CLIENTTYPE")
 
 let clientNm = arrClientType.first?["clientType"]
 let clientID = arrClientType.first?["value"]
 
 UserDefault.set(clientNm, forKey: "DEFAULTCLIENT")
 UserDefault.set(clientID, forKey: "DEFAULTCLIENTID")
 }
 }
 }
 }
 
 */


-(void)PostSearchData
{
    if (IDarray.count != totalRecords) {
        if ([globalObject checkInternetConnection]) {
            [globalObject showActivityIndicator];
            NSString *strURL = [NSString stringWithFormat:@"%@/%@/%d/%f/%f",slclistURL,[[sqlObj getAllUserInfo] valueForKey:@"userid"],pageNo, locationManager.location.coordinate.latitude,locationManager.location.coordinate.longitude];
            NSString *urlString = [strURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            NSURL *url = [NSURL URLWithString:urlString];
            
            ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
            
            [request setRequestMethod:@"POST"] ;
            [request setDelegate:self] ;
            
            //------- Post Data
            
            [request setPostValue:txtSLCId.text forKey:@"search"] ;
            NSString *strNodeType = _btnNodetype.titleLabel.text;
            if (([_btnNodetype.titleLabel.text isEqualToString:@"ALL"]) || ([_btnNodetype.titleLabel.text isEqualToString:@"Unknown"])) {
                strNodeType = @"";
            }
            
            [request setPostValue:strNodeType forKey:@"node_type"] ;
            
            [request setPostValue:txtFrmDate.text forKey:@"from_daterange"] ;
            [request setPostValue:txtToDate.text forKey:@"to_daterange"] ;
            [request setPostValue:[txtLSCType.text lowercaseString] forKey:@"slc_type"] ;
                     
//            NSLog(@"POST B DATA : %@",request.buildPostBody);
            
            [request startAsynchronous] ;
        } else {
            [globalObject removeActivityIndicator];
        }
    }
}

-(void)requestFailed:(ASIHTTPRequest *)request
{
    NSError *error = [request error];
    NSLog(@"error : %@" , error.localizedDescription);
    [globalObject removeActivityIndicator];
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:error.localizedDescription
                                  message:@""
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"OK"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action)
                                {
    }];
    
    [alert addAction:yesButton];
    
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)requestFinished:(ASIHTTPRequest *)request
{
    NSString *responseString = [request responseString];
    NSMutableDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:[responseString dataUsingEncoding:NSUTF8StringEncoding] options:0 error:nil];
    NSLog(@"Responce Dictionary : %@", responseDict);
    
    /*[IDarray removeAllObjects];
     [SlcIDarray removeAllObjects];
     [PoleIDarray removeAllObjects];*/
    
    [globalObject removeActivityIndicator];
    
    if ([[responseDict objectForKey:@"status"] isEqualToString:@"1"])
    {
        pageNo += 1;
        
        totalRecords = [[[responseDict objectForKey:@"data"] objectForKey: @"tot_no_of_records"] intValue];
        
        NSMutableArray *dataarray = [[responseDict objectForKey:@"data"] objectForKey: @"List"];
        
        strListView = [[responseDict objectForKey:@"setting"] valueForKey:@"client_slc_list_view"] ;
        
        strEditView = [[responseDict objectForKey:@"setting"] valueForKey:@"client_slc_edit_view"] ;
        
        for (int i = 0 ; i < dataarray.count ; i++)
        {
            NSDictionary *subDic = [dataarray objectAtIndex:i] ;
            
            [IDarray addObject:[subDic objectForKey:@"ID"]] ;
            [SlcIDarray addObject:[subDic objectForKey:@"slc_id"]] ;
            [PoleIDarray addObject:[subDic objectForKey:@"pole_id"]] ;
            [Addressarray addObject:[subDic objectForKey:@"address"]];
            [Addedarray addObject:[subDic objectForKey:@"created"]];
        }
    } else if ([[responseDict objectForKey:@"status"] isEqualToString:@"error"]) {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:[responseDict objectForKey:@"msg"]                                      message:@""
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
            
        }];
        
        [alert addAction:yesButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    } else if ([[responseDict objectForKey:@"status"] isEqualToString:@"0"]) {
        
        NSString *strMsg = [responseDict objectForKey:@"msg"];
        
        if (strMsg == nil || [strMsg  isEqualToString: @""]) {
            strMsg = @"Oops! Something went wrong. Try again later";
        }
        
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:LocalizedString(strMsg)
                                      message:@""
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
            sqlObj = [[SQLManager alloc] init] ;
            [sqlObj createAndOpenDatabase] ;
            
            [sqlObj DeleteData] ;
            
            UIStoryboard *story = [UIStoryboard storyboardWithName:@"SecurityCodeViewController" bundle:nil];
            
            SecurityCodeViewController *SecurityCodeViewController = [story instantiateViewControllerWithIdentifier:@"SecurityCodeViewController"];
            
            UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:SecurityCodeViewController];
            
            [[UIApplication sharedApplication].keyWindow setRootViewController:navController];
        }];
        
        [alert addAction:yesButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    }  else {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:LocalizedString(@"Oops! Something went wrong. Try again later")
                                      message:@""
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
        }];
        [alert addAction:yesButton];
        [self presentViewController:alert animated:YES completion:nil];
    }
    
    [tblObj reloadData];
    
    [refreshControl endRefreshing];
    
}

-(BOOL)textFieldShouldClear:(UITextField *)textField
{
    if (textField == txtSLCId) {
        textField.text = @"";
        [textField resignFirstResponder];
        [self btnSearch_Click:_btnSearch];
    }
    return NO;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    if (textField == txtFrmDate) {
        isFrmDate = true;
        [self showDatePicker];
       
        [self eventof:@"FromDate"];
        
    } else if (textField == txtToDate) {
        isFrmDate = false;
        [self showDatePicker];
        
        [self eventof:@"ToDate"];
        
    } else if (textField == txtLSCType) {
        [textField resignFirstResponder];
        [self typePickerView];
        
        [self eventof:@"SLCType"];
    }
}

-(void)eventof:(NSString *)strEventName {
    dispatch_async(dispatch_get_main_queue(), ^{
    NSString *strClientID = [[sqlObj getAllUserInfo] valueForKey:@"clientID"];
    NSString *strUserID = [[sqlObj getAllUserInfo] valueForKey:@"userid"];
    
    [FIRAnalytics logEventWithName:kFIREventSelectContent
                        parameters:@{
                                     kFIRParameterItemName:[NSString stringWithFormat:@"%@_%@_%@",strClientID, strUserID, strEventName],
                                     }];
    });
}

@end
