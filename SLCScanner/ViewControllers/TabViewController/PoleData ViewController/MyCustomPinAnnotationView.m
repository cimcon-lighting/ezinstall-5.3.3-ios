#import "MyCustomPinAnnotationView.h"

@implementation MyCustomPinAnnotationView

- (instancetype)initWithAnnotation:(id<MKAnnotation>)annotation
                             price:(float)price {
    self = [super initWithAnnotation:annotation
                     reuseIdentifier:nil];
    self.price = price;
    self.image = [UIImage imageNamed:@"cluster_blue"];
    UILabel* label = [[UILabel alloc] initWithFrame:CGRectMake(9,16,38,24)];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor whiteColor];
    if (self.price > 999){
        label.text = [NSString stringWithFormat:@"999+"];
    } else {
        label.text = [NSString stringWithFormat:@"%d", (unsigned)self.price];
    }
    label.font = [label.font fontWithSize:12];
    [label setTextAlignment:NSTextAlignmentCenter];
    [self addSubview:label];
    self.canShowCallout = YES;
    self.rightCalloutAccessoryView = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
    return self;
}
@end

