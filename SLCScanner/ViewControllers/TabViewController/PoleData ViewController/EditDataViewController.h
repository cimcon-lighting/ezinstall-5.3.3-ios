//
//  EditDataViewController.h
//  SLCScanner
//
//  Created by INT MAC 2015 on 12/03/18.
//  Copyright © 2018 INT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NSURLConnection_Class.h"
#import "SQLManager.h"
#import "GlobalAssistant.h"
#import "ASIFormDataRequest.h"
#import <MapKit/MapKit.h>

@interface EditDataViewController : UIViewController <NSURLConnection_CustomDelegate,ASIHTTPRequestDelegate,MKMapViewDelegate,UITextViewDelegate>
{
    NSMutableDictionary *DataDictionary ;
    
    NSMutableDictionary *SelectedDataForPostDictionary ;
    
    NSMutableArray *DataArray ;
    
    NSMutableArray *valuesArray ;
}

@property(strong,nonatomic) NSString *strSelecctedDataSlcID ;

//------------assign property--------------//

// --------- Initialize GlobalAssistant

@property (strong , nonatomic) GlobalAssistant *globalObject ;

//-------- SqlManager Obj -------//

@property (strong , nonatomic) SQLManager *sqlObj ;

@property (strong , nonatomic) NSString *strEditView ;

//***********************************************

@property (strong, nonatomic) IBOutlet UILabel *DatalblMacID ;
@property (strong, nonatomic) IBOutlet UILabel *DatalblSlcID ;
@property (strong, nonatomic) IBOutlet UILabel *DatallblPoleID ;

@property (strong, nonatomic) IBOutlet UILabel *lblTime ;
@property (strong, nonatomic) IBOutlet UILabel *lblNodeType ;
@property (strong, nonatomic) IBOutlet UILabel *lblTimeData ;

@property (strong, nonatomic) IBOutlet UIView *ShowForEditView;
@property (strong, nonatomic) IBOutlet UIScrollView *scroll;

@property (strong, nonatomic) IBOutlet UIButton *btnEdit ;

@property (strong, nonatomic) IBOutlet UILabel *lblAddress ;
@property (strong, nonatomic) IBOutlet UITextView *txtAddress ;

//----------------------------------------------

@property (strong, nonatomic) IBOutlet MKMapView *mapView ;

@property (strong, nonatomic) IBOutlet UIView *infoMapView ;

//***********************************************

@property (strong, nonatomic) IBOutlet UIView *BottomViewObj ;

@property (strong, nonatomic) IBOutlet UIView *PoleImageView ;

@property (strong, nonatomic) IBOutlet UIImageView *PoleImg ;

- (IBAction)ViewImgBtnBtn:(id)sender;


- (IBAction)SaveImgBtn:(id)sender;

//-------------------

- (IBAction)MapOkButton:(id)sender;

//----------------------------------------------

- (IBAction)EditButton:(id)sender;

@end
