//
//  MapSLCDataViewController.m
//  SLCScanner
//
//  Created by INT MAC 2015 on 12/03/18.
//  Copyright © 2018 INT. All rights reserved.
//

#import "MapSLCDataViewController.h"
#import "API.h"
#import "MyAnnotation.h"
#import "LocalizeHelper.h"
#import "FBClusteringManager.h"
#import "EditDataViewController.h"
#import "DetailMapTableViewCell.h"
#import "TBClusterAnnotationView.h"
#import "MyCustomPinAnnotationView.h"
#import "SecurityCodeViewController.h"

#import <GoogleMaps/GoogleMaps.h>
#import <Google-Maps-iOS-Utils/GMUMarkerClustering.h>
#import <FirebaseAnalytics/FirebaseAnalytics.h>

// Point of Interest Item which implements the GMUClusterItem protocol.

@interface POIItem : NSObject<GMUClusterItem>

@property(nonatomic, readonly) CLLocationCoordinate2D position;
@property(nonatomic, readonly) NSString *name;
@property(nonatomic, readonly) NSString *snippet;
@property(nonatomic, readonly) NSString *slcID;
- (instancetype)initWithPosition:(CLLocationCoordinate2D)position name:(NSString *)name;
@end

@implementation POIItem

- (instancetype)initWithPosition:(CLLocationCoordinate2D)position name:(NSString *)name snippet:(NSString *)sbutitle slc:(NSString *)slcid{
    if ((self = [super init])) {
        _position   = position;
        _name       = [name copy];
        _snippet    = [sbutitle copy];
        _slcID      = [slcid copy];
    }
    return self;
}
@end

@interface MapSLCDataViewController () <GMUClusterManagerDelegate, GMSMapViewDelegate, GMUClusterRenderer, GMUClusterRendererDelegate,  MKMapViewDelegate, FBClusteringManagerDelegate, CLLocationManagerDelegate,UIGestureRecognizerDelegate> {
    AppDelegate             *appDelegate;
    GMSMapView              *googlMapView;
    GMUClusterManager       *_clusterManager;
    MKTileOverlayRenderer   *tileRender;
    MKTileOverlay           *overLay;
    MKMapRect               boundingMapRect;
    FBClusteringManager     *clusteringManager;
    CLLocationManager       *locationManager;
    MKCoordinateRegion      mapRegion;
    CLLocationDistance      distanceInMeters;
    ASIFormDataRequest      *request;
}
@end

@implementation MapSLCDataViewController
@synthesize sqlObj, globalObject, mapView, strSearchValue ;
@synthesize strEditView, strListView ;

- (void)viewDidLoad {
    [super viewDidLoad];
    googlMapView = [GMSMapView mapWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-64-49) camera:nil];
    
    [self eventof:@"Map"];
    
    [self.view addSubview:googlMapView];
    mapView.hidden      = true;
    googlMapView.hidden = true;
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    self.mapTypes = @[@"Satellite",@"Standard", @"Hybrid"];
    UIImage* imageAlert = [UIImage imageNamed:@"Map.png"];
    CGRect frameimg = CGRectMake(0,0,25,25);
    UIButton *alertButton = [[UIButton alloc] initWithFrame:frameimg];
    [alertButton setBackgroundImage:imageAlert forState:UIControlStateNormal];
    [alertButton addTarget:self action:@selector(AlertAction)
          forControlEvents:UIControlEventTouchUpInside];
    [alertButton setShowsTouchWhenHighlighted:YES];
    
    id<GMUClusterAlgorithm> algorithm = [[GMUNonHierarchicalDistanceBasedAlgorithm alloc] init];
    id<GMUClusterIconGenerator> iconGenerator = [[GMUDefaultClusterIconGenerator alloc] init];
    
    GMUDefaultClusterRenderer *renderer = [[GMUDefaultClusterRenderer alloc] initWithMapView:googlMapView
                                                                        clusterIconGenerator:iconGenerator];
    
    _clusterManager = [[GMUClusterManager alloc] initWithMap:googlMapView algorithm:algorithm renderer:renderer];
    renderer.delegate = self;
    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [locationManager startUpdatingLocation];
    
    distanceInMeters = 15.0;
}
- (void)addBounceAnnimationToView:(UIView *)view {
    CAKeyframeAnimation *bounceAnimation = [CAKeyframeAnimation animationWithKeyPath:@"transform.scale"];
    bounceAnimation.values = @[@(0.05), @(1.1), @(0.9), @(1)];
    bounceAnimation.duration = 0.6;
    NSMutableArray *timingFunctions = [[NSMutableArray alloc] initWithCapacity:bounceAnimation.values.count];
    
    for (NSUInteger i = 0; i < bounceAnimation.values.count; i++) {
        [timingFunctions addObject:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    }
    [bounceAnimation setTimingFunctions:timingFunctions.copy];
    bounceAnimation.removedOnCompletion = NO;
    [view.layer addAnimation:bounceAnimation forKey:@"bounce"];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(manager.location.coordinate, 3000, 3000);
    MKCoordinateRegion adjustedRegion = [self.mapView regionThatFits:viewRegion];
    [self.mapView setRegion:adjustedRegion animated:YES];
    googlMapView.camera = [GMSCameraPosition cameraWithTarget:manager.location.coordinate zoom:14];
    [locationManager stopUpdatingLocation];
}
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    NSLog(@"failed to fetch current location : %@", error);
}

#pragma mark GMUClusterManagerDelegate
- (UIImage *)drawFront:(UIImage *)image text:(NSString *)text {
    UIGraphicsBeginImageContextWithOptions(image.size, false, [UIScreen mainScreen].scale);
    [image drawInRect:CGRectMake(0, 0, image.size.width, image.size.height)];
    NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
    [style setAlignment:NSTextAlignmentCenter];
    NSDictionary *attr = @{NSParagraphStyleAttributeName : style,
                           NSFontAttributeName : [UIFont systemFontOfSize:14 weight:UIFontWeightRegular], // set text font
                           NSForegroundColorAttributeName : [UIColor whiteColor] // set text color
                           };
    const CGRect rect = CGRectMake(0, 19, image.size.width, image.size.height);
    [text drawInRect:rect withAttributes:attr];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}


- (void)clusterManager:(GMUClusterManager *)clusterManager didTapCluster:(id<GMUCluster>)cluster {
    GMSCameraPosition *newCamera = [GMSCameraPosition cameraWithTarget:cluster.position zoom:googlMapView.camera.zoom + 1];
    GMSCameraUpdate *update = [GMSCameraUpdate setCamera:newCamera];
    [googlMapView moveCamera:update];
}

- (void)renderer:(id<GMUClusterRenderer>)renderer willRenderMarker:(GMSMarker *)marker {
    if ([marker.userData isKindOfClass:[POIItem class]]) {
        POIItem *item = marker.userData;
        marker.snippet = [NSString stringWithFormat:@"%@ \n%@",item.name, item.snippet] ;
        marker.icon = [UIImage imageNamed:@"pin"];
    } else if ([marker.userData conformsToProtocol:@protocol(GMUCluster)]) {
        id<GMUCluster> userData = marker.userData;
        marker.icon = [self drawFront:[UIImage imageNamed:@"cluster_blue"] text:[NSString stringWithFormat:@"%@", @(userData.count)]];
    }
}

#pragma mark - GMSMapViewDelegate
- (void)mapView:(GMSMapView *)mapView didTapInfoWindowOfMarker:(GMSMarker *)marker {
    [self eventof:@"MapPinInfo"];
    if ([marker.userData isKindOfClass:[POIItem class]]) {
        POIItem *item = marker.userData;
        EditDataViewController *EditView = [[EditDataViewController alloc] initWithNibName:@"EditDataViewController" bundle:nil] ;
        EditView.strSelecctedDataSlcID = item.slcID ;
        EditView.strEditView = strEditView ;
        [self.navigationController pushViewController:EditView animated:YES];
    }
}

- (BOOL)mapView:(GMSMapView *)mapView didTapMarker:(GMSMarker *)marker {
    if ( marker.title != nil) {
        NSLog(@"Did tap marker for cluster item %@", marker.title);
    } else {
        NSLog(@"Did tap a normal marker");
    }
    return NO;
}

#pragma mark Private
// Randomly generates cluster items within some extent of the camera and adds them to the

// cluster manager.

- (void)generateClusterItems:(NSMutableArray *)arrPins {
    
    for (int index = 0; index < arrPins.count; ++index) {
        
        double lat = [arrPins[index][@"lat"] doubleValue];
        
        double lng = [arrPins[index][@"lng"] doubleValue];
        
        NSString *name      = [NSString stringWithFormat:@"%@ : %@",LocalizedString(@"SLC ID"), arrPins[index][@"slc_id"]];
        
        NSString *subTitle  = [NSString stringWithFormat:@"%@ : %@",LocalizedString(@"POLE ID"), arrPins[index][@"pole_id"]];
        
        id<GMUClusterItem> item = [[POIItem alloc] initWithPosition:CLLocationCoordinate2DMake(lat, lng) name:name snippet:subTitle slc:arrPins[index][@"ID"]];
        
        [_clusterManager addItem:item];
        
    }
    
}



// Returns a random value between -1.0 and 1.0.

- (double)randomScale {
    
    return (double)arc4random() / UINT32_MAX * 2.0 - 1.0;
    
}



-(void)viewWillAppear:(BOOL)animated {
    
    [googlMapView clear];
    
    request = nil;
    mapView.delegate = self;
    [mapView removeAnnotations:mapView.annotations];
    self.currentMapTypeIndex = appDelegate.strSelectedMap;
    globalObject = [[GlobalAssistant alloc] init];
    sqlObj = [[SQLManager alloc] init] ;
    [sqlObj createAndOpenDatabase] ;
    [self getSLCData:distanceInMeters];
    
    if ((self.currentMapTypeIndex != 6) && (self.currentMapTypeIndex > 2)) {
        
        mapView.hidden      = true;
        
        googlMapView.hidden = false;
        
        [self dropdownViewDidHide:nil];
        
    } else {
        
        mapView.hidden      = false;
        
        googlMapView.hidden = true;
        
        [self dropdownViewDidHide:nil];
        
    }
    
    googlMapView.myLocationEnabled = true;
    [googlMapView setUserInteractionEnabled:true];
    [mapView setShowsUserLocation:true];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        [self setupOSMMap];
        
    });
    
    [self localizationSetup];
    
}



-(void)localizationSetup {
    
    self.title     = LocalizedString(@"MAP");
    
    self.tabBarController.viewControllers[2].title       = LocalizedString(@"MAP");
    
}



-(void)setupOSMMap {
    if (self.currentMapTypeIndex == 6) {
        overLay = [[MKTileOverlay alloc] initWithURLTemplate:@"https://tile.openstreetmap.org/{z}/{x}/{y}.png"];//];
        [overLay setCanReplaceMapContent:true];
        [mapView addOverlay:overLay];
        tileRender = [[MKTileOverlayRenderer alloc] initWithTileOverlay:overLay];
        [self.mapView reloadInputViews];
        [mapView setNeedsDisplay];
    } else {
        tileRender = nil;
        [self.mapView removeOverlay:overLay];
        [overLay setCanReplaceMapContent:false];
        [self.mapView reloadInputViews];
        [mapView setNeedsDisplay];
    }
}

#pragma mark - Data Get
-(void)getSLCData:(float)distance {
    if ([globalObject checkInternetConnection]) {
        [globalObject removeActivityIndicator];
        NSString *str = getSLCUsingLatLong ;
        NSString *urlString = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *url = [NSURL URLWithString:urlString];
        if (!request) {
            
            /*
             if (currentMapTypeIndex != 6) && (currentMapTypeIndex > 2) {
                 var bounds =  GMSCoordinateBounds.init(region: googlMapView!.projection.visibleRegion())
                 dictData[MIN_LAT]  = bounds.southWest.longitude      as AnyObject
                 dictData[MIN_LNG]  = bounds.southWest.latitude       as AnyObject
                 dictData[MAX_LAT]  = bounds.northEast.longitude      as AnyObject
                 dictData[MAX_LNG]  = bounds.northEast.latitude       as AnyObject
             } else {
                 let northEast = mapView.convert(CGPoint(x: mapView.bounds.width, y: 0), toCoordinateFrom: mapView)
                 let southWest = mapView.convert(CGPoint(x: 0, y: mapView.bounds.height), toCoordinateFrom: mapView)
                 
                 dictData[MIN_LAT]  = southWest.longitude      as AnyObject
                 dictData[MIN_LNG]  = southWest.latitude       as AnyObject
                 dictData[MAX_LAT]  = northEast.longitude      as AnyObject
                 dictData[MAX_LNG]  = northEast.latitude       as AnyObject
             }
             */
            
            CLLocationCoordinate2D northEast = CLLocationCoordinate2DMake(0.0,0.0);
            CLLocationCoordinate2D southWest = CLLocationCoordinate2DMake(0.0,0.0);
            if (_currentMapTypeIndex != 6 && _currentMapTypeIndex > 2) {
                GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] initWithRegion:googlMapView.projection.visibleRegion];
                northEast = bounds.northEast;
                southWest = bounds.southWest;
            } else {
                northEast = [mapView convertPoint:CGPointMake(mapView.bounds.size.width, 0) toCoordinateFromView:mapView];
                southWest = [mapView convertPoint:CGPointMake(0, mapView.bounds.size.height) toCoordinateFromView:mapView];
            }
            
                        
            [globalObject showActivityIndicator];
            request = [ASIFormDataRequest requestWithURL:url];
            [request setRequestMethod:@"POST"];
            [request setDelegate:self];
            [request setPostValue:[NSString stringWithFormat:@"%f", northEast.latitude] forKey:@"top_lat"] ;
            [request setPostValue:[NSString stringWithFormat:@"%f", northEast.longitude] forKey:@"top_lon"] ;
            [request setPostValue:[NSString stringWithFormat:@"%f", southWest.latitude] forKey:@"bottom_lat"] ;
            [request setPostValue:[NSString stringWithFormat:@"%f", southWest.longitude] forKey:@"bottom_lon"] ;
            [request setPostValue:[[sqlObj getAllUserInfo] valueForKey:@"clientID"] forKey:@"clientid"] ;
            [request setPostValue:[[sqlObj getAllUserInfo] valueForKey:@"userid"] forKey:@"userid"] ;
            [request setPostValue:[NSString stringWithFormat:@"%f", locationManager.location.coordinate.latitude] forKey:@"latitude"] ;
            [request setPostValue:[NSString stringWithFormat:@"%f", locationManager.location.coordinate.longitude] forKey:@"longitude"] ;
            [request setPostValue:[NSString stringWithFormat:@"%f", distanceInMeters] forKey:@"kilometer"] ;
            [request setTimeOutSeconds:180];
            [request startAsynchronous];
        } else {
            [globalObject removeActivityIndicator];
            [request cancel];
            [request clearDelegatesAndCancel];
            request = nil;
        }
    } else {
        [globalObject removeActivityIndicator];
    }
}

- (void)mapView:(GMSMapView *)mapView willMove:(BOOL)gesture {
    //float zoom = mapView.camera.zoom;
    static CGFloat lastZoom = 0;
    CGFloat currentZoom = [[googlMapView camera] zoom];
    //if (!(fabs((lastZoom) - (currentZoom)) > FLT_EPSILON)) {
        NSLog(@"Zoom changed to: %.2f", [[mapView camera] zoom]);
        [_clusterManager clearItems];
        
        GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] initWithRegion: googlMapView.projection.visibleRegion];
        
        [request cancel];
        [request clearDelegatesAndCancel];
        request = nil;
        
        CLLocationCoordinate2D northEast = bounds.northEast;
        CLLocationCoordinate2D centerCoordinates = [googlMapView.projection coordinateForPoint:googlMapView.center];
        CLLocation *topCenterLocation = [[CLLocation alloc] initWithLatitude:northEast.latitude longitude:northEast.longitude];
        CLLocation *centerLocation = [[CLLocation alloc] initWithLatitude:centerCoordinates.latitude longitude:centerCoordinates.longitude];
        distanceInMeters = [centerLocation distanceFromLocation:topCenterLocation]/1000;
        NSLog(@"KM GOOGLE: %f", distanceInMeters);
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [self getSLCData:self->distanceInMeters];
        });
    //}
    lastZoom = currentZoom;
}

#pragma mark - Custom Delegate [ NSURLConnection ]
-(void)connectionFailWithError:(NSError *)error {
    NSLog(@"Error: %@",error.localizedDescription) ;
    [globalObject showAlertWithTitle:@"Error" message:LocalizedString(@"No Internet Connection")];
    [globalObject removeActivityIndicator] ;
}

-(void)connectionFinishWithResponse:(NSString *)responseString {
    NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:[responseString dataUsingEncoding:NSUTF8StringEncoding] options:0 error:nil] ;
    
    [globalObject removeActivityIndicator] ;
    
    [request cancel];
    [request clearDelegatesAndCancel];
    request = nil;
    
    if([[responseDict valueForKey:@"status"] isEqualToString:@"success"]) {
        NSMutableArray *dataarray = [responseDict objectForKey:@"data"];
        NSMutableArray *pinarray = [[NSMutableArray alloc] init];
        NSMutableArray *arrForCluster = [[NSMutableArray alloc] initWithArray:dataarray];
        
        for (int i = 0 ; i < dataarray.count ; i++) {
            NSDictionary *subDic = [dataarray objectAtIndex:i] ;
            if (_currentMapTypeIndex != 6 && _currentMapTypeIndex > 2) {
                
            } else {
                CLLocationCoordinate2D location = CLLocationCoordinate2DMake([[subDic objectForKey:@"lat"] doubleValue], [[subDic objectForKey:@"lng"] doubleValue]) ;
                NSString *title = [NSString stringWithFormat:@"%@ : %@",LocalizedString(@"SLC ID"), [subDic objectForKey:@"slc_id"]];//[NSString stringWithFormat:@"SLC ID : %@",[subDic objectForKey:@"slc_id"]] ;
                NSString *subtitle = [NSString stringWithFormat:@"%@ : %@",LocalizedString(@"SLC ID"), [subDic objectForKey:@"pole_id"]] ;
                NSString *strID = [subDic objectForKey:@"ID"] ;
                MyAnnotation *annot = [[MyAnnotation alloc] initWithCoordinate:location title:title subtitle:subtitle strid:strID] ;
                [pinarray addObject:annot];
            }
        }
        
        if (_currentMapTypeIndex != 6 && _currentMapTypeIndex > 2) {
            [self generateClusterItems:arrForCluster];
            [_clusterManager cluster];
            [_clusterManager setDelegate:self mapDelegate:self];
        } else {
            clusteringManager = [[FBClusteringManager alloc] initWithAnnotations:pinarray];
            clusteringManager.delegate = self;
            [self->mapView addAnnotations:pinarray];
            dispatch_async(dispatch_get_main_queue(), ^(void){
                double scale = self.mapView.bounds.size.width / self.mapView.visibleMapRect.size.width;
                NSArray *annotations = [self->clusteringManager clusteredAnnotationsWithinMapRect:self->mapView.visibleMapRect withZoomScale:scale];
                [self->clusteringManager displayAnnotations:annotations onMapView:self->mapView];
            });
        }
    } else if ([[responseDict objectForKey:@"status"] isEqualToString:@"error"]) {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:[responseDict objectForKey:@"msg"]                                      message:@""
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
                                        
                                    }];
        
        [alert addAction:yesButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    } else if ([[responseDict objectForKey:@"status"] isEqualToString:@"0"]) {
        
        NSString *strMsg = [responseDict objectForKey:@"msg"];
        
        if (strMsg == nil || [strMsg  isEqualToString: @""]) {
            strMsg = @"Oops! Something went wrong. Try again later";
        }
        
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:LocalizedString(strMsg)
                                      message:@""
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
            sqlObj = [[SQLManager alloc] init] ;
            [sqlObj createAndOpenDatabase] ;
            
            [sqlObj DeleteData] ;
            
            UIStoryboard *story = [UIStoryboard storyboardWithName:@"SecurityCodeViewController" bundle:nil];
            
            SecurityCodeViewController *SecurityCodeViewController = [story instantiateViewControllerWithIdentifier:@"SecurityCodeViewController"];
            
            UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:SecurityCodeViewController];
            
            [[UIApplication sharedApplication].keyWindow setRootViewController:navController];
        }];
        
        [alert addAction:yesButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    } else {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:LocalizedString(@"Oops! Something went wrong. Try again later")
                                      message:@""
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
                                    }];
        [alert addAction:yesButton];
        [self presentViewController:alert animated:YES completion:nil];
    }
    [globalObject removeActivityIndicator] ;
    [self setupOSMMap];
}

#pragma mark :- MKMAP DELEGATE
- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation {
    
    NSString *pinIdentifier = @"red_pin";
    if ([annotation isKindOfClass:[MKUserLocation class]]){
        return nil;
    } else if ([annotation isKindOfClass:[FBAnnotationCluster class]]) {
        FBAnnotationCluster *cluster = (FBAnnotationCluster *)annotation;
        NSLog(@"Annotation is cluster. Number of annotations in cluster: %lu", (unsigned long)cluster.annotations.count);
        MyCustomPinAnnotationView* pin1 = [[MyCustomPinAnnotationView alloc] initWithAnnotation:annotation price:cluster.annotations.count];
        return pin1;
    }
    
    MKAnnotationView *pinView = (MKAnnotationView *)
    [self.mapView dequeueReusableAnnotationViewWithIdentifier:pinIdentifier];
    if (pinView == nil) {
        MKPinAnnotationView *customAnnotationView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:pinIdentifier];
        customAnnotationView.canShowCallout = YES;
        UIButton* disclosureButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
        customAnnotationView.rightCalloutAccessoryView = disclosureButton;
        UIImageView *iconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Pole_Blue.png"]];
        customAnnotationView.leftCalloutAccessoryView = iconView;
        return customAnnotationView;
    } else {
        pinView.annotation = annotation;
    }
    return pinView;
}
- (void)mapView:(MKMapView *)mapView didAddAnnotationViews:(NSArray *)views {
    for (UIView *view in views) {
        [self addBounceAnnimationToView:view];
    }
}
-(MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id <MKOverlay>)overlay{
    return tileRender;
}
- (void)mapView:(MKMapView *)mapView regionWillChangeAnimated:(BOOL)animated {
    mapRegion = self.mapView.region;
}

- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated {
    [[NSOperationQueue new] addOperationWithBlock:^{
        dispatch_async(dispatch_get_main_queue(), ^(void){
            double scale = self.mapView.bounds.size.width / self.mapView.visibleMapRect.size.width;
            NSArray *annotations = [self->clusteringManager clusteredAnnotationsWithinMapRect:mapView.visibleMapRect withZoomScale:scale];
            [self->clusteringManager displayAnnotations:annotations onMapView:mapView];
        });
    }];
    
    MKCoordinateRegion newRegion = self.mapView.region;
    /*if ((mapRegion.span.latitudeDelta/newRegion.span.latitudeDelta) > 1.5) {
        CLLocationCoordinate2D centerCoor = [self.mapView centerCoordinate];
        CLLocationCoordinate2D topCenterCoor = [self.mapView convertPoint:CGPointMake(self.mapView.frame.size.width / 2.0f, 0) toCoordinateFromView:self.mapView];
        CLLocation *topCenterLocation = [[CLLocation alloc] initWithLatitude:topCenterCoor.latitude longitude:topCenterCoor.longitude];
        CLLocation *centerLocation = [[CLLocation alloc] initWithLatitude:centerCoor.latitude longitude:centerCoor.longitude];
        distanceInMeters = [centerLocation distanceFromLocation:topCenterLocation]/1000;
        NSLog(@"KM IN: %f", distanceInMeters);
        [request cancel];
        [request clearDelegatesAndCancel];
        request = nil;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [self getSLCData:self->distanceInMeters];
        });
    }*/
    
    //if ((mapRegion.span.latitudeDelta/newRegion.span.latitudeDelta) < 0.75) {
        CLLocationCoordinate2D centerCoor = [self.mapView centerCoordinate];
        CLLocationCoordinate2D topCenterCoor = [self.mapView convertPoint:CGPointMake(self.mapView.frame.size.width / 2.0f, 0) toCoordinateFromView:self.mapView];
        CLLocation *topCenterLocation = [[CLLocation alloc] initWithLatitude:topCenterCoor.latitude longitude:topCenterCoor.longitude];
        CLLocation *centerLocation = [[CLLocation alloc] initWithLatitude:centerCoor.latitude longitude:centerCoor.longitude];
        distanceInMeters = [centerLocation distanceFromLocation:topCenterLocation]/1000;
        NSLog(@"KM OUT: %f", distanceInMeters);
        [request cancel];
        [request clearDelegatesAndCancel];
        request = nil;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [self getSLCData:self->distanceInMeters];
        });
    //}
}

- (void)mapView:(MKMapView *)eMapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control {
    NSLog(@"Button Tapped") ;
    if (![strListView isEqualToString:@"No"]) {
        MyAnnotation *mapAnn = (MyAnnotation *)view.annotation;
        id <MKAnnotation> annotation = [view annotation];
        if ([annotation isKindOfClass:[MyAnnotation class]]) {
            NSLog(@"mapAnn.sourceDictionary = %@", mapAnn.sID);
            EditDataViewController *EditView = [[EditDataViewController alloc] initWithNibName:@"EditDataViewController" bundle:nil] ;
            EditView.strSelecctedDataSlcID = mapAnn.sID ;
            EditView.strEditView = strEditView ;
            [self.navigationController pushViewController:EditView animated:YES];
        }
    }
}

#pragma mark - DROPDOWN VIEW

- (void)showDropDownViewFromDirection:(LMDropdownViewDirection)direction {
    if (!self.dropdownView) {
        self.dropdownView = [LMDropdownView dropdownView];
        self.dropdownView.delegate              = self;
        self.dropdownView.closedScale           = 0.85;
        self.dropdownView.blurRadius            = 5;
        self.dropdownView.blackMaskAlpha        = 0.5;
        self.dropdownView.animationDuration     = 0.5;
        self.dropdownView.animationBounceHeight = 20;
    }
    self.dropdownView.direction = direction;
    if ([self.dropdownView isOpen]) {
        [self.dropdownView hide];
    } else {
        switch (direction) {
            case LMDropdownViewDirectionTop: {
                self.dropdownView.contentBackgroundColor = [UIColor whiteColor];
                [self.dropdownView showFromNavigationController:self.navigationController
                                                withContentView:self.menuTableView];
                break;
            }
            case LMDropdownViewDirectionBottom: {
                self.dropdownView.contentBackgroundColor = [UIColor whiteColor];
                break;
            }
            default:
                break;
        }
    }
}

- (void)dropdownViewWillShow:(LMDropdownView *)dropdownView {
    NSLog(@"Dropdown view will show");
}

- (void)dropdownViewDidShow:(LMDropdownView *)dropdownView {
    NSLog(@"Dropdown view did show");
}

- (void)dropdownViewWillHide:(LMDropdownView *)dropdownView {
    NSLog(@"Dropdown view will hide");
}

- (void)dropdownViewDidHide:(LMDropdownView *)dropdownView {
    NSLog(@"Dropdown view did hide");
    
    switch (self.currentMapTypeIndex) {
        case 0:
            self.mapView.mapType = MKMapTypeSatellite ;
            break;
        case 1:
            self.mapView.mapType = MKMapTypeStandard ;
            break;
        case 2:
            self.mapView.mapType = MKMapTypeHybrid ;
            break;
        case 3:
            [googlMapView setMapType:kGMSTypeSatellite];
            break;
        case 4:
            [googlMapView setMapType:kGMSTypeTerrain];
            break;
        case 5:
            [googlMapView setMapType:kGMSTypeHybrid];
            break;
        case 6:
            [self.mapView reloadInputViews];
            break;
        default:
            break;
    }
}

#pragma mark - MENU TABLE VIEW
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.mapTypes count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    DetailMapTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (!cell) {
        cell = [[DetailMapTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    }
    
    cell.menuItemLabel.text = [self.mapTypes objectAtIndex:indexPath.row];
    cell.selectedMarkView.hidden = (indexPath.row != self.currentMapTypeIndex);
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    self.currentMapTypeIndex = indexPath.row;
    [self.dropdownView hide];
}

#pragma mark - MAP VIEW DELEGATE
-(void)AlertAction {
    [self.tabBarController setSelectedIndex:3];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    self.menuTableView.frame = CGRectMake(CGRectGetMinX(self.menuTableView.frame),
                                          CGRectGetMinY(self.menuTableView.frame),
                                          CGRectGetWidth(self.view.bounds),
                                          MIN(CGRectGetHeight(self.view.bounds) - 50, self.mapTypes.count * 50));
    
}

#pragma mark - Data Post
-(void)PostSearchData:(NSString *)Key {
    if ([globalObject checkInternetConnection]) {
        [self->globalObject showActivityIndicator];
        
        NSString *str = [NSString stringWithFormat:@"%@/%@",slclistURL,[[sqlObj getAllUserInfo] valueForKey:@"userid"]] ;
        NSString *urlString = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *url = [NSURL URLWithString:urlString];
        
        ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
        [request setRequestMethod:@"POST"] ;
        [request setDelegate:self] ;
        [request setPostValue:Key forKey:@"search"] ;
        [request startAsynchronous] ;
    } else {
        [globalObject removeActivityIndicator];
    }
}

-(void)requestFailed:(ASIHTTPRequest *)request {
    NSError *error = [request error];
    NSLog(@"error : %@" , error.localizedDescription);
    [globalObject removeActivityIndicator];
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:error.localizedDescription
                                  message:@""
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"OK"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action)
                                {
        [self->googlMapView clear];
        
        [self->mapView removeAnnotations:self->mapView.annotations];
                                }];
    
    [alert addAction:yesButton];
    
    [self presentViewController:alert animated:YES completion:nil];
    /*NSError *error = [request error];
    NSLog(@"error : %@" , error);
    [globalObject showAlertWithTitle:@"Error" message:LocalizedString(@"No Internet Connection")];
    [globalObject removeActivityIndicator] ;*/
}

-(void)requestFinished:(ASIHTTPRequest *)request {
    [globalObject removeActivityIndicator] ;
    NSString *responseString = [request responseString];
    NSMutableDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:[responseString dataUsingEncoding:NSUTF8StringEncoding] options:0 error:nil];
    NSLog(@"Responce Dictionary : %@", responseDict);
    
    if ([[responseDict objectForKey:@"status"] isEqualToString:@"1"]) {
        NSMutableArray *dataarray = [[responseDict objectForKey:@"data"] objectForKey:@"List"];
        NSMutableArray *pinarray = [[NSMutableArray alloc] init];
        NSMutableArray *arrForCluster = [[NSMutableArray alloc] initWithArray:dataarray];
        
        for (int i = 0 ; i < dataarray.count ; i++)  {
            NSDictionary *subDic = [dataarray objectAtIndex:i] ;
            if (_currentMapTypeIndex != 6 && _currentMapTypeIndex > 2) {
            } else {
                CLLocationCoordinate2D location = CLLocationCoordinate2DMake([[subDic objectForKey:@"lat"] doubleValue], [[subDic objectForKey:@"lng"] doubleValue]) ;
                NSString *title = [NSString stringWithFormat:@"%@ : %@",LocalizedString(@"SLC ID"), [subDic objectForKey:@"slc_id"]] ;
                NSString *subtitle = [NSString stringWithFormat:@"%@ : %@",LocalizedString(@"POLE ID"), [subDic objectForKey:@"pole_id"]] ;
                NSString *strID = [subDic objectForKey:@"ID"] ;
                MyAnnotation *annot = [[MyAnnotation alloc] initWithCoordinate:location title:title subtitle:subtitle strid:strID] ;
                [pinarray addObject:annot];
            }
        }
        
        if (_currentMapTypeIndex != 6 && _currentMapTypeIndex > 2) {
            [self generateClusterItems:arrForCluster];
            [_clusterManager cluster];
            [_clusterManager setDelegate:self mapDelegate:self];
        } else {
            clusteringManager = [[FBClusteringManager alloc] initWithAnnotations:pinarray];
            clusteringManager.delegate = self;
            [self->mapView addAnnotations:pinarray];
            
            dispatch_async(dispatch_get_main_queue(), ^(void){
                double scale = self.mapView.bounds.size.width / self.mapView.visibleMapRect.size.width;
                NSArray *annotations = [self->clusteringManager clusteredAnnotationsWithinMapRect:self->mapView.visibleMapRect withZoomScale:scale];
                [self->clusteringManager displayAnnotations:annotations onMapView:self->mapView];
            });
        }
    }  else if ([[responseDict objectForKey:@"status"] isEqualToString:@"error"]) {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:[responseDict objectForKey:@"msg"]                                      message:@""
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
            [self->googlMapView clear];
            
            [self->mapView removeAnnotations:self->mapView.annotations];
                                    }];
        
        [alert addAction:yesButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    } else if ([[responseDict objectForKey:@"status"] isEqualToString:@"0"]) {
        
        NSString *strMsg = [responseDict objectForKey:@"msg"];
        
        if (strMsg == nil || [strMsg  isEqualToString: @""]) {
            strMsg = @"Oops! Something went wrong. Try again later";
        }
        
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:LocalizedString(strMsg)
                                      message:@""
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
            sqlObj = [[SQLManager alloc] init] ;
            [sqlObj createAndOpenDatabase] ;
            
            [sqlObj DeleteData] ;
            
            UIStoryboard *story = [UIStoryboard storyboardWithName:@"SecurityCodeViewController" bundle:nil];
            
            SecurityCodeViewController *SecurityCodeViewController = [story instantiateViewControllerWithIdentifier:@"SecurityCodeViewController"];
            
            UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:SecurityCodeViewController];
            
            [[UIApplication sharedApplication].keyWindow setRootViewController:navController];
        }];
        
        [alert addAction:yesButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    } else {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:LocalizedString(@"Oops! Something went wrong. Try again later")
                                      message:@""
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
                                    }];
        [alert addAction:yesButton];
        [self presentViewController:alert animated:YES completion:nil];
    }
    [globalObject removeActivityIndicator];
}

-(void)eventof:(NSString *)strEventName {
    dispatch_async(dispatch_get_main_queue(), ^{
    NSString *strClientID = [[sqlObj getAllUserInfo] valueForKey:@"clientID"];
    NSString *strUserID = [[sqlObj getAllUserInfo] valueForKey:@"userid"];
    
    [FIRAnalytics logEventWithName:kFIREventSelectContent
                        parameters:@{
                                     kFIRParameterItemName:[NSString stringWithFormat:@"%@_%@_%@",strClientID, strUserID, strEventName],
                                     }];
    });
}

@end

