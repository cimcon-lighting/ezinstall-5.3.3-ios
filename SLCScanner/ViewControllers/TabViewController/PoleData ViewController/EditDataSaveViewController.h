//
//  EditDataSaveViewController.h
//  SLCScanner
//
//  Created by INT MAC 2015 on 30/04/18.
//  Copyright © 2018 INT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NSURLConnection_Class.h"
#import "SQLManager.h"
#import "GlobalAssistant.h"
#import "ASIFormDataRequest.h"
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>

@interface EditDataSaveViewController : UIViewController <NSURLConnection_CustomDelegate,ASIHTTPRequestDelegate,UITextFieldDelegate,MKMapViewDelegate,CLLocationManagerDelegate>
{
    NSMutableDictionary *DataDictionary ;
    
    NSMutableDictionary *SelectedDataForPostDictionary ;
    
    NSMutableArray *DataArray ;

}

@property(strong, nonatomic)NSString *strSelecctedDataSlcID ;

//------------assign property--------------//

// --------- Initialize GlobalAssistant

@property (strong , nonatomic) GlobalAssistant *globalObject ;

//-------- SqlManager Obj -------//

@property (strong , nonatomic) SQLManager *sqlObj ;

//***********************************************

@property (strong, nonatomic) IBOutlet UIDatePicker *datePicker ;

@property (strong, nonatomic) IBOutlet UIView *containerView;

@property (strong, nonatomic) IBOutlet UIScrollView *scroll;

@property (strong, nonatomic) IBOutlet UIButton *BtnSLCID ;
@property (strong, nonatomic) IBOutlet UITextField *txtPoleID ;
@property (strong, nonatomic) IBOutlet UIButton *BtnMacAddress ;

@property (strong, nonatomic) IBOutlet UIButton *btnSave ;

@property (strong, nonatomic) IBOutlet UILabel *lblSelectDate ;
@property (strong, nonatomic) IBOutlet UIButton *btnSelectDate ;

@property (strong, nonatomic) IBOutlet UILabel *lblAddress ;
@property (strong, nonatomic) IBOutlet UITextView *txtAddress ;

//----------------------------------------------

@property (strong, nonatomic) IBOutlet MKMapView *mapView ;

@property (strong, nonatomic) IBOutlet UIView *infoMapView ;

//***********************************************

@property (strong, nonatomic) IBOutlet UIView *PoleImageView ;

@property (strong, nonatomic) IBOutlet UIImageView *PoleImg ;

@property (strong, nonatomic) IBOutlet UIButton *ChooseImgBtn ;

@property (strong, nonatomic) IBOutlet UIButton *SaveImgBtn ;

@property (strong, nonatomic) IBOutlet UIView   *BottomViewObj ;

@property (strong, nonatomic) IBOutlet NSString *strNotes;

@property (strong, nonatomic) NSMutableArray    *arrOptionsData;

@property (nonatomic) BOOL                  isFrmOptUpdate;

- (IBAction)ChangeBtnBtn:(id)sender;

- (IBAction)ChooseImgBtn:(id)sender;

- (IBAction)SaveImgBtn:(id)sender;

//----------------------------------------------

- (IBAction)MapOkButton:(id)sender;

//----------------------------------------------

- (IBAction)SaveButton:(id)sender;


- (IBAction)MacIDButton:(id)sender;

- (IBAction)SLCIDButton:(id)sender;

@end
