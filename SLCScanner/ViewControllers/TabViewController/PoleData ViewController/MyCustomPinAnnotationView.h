//
//  MyCustomPinAnnotationView.h
//  MyCustomPinProject
//
//  Created by Thomas Lextrait on 1/4/16.
//  Copyright © 2016 com.tlextrait. All rights reserved.
//

#import <MapKit/MapKit.h>

@interface MyCustomPinAnnotationView : MKAnnotationView

@property float price;

- (instancetype)initWithAnnotation:(id<MKAnnotation>)annotation
                             price:(float)price;

@end

