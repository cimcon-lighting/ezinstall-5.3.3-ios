//
//  DetailMapTableViewCell.m
//  SLCScanner
//
//  Created by INT MAC 2015 on 15/03/18.
//  Copyright © 2018 INT. All rights reserved.
//

#import "DetailMapTableViewCell.h"

@implementation DetailMapTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
