//
//  DetailMapTableViewCell.h
//  SLCScanner
//
//  Created by INT MAC 2015 on 15/03/18.
//  Copyright © 2018 INT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailMapTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *menuItemLabel;
@property (strong, nonatomic) IBOutlet UIImageView *selectedMarkView;

@end
