//
//  MapSLCDataViewController.h
//  SLCScanner
//
//  Created by INT MAC 2015 on 12/03/18.
//  Copyright © 2018 INT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

#import "SQLManager.h"
#import "AppDelegate.h"
#import "LMDropdownView.h"
#import "GlobalAssistant.h"
#import "ASIFormDataRequest.h"
#import "NSURLConnection_Class.h"

@interface MapSLCDataViewController : UIViewController <NSURLConnection_CustomDelegate, ASIHTTPRequestDelegate>


//------------assign property--------------//

// --------- Initialize GlobalAssistant

@property (strong , nonatomic) GlobalAssistant *globalObject ;

//-------- SqlManager Obj -------//

@property (strong , nonatomic) SQLManager *sqlObj ;

@property (strong , nonatomic) NSString *strSearchValue ;

@property (strong , nonatomic) NSString *strListView ;
@property (strong , nonatomic) NSString *strEditView ;

@property (strong, nonatomic) IBOutlet MKMapView *mapView;

@property (strong, nonatomic) NSArray *mapTypes;
@property (assign, nonatomic) NSInteger currentMapTypeIndex;
@property (strong, nonatomic) IBOutlet UITableView *menuTableView;
@property (strong, nonatomic) LMDropdownView *dropdownView;

@end
