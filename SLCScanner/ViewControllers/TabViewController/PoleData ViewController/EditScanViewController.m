//
//  EditScanViewController.m
//  SLCScanner
//
//  Created by INT MAC 2015 on 25/06/18.
//  Copyright © 2018 INT. All rights reserved.
//

#import "EditScanViewController.h"

#import "API.h"
#import "AppDelegate.h"
#import "SQLManager.h"
#import "LocalizeHelper.h"
#import "SecurityCodeViewController.h"

#import <AudioToolbox/AudioToolbox.h>

@interface EditScanViewController () <UITextFieldDelegate>
{
    UIView      *laserView ;
    NSString    *scannedCode ;
    UIButton    *alertButton  ;
    NSString    *strRequestFor;
    
    IBOutlet UILabel *lblCustomerName;
    IBOutlet UIView *vwCustomerName;
}
@property (nonatomic, strong) ZXCapture *capture;
@property (nonatomic, weak) IBOutlet UIView *scanRectView;
@property (nonatomic, weak) IBOutlet UILabel *decodedLabel;

@end


@implementation EditScanViewController
{
    CGAffineTransform _captureSizeTransform;
}
@synthesize strMacAddress, strSLCAddress, globalObject ;
@synthesize sqlObj ;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    
    globalObject = [[GlobalAssistant alloc] init] ;
    
    sqlObj = [[SQLManager alloc] init] ;
    [sqlObj createAndOpenDatabase] ;
    
    //--------------------------------
    
    self.capture = [[ZXCapture alloc] init];
    self.capture.camera = self.capture.back;
    self.capture.focusMode = AVCaptureFocusModeContinuousAutoFocus;
    
    [self.view.layer addSublayer:self.capture.layer];
    
    [self.view bringSubviewToFront:self.scanRectView];
    [self.view bringSubviewToFront:self.decodedLabel];
    [self.view bringSubviewToFront:vwCustomerName];
    
    //-----------------------------------------------
    
    //-----------------------------------------------
    
    UIImage* imageAlert = [UIImage imageNamed:@"finger"];
    CGRect frameimg = CGRectMake(0,0,25,25);
    
    UIButton *alertButton = [[UIButton alloc] initWithFrame:frameimg];
    [alertButton setBackgroundImage:imageAlert forState:UIControlStateNormal];
    [alertButton addTarget:self action:@selector(AlertAction)
          forControlEvents:UIControlEventTouchUpInside];
    [alertButton setShowsTouchWhenHighlighted:YES];
    
    UIBarButtonItem *notificationbutton =[[UIBarButtonItem alloc] initWithCustomView:alertButton];
    self.navigationItem.rightBarButtonItem = notificationbutton;
    
    UIImage* imgback = [UIImage imageNamed:@"left_arrow"];
    CGRect frameimgBk = CGRectMake(0,0,25,25);
    UIBarButtonItem *bkNotiBtn =[[UIBarButtonItem alloc] initWithCustomView:alertButton];
    self.navigationItem.rightBarButtonItem = bkNotiBtn;
    UIButton *btnBack = [[UIButton alloc] initWithFrame:frameimgBk];
    [btnBack setBackgroundImage:imgback forState:UIControlStateNormal];
    [btnBack addTarget:self action:@selector(btnBackClick)
      forControlEvents:UIControlEventTouchUpInside];
    [btnBack setShowsTouchWhenHighlighted:YES];
    
    UIBarButtonItem *bkBtn =[[UIBarButtonItem alloc] initWithCustomView:btnBack];
    self.navigationItem.leftBarButtonItems = @[bkBtn];
    
}

-(void)btnBackClick{
    [self.navigationController popViewControllerAnimated:true];
}

-(void)AlertAction
{
    [self.capture stop];
    
    if (strMacAddress)
    {
        scannedCode = strMacAddress ;
    }
    else
    {
        scannedCode = strSLCAddress ;
    }
    
    
    [self AlertForConfirm] ;
}

-(AppDelegate *)appDelegate
{
    return (AppDelegate *) [[UIApplication sharedApplication] delegate];
}


-(void)redLineShow
{
    laserView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.scanRectView.frame.size.width, 2)];
    laserView.backgroundColor = [UIColor redColor];
    //  laserView.layer.shadowColor = CFBridgingRetain([UIColor redColor]);
    laserView.layer.shadowOffset = CGSizeMake(0.5, 0.5);
    laserView.layer.shadowOpacity = 0.6;
    laserView.layer.shadowRadius = 1.5;
    laserView.alpha = 0.0;
    [self.scanRectView addSubview:laserView];
    
    // Add the line
    [UIView animateWithDuration:0.2 animations:^{
        laserView.alpha = 1.0;
    }];
    
    [UIView animateWithDuration:4.0 delay:0.0 options:UIViewAnimationOptionAutoreverse | UIViewAnimationOptionRepeat | UIViewAnimationOptionCurveEaseInOut animations:^{
        laserView.frame = CGRectMake(0, self.scanRectView.frame.size.height, self.scanRectView.frame.size.width, 2);
    } completion:nil];
    
}

-(void)viewDidDisappear:(BOOL)animated
{
    [laserView removeFromSuperview];
    
    self.capture.delegate = nil ;
    
}



#pragma mark - View Controller Methods

- (void)dealloc {
    [self.capture.layer removeFromSuperlayer];
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    lblCustomerName.text = [[sqlObj getAllUserInfo] valueForKey:@"CUSTOMER_NAME"];
    
    [self redLineShow];
    
    self.capture.delegate = self;
    
    [self.capture start];
    
    [self applyOrientation];
    
    [self localizationSetup];
}

-(void)localizationSetup {
    if ([strMacAddress isEqualToString:@""] ||  strMacAddress == nil )  {
        self.title      = [NSString stringWithFormat:@"%@",LocalizedString(@"SCAN SLC ID")];
    } else {
        self.title      = [NSString stringWithFormat:@"%@",LocalizedString(@"SCAN UID")];
    }
    
    _decodedLabel.text = LocalizedString(@"Position Code in Box Above");
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    if ([_isFrmMacID isEqualToString:@"true"]) {
        NSRange lowercaseCharRange = [string rangeOfCharacterFromSet:[NSCharacterSet lowercaseLetterCharacterSet]];
        
        if (lowercaseCharRange.location != NSNotFound) {
            textField.text = [textField.text stringByReplacingCharactersInRange:range
                                                                     withString:[string uppercaseString]];
            return NO;
        }
        
        NSCharacterSet *numbersOnly = [NSCharacterSet characterSetWithCharactersInString:@"0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"];
        NSCharacterSet *characterSetFromTextField = [NSCharacterSet characterSetWithCharactersInString:string];
        
        BOOL stringIsValid = [numbersOnly isSupersetOfSet:characterSetFromTextField];
        
        return stringIsValid;
    } else {
        if([textField.text length]==0)
        {
            if([string isEqualToString:@" "])
            {
                return NO;
            }
        }
        return YES;
    }
    
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
    return toInterfaceOrientation == UIInterfaceOrientationPortrait;
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
    [self applyOrientation];
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id <UIViewControllerTransitionCoordinator>)coordinator {
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    [coordinator animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext> context) {
    } completion:^(id<UIViewControllerTransitionCoordinatorContext> context)
     {
        [self applyOrientation];
    }];
}

#pragma mark - Private

- (void)applyOrientation {
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    float scanRectRotation;
    float captureRotation;
    
    switch (orientation) {
        case UIInterfaceOrientationPortrait:
            captureRotation = 0;
            scanRectRotation = 90;
            break;
        case UIInterfaceOrientationLandscapeLeft:
            captureRotation = 90;
            scanRectRotation = 180;
            break;
        case UIInterfaceOrientationLandscapeRight:
            captureRotation = 270;
            scanRectRotation = 0;
            break;
        case UIInterfaceOrientationPortraitUpsideDown:
            captureRotation = 180;
            scanRectRotation = 270;
            break;
        default:
            captureRotation = 0;
            scanRectRotation = 90;
            break;
    }
    [self applyRectOfInterest:orientation];
    CGAffineTransform transform = CGAffineTransformMakeRotation((CGFloat) (captureRotation / 180 * M_PI));
    [self.capture setTransform:transform];
    [self.capture setRotation:scanRectRotation];
    
    self.capture.layer.frame = CGRectMake(self.view.frame.origin.x, 0, self.view.frame.size.width, self.view.frame.size.height) ;
    
    // self.capture.layer.frame = self.view.frame;
}

- (void)applyRectOfInterest:(UIInterfaceOrientation)orientation {
    CGFloat scaleVideo, scaleVideoX, scaleVideoY;
    CGFloat videoSizeX, videoSizeY;
    CGRect transformedVideoRect = self.scanRectView.frame;
    if([self.capture.sessionPreset isEqualToString:AVCaptureSessionPreset1920x1080]) {
        videoSizeX = 1080;
        videoSizeY = 1920;
    } else {
        videoSizeX = 720;
        videoSizeY = 1280;
    }
    if(UIInterfaceOrientationIsPortrait(orientation)) {
        scaleVideoX = self.view.frame.size.width / videoSizeX;
        scaleVideoY = self.view.frame.size.height / videoSizeY;
        scaleVideo = MAX(scaleVideoX, scaleVideoY);
        if(scaleVideoX > scaleVideoY) {
            transformedVideoRect.origin.y += (scaleVideo * videoSizeY - self.view.frame.size.height) / 2;
        } else {
            transformedVideoRect.origin.x += (scaleVideo * videoSizeX - self.view.frame.size.width) / 2;
        }
    } else {
        scaleVideoX = self.view.frame.size.width / videoSizeY;
        scaleVideoY = self.view.frame.size.height / videoSizeX;
        scaleVideo = MAX(scaleVideoX, scaleVideoY);
        if(scaleVideoX > scaleVideoY) {
            transformedVideoRect.origin.y += (scaleVideo * videoSizeX - self.view.frame.size.height) / 2;
        } else {
            transformedVideoRect.origin.x += (scaleVideo * videoSizeY - self.view.frame.size.width) / 2;
        }
    }
    _captureSizeTransform = CGAffineTransformMakeScale(1/scaleVideo, 1/scaleVideo);
    self.capture.scanRect = CGRectApplyAffineTransform(transformedVideoRect, _captureSizeTransform);
    
    
}

#pragma mark - Private Methods

- (NSString *)barcodeFormatToString:(ZXBarcodeFormat)format {
    switch (format) {
        case kBarcodeFormatAztec:
            return @"Aztec";
            
        case kBarcodeFormatCodabar:
            return @"CODABAR";
            
        case kBarcodeFormatCode39:
            return @"Code 39";
            
        case kBarcodeFormatCode93:
            return @"Code 93";
            
        case kBarcodeFormatCode128:
            return @"Code 128";
            
        case kBarcodeFormatDataMatrix:
            return @"Data Matrix";
            
        case kBarcodeFormatEan8:
            return @"EAN-8";
            
        case kBarcodeFormatEan13:
            return @"EAN-13";
            
        case kBarcodeFormatITF:
            return @"ITF";
            
        case kBarcodeFormatPDF417:
            return @"PDF417";
            
        case kBarcodeFormatQRCode:
            return @"QR Code";
            
        case kBarcodeFormatRSS14:
            return @"RSS 14";
            
        case kBarcodeFormatRSSExpanded:
            return @"RSS Expanded";
            
        case kBarcodeFormatUPCA:
            return @"UPCA";
            
        case kBarcodeFormatUPCE:
            return @"UPCE";
            
        case kBarcodeFormatUPCEANExtension:
            return @"UPC/EAN extension";
            
        default:
            return @"Unknown";
    }
}

#pragma mark - ZXCaptureDelegate Methods

- (void)captureResult:(ZXCapture *)capture result:(ZXResult *)result {
    if (!result) return;
    
    CGAffineTransform inverse = CGAffineTransformInvert(_captureSizeTransform);
    NSMutableArray *points = [[NSMutableArray alloc] init];
    NSString *location = @"";
    for (ZXResultPoint *resultPoint in result.resultPoints) {
        CGPoint cgPoint = CGPointMake(resultPoint.x, resultPoint.y);
        CGPoint transformedPoint = CGPointApplyAffineTransform(cgPoint, inverse);
        transformedPoint = [self.scanRectView convertPoint:transformedPoint toView:self.scanRectView.window];
        NSValue* windowPointValue = [NSValue valueWithCGPoint:transformedPoint];
        location = [NSString stringWithFormat:@"%@ (%f, %f)", location, transformedPoint.x, transformedPoint.y];
        [points addObject:windowPointValue];
    }
    
    
    // We got a result. Display information about the result onscreen.
    NSString *formatString = [self barcodeFormatToString:result.barcodeFormat];
    NSString *display = [NSString stringWithFormat:@"Scanned!\n\nFormat: %@\n\nContents:\n%@\nLocation: %@", formatString, result.text, location];
    NSLog(@"%@",display);
    
    // Vibrate
    AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
    
    scannedCode = result.text ;
    
    [self.capture stop];
    
    [self AlertForConfirm] ;
    
    //  [self.decodedLabel performSelectorOnMainThread:@selector(setText:) withObject:display waitUntilDone:YES];
    
    /*
     dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
     [self.capture start];
     });*/
}


#pragma mark - checkmacaddressInternal
-(void)checkmacaddressInternal {
    if ([globalObject checkInternetConnection]) {
        [globalObject showActivityIndicator];
        
        strRequestFor = @"checkmacInternal" ;
        
        NSString *str =  checkInternalUniqueEDITURL;
        NSString *urlString = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *url = [NSURL URLWithString:urlString];
        
        ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
        
        [request setRequestMethod:@"POST"] ;
        [request setDelegate:self] ;
        
        
        sqlObj = [[SQLManager alloc] init] ;
        
        [sqlObj createAndOpenDatabase] ;
        
        [request setPostValue:[[sqlObj getAllUserInfo] valueForKey:@"clientID"] forKey:@"client_id"] ;
        
        [request setPostValue:[[sqlObj getAllUserInfo] valueForKey:@"userid"] forKey:@"user_id"] ;
        
        [request setPostValue:scannedCode forKey:@"mac_address"] ;
        
        [request setPostValue:@"IOS" forKey:@"source"];
        
        [request setTimeOutSeconds:180] ;
        
        [request startAsynchronous] ;
        
    } else {
        [globalObject removeActivityIndicator];
    }
}

#pragma mark - Data get

-(void)checkMACidSLCid
{
    if ([globalObject checkInternetConnection]) {
        [globalObject showActivityIndicator];
        
        strRequestFor = @"checkSLCMACid";
        
        NSString *str =  checkmacandslcURL;
        NSString *urlString = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *url = [NSURL URLWithString:urlString];
        
        ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
        
        [request setRequestMethod:@"POST"] ;
        [request setDelegate:self] ;
        
        // ---------- Initialize SqlObject
        
        sqlObj = [[SQLManager alloc] init] ;
        
        [sqlObj createAndOpenDatabase] ;
        
        //------- Post Data
        
        // ---------- Initialize SqlObject
        
        [request setPostValue:[[sqlObj getAllUserInfo] valueForKey:@"clientID"] forKey:@"client_id"] ;
        
        [request setPostValue:[[sqlObj getAllUserInfo] valueForKey:@"userid"] forKey:@"user_id"] ;
        
        [request setPostValue:strSLCAddress forKey:@"slc_id"] ;
        [request setPostValue:@"IOS" forKey:@"source"] ;
        [request setPostValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"SELECTDODETYPE"] forKey:@"node_type"] ;
        
        [request setPostValue:scannedCode forKey:@"mac_address"] ;
        
        [request setTimeOutSeconds:180] ;
        
        //-------
        
        [request startAsynchronous] ;
    } else {
        [globalObject removeActivityIndicator];
    }
}


#pragma mark - Data Post

-(void)checkmacaddress
{
    if ([globalObject checkInternetConnection]) {
        [globalObject showActivityIndicator];
        
        strRequestFor = @"checkmacaddress" ;
        
        NSString *str =  checkmacaddressURL;
        NSString *urlString = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *url = [NSURL URLWithString:urlString];
        
        ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
        
        [request setRequestMethod:@"POST"] ;
        [request setDelegate:self] ;
        
        // ---------- Initialize SqlObject
        
        sqlObj = [[SQLManager alloc] init] ;
        
        [sqlObj createAndOpenDatabase] ;
        
        //------- Post Data
        
        // ---------- Initialize SqlObject
        
        [request setPostValue:[[sqlObj getAllUserInfo] valueForKey:@"clientID"] forKey:@"client_id"] ;
        
        [request setPostValue:[[sqlObj getAllUserInfo] valueForKey:@"userid"] forKey:@"user_id"] ;
        
        [request setPostValue:strMacAddress forKey:@"macaddress"] ;
        
        [request setPostValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"SELECTDODETYPE"] forKey:@"node_type"] ;
        
        [request setPostValue:@"IOS" forKey:@"source"];
        
        [request setTimeOutSeconds:180] ;
        
        //-------
        
        [request startAsynchronous] ;
    } else {
        [globalObject removeActivityIndicator];
    }
}


- (void)AlertForConfirm
{
    
    // ---------- Initialize SqlObject
    
    sqlObj = [[SQLManager alloc] init] ;
    
    [sqlObj createAndOpenDatabase] ;
    
    NSString  *title ;
    NSString  *placeholder ;
    
    
    if (strMacAddress)
    {
        title = [[sqlObj getAllUserInfo] valueForKey:@"ScanLBL"] ;
        placeholder = [[sqlObj getAllUserInfo] valueForKey:@"ScanPH"] ;
    }
    else
    {
        title = LocalizedString(@"SLC ID") ;
        placeholder = LocalizedString(@"SLC ID");//@"SLC ID" ;
    }
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title
                                                                   message:nil
                                                            preferredStyle:UIAlertControllerStyleAlert];
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        // optionally configure the text field
        textField.text = scannedCode ;
        textField.placeholder = placeholder ;
        textField.borderStyle = UITextBorderStyleRoundedRect ;
        textField.autocapitalizationType = UITextAutocapitalizationTypeAllCharacters;
        textField.font = [UIFont systemFontOfSize:16.0] ;
        textField.keyboardType = UIKeyboardTypeAlphabet;
        textField.returnKeyType = UIReturnKeySend ;
        textField.delegate = self ;
        
    }];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:LocalizedString(@"Confirm")
                                                       style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction *action) {
        UITextField *textField = [alert.textFields firstObject];
        
        scannedCode = textField.text ;
        if ([_isFrmMacID isEqualToString:@"true"]) {
            [self checkmacaddressInternal];
        } else {
            [self appDelegate].strEditSLCID = scannedCode ;
            [[self navigationController] popViewControllerAnimated:true];
        }
    }];
    
    UIAlertAction *CancelAction = [UIAlertAction actionWithTitle:LocalizedString(@"Cancel")
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction *action) {
        [alertButton setTitle:LocalizedString(@"Stop") forState:UIControlStateNormal];
        
        [self.capture start];
        
    }];
    
    [alert addAction:CancelAction];
    [alert addAction:okAction];
    
    [self presentViewController:alert animated:YES completion:nil];
    
}
-(void)requestFailed:(ASIHTTPRequest *)request
{
    NSError *error = [request error];
    NSLog(@"error : %@" , error.localizedDescription);
    [globalObject removeActivityIndicator];
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:error.localizedDescription
                                  message:@""
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"OK"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action)
                                {
    }];
    
    [alert addAction:yesButton];
    
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)requestFinished:(ASIHTTPRequest *)request
{
    NSString *responseString = [request responseString];
    NSMutableDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:[responseString dataUsingEncoding:NSUTF8StringEncoding] options:0 error:nil];
    NSLog(@"Responce Dictionary : %@", responseDict);
    
    [globalObject removeActivityIndicator];
    
    if (strMacAddress)
    {
        [self appDelegate].strEditMacID = scannedCode ;
    }
    else
    {
        [self appDelegate].strEditSLCID = scannedCode ;
    }
    
    if([[responseDict valueForKey:@"status"] isEqualToString:@"success"]) {
        [[self navigationController] popViewControllerAnimated:true];
    } else if ([[responseDict objectForKey:@"status"] isEqualToString:@"error"]) {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:[responseDict objectForKey:@"msg"]                                      message:@""
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
            [self.capture start];
        }];
        
        [alert addAction:yesButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    } else if ([[responseDict objectForKey:@"status"] isEqualToString:@"internal"]) {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:[responseDict objectForKey:@"msg"]                                      message:@""
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
            [self appDelegate].strFrmExternal = @"internal" ;
            [self appDelegate].strEditSLCID = @"" ;
            [self appDelegate].strEditMacID = strMacAddress;
            [[self navigationController] popViewControllerAnimated:true];
        }];
        
        [alert addAction:yesButton];
        
        [self presentViewController:alert animated:YES completion:nil];
        
    } else if ([[responseDict objectForKey:@"status"] isEqualToString:@"external"]) {
        [self appDelegate].strFrmExternal = @"external" ;
        [self appDelegate].strEditSLCID = [responseDict objectForKey:@"slc_id"] ;
        [[self navigationController] popViewControllerAnimated:true];
        //[self checkmacaddress];
    } else if ([[responseDict objectForKey:@"status"] isEqualToString:@"0"]) {
        
        NSString *strMsg = [responseDict objectForKey:@"msg"];
        
        if (strMsg == nil || [strMsg  isEqualToString: @""]) {
            strMsg = @"Oops! Something went wrong. Try again later";
        }
        
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:LocalizedString(strMsg)
                                      message:@""
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
            sqlObj = [[SQLManager alloc] init] ;
            [sqlObj createAndOpenDatabase] ;
            
            [sqlObj DeleteData] ;
            
            UIStoryboard *story = [UIStoryboard storyboardWithName:@"SecurityCodeViewController" bundle:nil];
            
            SecurityCodeViewController *SecurityCodeViewController = [story instantiateViewControllerWithIdentifier:@"SecurityCodeViewController"];
            
            UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:SecurityCodeViewController];
            
            [[UIApplication sharedApplication].keyWindow setRootViewController:navController];
        }];
        
        [alert addAction:yesButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    } else {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:LocalizedString(@"Oops! Something went wrong. Try again later")
                                      message:@""
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
        }];
        [alert addAction:yesButton];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
