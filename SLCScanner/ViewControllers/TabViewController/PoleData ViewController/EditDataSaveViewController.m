//
//  EditDataSaveViewController.m
//  SLCScanner
//
//  Created by INT MAC 2015 on 30/04/18.
//  Copyright © 2018 INT. All rights reserved.
//

#import "EditDataSaveViewController.h"

#import "API.h"
#import "AppDelegate.h"
#import "LocalizeHelper.h"
#import "UIImageView+WebCache.h"
#import "PoleOptionController.h"
#import "EditScanViewController.h"
#import "SecurityCodeViewController.h"

#import <GoogleMaps/GoogleMaps.h>
#import <FirebaseAnalytics/FirebaseAnalytics.h>

@interface EditDataSaveViewController () <UIImagePickerControllerDelegate,UINavigationControllerDelegate, GMSMapViewDelegate>
{
    UIButton *Databutton ;
    UIToolbar *toolbar ;
    
    NSString *strSlcID ;
    NSString *strPoleID ;
    
    NSString *PoleImgURL ;
    
    NSString *isFrmInternal;
    NSString *uidCheck ;
    NSString *SelectNewImage ;
    
    NSString *ImageSelectionNavigation ;
    
    int     currentMapTypeIndex;
    
    IBOutlet UIButton *btnOK;
    IBOutlet UIButton *btnChooseImg;
    
    IBOutlet UILabel *lblMac;
    IBOutlet UILabel *lblSlc;
    IBOutlet UILabel *lblpole;
    IBOutlet UILabel *lblNodeheading;
    
    IBOutlet NSLayoutConstraint *constPoleIDLable;
    IBOutlet NSLayoutConstraint *constPoleIDtxt;
    IBOutlet NSLayoutConstraint *constPoleIDlblBottom;
    IBOutlet NSLayoutConstraint *constPoleIDtxtBottom;
    
    GMSMarker *marker;
    MKPointAnnotation *annotation;
    
    GMSCameraPosition *cameraPosition;
    
    //--------------------------------------
    
    NSString *strSelectedTime ;
    
    //--------------------------------------
    
    int selectedIndex ;
    int tagValue ;
    
    NSString *StrRequestFor ;
    NSString *StrNewAddedData ;
    
    NSString *strLatitude ;
    NSString *strLongitude ;
    
    NSString *strSelectedLatitude ;
    NSString *strSelectedLongitude ;
    
    NSString *strUsername ;
    NSString *strPassword ;
    
    CLLocationManager       *locationManager;
    
    IBOutlet GMSMapView     *googlMapView;
    MKTileOverlayRenderer   *tileRender;
    MKTileOverlay           *overLay;
    MKMapRect               boundingMapRect;
    
    IBOutlet UILabel *lblNotes;
    IBOutlet UIButton *btnNotes;
    
    BOOL isFrmMacButton ;
    
    NSMutableArray *arrNodetype ;
}

@property (nonatomic, weak) IBOutlet UIButton *btnNodetype;

@end

@implementation EditDataSaveViewController
@synthesize BtnSLCID, txtPoleID ;
@synthesize globalObject, sqlObj, infoMapView, mapView ;
@synthesize datePicker, btnSave, btnSelectDate, lblSelectDate ;
@synthesize strSelecctedDataSlcID ;
@synthesize lblAddress, txtAddress, BtnMacAddress ;
@synthesize PoleImageView, PoleImg, BottomViewObj ;


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"POLE DETAILS" ;
    
    [self eventof:@"SLCDetailSave"];
    
    annotation = [[MKPointAnnotation alloc] init] ;
    
    mapView.hidden      = true;
    googlMapView.hidden = true;
    
    // --------- Initialize GlobalAssistant
    
    globalObject = [[GlobalAssistant alloc] init] ;
    
    // ---------- Initialize SqlObject
    
    sqlObj = [[SQLManager alloc] init] ;
    
    [sqlObj createAndOpenDatabase] ;
    
    //--------------------------------
    
    UILabel  *myLabel= [[UILabel alloc] initWithFrame:CGRectMake([[UIScreen mainScreen] bounds].size.width /2 - 150 , 50 , 300 , 40)];
    
    myLabel.text = LocalizedString(@"You can edit this data now");
    
    myLabel.numberOfLines = 0 ;
    
    myLabel.textAlignment = NSTextAlignmentCenter ;
    
    myLabel.textColor = [UIColor whiteColor];
    
    myLabel.layer.masksToBounds = YES;
    
    myLabel.layer.cornerRadius = 8.0 ;
    
    myLabel.backgroundColor = [UIColor blackColor];
    
    [self.view addSubview:myLabel];
    
    myLabel.hidden = NO;
    
    myLabel.alpha = 0.8f;
    
    [UIView animateWithDuration:3.0 delay:2.0 options:0 animations:^{
        
        myLabel.alpha = 0.0f;
    } completion:^(BOOL finished) {
        
        myLabel.hidden = YES;
    }];
    
    //****************************************
    
    //   BtnMacAddress = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    
    BtnMacAddress.layer.cornerRadius=5.0;
    
    BtnMacAddress.layer.borderColor=[[UIColor blackColor]CGColor];
    
    BtnMacAddress.layer.borderWidth=1.0;
    
    //----------------------
    
    //  txtSlcID.borderStyle = UITextBorderStyleRoundedRect ;
    
    BtnSLCID.layer.cornerRadius=5.0;
    
    BtnSLCID.layer.borderColor=[[UIColor blackColor]CGColor];
    
    BtnSLCID.layer.borderWidth=1.0;
    
    //----------------------
    
    txtPoleID.borderStyle = UITextBorderStyleRoundedRect ;
    
    txtPoleID.layer.cornerRadius=5.0;
    
    txtPoleID.layer.borderColor=[[UIColor blackColor]CGColor];
    
    [txtPoleID setBackgroundColor:[UIColor colorWithRed:50.0f/255.0f green:126.0f/255.0f blue:203.0f/255.0f alpha:1]];
    //    txtPoleID.backgroundColor = [UIColor colorWithRed:50/255 green:126/255 blue:203/255 alpha:1];
    
    txtPoleID.layer.borderWidth=1.0;
    
    
    //----------------------
    
    
    
    //----------------------
    
    /*  txtAddress.layer.cornerRadius=5.0;
     
     txtAddress.layer.borderColor=[[UIColor blackColor]CGColor];
     
     txtAddress.layer.borderWidth=1.0;*/
    
    //****************************************
    
    self.containerView.frame = CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, 927);
    
    [_scroll setContentSize:CGSizeMake([[UIScreen mainScreen]bounds].size.width, _containerView.frame.size.height)] ;
    
    [_scroll scrollRectToVisible:CGRectMake(0,  0 , [[UIScreen mainScreen] bounds].size.width , 0) animated:YES] ;
    
    [self.scroll addSubview:_containerView];
    
    //--------------------------------
    
    UIImage* imageAlert = [UIImage imageNamed:@"pin_Nav"];
    CGRect frameimg = CGRectMake(0,0,25,25);
    
    UIButton *alertButton = [[UIButton alloc] initWithFrame:frameimg];
    [alertButton setBackgroundImage:imageAlert forState:UIControlStateNormal];
    [alertButton addTarget:self action:@selector(AlertAction)
          forControlEvents:UIControlEventTouchUpInside];
    [alertButton setShowsTouchWhenHighlighted:YES];
    
    UIBarButtonItem *notificationbutton =[[UIBarButtonItem alloc] initWithCustomView:alertButton];
    self.navigationItem.rightBarButtonItems = @[notificationbutton] ;
    
    UIImage* imgback = [UIImage imageNamed:@"left_arrow"];
    CGRect frameimgBk = CGRectMake(0,0,25,25);
    
    UIButton *btnBack = [[UIButton alloc] initWithFrame:frameimgBk];
    [btnBack setBackgroundImage:imgback forState:UIControlStateNormal];
    [btnBack addTarget:self action:@selector(btnBackClick)
      forControlEvents:UIControlEventTouchUpInside];
    [btnBack setShowsTouchWhenHighlighted:YES];
    
    UIBarButtonItem *bkBtn =[[UIBarButtonItem alloc] initWithCustomView:btnBack];
    self.navigationItem.leftBarButtonItems = @[bkBtn];
    
    //Lat long code
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self ;
    [locationManager requestWhenInUseAuthorization] ;
    locationManager.distanceFilter = kCLDistanceFilterNone; // whenever we move
    locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation ;
    
    [locationManager startUpdatingLocation];
    
    [globalObject buttonWithCornerRadious:24.0 btnView:btnOK];
    [globalObject buttonWithCornerRadious:22.0 btnView:_ChooseImgBtn];
    [globalObject buttonWithCornerRadious:22.0 btnView:_SaveImgBtn];
    [globalObject buttonWithCornerRadious:24.0 btnView:btnSave];
    
    DataArray = [[NSMutableArray alloc] init] ;
    
    if ([SelectNewImage isEqualToString:@"YES"]) {
        SelectNewImage = @"" ;
    } else {
        SelectedDataForPostDictionary = [[NSMutableDictionary alloc] init] ;
        [self getSLCData] ;
    }
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithDictionary:[[NSUserDefaults standardUserDefaults] valueForKey:@"NODETYPE"]];
    arrNodetype = [@[ @{ @"clientType" : @"Unknown",
                         @"value" : @""
    }] mutableCopy];
    
    [arrNodetype addObjectsFromArray:dict[@"data"]];
}

-(AppDelegate *)appDelegate {
    return (AppDelegate *) [[UIApplication sharedApplication] delegate];
}

-(void) addNumberToString: (id) sender {
    //Where current string is the string that you're appending to in whatever place you need to be keeping track of the current view's string.
    
    NSString *currentString = txtPoleID.text ;
    
    currentString = [currentString stringByAppendingString: ((UIBarButtonItem *) sender).title] ;
    
    txtPoleID.text = currentString ;
}

-(void)viewWillAppear:(BOOL)animated {
    
    if ([[[sqlObj getAllUserInfo] valueForKey:@"PoleID"] isEqualToString:@"No"]) {
        constPoleIDLable.constant = 0.0;
        constPoleIDtxt.constant = 0.0;
        constPoleIDtxtBottom.constant = 0.0;
        constPoleIDlblBottom.constant = 0.0;
    }
    
    [googlMapView clear];
    
    UIColor *color = [UIColor whiteColor];
    
    currentMapTypeIndex = [self appDelegate].strSelectedMap;
    
    [self mapSelectionView];
    
    mapView.delegate = self;
    
    txtPoleID.attributedPlaceholder =
    [[NSAttributedString alloc] initWithString:@""//LocalizedString(@"Please enter Pole ID")
                                    attributes:@{
                                        NSForegroundColorAttributeName: color,
                                        NSFontAttributeName : [UIFont systemFontOfSize:14.0]
                                    }];
    
    if (![[self appDelegate].strFrmExternal isEqualToString:@""]) {
        isFrmInternal = [self appDelegate].strFrmExternal;
        [self appDelegate].strFrmExternal = @"" ;
        strSlcID = [self appDelegate].strEditSLCID ;
        [BtnSLCID setTitle:strSlcID forState:UIControlStateNormal] ;
        [self appDelegate].strEditSLCID = @"" ;
    }
    
    if (![[self appDelegate].strEditMacID isEqualToString:@""]) {
        isFrmInternal = [self appDelegate].strFrmExternal;
        [self appDelegate].strFrmExternal = @"" ;
        [BtnMacAddress setTitle:[self appDelegate].strEditMacID forState:UIControlStateNormal] ;
        [self appDelegate].strEditMacID = @"" ;
        strSlcID = [self appDelegate].strEditSLCID ;
        [BtnSLCID setTitle:strSlcID forState:UIControlStateNormal] ;
        [self appDelegate].strEditSLCID = @"" ;
    }
    
    if (![[self appDelegate].strEditSLCID isEqualToString:@""]) {
        isFrmInternal = [self appDelegate].strFrmExternal;
        [self appDelegate].strFrmExternal = @"" ;
        strSlcID = [self appDelegate].strEditSLCID ;
        [BtnSLCID setTitle:strSlcID forState:UIControlStateNormal] ;
        [self appDelegate].strEditSLCID = @"" ;
    } else {
        //strSlcID = [[DataDictionary valueForKey:@"data"] valueForKey:@"slc_id"];
        //[BtnSLCID setTitle:strSlcID forState:UIControlStateNormal] ;
    }
    
    [self setupOSMMap];
    [self localizationSetup];
    
    if ([[[sqlObj getAllUserInfo] valueForKey:@"ImageView"] isEqualToString:@"No"]) {
        [btnSave setFrame:CGRectMake(30.0, btnSave.frame.origin.y, btnSave.frame.size.width, btnSave.frame.size.height)];
        [btnChooseImg setHidden:true];
    } else {
        [btnChooseImg setHidden:false];
    }
}

-(void)btnBackClick{
    [self eventof:@"SLCDetailEditBack"];
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:LocalizedString(@"Are you sure you want to leave, you will lose your data?")
                                  message: @""
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:LocalizedString(@"OK")
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action)
                                {
        [self.navigationController popViewControllerAnimated:true];
        
    }];
    UIAlertAction* noButton = [UIAlertAction
                                actionWithTitle:LocalizedString(@"Cancel")
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action)
                                {        
    }];
    
    [alert addAction:yesButton];
    [alert addAction:noButton];
    
    [self presentViewController:alert animated:YES completion:nil];
    
   
}

-(void)localizationSetup {
    self.title                  = LocalizedString(@"POLE DETAILS");
    
    [btnNotes setTitle:LocalizedString(@"View") forState:UIControlStateNormal];
    
    [btnSave setTitle:LocalizedString(@"SAVE") forState:UIControlStateNormal];
    [_ChooseImgBtn setTitle:LocalizedString(@"SELECT") forState:UIControlStateNormal];
    
    NSString *strMACID = [[self->sqlObj getAllUserInfo] valueForKey:@"ScanPH"];
    lblMac.text = LocalizedString(strMACID);
    lblpole.text = LocalizedString(@"POLE ID");
    lblSlc.text = LocalizedString(@"SLC ID");
    lblAddress.text = LocalizedString(@"Address");
    lblSelectDate.text = LocalizedString(@"Date Of Installation");
    lblNotes.text = LocalizedString(@"Notes");
    lblNodeheading.text = LocalizedString(@"NODE TYPE");
    
}

-(void)setupOSMMap {
    if (currentMapTypeIndex == 6) {
        overLay = [[MKTileOverlay alloc] initWithURLTemplate:@"https://tile.openstreetmap.org/{z}/{x}/{y}.png"];//];
        [overLay setCanReplaceMapContent:true];
        [mapView addOverlay:overLay];
        tileRender = [[MKTileOverlayRenderer alloc] initWithTileOverlay:overLay];
        [self.mapView reloadInputViews];
    } else {
        [self.mapView removeOverlay:overLay];
        [overLay setCanReplaceMapContent:false];
        [self.mapView reloadInputViews];
        [mapView setNeedsDisplay];
    }
}

- (void)mapSelectionView {
    
    switch (currentMapTypeIndex) {
        case 0:
            self.mapView.mapType = MKMapTypeSatellite ;
            break;
        case 1:
            self.mapView.mapType = MKMapTypeStandard ;
            break;
        case 2:
            self.mapView.mapType = MKMapTypeHybrid ;
            break;
        case 3:
            [googlMapView setMapType:kGMSTypeSatellite];
            break;
        case 4:
            [googlMapView setMapType:kGMSTypeTerrain];
            break;
        case 5:
            [googlMapView setMapType:kGMSTypeHybrid];
            break;
        case 6:
            [self.mapView reloadInputViews];
            break;
        default:
            break;
    }
}


-(void)viewWillDisappear:(BOOL)animated {
    /*if ([ImageSelectionNavigation isEqualToString:@"YES"])
     {
     ImageSelectionNavigation = @"" ;
     }
     else
     {
     for (UIView *i in _containerView.subviews)
     {
     if([i isKindOfClass:[UILabel class]])
     {
     UILabel *newLbl = (UILabel *)i;
     if(newLbl.tag == 2000)
     {
     newLbl.text = @"" ;
     [newLbl removeFromSuperview];
     }
     }
     else if([i isKindOfClass:[UIButton class]])
     {
     UIButton *newbtn = (UIButton *)i;
     if(newbtn.tag >= 1000)
     {
     [newbtn setTitle:@"" forState:UIControlStateNormal ];
     [newbtn removeFromSuperview];
     }
     }
     }
     }*/
    [googlMapView removeFromSuperview];
    [infoMapView removeFromSuperview];
    
}

#pragma Mark - Data Get

-(void)getSLCData
{
    //---------------------- Request for Getting Client data List  ----------------------//
    
    // --------- Initialize GlobalAssistant
    if ([globalObject checkInternetConnection]) {
        globalObject = [[GlobalAssistant alloc] init] ;
        
        [globalObject showActivityIndicator];
        
        // ---------- Initialize SqlObject
        
        sqlObj = [[SQLManager alloc] init] ;
        
        [sqlObj createAndOpenDatabase] ;
        
        NSString *strURL = [NSString stringWithFormat:@"%@/%@/%@",slcDetailURL,strSelecctedDataSlcID,[[sqlObj getAllUserInfo] valueForKey:@"MeasurmentUnit"]];
        
        NSURLConnection_Class *object = [[NSURLConnection_Class alloc] init] ;
        [object RequestForURL:strURL];
        object.delegate = self ;
    } else {
        [globalObject removeActivityIndicator];
    }
    
}

#pragma mark - Custom Delegate [ NSURLConnection ]

-(void)connectionFailWithError:(NSError *)error
{
    NSLog(@"Error: %@",error.localizedDescription) ;
    [globalObject showAlertWithTitle:@"Error" message:LocalizedString(@"No Internet Connection")];
    [globalObject removeActivityIndicator] ;
}


-(void)connectionFinishWithResponse:(NSString *)responseString
{
    NSMutableDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:[responseString dataUsingEncoding:NSUTF8StringEncoding] options:0 error:nil] ;
    
    NSLog(@"%@",responseDict);
    
    DataDictionary = [[NSMutableDictionary alloc] initWithDictionary:responseDict] ;
    
    DataArray = [[DataDictionary valueForKey:@"data"] valueForKey:@"Assets"] ;
    
    if (!_isFrmOptUpdate) {
        _strNotes = [[DataDictionary valueForKey:@"data"] valueForKey:@"notes"];
        _arrOptionsData = [[DataDictionary valueForKey:@"data"] valueForKey:@"pole_options"];
    }
    
    self.containerView.frame = CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, self.containerView.frame.size.height) ;
    
    if([[responseDict valueForKey:@"status"] isEqualToString:@"success"])
    {
        
        PoleImgURL =  [[DataDictionary valueForKey:@"data"] valueForKey:@"pole_image_url"] ;
        
        [PoleImg setImageWithURL:[NSURL URLWithString:PoleImgURL] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
            if (image == nil) {
                NSString *strSelectedlang = [[NSUserDefaults standardUserDefaults] stringForKey:@"appLanguage"];
                
                if ([strSelectedlang isEqualToString:@"en"]) {
                    [PoleImg setImage: [UIImage imageNamed:@"no_image"]];
                } else if ([strSelectedlang isEqualToString:@"es"]) {
                    [PoleImg setImage: [UIImage imageNamed:@"no_image_es"]];
                } else {
                    [PoleImg setImage: [UIImage imageNamed:@"no_images_pt"]];
                }
                
            }
        }];
        
        CGRect LabelFrame = CGRectMake(7.0f, 167, self.containerView.frame.size.width -172.0f, 34.0f);
        
        CGRect buttonFrame = CGRectMake(self.containerView.frame.size.width - 164.0f, 167, 156 , 34.0f);
        
        if ([[[sqlObj getAllUserInfo] valueForKey:@"PoleID"] isEqualToString:@"No"]) {
            LabelFrame = CGRectMake(7.0f, 157, self.containerView.frame.size.width -172.0f, 34.0f);
            
            buttonFrame = CGRectMake(self.containerView.frame.size.width - 164.0f, 157, 156 , 34.0f);
        }
        
        for (int index = 0; index <DataArray.count; index++)
        {
            NSString *strAttKey = [[DataArray objectAtIndex:index] valueForKey:@"AttributeName"];
            NSString *strAttKeyBlnk = [[DataArray objectAtIndex:index] valueForKey:@"AttrKey"];
            if ((![strAttKey.lowercaseString isEqualToString:@"pole image"]
                 && ![strAttKey.lowercaseString isEqualToString:@"imagem do pólo"]
                 && ![strAttKey.lowercaseString isEqualToString:@"imagen de poste"])
                
                && (![strAttKey.lowercaseString isEqualToString:@"pole id"]
                    && ![strAttKey.lowercaseString isEqualToString:@"polo id"]
                    && ![strAttKey.lowercaseString isEqualToString:@"pólo id"])
                
                && (![strAttKey.lowercaseString isEqualToString:@"notes"]
                    && ![strAttKey.lowercaseString isEqualToString:@"notas"]
                    && ![strAttKey.lowercaseString isEqualToString:@"notas"]))
            {
                Databutton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
                [Databutton setFrame:buttonFrame];
                [Databutton addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
                [Databutton setTag:index+1000];
                Databutton.titleLabel.font = btnSelectDate.titleLabel.font ;
                Databutton.layer.cornerRadius = 5 ;
                Databutton.layer.borderWidth = 1 ;
                [Databutton setBackgroundColor:btnSelectDate.backgroundColor] ;
                [Databutton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal] ;
                
                NSString *title = [NSString stringWithFormat:@"%@",[[DataArray objectAtIndex:index] valueForKey:@"Selected"]];
                [Databutton setTitle:title forState:UIControlStateNormal];
                
                [self.containerView addSubview:Databutton];
                buttonFrame.origin.y+=buttonFrame.size.height+15.0f;
                
                //**********************************************************
                
                
                UILabel *label = [[UILabel alloc] initWithFrame:LabelFrame] ;
                label.text = [[DataArray objectAtIndex:index] valueForKey:@"AttributeName"] ;
                label.font = lblSelectDate.font ;
                label.textColor = [UIColor colorWithRed:(50/255.f) green:(50/255.f) blue:(50/255.f) alpha:1.0f] ;
                label.tag = 2000 ;
                [self.containerView addSubview:label];
                
                LabelFrame.origin.y+=LabelFrame.size.height+15.0f;
                
                [SelectedDataForPostDictionary setObject:title forKey:[[DataArray objectAtIndex:index] valueForKey:@"AttrKey"]] ;
            } else {
                [SelectedDataForPostDictionary setObject:@"" forKey:strAttKeyBlnk];
            }
            
            //NSLog(@"%@",SelectedDataForPostDictionary);
            
        }
        
        lblNotes.frame = CGRectMake(7.0f, LabelFrame.origin.y , self.containerView.frame.size.width -172.0f, 34.0f) ;
        
        lblSelectDate.frame = CGRectMake(7.0f, LabelFrame.origin.y + 49 , self.containerView.frame.size.width -172.0f, 34.0f) ;
        
        lblAddress.frame = CGRectMake(7.0f, LabelFrame.origin.y + 49 + 49 , self.containerView.frame.size.width -172.0f, 34.0f) ;
        
        btnNotes.frame = CGRectMake(self.containerView.frame.size.width - 164.0f, buttonFrame.origin.y, 156, 34.0f) ;
        
        btnSelectDate.frame = CGRectMake(self.containerView.frame.size.width - 164.0f, buttonFrame.origin.y + 49, 156, 34.0f) ;
        
        txtAddress.frame = CGRectMake(self.containerView.frame.size.width - 164.0f, LabelFrame.origin.y + 49 + 49, 156, 100.0f) ;
        
        BottomViewObj.frame = CGRectMake(self.containerView.frame.size.width/2 - 106.0f , txtAddress.frame.origin.y + 140 , 212 , 45) ;
        
        /*   btnSave.frame = CGRectMake(self.containerView.frame.size.width/2 - 75, ChangeBtnObj.frame.origin.y + 65 , 150, 46) ;*/
        
        //**********************************************************
        
        isFrmInternal = [[DataDictionary valueForKey:@"data"] valueForKey:@"mac_address_type"];
        
        if ([[self appDelegate].strEditMacID isEqualToString:@""])
        {
            [BtnMacAddress setTitle:[NSString stringWithFormat:@"%@",[[DataDictionary valueForKey:@"data"] valueForKey:@"mac_address"] ] forState:UIControlStateNormal] ;
            
        }
        else
        {
            [BtnMacAddress setTitle:[self appDelegate].strEditMacID forState:UIControlStateNormal] ;
            
            [self appDelegate].strEditMacID = @"" ;
        }
        
        //**********************************************************
        
        if ([[self appDelegate].strEditSLCID isEqualToString:@""])
        {
            
            strSlcID = [[DataDictionary valueForKey:@"data"] valueForKey:@"slc_id"] ;
            
        }
        else
        {
            
            strSlcID = [self appDelegate].strEditSLCID ;
            
            [self appDelegate].strEditSLCID = @"" ;
        }
        
        [BtnSLCID setTitle:strSlcID forState:UIControlStateNormal] ;
        
        strPoleID = [[DataDictionary valueForKey:@"data"] valueForKey:@"pole_id"] ;
        txtPoleID.text = [NSString stringWithFormat:@"%@",strPoleID];
        
        NSString *strNode = [[DataDictionary valueForKey:@"data"] valueForKey:@"node_type"];
        /*if ([strNode isEqualToString:@""]){
         strNode = @"Unknown";
         }*/
        
        [_btnNodetype setTitle:@"" forState: UIControlStateNormal];
        
        strLatitude = [[DataDictionary valueForKey:@"data"] valueForKey:@"lat"] ;
        strLongitude = [[DataDictionary valueForKey:@"data"] valueForKey:@"lng"] ;
        
        strSelectedLatitude = strLatitude ;
        strSelectedLongitude = strLongitude ;
        
        strSelectedTime = [[DataDictionary valueForKey:@"data"] valueForKey:@"date_of_installation"] ;
        
        [btnSelectDate setTitle:strSelectedTime forState:UIControlStateNormal];
        
        txtAddress.text = [[DataDictionary valueForKey:@"data"] valueForKey:@"address"] ;
        
        //******************************************************************
        
        //**********************************************************
        
        self.containerView.frame = CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, BottomViewObj.frame.origin.y + 70);
        
        [_scroll setContentSize:CGSizeMake([[UIScreen mainScreen]bounds].size.width, _containerView.frame.size.height)] ;
        
        [_scroll scrollRectToVisible:CGRectMake(0,  0 , [[UIScreen mainScreen] bounds].size.width , 0) animated:YES] ;
        
        [self.scroll addSubview:_containerView];
        
        if(![[responseDict valueForKey:@"msg"] isEqualToString:@""])
        {
            UIAlertController * alert=   [UIAlertController
                                          alertControllerWithTitle:[responseDict objectForKey:@"msg"]
                                          message:@""
                                          preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* yesButton = [UIAlertAction
                                        actionWithTitle:@"OK"
                                        style:UIAlertActionStyleDefault
                                        handler:^(UIAlertAction * action)
                                        {
                //Handel your yes please button action here
                
            }];
            
            [alert addAction:yesButton];
            
            [self presentViewController:alert animated:YES completion:nil];
        }
        
        
    }
    else if ([[responseDict objectForKey:@"status"] isEqualToString:@"error"])
    {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:[responseDict objectForKey:@"msg"]
                                      message:@""
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
            //Handel your yes please button action here
            
        }];
        
        [alert addAction:yesButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    } else if ([[responseDict objectForKey:@"status"] isEqualToString:@"0"]) {
        
        NSString *strMsg = [responseDict objectForKey:@"msg"];
        
        if (strMsg == nil || [strMsg  isEqualToString: @""]) {
            strMsg = @"Oops! Something went wrong. Try again later";
        }
        
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:LocalizedString(strMsg)
                                      message:@""
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
            sqlObj = [[SQLManager alloc] init] ;
            [sqlObj createAndOpenDatabase] ;
            
            [sqlObj DeleteData] ;
            
            UIStoryboard *story = [UIStoryboard storyboardWithName:@"SecurityCodeViewController" bundle:nil];
            
            SecurityCodeViewController *SecurityCodeViewController = [story instantiateViewControllerWithIdentifier:@"SecurityCodeViewController"];
            
            UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:SecurityCodeViewController];
            
            [[UIApplication sharedApplication].keyWindow setRootViewController:navController];
        }];
        
        [alert addAction:yesButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    } else {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:LocalizedString(@"Oops! Something went wrong. Try again later")
                                      message:@""
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
        }];
        [alert addAction:yesButton];
        [self presentViewController:alert animated:YES completion:nil];
    }
    
    [globalObject removeActivityIndicator] ;
    
}

- (IBAction)nodeType_Click:(UIButton *)sender {
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle: @"Select Node Type"
                                                                         message:nil
                                                                  preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:LocalizedString(@"Cancel") style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    
    for (NSMutableDictionary *dictData in arrNodetype) {
        
        NSString *strClientNme = dictData[@"clientType"];
        
        if ([strClientNme isEqualToString: _btnNodetype.titleLabel.text])
        {
            [actionSheet addAction:[UIAlertAction actionWithTitle:strClientNme style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
                [self dismissViewControllerAnimated:YES completion:^{
                }];
            }]];
        }
        else
        {
            [actionSheet addAction:[UIAlertAction actionWithTitle:strClientNme style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                
                [self->_btnNodetype setTitle:action.title forState:UIControlStateNormal];;
                
                [self dismissViewControllerAnimated:YES completion:^{
                    
                }];
            }]];
        }
    }
    
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
        [actionSheet setModalPresentationStyle:UIModalPresentationPopover];
        
        UIPopoverPresentationController *popPresenter = [actionSheet
                                                         popoverPresentationController];
        popPresenter.sourceView = _btnNodetype;
        popPresenter.sourceRect = _btnNodetype.bounds;
        
    }
    
    [self presentViewController:actionSheet animated:YES completion:nil];
    
}
-(IBAction)btnOptions_Click:(UIButton *)sender{
    UIStoryboard *story = [UIStoryboard storyboardWithName:@"PoleOptionController" bundle:nil];
    
    PoleOptionController *PoleDataView = [story instantiateViewControllerWithIdentifier:@"PoleOptionController"];
    PoleDataView.isFrmEditView  = true;
    PoleDataView.isEditable     = true;
    PoleDataView.arrOptionsData = _arrOptionsData;
    PoleDataView.strNotes       = _strNotes;
    [self.navigationController pushViewController:PoleDataView animated:YES] ;
}
- (void)buttonPressed:(UIButton *)button {
    [self.view endEditing:YES];
    
    NSMutableArray *valuesArray = [[NSMutableArray alloc] init] ;
    
    NSLog(@"Button Index: %ld",(long)button.tag) ;
    
    selectedIndex = (int)button.tag ;
    
    tagValue = (int)button.tag - 1000 ;
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle: [[DataArray objectAtIndex:tagValue] valueForKey:@"Note"]
                                                                         message:nil
                                                                  preferredStyle:UIAlertControllerStyleActionSheet];
    
    valuesArray = [[DataArray objectAtIndex:tagValue] valueForKey:@"Values"] ;
    
    NSString *PostDataKey = [[DataArray objectAtIndex:tagValue] valueForKey:@"AttrKey"] ;
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:LocalizedString(LocalizedString(@"Cancel")) style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        // Cancel button tappped.
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    
    //
    //if ([[[DataArray objectAtIndex:tagValue] valueForKey:@"isRequire"] isEqualToString:@"1"]) {
    if ([[[DataArray objectAtIndex:tagValue] valueForKey:@"ispicklist"] isEqualToString:@"0"]) {
        /*UIAlertAction* online = [UIAlertAction
         actionWithTitle:LocalizedString(@"Other")
         style:UIAlertActionStyleDefault
         handler:^(UIAlertAction * action)
         {
         
         }];
         [online setValue:[UIColor blackColor] forKey:@"titleTextColor"];
         
         [actionSheet addAction:online];*/
        if ([[[self->DataArray objectAtIndex:self->tagValue] valueForKey:@"Selected"] isEqualToString:@"None"]) {
            [self addOtherData:PostDataKey
                  keyboardType:[[self->DataArray objectAtIndex:self->tagValue] valueForKey:@"type"]
                 selectedValue:@""
                       heading:[[self->DataArray objectAtIndex:self->tagValue] valueForKey:@"AttributeName"]];
        } else {
            [self addOtherData:PostDataKey
                  keyboardType:[[self->DataArray objectAtIndex:self->tagValue] valueForKey:@"type"]
                 selectedValue:[[self->DataArray objectAtIndex:self->tagValue] valueForKey:@"Selected"]
                       heading:[[self->DataArray objectAtIndex:self->tagValue] valueForKey:@"AttributeName"]];
        }
    }
    
    
    for (NSString *title in valuesArray) {
        
        if ([title isEqualToString:button.titleLabel.text])
        {
            [actionSheet addAction:[UIAlertAction actionWithTitle:title style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
                
                [button setTitle:title forState:UIControlStateNormal] ;
                
                [SelectedDataForPostDictionary setObject:title forKey:PostDataKey] ;
                
                [self dismissViewControllerAnimated:YES completion:^{
                }];
            }]];
        }
        else
        {
            [actionSheet addAction:[UIAlertAction actionWithTitle:title style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                
                [button setTitle:title forState:UIControlStateNormal] ;
                
                [SelectedDataForPostDictionary setObject:title forKey:PostDataKey] ;
                
                [self dismissViewControllerAnimated:YES completion:^{
                }];
            }]];
        }
        
    }
    
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
        [actionSheet setModalPresentationStyle:UIModalPresentationPopover];
        
        UIPopoverPresentationController *popPresenter = [actionSheet
                                                         popoverPresentationController];
        popPresenter.sourceView = button;
        popPresenter.sourceRect = button.bounds;
        
    }
    
    [self presentViewController:actionSheet animated:YES completion:nil];
    
}


-(void)addOtherData : (NSString *)Key keyboardType: (NSString *)keyBoardType selectedValue:(NSString *)selectedString heading:(NSString *)strHeading
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:[NSString stringWithFormat:@"Add %@",strHeading]
                                                                   message:nil
                                                            preferredStyle:UIAlertControllerStyleAlert];
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        // optionally configure the text field
        textField.placeholder = @"Enter Here" ;
        textField.text = selectedString;
        textField.borderStyle = UITextBorderStyleRoundedRect;
        if ([keyBoardType isEqualToString:@"1"]){
            textField.keyboardType = UIKeyboardTypeDefault;
        } else {
            textField.keyboardType = UIKeyboardTypeNumberPad;
        }
        textField.returnKeyType = UIReturnKeySend ;
        textField.delegate = self ;
        
    }];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:LocalizedString(@"ADD")
                                                       style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction *action) {
        UITextField *textField = [alert.textFields firstObject];
        
        StrRequestFor = Key ;
        
        StrNewAddedData = textField.text ;
        
        [self sendRequestForAddNewData];
        
        //---------------------------------------
        
    }];
    
    UIAlertAction *Cancel = [UIAlertAction actionWithTitle:LocalizedString(@"Cancel")
                                                     style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction *action) {
        
    }];
    
    
    
    [alert addAction:Cancel];
    
    [alert addAction:okAction];
    
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)sendRequestForAddNewData
{
    if ([globalObject checkInternetConnection]) {
        [globalObject showActivityIndicator];
        
        NSString *str = addotherdataURL ;
        NSString *urlString = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *url = [NSURL URLWithString:urlString];
        
        ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
        
        [request setRequestMethod:@"POST"] ;
        [request setDelegate:self] ;
        
        sqlObj = [[SQLManager alloc] init] ;
        
        [sqlObj createAndOpenDatabase] ;
        
        [request setPostValue:[[sqlObj getAllUserInfo] valueForKey:@"clientID"] forKey:@"client_id"] ;
        [request setPostValue:StrNewAddedData forKey:@"name"] ;
        [request setPostValue:[[sqlObj getAllUserInfo] valueForKey:@"userid"] forKey:@"user_id"] ;
        [request setPostValue:StrRequestFor forKey:@"tag"] ;
        [request setPostValue:[[sqlObj getAllUserInfo] valueForKey:@"MeasurmentUnit"] forKey:@"measurement_units"] ;
        
        //-------
        
        NSLog(@"%@",request.postBody);
        
        [request startAsynchronous] ;
    } else {
        [globalObject removeActivityIndicator];
    }
}

#pragma mark - Data Post

-(void)PostOtherData
{
    if ([globalObject checkInternetConnection]) {
        [globalObject showActivityIndicatorForSave];
        
        StrRequestFor = @"PostOtherData" ;
        
        NSString *str = EditSlcDetailURL ;
        NSString *urlString = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *url = [NSURL URLWithString:urlString];
        
        ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
        
        [request setRequestMethod:@"POST"] ;
        [request setDelegate:self] ;
        
        //------- Post Data
        
        NSError *error;
        
        NSData *jsonData2 = [NSJSONSerialization dataWithJSONObject:SelectedDataForPostDictionary options:NSJSONWritingPrettyPrinted error:&error];
        
        NSString *jsonString = [[NSString alloc] initWithData:jsonData2 encoding:NSUTF8StringEncoding];
        
        NSLog(@"jsonData as string:\n%@", jsonString);
        
        // ---------- Initialize SqlObject
        
        sqlObj = [[SQLManager alloc] init] ;
        
        [sqlObj createAndOpenDatabase] ;
        
        NSLog(@"%@",[[sqlObj getAllUserInfo] valueForKey:@"clientID"]);
        
        NSLog(@"%@",[[sqlObj getAllUserInfo] valueForKey:@"userid"]);
        
        NSLog(@"%@",[[sqlObj getAllUserInfo] valueForKey:@"MeasurmentUnit"]);
        
        [request setPostValue:strSelecctedDataSlcID forKey:@"id"] ;
        
        [request setPostValue:[[sqlObj getAllUserInfo] valueForKey:@"clientID"] forKey:@"client_id"] ;
        
        [request setPostValue:[[sqlObj getAllUserInfo] valueForKey:@"userid"] forKey:@"user_id"] ;
        
        [request setPostValue:[[sqlObj getAllUserInfo] valueForKey:@"MeasurmentUnit"] forKey:@"measurement_units"] ;
        
        [request setPostValue:strSlcID forKey:@"slc_id"] ;
        
        [request setPostValue:BtnMacAddress.titleLabel.text forKey:@"mac_address"] ;
        
        [request setPostValue:txtPoleID.text forKey:@"pole_id"] ;
        
        /*if (![_btnNodetype.titleLabel.text isEqualToString:@"Unknown"]) {
         [request setPostValue:_btnNodetype.titleLabel.text forKey:@"node_type"] ;
         } else {*/
        [request setPostValue:@"" forKey:@"node_type"] ;
        // }
        
        [request setPostValue:strSelectedTime forKey:@"date_of_installation"] ;
        
        [request setPostValue:strSelectedLatitude forKey:@"lat"] ;
        
        [request setPostValue:strSelectedLongitude forKey:@"lng"] ;
        
        [request setPostValue:@"IOS" forKey:@"source"] ;
        
        NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
        
        [request setPostValue:version forKey:@"Version"] ;
        
        if ([PoleImgURL isEqualToString:@""]) {
            NSData *imgData = [[NSData alloc] initWithData:UIImageJPEGRepresentation((PoleImg.image), 0.1)];
            
            NSLog(@"IMG : %@",imgData);
            
            [request setPostFormat:ASIMultipartFormDataPostFormat];
            
            [request addRequestHeader:@"Content-Type" value:@"multipart/form-data"];
            
            [request setData:imgData withFileName:@"abc.jpg" andContentType:@"multipart/form-data" forKey:@"pole_image"];
        }
        
        for (int i =0; i<_arrOptionsData.count; i++) {
            NSMutableDictionary *dictTmp = [[NSMutableDictionary alloc] initWithDictionary:_arrOptionsData[i]];
            NSArray *arrCount = [[NSArray alloc] init];
            arrCount = [dictTmp allKeys];
            for (int j =0; j<arrCount.count; j++) {
                if (![arrCount[j] isEqualToString:@"key"] && ![arrCount[j] isEqualToString:@"value"]) {
                    [request setPostValue:dictTmp[arrCount[j]] forKey:arrCount[j]];
                }
            }
        }
        
        [request setPostValue:_strNotes forKey:@"notes"];
        
        [request setPostValue:txtAddress.text forKey:@"address"];
        NSLog(@"%@",jsonString);
        NSLog(@"%@",request);
        
        if (!error) {
            [request setPostValue:jsonString forKey:@"Assets"];
        }
        
        [request setTimeOutSeconds:180] ;
        
        //-------
        
        [request startAsynchronous] ;
    } else {
        [globalObject removeActivityIndicator];
    }
}

-(void)requestFailed:(ASIHTTPRequest *)request
{   
    NSError *error = [request error];
    NSLog(@"error : %@" , error.localizedDescription);
    [globalObject removeActivityIndicator];
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:error.localizedDescription
                                  message:@""
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"OK"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action)
                                {
    }];
    
    [alert addAction:yesButton];
    
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)requestFinished:(ASIHTTPRequest *)request
{
    NSString *responseString = [request responseString];
    NSMutableDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:[responseString dataUsingEncoding:NSUTF8StringEncoding] options:0 error:nil];
    NSLog(@"Responce Dictionary : %@", responseDict);
    
    [globalObject removeActivityIndicator];
    
    
    if ([[responseDict objectForKey:@"status"] isEqualToString:@"success"])
    {
        if ([StrRequestFor isEqualToString:@"PostOtherData"])
        {
            
            UILabel  *myLabel= [[UILabel alloc] initWithFrame:CGRectMake([[UIScreen mainScreen] bounds].size.width /2 - 150 , self.view.frame.size.height/2 - 50 , 300 , 100)];
            
            myLabel.text = [responseDict objectForKey:@"msg"] ;
            
            myLabel.numberOfLines = 0 ;
            
            myLabel.textAlignment = NSTextAlignmentCenter ;
            
            myLabel.textColor = [UIColor whiteColor];
            
            myLabel.layer.masksToBounds = YES;
            
            myLabel.layer.cornerRadius = 8.0 ;
            
            myLabel.backgroundColor = [UIColor blackColor];
            
            [self.view addSubview:myLabel];
            
            myLabel.hidden = NO;
            
            myLabel.alpha = 0.8f;
            
            [UIView animateWithDuration:0.0 delay:1.0 options:0 animations:^{
                
                myLabel.alpha = 0.0f;
                
            } completion:^(BOOL finished) {
                
                myLabel.hidden = YES;
                
                [self.navigationController popViewControllerAnimated:YES];
            }];
        }
        else if ([StrRequestFor isEqualToString:@"Address"]) {
            txtAddress.text = [responseDict objectForKey:@"address"] ;
            annotation.title    = [responseDict objectForKey:@"shortaddress"];
            annotation.subtitle = @"";
            marker.title        = [responseDict objectForKey:@"shortaddress"];
            marker.snippet      = @"";
        }
        else if ([StrRequestFor isEqualToString:@"Login"]) {
            [sqlObj insertKeyToDatabase:@"user_logged_in" value:@"yes"];
            [sqlObj insertKeyToDatabase:@"token" value:[responseDict objectForKey:@"token"]];
            [self newAssetData] ;
        }
        else
        {
            NSMutableArray *mutableArray = [NSMutableArray arrayWithArray:DataArray];
            [mutableArray replaceObjectAtIndex:tagValue withObject:[responseDict objectForKey:@"data"]];
            DataArray = mutableArray;
            
            UIButton *button = (UIButton *)[self.view viewWithTag:selectedIndex] ;
            
            [button setTitle:StrNewAddedData forState:UIControlStateNormal] ;
            
            [SelectedDataForPostDictionary setObject:StrNewAddedData forKey:StrRequestFor] ;
            
            UIAlertController * alert=   [UIAlertController
                                          alertControllerWithTitle:[responseDict objectForKey:@"msg"]                                      message:@""
                                          preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* yesButton = [UIAlertAction
                                        actionWithTitle:@"OK"
                                        style:UIAlertActionStyleDefault
                                        handler:^(UIAlertAction * action)
                                        {
                
            }];
            
            [alert addAction:yesButton];
            
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
    else if ([[responseDict objectForKey:@"status"] isEqualToString:@"error"]) {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:[responseDict objectForKey:@"msg"]                                      message:@""
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
            if ([[responseDict objectForKey:@"msg"] isEqualToString:@"Address Not found" ]) {
                self.txtAddress.text = @"";
                [self.infoMapView removeFromSuperview];
                [self.txtAddress setUserInteractionEnabled:YES];
                [self.txtAddress becomeFirstResponder];
            } else {
                [self.txtAddress setUserInteractionEnabled:NO];
            }
        }];
        
        [alert addAction:yesButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    } else if ([[responseDict objectForKey:@"status"] isEqualToString:@"internal"]) {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:[responseDict objectForKey:@"msg"]                                      message:@""
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
            [BtnMacAddress setTitle:[[DataDictionary valueForKey:@"data"] valueForKey:@"mac_address"] forState:UIControlStateNormal] ;
            isFrmInternal = @"internal";
            strSlcID = [responseDict objectForKey:@"slc_id"];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                [self.BtnSLCID setTitle:strSlcID forState:UIControlStateNormal] ;
                
                [self checkmacaddressInternal];
            });
        }];
        
        [alert addAction:yesButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    } else if ([[responseDict objectForKey:@"status"] isEqualToString:@"external"]) {
        isFrmInternal = @"external";
        strSlcID = [responseDict objectForKey:@"slc_id"];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [self.BtnSLCID setTitle:strSlcID forState:UIControlStateNormal] ;
        });
    } else if ([[responseDict objectForKey:@"status"] isEqualToString:@"0"]) {
        
        NSString *strMsg = [responseDict objectForKey:@"msg"];
        
        if (strMsg == nil || [strMsg  isEqualToString: @""]) {
            strMsg = @"Oops! Something went wrong. Try again later";
        }
        
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:LocalizedString(strMsg)
                                      message:@""
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
            sqlObj = [[SQLManager alloc] init] ;
            [sqlObj createAndOpenDatabase] ;
            
            [sqlObj DeleteData] ;
            
            UIStoryboard *story = [UIStoryboard storyboardWithName:@"SecurityCodeViewController" bundle:nil];
            
            SecurityCodeViewController *SecurityCodeViewController = [story instantiateViewControllerWithIdentifier:@"SecurityCodeViewController"];
            
            UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:SecurityCodeViewController];
            
            [[UIApplication sharedApplication].keyWindow setRootViewController:navController];
        }];
        
        [alert addAction:yesButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    }  else {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:LocalizedString(@"Oops! Something went wrong. Try again later")
                                      message:@""
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
        }];
        [alert addAction:yesButton];
        [self presentViewController:alert animated:YES completion:nil];
    }
}


#pragma mark -  Button Action

- (IBAction)SelectDateButton:(id)sender
{
    [self eventof:@"SLCDetailEditDate"];
    
    datePicker = [[UIDatePicker alloc] init];
    datePicker.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    datePicker.datePickerMode = UIDatePickerModeDate ;
    
    if (@available(iOS 13.4, *)) {
        datePicker.preferredDatePickerStyle = UIDatePickerStyleWheels;
    }
    
    [datePicker setMaximumDate:[NSDate date]];
    
    datePicker.frame =  CGRectMake(0.0, self.view.frame.size.height - 250, self.view.frame.size.width, 250);
    datePicker.backgroundColor = [UIColor whiteColor];
    
    NSDateFormatter *formatter=[[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"M-d-yyyy"];
    datePicker.date = [formatter dateFromString:strSelectedTime] ;
    
    //  datePicker.minimumDate = [NSDate date];
    
    //*******************
    
    toolbar= [[UIToolbar alloc] initWithFrame:CGRectMake(0,self.view.frame.size.height - 294,self.view.frame.size.width,44)];
    toolbar.barStyle = UIBarStyleBlackOpaque;
    
    
    UIButton* SendButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 0 , 30)];
    [SendButton setTitle:LocalizedString(@"Set") forState:UIControlStateNormal];
    [SendButton addTarget:self action:@selector(SetButton) forControlEvents:UIControlEventTouchDown];
    
    UIBarButtonItem *SendB =[[UIBarButtonItem alloc]initWithCustomView:SendButton];
    
    UIButton* CancelButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 0 , 30)];
    [CancelButton setTitle:LocalizedString(@"Cancel") forState:UIControlStateNormal] ;
    [CancelButton addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchDown];
    UIBarButtonItem *CancelB =[[UIBarButtonItem alloc]initWithCustomView:CancelButton];
    
    [toolbar setItems:[NSArray arrayWithObjects: CancelB,SendB, nil]];
    
    [self.view addSubview:toolbar];
    [self.view addSubview:datePicker];
}

-(void)dismiss{
    
    [datePicker removeFromSuperview];
    [toolbar removeFromSuperview];
    
}

-(void)SetButton{
    
    [datePicker removeFromSuperview];
    [toolbar removeFromSuperview];
    
    NSDateFormatter *formatter=[[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"M-d-yyyy"];
    strSelectedTime = [NSString stringWithFormat:@"%@",[formatter stringFromDate:datePicker.date]] ;
    
    [btnSelectDate setTitle:strSelectedTime forState:UIControlStateNormal] ;
    
}

- (IBAction)SaveButton:(id)sender
{
    [self eventof:@"SLCDetailEditSave"];
    for (UIView *i in _containerView.subviews)  {
        if([i isKindOfClass:[UIButton class]]) {
            UIButton *newbtn = (UIButton *)i;
            if(newbtn.tag >= 1000) {
                int newTagValue = (int)newbtn.tag - 1000;
                NSDictionary *newDataDict = [DataArray objectAtIndex:newTagValue];
                if ([[newDataDict valueForKey:@"isRequire"] isEqualToString:@"1"] && [newbtn.titleLabel.text isEqualToString:@"None"])  {
                    NSString *strMsg = [NSString stringWithFormat:@"%@ is required", [newDataDict valueForKey:@"AttributeName"]];
                    [globalObject showAlertWithTitle:strMsg message:@""];
                    return;
                    break;
                }
            }
        }
    }
    
    if ([uidCheck isEqualToString:@""]) {
        NSString *strMACID = [[self->sqlObj getAllUserInfo] valueForKey:@"ScanPH"];
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:[NSString stringWithFormat:@"%@ %@",LocalizedString(@"Please enter "), LocalizedString(strMACID)]
                                      message:@""
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
        }];
        [alert addAction:yesButton];
        [self presentViewController:alert animated:YES completion:nil];
        return ;
    } else if ([strSlcID isEqualToString:@""]) {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:LocalizedString(@"Please enter SLC ID")
                                      message:@""
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
        }];
        [alert addAction:yesButton];
        [self presentViewController:alert animated:YES completion:nil];
        return ;
    } else if ([txtAddress.text isEqualToString:@""]) {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:LocalizedString(@"Please enter address")
                                      message:@""
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
            [txtAddress becomeFirstResponder];
        }];
        [alert addAction:yesButton];
        [self presentViewController:alert animated:YES completion:nil];
        return ;
    }
    
    [self.view endEditing:YES];
    
    [infoMapView removeFromSuperview];
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:LocalizedString(@"Please confirm you would like to make these changes")                                    message:@""
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"Yes"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action)
                                {
        [self PostOtherData] ;
    }];
    
    UIAlertAction* NoButton = [UIAlertAction
                               actionWithTitle:@"No"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
    }];
    
    [alert addAction:yesButton];
    [alert addAction:NoButton];
    
    [self presentViewController:alert animated:YES completion:nil];
    
    /* if([txtMacAddress.text isEqualToString:@""])
     {
     UIAlertController * alert=   [UIAlertController
     alertControllerWithTitle:@"Please Enter Valid MAC ID"                                    message:@""
     preferredStyle:UIAlertControllerStyleAlert];
     
     UIAlertAction* yesButton = [UIAlertAction
     actionWithTitle:@"OK"
     style:UIAlertActionStyleDefault
     handler:^(UIAlertAction * action)
     {
     
     }];
     
     [alert addAction:yesButton];
     
     [self presentViewController:alert animated:YES completion:nil];
     }
     else
     {
     if([txtAddress.text isEqualToString:@""])
     {
     UIAlertController * alert=   [UIAlertController
     alertControllerWithTitle:@"Please Enter Valid Address"                                    message:@""
     preferredStyle:UIAlertControllerStyleAlert];
     
     UIAlertAction* yesButton = [UIAlertAction
     actionWithTitle:@"OK"
     style:UIAlertActionStyleDefault
     handler:^(UIAlertAction * action)
     {
     
     }];
     
     [alert addAction:yesButton];
     
     [self presentViewController:alert animated:YES completion:nil];
     }
     else
     {
     [self PostOtherData] ;
     }
     }*/
}

-(void)AlertAction {
    [self eventof:@"SLCDetailEditMap"];
    if ((currentMapTypeIndex != 6) && (currentMapTypeIndex > 2)) {
        mapView.hidden      = true;
        googlMapView.hidden = false;
        
        infoMapView.frame = CGRectMake(50, 100, [UIScreen mainScreen].bounds.size.width - 100, [UIScreen mainScreen].bounds.size.height - 313);
        
        infoMapView.layer.borderWidth = 4.0 ;
        infoMapView.layer.borderColor = [UIColor colorWithRed:(0/255.f) green:(114/255.f) blue:(188/255.f) alpha:1.0f].CGColor ;
        infoMapView.layer.cornerRadius = 8.0 ;
        
        cameraPosition = [GMSCameraPosition cameraWithLatitude:[strLatitude doubleValue]
                                                     longitude:[strLongitude doubleValue]
                                                          zoom:16];
        googlMapView = [GMSMapView mapWithFrame:CGRectMake(0, 0, infoMapView.frame.size.width, infoMapView.frame.size.height)                                                camera:cameraPosition];
        [infoMapView addSubview:googlMapView];
        googlMapView.delegate = self;
        
        marker = [[GMSMarker alloc] init];
        marker.position = CLLocationCoordinate2DMake([strLatitude doubleValue], [strLongitude doubleValue]);
        marker.title = [NSString stringWithFormat:@"%@ : %@",LocalizedString(@"SLC ID"),strSlcID];
        marker.snippet =[NSString stringWithFormat:@"%@ : %@",LocalizedString(@"POLE ID"), strPoleID];
        marker.map = googlMapView;
        [marker setDraggable:true];
        marker.icon = [UIImage imageNamed:@"pin"];
        
        [self.view addSubview:infoMapView];
        [self mapSelectionView];
        [infoMapView bringSubviewToFront:btnOK];
    } else {
        mapView.hidden      = false;
        googlMapView.hidden = true;
        [mapView removeAnnotations:mapView.annotations];
        
        infoMapView.frame = CGRectMake(50, 100, [UIScreen mainScreen].bounds.size.width - 100, [UIScreen mainScreen].bounds.size.height - 313);
        
        infoMapView.layer.borderWidth = 4.0 ;
        infoMapView.layer.borderColor = [UIColor colorWithRed:(0/255.f) green:(114/255.f) blue:(188/255.f) alpha:1.0f].CGColor ;
        infoMapView.layer.cornerRadius = 8.0 ;
        
        mapView.layer.borderWidth = 4.0 ;
        mapView.layer.cornerRadius = 8.0 ;
        
        strSelectedLatitude = strLatitude ;
        strSelectedLongitude = strLongitude ;
        
        CLLocationCoordinate2D location = CLLocationCoordinate2DMake([strLatitude floatValue], [strLongitude floatValue]) ;
        
        MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(location, 800, 800);
        [mapView setRegion:[mapView regionThatFits:region] animated:YES];
        
        //        annotation.title = [NSString stringWithFormat:@"%@ : %@",LocalizedString(@"SLC ID"), strSlcID] ;
        annotation.coordinate = location ;
        
        //        annotation.title = [NSString stringWithFormat:@"%@ : %@",LocalizedString(@"SLC ID"), strSlcID] ;
        
        //annotation.subtitle = [NSString stringWithFormat:@"%@ : %@",LocalizedString(@"POLE ID"), strPoleID] ;
        
        
        [mapView addAnnotation:annotation ];
        
        [self.view addSubview:infoMapView];
        
        [self mapSelectionView];
    }
}

#pragma mark -  MapView Methods

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation
{
    MKPinAnnotationView *pav = nil;
    
    static NSString *reuseId = @"pin";
    pav = (MKPinAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:reuseId];
    if (pav == nil)
    {
        pav = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:reuseId];
        [pav setPinTintColor:[UIColor redColor]];
        UIImageView *iconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Pole_Blue.png"]];
        pav.leftCalloutAccessoryView = iconView;
        pav.canShowCallout = YES;
        pav.animatesDrop = YES ;
        pav.selected = YES ;
        pav.draggable = YES ;
        
    }
    else
    {
        pav.annotation = annotation;
    }
    
    return pav;
}
#pragma mark - Map Delegate

- (MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id<MKOverlay>)overlay {
    return tileRender;
}
- (IBAction)MapOkButton:(id)sender
{
    [self eventof:@"SLCDetailEditMapOK"];
    [infoMapView removeFromSuperview];
    
    [self AddressDataPost] ;
    
}
#pragma mark - Data Post For Address

-(void)AddressDataPost
{
    if ([globalObject checkInternetConnection]) {
        [globalObject showActivityIndicator];
        
        StrRequestFor = @"Address" ;
        
        NSString *str =  addressURL;
        NSString *urlString = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *url = [NSURL URLWithString:urlString];
        
        [self.txtAddress setUserInteractionEnabled:NO];
        
        ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
        
        [request setRequestMethod:@"POST"] ;
        [request setDelegate:self] ;
        
        //------- Post Data
        
        [request setPostValue:strSelectedLatitude forKey:@"lat"] ;
        
        [request setPostValue:strSelectedLongitude forKey:@"lng"] ;
        
        [request setPostValue:[[sqlObj getAllUserInfo] valueForKey:@"clientID"] forKey:@"client_id"] ;
        
        [request setPostValue:[[sqlObj getAllUserInfo] valueForKey:@"userid"] forKey:@"user_id"] ;
        
        //-------
        
        [request startAsynchronous] ;
    } else {
        [globalObject removeActivityIndicator];
    }
}

- (void)mapView:(MKMapView *)mapView
 annotationView:(MKAnnotationView *)annotationView
didChangeDragState:(MKAnnotationViewDragState)newState
   fromOldState:(MKAnnotationViewDragState)oldState
{
    if (newState == MKAnnotationViewDragStateEnding)
    {
        CLLocationCoordinate2D droppedAt = annotationView.annotation.coordinate;
        NSLog(@"Pin dropped at %f,%f", droppedAt.latitude, droppedAt.longitude);
        
        strSelectedLatitude = [NSString stringWithFormat:@"%f",droppedAt.latitude];
        
        strSelectedLongitude = [NSString stringWithFormat:@"%f",droppedAt.longitude];
        
        [self AddressDataPost] ;
    }
}

- (void)mapView:(GMSMapView *)mapView didEndDraggingMarker:(GMSMarker *)marker {
    strSelectedLatitude = [NSString stringWithFormat:@"%f",marker.position.latitude];
    strSelectedLongitude = [NSString stringWithFormat:@"%f",marker.position.longitude];
    NSLog(@"%f",marker.position.latitude);
    NSLog(@"%f",marker.position.longitude);
    [self AddressDataPost] ;
}
-(void)AlertForLogin
{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Login"
                                                                   message:nil
                                                            preferredStyle:UIAlertControllerStyleAlert];
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        // optionally configure the text field
        
        textField.placeholder = @"Username" ;
        textField.borderStyle = UITextBorderStyleRoundedRect ;
        textField.font = [UIFont systemFontOfSize:16.0] ;
        textField.keyboardType = UIKeyboardTypeAlphabet;
        textField.returnKeyType = UIReturnKeySend ;
        textField.delegate = self ;
        
    }];
    
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textFieldPass) {
        // optionally configure the text field
        
        textFieldPass.placeholder = @"Password" ;
        textFieldPass.secureTextEntry = YES;
        textFieldPass.borderStyle = UITextBorderStyleRoundedRect ;
        textFieldPass.font = [UIFont systemFontOfSize:16.0] ;
        textFieldPass.keyboardType = UIKeyboardTypeAlphabet;
        textFieldPass.returnKeyType = UIReturnKeySend ;
        textFieldPass.delegate = self ;
        
    }];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Login"
                                                       style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction *action) {
        NSArray * textfields = alert.textFields;
        UITextField * namefield = textfields[0];
        UITextField * passfield = textfields[1];
        
        strUsername = namefield.text ;
        strPassword = passfield.text ;
        
        [self loginAction];
        
    }];
    
    
    [alert addAction:okAction];
    
    [self presentViewController:alert animated:YES completion:nil];
    
    
}

-(void)loginAction
{
    if ([strUsername isEqualToString:@""] || [strPassword isEqualToString:@""])
    {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:@"Please Enter Valid Username and Password."                                     message:@""
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
            [self AlertForLogin] ;
            
        }];
        
        [alert addAction:yesButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
    else
    {
        [self loginRequest];
    }
}


-(IBAction)btnClose_Click:(UIButton *)sender {
    [PoleImageView removeFromSuperview];
}

#pragma mark - Data Post

-(void)loginRequest
{
    if ([globalObject checkInternetConnection]) {
        [globalObject showActivityIndicator];
        
        StrRequestFor = @"Login" ;
        
        NSString *str = userLoginURL ;
        NSString *urlString = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *url = [NSURL URLWithString:urlString];
        
        ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
        
        [request setRequestMethod:@"POST"] ;
        [request setDelegate:self] ;
        
        //------- Post Data
        
        // ---------- Initialize SqlObject
        
        sqlObj = [[SQLManager alloc] init] ;
        
        [sqlObj createAndOpenDatabase] ;
        
        NSLog(@"%@",[[sqlObj getAllUserInfo] valueForKey:@"clientID"]);
        
        NSLog(@"%@",[[sqlObj getAllUserInfo] valueForKey:@"userid"]);
        
        NSLog(@"%@",[[UIDevice currentDevice] identifierForVendor]);
        
        NSLog(@"%f",locationManager.location.coordinate.latitude);
        
        NSLog(@"%f",locationManager.location.coordinate.longitude);
        
        // 65D78ADF-32BC-4A73-B7BC-C9ADBC4C9F55
        
        NSString *strLat = [NSString stringWithFormat:@"%f",locationManager.location.coordinate.latitude];
        
        NSString *strLong = [NSString stringWithFormat:@"%f",locationManager.location.coordinate.longitude];
        
        [request setPostValue:[[sqlObj getAllUserInfo] valueForKey:@"clientID"] forKey:@"client_id"] ;
        
        [request setPostValue:[[sqlObj getAllUserInfo] valueForKey:@"userid"] forKey:@"userid"] ;
        
        [request setPostValue:strUsername forKey:@"username"] ;
        
        [request setPostValue:strPassword forKey:@"password"] ;
        
        [request setPostValue:strLat forKey:@"lat"] ;
        
        [request setPostValue:strLong forKey:@"long"] ;
        
        // [request setPostValue:[[[UIDevice currentDevice] identifierForVendor] UUIDString] forKey:@"DeviceID"] ;
        
        [request setPostValue:@"IOS" forKey:@"source"] ;
        
        [request setTimeOutSeconds:180] ;
        
        //-------
        
        [request startAsynchronous] ;
    } else {
        [globalObject removeActivityIndicator];
    }
}

-(void)newAssetData
{
    
    [SelectedDataForPostDictionary removeAllObjects];
    
    DataArray = [[NSMutableArray alloc] init] ;
    
    SelectedDataForPostDictionary = [[NSMutableDictionary alloc] init] ;
    
    [self getSLCData] ;
    
    for (UIView *i in _containerView.subviews)
    {
        if([i isKindOfClass:[UILabel class]])
        {
            UILabel *newLbl = (UILabel *)i;
            if(newLbl.tag == 2000)
            {
                newLbl.text = @"" ;
                [newLbl removeFromSuperview];
            }
        }
        else if([i isKindOfClass:[UIButton class]])
        {
            UIButton *newbtn = (UIButton *)i;
            if(newbtn.tag >= 1000)
            {
                [newbtn setTitle:@"" forState:UIControlStateNormal ];
                [newbtn removeFromSuperview];
            }
        }
    }
}

#pragma mark - TextField Delegate

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    if (isFrmMacButton) {
        NSRange lowercaseCharRange = [string rangeOfCharacterFromSet:[NSCharacterSet lowercaseLetterCharacterSet]];
        
        if (lowercaseCharRange.location != NSNotFound) {
            textField.text = [textField.text stringByReplacingCharactersInRange:range
                                                                     withString:[string uppercaseString]];
            return NO;
        }
        
        NSCharacterSet *numbersOnly = [NSCharacterSet characterSetWithCharactersInString:@"0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"];
        NSCharacterSet *characterSetFromTextField = [NSCharacterSet characterSetWithCharactersInString:string];
        
        BOOL stringIsValid = [numbersOnly isSupersetOfSet:characterSetFromTextField];
        
        return stringIsValid;
    } else {
        if([textField.text length]==0)
        {
            if([string isEqualToString:@" "])
            {
                return NO;
            }
        }
        return YES;
    }
}

-(BOOL)textFieldShouldReturn:(UITextField*)textField
{
    [textField resignFirstResponder];
    
    return YES;
}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    if (textField == txtPoleID) {
        if (textField.text == LocalizedString(@"None")) {
            txtPoleID.placeholder = @"";
            txtPoleID.text = @"";
        }
    }
    return  true;
}
- (void)textViewDidBeginEditing:(UITextView *)textView
{
    if(textView == txtAddress)
    {
        [self eventof:@"SLCDetailEditAddress"];
        [self animateTextView:textView up:YES];
    }
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    if(textView == txtAddress)
    {
        [self animateTextView:textView up:NO];
    }
}

-(void)animateTextView:(UITextView*)textview up:(BOOL)up
{
    if(textview == txtAddress)
    {
        const int movementDistance = -180; // tweak as needed
        const float movementDuration = 0.3f; // tweak as needed
        
        int movement = (up ? movementDistance : -movementDistance);
        
        [UIView beginAnimations: @"animateTextField" context: nil];
        [UIView setAnimationBeginsFromCurrentState: YES];
        [UIView setAnimationDuration: movementDuration];
        self.view.frame = CGRectOffset(self.view.frame, 0, movement);
        [UIView commitAnimations];
    }
    
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    
    if([text isEqualToString:@"\n"])
    {
        [self.view endEditing:YES];
    }
    
    
    return YES;
}


#pragma mark - checkmacaddressInternal
-(void)checkmacaddressInternal {
    if ([globalObject checkInternetConnection]) {
        [globalObject showActivityIndicator];
        
        StrRequestFor = @"checkmacInternal" ;
        
        NSString *str =  checkInternalUniqueEDITURL;
        NSString *urlString = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *url = [NSURL URLWithString:urlString];
        
        ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
        
        [request setRequestMethod:@"POST"] ;
        [request setDelegate:self] ;
        
        sqlObj = [[SQLManager alloc] init] ;
        
        [sqlObj createAndOpenDatabase] ;
        
        [request setPostValue:[[sqlObj getAllUserInfo] valueForKey:@"clientID"] forKey:@"client_id"] ;
        
        [request setPostValue:[[sqlObj getAllUserInfo] valueForKey:@"userid"] forKey:@"user_id"] ;
        
        [request setPostValue:BtnMacAddress.titleLabel.text forKey:@"mac_address"] ;
        
        [request setPostValue:@"IOS" forKey:@"source"];
        
        [request setTimeOutSeconds:180] ;
        
        [request startAsynchronous] ;
    } else {
        [globalObject removeActivityIndicator];
    }
}

#pragma mark -  New Pole Image Action And Methods

- (IBAction)ChangeBtnBtn:(id)sender
{
    [self eventof:@"SLCDetailEditImage"];
    
    [PoleImg setImageWithURL:[NSURL URLWithString:PoleImgURL] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
        if (image == nil) {
            NSString *strSelectedlang = [[NSUserDefaults standardUserDefaults] stringForKey:@"appLanguage"];
            
            if ([strSelectedlang isEqualToString:@"en"]) {
                [PoleImg setImage: [UIImage imageNamed:@"no_image"]];
            } else if ([strSelectedlang isEqualToString:@"es"]) {
                [PoleImg setImage: [UIImage imageNamed:@"no_image_es"]];
            } else {
                [PoleImg setImage: [UIImage imageNamed:@"no_images_pt"]];
            }
            
        }
    }];
    
    PoleImageView.frame = CGRectMake([UIScreen mainScreen].bounds.size.width / 2 - 150 , 20 , 300, 500 ) ;
    
    PoleImageView.layer.borderWidth = 4.0 ;
    PoleImageView.layer.borderColor = [UIColor colorWithRed:(0/255.f) green:(114/255.f) blue:(188/255.f) alpha:1.0f].CGColor ;
    
    [self.view addSubview:PoleImageView];
}

-(void)ChooseImage
{
    
    
}

- (IBAction)ChooseImgBtn:(id)sender
{
    ImageSelectionNavigation = @"YES";
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle: @"Select Pole Image"
                                                                         message:nil
                                                                  preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:LocalizedString(@"Cancel") style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        // Cancel button tappped.
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    
    imagePicker.delegate = self;
    
    UIAlertAction* gallery = [UIAlertAction
                              actionWithTitle:LocalizedString(@"Photo Library")
                              style:UIAlertActionStyleDefault
                              handler:^(UIAlertAction * action)
                              {
        
        imagePicker.sourceType =UIImagePickerControllerSourceTypePhotoLibrary;
        
        [self presentViewController:imagePicker animated:YES completion:nil];
    }];
    
    
    [actionSheet addAction:gallery];
    
    UIAlertAction* Camera = [UIAlertAction
                             actionWithTitle:LocalizedString(@"Camera")
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
        imagePicker.sourceType =UIImagePickerControllerSourceTypeCamera;
        imagePicker.allowsEditing = true;
        [self presentViewController:imagePicker animated:YES completion:nil];
    }];
    
    [actionSheet addAction:Camera];
    
    
    
    
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
        [actionSheet setModalPresentationStyle:UIModalPresentationPopover];
        
        UIPopoverPresentationController *popPresenter = [actionSheet
                                                         popoverPresentationController];
        popPresenter.sourceView = _ChooseImgBtn;
        popPresenter.sourceRect = _ChooseImgBtn.bounds;
        
    }
    
    [self presentViewController:actionSheet animated:YES completion:nil];
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void) imagePickerController:(UIImagePickerController *)picker
         didFinishPickingImage:(UIImage *)image
                   editingInfo:(NSDictionary *)editingInfo
{
//    NSString *mediaType = [editingInfo valueForKey:UIImagePickerControllerMediaType];
    
    UIImage *photoTaken = [editingInfo objectForKey:@"UIImagePickerControllerOriginalImage"];

    //Save Photo to library only if it wasnt already saved i.e. its just been taken
    if (picker.sourceType == UIImagePickerControllerSourceTypeCamera) {
        UIImageWriteToSavedPhotosAlbum(photoTaken, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);
    }
    
    SelectNewImage = @"YES";
    
    PoleImg.image = image;
    
    PoleImgURL = @"";
    
    [picker dismissViewControllerAnimated:YES completion:nil];
    
    //if([mediaType isEqualToString:(NSString*)kUTTypeImage]) {
            

//
       // }
}
- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo {
    UIAlertView *alert;
    //NSLog(@"Image:%@", image);
    if (error) {
        alert = [[UIAlertView alloc] initWithTitle:@"Error!"
                                           message:[error localizedDescription]
                                          delegate:nil
                                 cancelButtonTitle:@"OK"
                                 otherButtonTitles:nil];
        [alert show];
    }

}

- (IBAction)SaveImgBtn:(id)sender
{
    [self eventof:@"SLCDetailEditSaveImage"];
    [PoleImageView removeFromSuperview];
}

//***********************************************

- (IBAction)MacIDButton:(id)sender
{
    [self eventof:@"SLCDetailEditUID"];
    isFrmMacButton = true;
    // ---------- Initialize SqlObject
    if ([isFrmInternal isEqualToString:@"internal"]) {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:LocalizedString(@"UID for internal SLC cannot be edited")                                      message:@""
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {  }];
        [alert addAction:yesButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    } else {
        sqlObj = [[SQLManager alloc] init] ;
        
        [sqlObj createAndOpenDatabase] ;
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:[[sqlObj getAllUserInfo] valueForKey:@"ScanLBL"]
                                                                       message:nil
                                                                preferredStyle:UIAlertControllerStyleAlert];
        [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
            // optionally configure the text field
            textField.text = BtnMacAddress.titleLabel.text ;
            textField.placeholder = [[sqlObj getAllUserInfo] valueForKey:@"ScanPH"] ;
            textField.borderStyle = UITextBorderStyleRoundedRect ;
            textField.autocapitalizationType = UITextAutocapitalizationTypeAllCharacters;
            textField.font = [UIFont systemFontOfSize:16.0] ;
            textField.keyboardType = UIKeyboardTypeAlphabet;
            textField.returnKeyType = UIReturnKeySend ;
            textField.delegate = self ;
        }];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Confirm"
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction *action) {
            UITextField *textField = [alert.textFields firstObject];
            
            NSLog(@"%@",textField.text);
            
            isFrmMacButton = false;
            
            self->uidCheck = textField.text;
            
            [self->BtnMacAddress setTitle:textField.text forState:UIControlStateNormal] ;
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                [self checkmacaddressInternal];
            });
            
        }];
        
        UIAlertAction *CancelAction = [UIAlertAction actionWithTitle:LocalizedString(@"Cancel")
                                                               style:UIAlertActionStyleDefault
                                                             handler:^(UIAlertAction *action) {
            isFrmMacButton = false;
            
        }];
        
        UIAlertAction *ScanAction = [UIAlertAction actionWithTitle:[NSString stringWithFormat:@"%@",LocalizedString(@"Scan")]
                                                             style:UIAlertActionStyleDefault
                                                           handler:^(UIAlertAction *action) {
            UITextField *textField = [alert.textFields firstObject];
            
            [self appDelegate].strEditMacID = textField.text ;
            
            UIStoryboard *story = [UIStoryboard storyboardWithName:@"EditScanViewController" bundle:nil];
            
            EditScanViewController *EditScanViewController = [story instantiateViewControllerWithIdentifier:@"EditScanViewController"];
            
            EditScanViewController.isFrmMacID = @"true";
            
            EditScanViewController.strMacAddress = BtnMacAddress.titleLabel.text ;
            
            [self.navigationController pushViewController:EditScanViewController animated:YES];
            
        }];
        
        [alert addAction:okAction];
        [alert addAction:ScanAction];
        [alert addAction:CancelAction];
        
        
        [self presentViewController:alert animated:YES completion:nil];
    }
}

- (IBAction)SLCIDButton:(id)sender
{
    [self eventof:@"SLCDetailEditSLCID"];
    isFrmMacButton = false;
    if ([isFrmInternal isEqualToString:@"internal"]) {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:LocalizedString(@"SLC ID for internal SLC cannot be edited")                                      message:@""
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {  }];
        [alert addAction:yesButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    } else {
        sqlObj = [[SQLManager alloc] init] ;
        
        [sqlObj createAndOpenDatabase] ;
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:LocalizedString(@"SLC ID")
                                                                       message:nil
                                                                preferredStyle:UIAlertControllerStyleAlert];
        [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
            // optionally configure the text field
            textField.text = strSlcID ;
            textField.placeholder = LocalizedString(@"SLC ID");//@"SLC ID" ;
            textField.borderStyle = UITextBorderStyleRoundedRect ;
            textField.font = [UIFont systemFontOfSize:16.0] ;
            textField.keyboardType = UIKeyboardTypeAlphabet;
            textField.returnKeyType = UIReturnKeySend ;
            textField.delegate = self ;
            
        }];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Confirm"
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction *action) {
            UITextField *textField = [alert.textFields firstObject];
            strSlcID = textField.text;
            [BtnSLCID setTitle:textField.text forState:UIControlStateNormal] ;
        }];
        
        UIAlertAction *CancelAction = [UIAlertAction actionWithTitle:LocalizedString(@"Cancel")
                                                               style:UIAlertActionStyleDefault
                                                             handler:^(UIAlertAction *action) {
            
        }];
        
        UIAlertAction *ScanAction = [UIAlertAction actionWithTitle:[NSString stringWithFormat:@"%@",LocalizedString(@"Scan")]
                                                             style:UIAlertActionStyleDefault
                                                           handler:^(UIAlertAction *action) {
            UITextField *textField = [alert.textFields firstObject];
            
            [self appDelegate].strEditSLCID = textField.text ;
            
            UIStoryboard *story = [UIStoryboard storyboardWithName:@"EditScanViewController" bundle:nil];
            
            EditScanViewController *EditScanViewController = [story instantiateViewControllerWithIdentifier:@"EditScanViewController"];
            
            EditScanViewController.isFrmMacID = @"false";
            
            EditScanViewController.strSLCAddress = strSlcID ;
            
            [self.navigationController pushViewController:EditScanViewController animated:YES];
        }];
        
        [alert addAction:okAction];
        [alert addAction:ScanAction];
        [alert addAction:CancelAction];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)eventof:(NSString *)strEventName {
    dispatch_async(dispatch_get_main_queue(), ^{
    NSString *strClientID = [[sqlObj getAllUserInfo] valueForKey:@"clientID"];
    NSString *strUserID = [[sqlObj getAllUserInfo] valueForKey:@"userid"];
    
    [FIRAnalytics logEventWithName:kFIREventSelectContent
                        parameters:@{
                                     kFIRParameterItemName:[NSString stringWithFormat:@"%@_%@_%@",strClientID, strUserID, strEventName],
                                     }];
    });
}

@end
