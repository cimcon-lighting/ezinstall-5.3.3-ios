//
//  EditScanViewController.h
//  SLCScanner
//
//  Created by INT MAC 2015 on 25/06/18.
//  Copyright © 2018 INT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ZXingObjC/ZXingObjC.h>
#import "SQLManager.h"

#import "GlobalAssistant.h"

@interface EditScanViewController : UIViewController <ZXCaptureDelegate>

@property (strong , nonatomic) GlobalAssistant *globalObject ;

@property (strong, nonatomic) NSString *isFrmMacID ;

@property (strong, nonatomic) NSString *strMacAddress ;

@property (strong, nonatomic) NSString *strSLCAddress ;

//-------- SqlManager Obj -------//

@property (strong , nonatomic) SQLManager *sqlObj ;

@end
