//
//  EditDataViewController.m
//  SLCScanner
//
//  Created by INT MAC 2015 on 12/03/18.
//  Copyright © 2018 INT. All rights reserved.
//

#import "EditDataViewController.h"

#import "API.h"
#import "AppDelegate.h"
#import "LocalizeHelper.h"
#import "UIImageView+WebCache.h"
#import "EditDataSaveViewController.h"
#import "PoleOptionController.h"
#import "SecurityCodeViewController.h"

#import <GoogleMaps/GoogleMaps.h>
#import <FirebaseAnalytics/FirebaseAnalytics.h>

@interface EditDataViewController ()
{
    NSString *strLatitude ;
    NSString *strLongitude ;
    
    NSString *strSlcID ;
    NSString *strPoleID ;
    NSString *strNotes;
    
    int     currentMapTypeIndex;
    
    AppDelegate             *appDelegate;
    
    IBOutlet UIButton       *btnImgOk;
    IBOutlet UIButton       *btnChooseImg;
    IBOutlet UIButton       *btnOptions;
    
    IBOutlet UILabel        *lblNotes;
    
    IBOutlet GMSMapView     *googlMapView;
    
    MKTileOverlayRenderer   *tileRender;
    MKTileOverlay           *overLay;
    MKMapRect               boundingMapRect;
    
    IBOutlet UIButton *btnOK;
    
    NSMutableArray          *arrOptionsData;
    
    IBOutlet NSLayoutConstraint *constPoldIDlbl;
    IBOutlet NSLayoutConstraint *constTopViewHeight;
}
@end

@implementation EditDataViewController
@synthesize strSelecctedDataSlcID ;
@synthesize sqlObj, globalObject, ShowForEditView ;
@synthesize mapView, infoMapView, lblTime, lblTimeData, DatalblSlcID, DatallblPoleID, btnEdit ;
@synthesize lblAddress, txtAddress ;
@synthesize PoleImageView, PoleImg, DatalblMacID , BottomViewObj;

@synthesize strEditView ;

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self eventof:@"SLCDetails"];
    
    arrOptionsData = [[NSMutableArray alloc] init];
    
    mapView.hidden      = true;
    googlMapView.hidden = true;
    
    self.title = @"POLE DETAILS" ;
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    // --------- Initialize GlobalAssistant
    
    globalObject = [[GlobalAssistant alloc] init] ;
    
    // ---------- Initialize SqlObject
    
    sqlObj = [[SQLManager alloc] init] ;
    
    [sqlObj createAndOpenDatabase] ;
    
    [globalObject buttonWithCornerRadious:24.0 btnView:btnImgOk];
    [globalObject buttonWithCornerRadious:24.0 btnView:btnOK];
    [globalObject buttonWithCornerRadious:22.0 btnView:btnEdit];
    
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [ShowForEditView removeFromSuperview] ;
    
    mapView.delegate = self;
    
    [googlMapView clear];
    
    valuesArray = [[NSMutableArray alloc] init] ;
    
    DataArray = [[NSMutableArray alloc] init] ;
    
    SelectedDataForPostDictionary = [[NSMutableDictionary alloc] init] ;
    
    currentMapTypeIndex = appDelegate.strSelectedMap;
    
    [self getSLCData] ;
    
    [self setupOSMMap];
    [self localizationSetup];
    
    if ([[[sqlObj getAllUserInfo] valueForKey:@"ImageView"] isEqualToString:@"No"])
    {
        [btnEdit setFrame:CGRectMake(30.0, btnEdit.frame.origin.y, btnEdit.frame.size.width, btnEdit.frame.size.height)];
        [btnChooseImg setHidden:true];
    } else {
        [btnChooseImg setHidden:false];
    }
    
    if ([[[sqlObj getAllUserInfo] valueForKey:@"PoleID"] isEqualToString:@"No"]) {
        constPoldIDlbl.constant = 0.0;
        constTopViewHeight.constant = 88.0;
    }
}
-(void)localizationSetup {
    self.title                  = LocalizedString(@"POLE DETAILS");
    [btnOptions setTitle:LocalizedString(@"View") forState:UIControlStateNormal];
    if ([strEditView isEqualToString:@"Yes"]) {
        [btnEdit setTitle:LocalizedString(@"EDIT") forState:UIControlStateNormal];
    } else {
        [btnEdit setTitle:LocalizedString(@"OK") forState:UIControlStateNormal];
    }
    
    /*if ([strEditView isEqualToString:@"No"])
     {
     //[btnEdit setBackgroundImage:[UIImage imageNamed:@"OK.png"] forState:UIControlStateNormal];
     [btnEdit setTitle:@"OK" forState:UIControlStateNormal] ;
     }*/
    UIImage* imageAlert = [UIImage imageNamed:@"pin_Nav"];
    CGRect frameimg = CGRectMake(0,0,25,25);
    
    UIButton *alertButton = [[UIButton alloc] initWithFrame:frameimg];
    [alertButton setBackgroundImage:imageAlert forState:UIControlStateNormal];
    [alertButton addTarget:self action:@selector(AlertAction)
          forControlEvents:UIControlEventTouchUpInside];
    [alertButton setShowsTouchWhenHighlighted:YES];
    
    UIBarButtonItem *notificationbutton =[[UIBarButtonItem alloc] initWithCustomView:alertButton];
    self.navigationItem.rightBarButtonItems = @[notificationbutton] ;
    
    UIImage* imgback = [UIImage imageNamed:@"left_arrow"];
    CGRect frameimgBk = CGRectMake(0,0,25,25);
    
    UIButton *btnBack = [[UIButton alloc] initWithFrame:frameimgBk];
    [btnBack setBackgroundImage:imgback forState:UIControlStateNormal];
    [btnBack addTarget:self action:@selector(btnBackClick)
      forControlEvents:UIControlEventTouchUpInside];
    [btnBack setShowsTouchWhenHighlighted:YES];
    
    UIBarButtonItem *bkBtn =[[UIBarButtonItem alloc] initWithCustomView:btnBack];
    self.navigationItem.leftBarButtonItems = @[bkBtn];
}

-(void)setupOSMMap {
    if (currentMapTypeIndex == 6) {
        overLay = [[MKTileOverlay alloc] initWithURLTemplate:@"https://tile.openstreetmap.org/{z}/{x}/{y}.png"];//];
        [overLay setCanReplaceMapContent:true];
        [mapView addOverlay:overLay];
        tileRender = [[MKTileOverlayRenderer alloc] initWithTileOverlay:overLay];
        [self.mapView reloadInputViews];
        [mapView setNeedsDisplay];
    } else {
        [self.mapView removeOverlay:overLay];
        [overLay setCanReplaceMapContent:false];
        [self.mapView reloadInputViews];
        [mapView setNeedsDisplay];
    }
}

-(void)viewWillDisappear:(BOOL)animated
{
    for (UIView *i in ShowForEditView.subviews)
    {
        if([i isKindOfClass:[UILabel class]])
        {
            UILabel *newLbl = (UILabel *)i;
            if(newLbl.tag == 1000)
            {
                newLbl.text = @"" ;
                [newLbl removeFromSuperview];
            }
        }
    }
    
    [googlMapView removeFromSuperview];
    [infoMapView removeFromSuperview];
}


- (void)mapSelectionView {
    switch (currentMapTypeIndex) {
        case 0:
            self.mapView.mapType = MKMapTypeSatellite ;
            break;
        case 1:
            self.mapView.mapType = MKMapTypeStandard ;
            break;
        case 2:
            self.mapView.mapType = MKMapTypeHybrid ;
            break;
        case 3:
            [googlMapView setMapType:kGMSTypeSatellite];
            break;
        case 4:
            [googlMapView setMapType:kGMSTypeTerrain];
            break;
        case 5:
            [googlMapView setMapType:kGMSTypeHybrid];
            break;
        case 6:
            [self.mapView reloadInputViews];
            break;
        default:
            break;
    }
}


#pragma Mark - Data Get

-(void)getSLCData
{
    //---------------------- Request for Getting Client data List  ----------------------//
    
    
    // --------- Initialize GlobalAssistant
    if ([globalObject checkInternetConnection]) {
        globalObject = [[GlobalAssistant alloc] init] ;
        
        [globalObject showActivityIndicator];
        
        NSString *strURL = [NSString stringWithFormat:@"%@/%@",slcDetailURL,strSelecctedDataSlcID];
        
        NSURLConnection_Class *object = [[NSURLConnection_Class alloc] init] ;
        [object RequestForURL:strURL];
        object.delegate = self ;
    } else {
        [globalObject removeActivityIndicator];
    }
}

#pragma mark - Custom Delegate [ NSURLConnection ]

-(void)connectionFailWithError:(NSError *)error {
    NSLog(@"Error: %@",error.localizedDescription) ;
    [globalObject showAlertWithTitle:@"Error" message:LocalizedString(@"No Internet Connection")];
    [globalObject removeActivityIndicator] ;
}

-(void)connectionFinishWithResponse:(NSString *)responseString
{
    NSMutableDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:[responseString dataUsingEncoding:NSUTF8StringEncoding] options:0 error:nil] ;
    
    NSLog(@"%@",responseDict);
    
    DataDictionary = [[NSMutableDictionary alloc] initWithDictionary:responseDict] ;
    
    DataArray = [[DataDictionary valueForKey:@"data"] valueForKey:@"Assets"] ;
    
    strNotes = [[DataDictionary valueForKey:@"data"] valueForKey:@"notes"];
    
    arrOptionsData = [[DataDictionary valueForKey:@"data"] valueForKey:@"pole_options"];
    
    [globalObject removeActivityIndicator] ;
    
    self.ShowForEditView.frame = CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, self.ShowForEditView.frame.size.height) ;
    
    if([[responseDict valueForKey:@"status"] isEqualToString:@"success"])
    {
        //[PoleImg setImageWithURL:[NSURL URLWithString:[[DataDictionary valueForKey:@"data"] valueForKey:@"pole_image_url"]] placeholderImage:[UIImage imageNamed:@"Loading_Image.png"]];
        [PoleImg setImageWithURL:[NSURL URLWithString:[[DataDictionary valueForKey:@"data"] valueForKey:@"pole_image_url"]] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
            if (image == nil) {
                NSString *strSelectedlang = [[NSUserDefaults standardUserDefaults] stringForKey:@"appLanguage"];
                
                if ([strSelectedlang isEqualToString:@"en"]) {
                    [PoleImg setImage: [UIImage imageNamed:@"no_image"]];
                } else if ([strSelectedlang isEqualToString:@"es"]) {
                    [PoleImg setImage: [UIImage imageNamed:@"no_image_es"]];
                } else {
                    [PoleImg setImage: [UIImage imageNamed:@"no_images_pt"]];
                }
                
            }
        }];
        
        CGRect LabelFrame = CGRectMake(12.0f, 105.0f, self.ShowForEditView.frame.size.width - 170.0f, 34.0f);
        
        CGRect DataFrame = CGRectMake(self.ShowForEditView.frame.size.width - 168.0f, 105, 156 , 34.0f);
        
        if ([[[sqlObj getAllUserInfo] valueForKey:@"PoleID"] isEqualToString:@"No"]) {
            LabelFrame = CGRectMake(12.0f, 103.0, self.ShowForEditView.frame.size.width - 170.0f, 34.0f);
                   
            DataFrame = CGRectMake(self.ShowForEditView.frame.size.width - 168.0f, 103.0, 156 , 34.0f);
        }
        
        for (int index = 0; index <DataArray.count; index++)
        {
            NSString *strAttKey = [[DataArray objectAtIndex:index] valueForKey:@"AttributeName"];
            NSString *strAttKeyBlnk = [[DataArray objectAtIndex:index] valueForKey:@"AttrKey"];
            if ((![strAttKey.lowercaseString isEqualToString:@"pole image"]
                && ![strAttKey.lowercaseString isEqualToString:@"imagem do pólo"]
                && ![strAttKey.lowercaseString isEqualToString:@"imagen de poste"])
                
                && (![strAttKey.lowercaseString isEqualToString:@"pole id"]
                    && ![strAttKey.lowercaseString isEqualToString:@"polo id"]
                    && ![strAttKey.lowercaseString isEqualToString:@"pólo id"])
                
            && (![strAttKey.lowercaseString isEqualToString:@"notes"]
                && ![strAttKey.lowercaseString isEqualToString:@"notas"]
                && ![strAttKey.lowercaseString isEqualToString:@"notas"]))
            {
                UILabel *label = [[UILabel alloc] initWithFrame:LabelFrame] ;
                label.text = [[DataArray objectAtIndex:index] valueForKey:@"AttributeName"] ;
                label.font = lblTime.font ;
                label.textColor = [UIColor colorWithRed:(50/255.f) green:(50/255.f) blue:(50/255.f) alpha:1.0f] ;
                label.tag = 1000 ;
                [self.ShowForEditView addSubview:label];
                LabelFrame.origin.y+=LabelFrame.size.height+15.0f;
                
                //**********************************************************
                
                UILabel *Datalabel = [[UILabel alloc] initWithFrame:DataFrame] ;
                Datalabel.text = [[DataArray objectAtIndex:index] valueForKey:@"Selected"] ;
                Datalabel.font = lblTimeData.font ;
                Datalabel.textColor = [UIColor colorWithRed:(50/255.f) green:(50/255.f) blue:(50/255.f) alpha:1.0f] ;
                Datalabel.tag = 1000 ;
                Datalabel.textAlignment = NSTextAlignmentRight ;
                
                [self.ShowForEditView addSubview:Datalabel];
                
                DataFrame.origin.y+=DataFrame.size.height+15.0f;
            } else {
                [SelectedDataForPostDictionary setObject:@"" forKey:strAttKeyBlnk];
            }
        }
        
        lblNotes.frame = CGRectMake(12.0f, LabelFrame.origin.y , self.ShowForEditView.frame.size.width -172.0f, 34.0f) ;
        
        lblTime.frame = CGRectMake(12.0f, LabelFrame.origin.y + 49 , self.ShowForEditView.frame.size.width -172.0f, 34.0f) ;
        
        lblAddress.frame = CGRectMake(12.0f, LabelFrame.origin.y + 49 + 49 , self.ShowForEditView.frame.size.width -172.0f, 34.0f) ;
        
        btnOptions.frame = CGRectMake(self.ShowForEditView.frame.size.width - 164.0f, DataFrame.origin.y  , 156, 34.0f) ;
        
        lblTimeData.frame = CGRectMake(self.ShowForEditView.frame.size.width - 164.0f, DataFrame.origin.y + 49 , 156, 34.0f) ;
        
        txtAddress.frame = CGRectMake(self.ShowForEditView.frame.size.width - 164.0f, LabelFrame.origin.y + 49 + 49, 156, 82.0f) ;
        
        BottomViewObj.frame = CGRectMake(self.ShowForEditView.frame.size.width/2 - 106.0f , lblAddress.frame.origin.y + 120 , 212 , 45) ;
        
        /*  btnEdit.frame = CGRectMake(self.ShowForEditView.frame.size.width/2 - 75, lblAddress.frame.origin.y + 85 , 150, 46) ;*/
        
        //**********************************************************
        
        NSString *strMACID = [[self->sqlObj getAllUserInfo] valueForKey:@"ScanPH"];
        
        DatalblMacID.text = [NSString stringWithFormat:@"%@ : %@",LocalizedString(strMACID) ,[[DataDictionary valueForKey:@"data"] valueForKey:@"mac_address"]];
        
        NSString *strNode = [[DataDictionary valueForKey:@"data"] valueForKey:@"node_type"];
        if ([strNode isEqualToString:@""]){
                strNode = @"Unknown";
        }
        _lblNodeType.text = [NSString stringWithFormat:@"%@ : %@",LocalizedString(@"NODE TYPE") ,strNode];
        
        strSlcID = [[DataDictionary valueForKey:@"data"] valueForKey:@"slc_id"] ;
        DatalblSlcID.text = [NSString stringWithFormat:@"%@ : %@", LocalizedString(@"SLC ID"),strSlcID];
        
        strPoleID = [[DataDictionary valueForKey:@"data"] valueForKey:@"pole_id"] ;
        DatallblPoleID.text = [NSString stringWithFormat:@"%@ : %@", LocalizedString(@"POLE ID"), strPoleID];
        
        strLatitude = [[DataDictionary valueForKey:@"data"] valueForKey:@"lat"] ;
        strLongitude = [[DataDictionary valueForKey:@"data"] valueForKey:@"lng"] ;
        
        lblTimeData.text = [[DataDictionary valueForKey:@"data"] valueForKey:@"date_of_installation"] ;
        
        txtAddress.text = [[DataDictionary valueForKey:@"data"] valueForKey:@"address"] ;
        
        //******************************************************************
        
        self.ShowForEditView.frame = CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, BottomViewObj.frame.origin.y + 70);
        
        [_scroll setContentSize:CGSizeMake([[UIScreen mainScreen]bounds].size.width, ShowForEditView.frame.size.height)] ;
        
        [_scroll scrollRectToVisible:CGRectMake(0,  0 , [[UIScreen mainScreen] bounds].size.width , 0) animated:YES] ;
        
        [self.scroll addSubview:ShowForEditView];
    } else if ([[responseDict objectForKey:@"status"] isEqualToString:@"error"]) {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:[responseDict objectForKey:@"msg"]
                                      message:@""
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
            //Handel your yes please button action here
            
        }];
        
        [alert addAction:yesButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    } else if ([[responseDict objectForKey:@"status"] isEqualToString:@"0"]) {
        
        NSString *strMsg = [responseDict objectForKey:@"msg"];
        
        if (strMsg == nil || [strMsg  isEqualToString: @""]) {
            strMsg = @"Oops! Something went wrong. Try again later";
        }
        
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:LocalizedString(strMsg)
                                      message:@""
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
            sqlObj = [[SQLManager alloc] init] ;
            [sqlObj createAndOpenDatabase] ;
            
            [sqlObj DeleteData] ;
            
            UIStoryboard *story = [UIStoryboard storyboardWithName:@"SecurityCodeViewController" bundle:nil];
            
            SecurityCodeViewController *SecurityCodeViewController = [story instantiateViewControllerWithIdentifier:@"SecurityCodeViewController"];
            
            UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:SecurityCodeViewController];
            
            [[UIApplication sharedApplication].keyWindow setRootViewController:navController];
        }];
        
        [alert addAction:yesButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    } else {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:LocalizedString(@"Oops! Something went wrong. Try again later")
                                      message:@""
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
        }];
        [alert addAction:yesButton];
        [self presentViewController:alert animated:YES completion:nil];
    }
    
}

-(IBAction)btnOptions_Click:(UIButton *)sender{
    UIStoryboard *story = [UIStoryboard storyboardWithName:@"PoleOptionController" bundle:nil];
    
    PoleOptionController *PoleDataView = [story instantiateViewControllerWithIdentifier:@"PoleOptionController"];
    PoleDataView.isFrmEditView  = true;
    PoleDataView.isEditable     = false;
    PoleDataView.arrOptionsData = arrOptionsData;
    PoleDataView.strNotes       = strNotes;
    [self.navigationController pushViewController:PoleDataView animated:YES] ;
}

#pragma mark - Map Delegate

- (MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id<MKOverlay>)overlay {
    return tileRender;
}
-(void)btnBackClick{
    [self eventof:@"SLCDetailsBack"];
    [self.navigationController popViewControllerAnimated:true];
}
-(void)AlertAction
{
    [self eventof:@"SLCDetailsPin"];
    if ((currentMapTypeIndex != 6) && (currentMapTypeIndex > 2)) {
        mapView.hidden      = true;
        googlMapView.hidden = false;
        
        infoMapView.frame = CGRectMake(50, 100, [UIScreen mainScreen].bounds.size.width - 100, [UIScreen mainScreen].bounds.size.height - 313);
        
        infoMapView.layer.borderWidth = 4.0 ;
        infoMapView.layer.borderColor = [UIColor colorWithRed:(0/255.f) green:(114/255.f) blue:(188/255.f) alpha:1.0f].CGColor ;
        infoMapView.layer.cornerRadius = 8.0 ;
        
        GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:[strLatitude doubleValue]
                                                                longitude:[strLongitude doubleValue]
                                                                     zoom:16];
        googlMapView = [GMSMapView mapWithFrame:CGRectMake(0, 0, infoMapView.frame.size.width, infoMapView.frame.size.height)                                                camera:camera];
        [infoMapView addSubview:googlMapView];
        
        GMSMarker *marker = [[GMSMarker alloc] init];
        marker.position = CLLocationCoordinate2DMake([strLatitude doubleValue], [strLongitude doubleValue]);
        marker.title = [NSString stringWithFormat:@"%@ : %@", LocalizedString(@"SLC ID"), strSlcID];
        marker.snippet =[NSString stringWithFormat:@"%@ : %@", LocalizedString(@"POLE ID"), strPoleID];
        marker.icon = [UIImage imageNamed:@"pin"];
        marker.map = googlMapView;
        
        [self.view addSubview:infoMapView];
        [self mapSelectionView];
        [infoMapView bringSubviewToFront:btnOK];
    } else {
        mapView.hidden      = false;
        googlMapView.hidden = true;
        [mapView removeAnnotations:mapView.annotations];
        
        infoMapView.frame = CGRectMake(50, 100, [UIScreen mainScreen].bounds.size.width - 100, [UIScreen mainScreen].bounds.size.height - 313);
        
        infoMapView.layer.borderWidth = 4.0 ;
        infoMapView.layer.borderColor = [UIColor colorWithRed:(0/255.f) green:(114/255.f) blue:(188/255.f) alpha:1.0f].CGColor ;
        infoMapView.layer.cornerRadius = 8.0 ;
        
        mapView.layer.borderWidth = 4.0 ;
        mapView.layer.cornerRadius = 8.0 ;
        
        CLLocationCoordinate2D location = CLLocationCoordinate2DMake([strLatitude floatValue], [strLongitude floatValue]) ;
        
        MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(location, 800, 800);
        [mapView setRegion:[mapView regionThatFits:region] animated:YES];
        
        MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init] ;
        
//        annotation.title = [NSString stringWithFormat:@"%@ : %@", LocalizedString(@"SLC ID"),strSlcID] ;
        annotation.coordinate = location ;
        
        
        annotation.subtitle = [NSString stringWithFormat:@"%@ : %@", LocalizedString(@"POLE ID"), strPoleID] ;
        
        [mapView addAnnotation:annotation];
        
        [self.view addSubview:infoMapView];
        
        [self mapSelectionView];
    }
}

#pragma mark -  MapView Methods

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation
{
    MKPinAnnotationView *pav = nil;
    
    static NSString *reuseId = @"pin";
    pav = (MKPinAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:reuseId];
    if (pav == nil)
    {
        pav = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:reuseId];
        [pav setPinTintColor:[UIColor redColor]];
        UIImageView *iconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Pole_Blue.png"]];
        pav.leftCalloutAccessoryView = iconView;
        pav.canShowCallout = YES;
        pav.animatesDrop = YES ;
        pav.selected = YES ;
        
    }
    else
    {
        pav.annotation = annotation;
    }
    
    return pav;
}

- (IBAction)MapOkButton:(id)sender
{
    [self eventof:@"SLCDetailsMapOK"];
    [infoMapView removeFromSuperview];
}

- (IBAction)EditButton:(id)sender
{
    [self eventof:@"SLCDetailsEdit"];
    [infoMapView removeFromSuperview];
    
    if ([btnEdit.titleLabel.text isEqualToString:@"OK"])
    {
        [self.navigationController popViewControllerAnimated:YES] ;
    }
    else
    {
        EditDataSaveViewController *EditView = [[EditDataSaveViewController alloc] initWithNibName:@"EditDataSaveViewController" bundle:nil] ;
        EditView.isFrmOptUpdate = false;
        EditView.strSelecctedDataSlcID = strSelecctedDataSlcID ;
        [self.navigationController pushViewController:EditView animated:YES];
        
    }
    
}

#pragma mark -  New Pole Image Action And Methods

- (IBAction)ViewImgBtnBtn:(id)sender {
    [self eventof:@"SLCDetailsImage"];
    
    PoleImageView.frame = CGRectMake([UIScreen mainScreen].bounds.size.width / 2 - 150 , 20 , 300, 500 ) ;
    
    PoleImageView.layer.borderWidth = 4.0 ;
    PoleImageView.layer.borderColor = [UIColor colorWithRed:(0/255.f) green:(114/255.f) blue:(188/255.f) alpha:1.0f].CGColor ;
    
    [self.view addSubview:PoleImageView];
}

-(void)ChooseImage
{
    
}

- (IBAction)SaveImgBtn:(id)sender
{
    [PoleImageView removeFromSuperview];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)eventof:(NSString *)strEventName {
    dispatch_async(dispatch_get_main_queue(), ^{
    NSString *strClientID = [[sqlObj getAllUserInfo] valueForKey:@"clientID"];
    NSString *strUserID = [[sqlObj getAllUserInfo] valueForKey:@"userid"];
    
    [FIRAnalytics logEventWithName:kFIREventSelectContent
                        parameters:@{
                                     kFIRParameterItemName:[NSString stringWithFormat:@"%@_%@_%@",strClientID, strUserID, strEventName],
                                     }];
    });
}

@end




