//
//  SlcDataTableViewCell.h
//  SLCScanner
//
//  Created by INT MAC 2015 on 12/03/18.
//  Copyright © 2018 INT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SlcDataTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *lblSlcCap;
@property (strong, nonatomic) IBOutlet UILabel *lblPoleCap;
@property (strong, nonatomic) IBOutlet UILabel *lblAddedCap;
@property (strong, nonatomic) IBOutlet UILabel *lblAddressCap;

@property (strong, nonatomic) IBOutlet UILabel *lblSlcID ;

@property (strong, nonatomic) IBOutlet UILabel *lblPoleID ;

@property (strong, nonatomic) IBOutlet UILabel *lblTime ;

@property (strong, nonatomic) IBOutlet UILabel *lblAddress ;

@property (strong, nonatomic) IBOutlet UIImageView *img ;


@property (strong, nonatomic) IBOutlet NSLayoutConstraint *consPoleIdheight;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *consPoleValueheight;

@end
