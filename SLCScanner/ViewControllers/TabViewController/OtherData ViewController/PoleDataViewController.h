//
//  PoleDataViewController.h
//  SLCScanner
//
//  Created by INT MAC 2015 on 27/02/18.
//  Copyright © 2018 INT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASIFormDataRequest.h"
#import "SQLManager.h"
#import "GlobalAssistant.h"
#import "API.h"
#import "NSURLConnection_Class.h"
#import <CoreLocation/CoreLocation.h>

@interface PoleDataViewController : UIViewController <ASIHTTPRequestDelegate,NSURLConnection_CustomDelegate,CLLocationManagerDelegate>
{
    NSMutableDictionary *DataDictionary ;
    
    NSMutableDictionary *SelectedDataForPostDictionary ;
    
    NSMutableArray *DataArray ;

}

@property (strong, nonatomic) IBOutlet UIDatePicker *datePicker ;

//------------assign property--------------//

// --------- Initialize GlobalAssistant

@property (strong , nonatomic) GlobalAssistant *globalObject ;

//-------- SqlManager Obj -------//

@property (strong , nonatomic) SQLManager *sqlObj ;

@property (strong, nonatomic) IBOutlet UIView *containerView;
@property (strong, nonatomic) IBOutlet UIScrollView *scroll;

@property (strong , nonatomic) NSString *strMacID ;
@property (strong , nonatomic) NSString *strSlcID ;
@property (strong , nonatomic) NSString *strLat ;
@property (strong , nonatomic) NSString *strLong ;
@property (strong , nonatomic) NSString *strPoleID ;
@property (strong , nonatomic) NSString *strAddress ;
@property (strong , nonatomic) NSString *strNotes ;
@property (strong , nonatomic) NSMutableArray *arrPoleOpt ;
@property (strong , nonatomic) UIImage *PoleImg ;
@property (strong , nonatomic) NSString *strPreviousEntry ;

@property (strong, nonatomic) IBOutlet UILabel *lblSelectDate ;


@property (strong, nonatomic) IBOutlet UIButton *btnSelectDate ;

@property (strong, nonatomic) IBOutlet UIButton *btnSave ;

@property (strong, nonatomic) IBOutlet UILabel *lblDataCopy ;

@property (strong, nonatomic) IBOutlet UISegmentedControl *dataSegmentObj ;


- (IBAction)SelectDateButton:(id)sender;

- (IBAction)SaveButton:(id)sender;

-(IBAction)SegmentChangeViewValueChanged:(UISegmentedControl *)SControl ;

@end
