//
//  SlcIDViewController.h
//  SLCScanner
//
//  Created by INT MAC 2015 on 07/03/18.
//  Copyright © 2018 INT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "ASIFormDataRequest.h"
#import "GlobalAssistant.h"
#import "SQLManager.h"
#import <ZXingObjC/ZXingObjC.h>
#import <AudioToolbox/AudioToolbox.h>

@interface SlcIDViewController : UIViewController <ZXCaptureDelegate, ASIHTTPRequestDelegate>
{

}

//------------assign property--------------//

// --------- Initialize GlobalAssistant

@property (strong , nonatomic) GlobalAssistant *globalObject ;

//-------- SqlManager Obj -------//

@property (strong , nonatomic) SQLManager *sqlObj ;

@property (strong, nonatomic) NSString *strMacAddress ;

@end
