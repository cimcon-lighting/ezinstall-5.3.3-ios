//
//  ScanPoleIDViewController.h
//  SLCScanner
//
//  Created by INT MAC 2015 on 26/02/18.
//  Copyright © 2018 INT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Speech/Speech.h>
#import "UIImage+animatedGIF.h"
#import <AVFoundation/AVFoundation.h>
#import "GlobalAssistant.h"
#import "SQLManager.h"
#import <CoreLocation/CoreLocation.h>
#import "ASIFormDataRequest.h"

@interface ScanPoleIDViewController : UIViewController <CLLocationManagerDelegate,ASIHTTPRequestDelegate>
{
    // UI
    __weak IBOutlet UIButton *speakButton;
    __weak IBOutlet UIImageView *animationImageView;
    
    // Speech recognize
    SFSpeechRecognizer *speechRecognizer;
    SFSpeechAudioBufferRecognitionRequest *recognitionRequest;
    SFSpeechRecognitionTask *recognitionTask;
    SFSpeechURLRecognitionRequest *urlRequest;
    
    // Record speech using audio Engine
    AVAudioInputNode *inputNode;
    AVAudioEngine *audioEngine;
    
}

@property (strong , nonatomic) NSString *strNotes;

@property (strong , nonatomic) NSString *strMacID ;

@property (strong , nonatomic) NSString *strSlcID ;

@property (strong , nonatomic) NSString *strLat ;

@property (strong , nonatomic)  NSString *strLong ;

@property (strong , nonatomic)  NSString *strAddress ;

@property (strong , nonatomic)  NSMutableArray *arrPoleOpt;

@property (strong , nonatomic)  UIImage *PoleImg ;

// --------- Initialize GlobalAssistant

@property (strong , nonatomic) GlobalAssistant *globalObject ;

//-------- SqlManager Obj -------//

@property (strong , nonatomic) SQLManager *sqlObj ;

@property (strong, nonatomic) IBOutlet UITextField *txtPoleID ;

@property (strong, nonatomic) IBOutlet UIButton *btnNoneSelection ;

@property (strong, nonatomic) IBOutlet UILabel *lblNone ;

- (IBAction)OKButton:(id)sender;
- (IBAction)CancelButton:(id)sender;
- (IBAction)NoneSelectButton:(id)sender;
- (IBAction)VoiceRecButton:(id)sender;
- (IBAction)ImageScanButton:(id)sender;

@end
