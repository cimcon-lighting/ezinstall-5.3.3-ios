//
//  PoleImageViewController.m
//  SLCScanner
//
//  Created by INT MAC 2015 on 08/06/18.
//  Copyright © 2018 INT. All rights reserved.
//

#import "PoleImageViewController.h"

#import "LocalizeHelper.h"
#import "UIImageView+WebCache.h"
#import "PoleDataViewController.h"
#import "PoleOptionController.h"
#import "ScanPoleIDViewController.h"
#import "SecurityCodeViewController.h"

#import <CoreLocation/CoreLocation.h>
#import <FirebaseAnalytics/FirebaseAnalytics.h>

@interface PoleImageViewController () <UIImagePickerControllerDelegate,UINavigationControllerDelegate,CLLocationManagerDelegate,UIGestureRecognizerDelegate>
{
    NSString *strRequestFor ;
    
    //-------------------------------------------
    
    NSString *strUsername ;
    
    NSString *strPassword ;
    
    CLLocationManager *locationManager;
    
    NSString *strIDForUpdateData ;
    
    NSString *ImageSelect ;
    
    IBOutlet UIButton *btnAddPhoto;
    
    NSString *strPoleID ;
    
    IBOutlet UIImageView *imgCamera;
}
@end

@implementation PoleImageViewController
@synthesize PoleImg ;
@synthesize strLat,strLong,strMacID,strSlcID, strAddress ;
@synthesize sqlObj, globalObject ;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self eventof:@"Camera"];
    
    globalObject = [[GlobalAssistant alloc] init] ;
    
    //Lat long code
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self ;
    [locationManager requestWhenInUseAuthorization] ;
    locationManager.distanceFilter = kCLDistanceFilterNone; // whenever we move
    locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation ;
    
    [locationManager startUpdatingLocation];
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDetected)];
    singleTap.numberOfTapsRequired = 1;
    [PoleImg setUserInteractionEnabled:YES];
    [PoleImg addGestureRecognizer:singleTap];
    
    /*  if ([strPreviousEntry isEqualToString:@"Yes"])
     {
     [self getOtherData] ;
     }*/
    
    [globalObject buttonWithCornerRadious:22.0 btnView:_SaveImgBtn];
    [globalObject buttonWithCornerRadious:22.0 btnView:_ChooseImgBtn];
    
    UIImage* imgback = [UIImage imageNamed:@"left_arrow"];
    CGRect frameimgBk = CGRectMake(0,0,25,25);
    
    UIButton *btnBack = [[UIButton alloc] initWithFrame:frameimgBk];
    [btnBack setBackgroundImage:imgback forState:UIControlStateNormal];
    [btnBack addTarget:self action:@selector(btnBackClick)
      forControlEvents:UIControlEventTouchUpInside];
    [btnBack setShowsTouchWhenHighlighted:YES];
    
    UIBarButtonItem *bkBtn =[[UIBarButtonItem alloc] initWithCustomView:btnBack];
    self.navigationItem.leftBarButtonItems = @[bkBtn];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    
    
    self.title = LocalizedString(@"CAMERA");
    
    [_ChooseImgBtn setTitle:LocalizedString(@"SELECT") forState:UIControlStateNormal];
    if ([ImageSelect isEqualToString:@"YES"]) {
        [_SaveImgBtn setTitle:LocalizedString(@"NEXT") forState:UIControlStateNormal];
    } else {
        [_SaveImgBtn setTitle:LocalizedString(@"SKIP") forState:UIControlStateNormal];
    }
    [btnAddPhoto setTitle:LocalizedString(@"ADD PHOTO") forState:UIControlStateNormal];
}
-(void)btnBackClick{
    [self.navigationController popViewControllerAnimated:true];
}
/*
 #pragma mark - Data Get
 
 -(void)getOtherData
 {
 //---------------------- Request for Getting Client data List  ----------------------//
 
 [globalObject showActivityIndicator];
 
 // ---------- Initialize SqlObject
 
 sqlObj = [[SQLManager alloc] init] ;
 
 [sqlObj createAndOpenDatabase] ;
 
 NSString *strURL = [NSString stringWithFormat:@"%@/%@/%@/Yes",DynamicUIdataURL,[[sqlObj getAllUserInfo] valueForKey:@"clientID"],[[sqlObj getAllUserInfo] valueForKey:@"MeasurmentUnit"]] ;
 
 NSURLConnection_Class *object = [[NSURLConnection_Class alloc] init] ;
 [object RequestForURL:strURL];
 object.delegate = self ;
 
 }
 
 #pragma mark - Custom Delegate [ NSURLConnection ]
 
 -(void)connectionFailWithError:(NSError *)error
 {
 NSLog(@"Error: %@",error.localizedDescription) ;
 }
 
 -(void)connectionFinishWithResponse:(NSString *)responseString
 {
 NSMutableDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:[responseString dataUsingEncoding:NSUTF8StringEncoding] options:0 error:nil] ;
 
 NSLog(@"%@",responseDict);
 
 [globalObject removeActivityIndicator] ;
 
 if([[responseDict valueForKey:@"status"] isEqualToString:@"success"])
 {
 [PoleImg setImageWithURL:[NSURL URLWithString:[responseDict valueForKey:@"pole_image_url"]] placeholderImage:[UIImage imageNamed:@"Loading_Image.png"]];
 
 strPoleID = [responseDict valueForKey:@"pole_id"] ;
 
 if (![[[sqlObj getAllUserInfo] valueForKey:@"MeasurmentUnit"] isEqualToString:[responseDict valueForKey:@"measurement_units"]])
 {
 UIAlertController * alert=   [UIAlertController
 alertControllerWithTitle:[responseDict objectForKey:@"msg"]                                      message:@""
 preferredStyle:UIAlertControllerStyleAlert];
 
 UIAlertAction * ListButton = [UIAlertAction
 actionWithTitle:@"Ok"
 style:UIAlertActionStyleDefault
 handler:^(UIAlertAction * action)
 {
 [sqlObj updateDATA:[responseDict valueForKey:@"measurement_units"] :@"MeasurmentUnit"];
 }];
 
 [alert addAction:ListButton];
 
 [self presentViewController:alert animated:YES completion:nil];
 }
 
 }
 }
 */

-(void)tapDetected
{
    [self CallSelectImageAction];
}

- (IBAction)ChooseImgBtn:(id)sender
{
    [self CallSelectImageAction];
}

-(void)CallSelectImageAction
{
    [self eventof:@"CameraSelect"];
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle: LocalizedString(@"Select Pole Image")
                                                                         message:nil
                                                                  preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:LocalizedString(@"Cancel") style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        // Cancel button tappped.
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    
    imagePicker.delegate = self;
    
    UIAlertAction* gallery = [UIAlertAction
                              actionWithTitle:LocalizedString(@"Photo Library")
                              style:UIAlertActionStyleDefault
                              handler:^(UIAlertAction * action)
                              {
        
        imagePicker.sourceType =UIImagePickerControllerSourceTypePhotoLibrary;
        
        [self presentViewController:imagePicker animated:YES completion:nil];
    }];
    
    
    [actionSheet addAction:gallery];
    
    UIAlertAction* Camera = [UIAlertAction
                             actionWithTitle:LocalizedString(@"Camera")
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
        imagePicker.sourceType =UIImagePickerControllerSourceTypeCamera;
        imagePicker.allowsEditing = true;
        
        [self presentViewController:imagePicker animated:YES completion:nil];
        
    }];
    
    
    [actionSheet addAction:Camera];
    
    
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
        [actionSheet setModalPresentationStyle:UIModalPresentationPopover];
        
        UIPopoverPresentationController *popPresenter = [actionSheet
                                                         popoverPresentationController];
        popPresenter.sourceView = _ChooseImgBtn;
        popPresenter.sourceRect = _ChooseImgBtn.bounds;
        
    }
    
    [self presentViewController:actionSheet animated:YES completion:nil];
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void) imagePickerController:(UIImagePickerController *)picker
         didFinishPickingImage:(UIImage *)image
                   editingInfo:(NSDictionary *)editingInfo
{
    
    UIImage *photoTaken = [editingInfo objectForKey:@"UIImagePickerControllerOriginalImage"];

    //Save Photo to library only if it wasnt already saved i.e. its just been taken
    if (picker.sourceType == UIImagePickerControllerSourceTypeCamera) {
        UIImageWriteToSavedPhotosAlbum(photoTaken, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);
    }
    
    ImageSelect = @"YES" ;
    [imgCamera setHidden:true];
    [btnAddPhoto setHidden:true];
    
    PoleImg.image = image;
    
    [picker dismissViewControllerAnimated:YES completion:nil];
    
    if ([ImageSelect isEqualToString:@"YES"]) {
        [_SaveImgBtn setTitle:LocalizedString(@"NEXT") forState:UIControlStateNormal];
    } else {
        [_SaveImgBtn setTitle:LocalizedString(@"SKIP") forState:UIControlStateNormal];
    }
}

- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo {
    UIAlertView *alert;
    //NSLog(@"Image:%@", image);
    if (error) {
        alert = [[UIAlertView alloc] initWithTitle:@"Error!"
                                           message:[error localizedDescription]
                                          delegate:nil
                                 cancelButtonTitle:@"OK"
                                 otherButtonTitles:nil];
        [alert show];
    }

}

- (IBAction)SaveImgBtn:(id)sender
{
    sqlObj = [[SQLManager alloc] init] ;
    
    [sqlObj createAndOpenDatabase] ;
    
    UIStoryboard *story = [UIStoryboard storyboardWithName:@"PoleOptionController" bundle:nil];
    
    PoleOptionController *PoleDataView = [story instantiateViewControllerWithIdentifier:@"PoleOptionController"];
    
    PoleDataView.isFrmEditView = false;
    PoleDataView.strMacID = strMacID ;
    PoleDataView.strSlcID = strSlcID ;
    PoleDataView.strLat = strLat ;
    PoleDataView.strLong = strLong ;
    PoleDataView.strAddress = strAddress ;
    PoleDataView.strPoleID = @"" ;
    
    if ([ImageSelect isEqualToString:@"YES"])
    {
        PoleDataView.PoleImg = PoleImg.image ;
    }
    
    [self.navigationController pushViewController:PoleDataView animated:YES] ;
    
    //    if ([[[sqlObj getAllUserInfo] valueForKey:@"PoleID"] isEqualToString:@"No"])
    //    {
    //        if ([[[sqlObj getAllUserInfo] valueForKey:@"OtherData"] isEqualToString:@"No"]){
    //        {
    //            [self PostSLCData];
    //        }
    //        else
    //        {
    //            UIStoryboard *story = [UIStoryboard storyboardWithName:@"PoleOptionController" bundle:nil];
    //
    //            PoleOptionController *PoleDataView = [story instantiateViewControllerWithIdentifier:@"PoleOptionController"];
    //
    //            PoleDataView.isFrmEditView = false;
    //            PoleDataView.strMacID = strMacID ;
    //            PoleDataView.strSlcID = strSlcID ;
    //            PoleDataView.strLat = strLat ;
    //            PoleDataView.strLong = strLong ;
    //            PoleDataView.strAddress = strAddress ;
    //            PoleDataView.strPoleID = @"" ;
    //
    //            if ([ImageSelect isEqualToString:@"YES"])
    //            {
    //                PoleDataView.PoleImg = PoleImg.image ;
    //            }
    //
    //            [self.navigationController pushViewController:PoleDataView animated:YES] ;
    //        }
    //            /*PoleDataViewController *PoleDataView = [[PoleDataViewController alloc] initWithNibName:@"PoleDataViewController" bundle:nil] ;
    //
    //            PoleDataView.strMacID   = strMacID ;
    //            PoleDataView.strSlcID   = strSlcID ;
    //            PoleDataView.strLat     = strLat ;
    //            PoleDataView.strLong    = strLong ;
    //            PoleDataView.strAddress = strAddress ;
    //            PoleDataView.strPoleID  = @"" ;
    //
    //            if ([ImageSelect isEqualToString:@"YES"])
    //            {
    //                PoleDataView.PoleImg = PoleImg.image ;
    //            }
    //
    //            [self.navigationController pushViewController:PoleDataView animated:YES] ;*/
    //        }
    //    }
    //    else
    //    {
    //        UIStoryboard *story = [UIStoryboard storyboardWithName:@"PoleOptionController" bundle:nil];
    //
    //        PoleOptionController *PoleDataView = [story instantiateViewControllerWithIdentifier:@"PoleOptionController"];
    //
    //        PoleDataView.isFrmEditView = false;
    //        PoleDataView.strMacID = strMacID ;
    //        PoleDataView.strSlcID = strSlcID ;
    //        PoleDataView.strLat = strLat ;
    //        PoleDataView.strLong = strLong ;
    //        PoleDataView.strAddress = strAddress ;
    //        PoleDataView.strPoleID = @"" ;
    //
    //        if ([ImageSelect isEqualToString:@"YES"])
    //        {
    //            PoleDataView.PoleImg = PoleImg.image ;
    //        }
    //
    //        [self.navigationController pushViewController:PoleDataView animated:YES] ;
    //
    //        /*UIStoryboard *story = [UIStoryboard storyboardWithName:@"ScanPoleIDViewController" bundle:nil];
    //
    //        ScanPoleIDViewController *ScanPoleIDViewController = [story instantiateViewControllerWithIdentifier:@"ScanPoleIDViewController"];
    //
    //        ScanPoleIDViewController.strMacID = strMacID ;
    //
    //        ScanPoleIDViewController.strSlcID = strSlcID ;
    //
    //        ScanPoleIDViewController.strLat = strLat ;
    //
    //        ScanPoleIDViewController.strLong = strLong ;
    //
    //        ScanPoleIDViewController.strAddress = strAddress ;
    //
    //        if ([ImageSelect isEqualToString:@"YES"])
    //        {
    //            ScanPoleIDViewController.PoleImg = PoleImg.image ;
    //        }
    //
    //        [self.navigationController pushViewController:ScanPoleIDViewController animated:YES];*/
    //    }
}

#pragma mark - Data Post

-(void)PostSLCData
{
    // --------- Initialize GlobalAssistant
    if ([globalObject checkInternetConnection]) {
        globalObject = [[GlobalAssistant alloc] init] ;
        
        [globalObject showActivityIndicatorForSave];
        
        strRequestFor = @"DataPost" ;
        
        NSString *str = PostOtherDataURL ;
        NSString *urlString = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *url = [NSURL URLWithString:urlString];
        
        ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
        
        [request setRequestMethod:@"POST"] ;
        [request setDelegate:self] ;
        
        //------- Post Data
        
        // ---------- Initialize SqlObject
        
        sqlObj = [[SQLManager alloc] init] ;
        
        [sqlObj createAndOpenDatabase] ;
        
        NSLog(@"%@",[[sqlObj getAllUserInfo] valueForKey:@"clientID"]);
        
        NSLog(@"%@",[[sqlObj getAllUserInfo] valueForKey:@"userid"]);
        
        [request setPostValue:[[sqlObj getAllUserInfo] valueForKey:@"clientID"] forKey:@"client_id"] ;
        
        [request setPostValue:[[sqlObj getAllUserInfo] valueForKey:@"userid"] forKey:@"user_id"] ;
        
        [request setPostValue:[[sqlObj getAllUserInfo] valueForKey:@"MeasurmentUnit"] forKey:@"measurement_units"] ;
        
        [request setPostValue:strMacID forKey:@"mac_address"] ;
        
        [request setPostValue:strSlcID forKey:@"slc_id"] ;
        
        [request setPostValue:strLat forKey:@"lat"] ;
        
        [request setPostValue:strLong forKey:@"lng"] ;
        
        NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
        
        [request setPostValue:version forKey:@"Version"] ;
        
        // [request setPostValue:strIDForUpdateData forKey:@"SLCID"] ;
        
        [request setPostValue:strAddress forKey:@"address"] ;
        
        [request setPostValue:@"NO" forKey:@"copyasset"] ;
        
        [request setPostValue:@"IOS" forKey:@"source"] ;
        
        if ([ImageSelect isEqualToString:@"YES"])
        {
            NSData *imgData = [[NSData alloc] initWithData:UIImageJPEGRepresentation((PoleImg.image), 0.1)];
            
            [request setPostFormat:ASIMultipartFormDataPostFormat];
            
            [request addRequestHeader:@"Content-Type" value:@"multipart/form-data"];
            
            [request setData:imgData withFileName:@"abc.jpg" andContentType:@"multipart/form-data" forKey:@"pole_image"];
        }
        
        [request setTimeOutSeconds:180] ;
        
        //-------
        
        [request startAsynchronous] ;
    } else {
        [globalObject removeActivityIndicator];
    }
}

-(void)requestFailed:(ASIHTTPRequest *)request
{
    NSError *error = [request error];
    NSLog(@"error : %@" , error.localizedDescription);
    [globalObject removeActivityIndicator];
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:error.localizedDescription
                                  message:@""
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"OK"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action)
                                {
    }];
    
    [alert addAction:yesButton];
    
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)requestFinished:(ASIHTTPRequest *)request
{
    NSString *responseString = [request responseString];
    NSMutableDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:[responseString dataUsingEncoding:NSUTF8StringEncoding] options:0 error:nil];
    NSLog(@"Responce Dictionary : %@", responseDict);
    
    // --------- Initialize GlobalAssistant
    
    globalObject = [[GlobalAssistant alloc] init] ;
    
    [globalObject removeActivityIndicator];
    
    
    if ([[responseDict objectForKey:@"status"] isEqualToString:@"success"])
    {
        
        if ([strRequestFor isEqualToString:@"Login"])
        {
            [sqlObj insertKeyToDatabase:@"user_logged_in" value:@"yes"];
            
            [sqlObj insertKeyToDatabase:@"token" value:[responseDict objectForKey:@"token"]];
            
        }
        else
        {
            UIAlertController * alert=   [UIAlertController
                                          alertControllerWithTitle:[responseDict objectForKey:@"msg"]                                      message:@""
                                          preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction * ListButton = [UIAlertAction
                                          actionWithTitle:LocalizedString(@"List")
                                          style:UIAlertActionStyleDefault
                                          handler:^(UIAlertAction * action)
                                          {
                [self.tabBarController setSelectedIndex:0];
                
                [self.navigationController popToRootViewControllerAnimated:YES];
            }];
            
            
            
            UIAlertAction* NewButton = [UIAlertAction
                                        actionWithTitle:LocalizedString(@"New Scan")
                                        style:UIAlertActionStyleDefault
                                        handler:^(UIAlertAction * action)
                                        {
                [self.navigationController popToRootViewControllerAnimated:YES];
            }];
            
            [alert addAction:ListButton];
            
            [alert addAction:NewButton];
            
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
    else if ([[responseDict objectForKey:@"status"] isEqualToString:@"logout"])
    {
        // strIDForUpdateData = [responseDict objectForKey:@"SLCID"] ;
        
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:[responseDict objectForKey:@"msg"]                                      message:@""
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
            sqlObj = [[SQLManager alloc] init] ;
            [sqlObj createAndOpenDatabase] ;
            
            NSString *strClientID = [[sqlObj getAllUserInfo] valueForKey:@"clientID"] ;
            NSString *strUserID = [[sqlObj getAllUserInfo] valueForKey:@"userid"] ;
            NSString *strScanLBL = [[sqlObj getAllUserInfo] valueForKey:@"ScanLBL"] ;
            NSString *strScanPH = [[sqlObj getAllUserInfo] valueForKey:@"ScanPH"] ;
            
            NSString *strPoleID = [[sqlObj getAllUserInfo] valueForKey:@"PoleID"] ;
            NSString *strMeasurmentUnit = [[sqlObj getAllUserInfo] valueForKey:@"MeasurmentUnit"] ;
            NSString *strOtherData = [[sqlObj getAllUserInfo] valueForKey:@"OtherData"] ;
            
            [sqlObj DeleteData] ;
            
            [sqlObj createAndOpenDatabase] ;
            
            [sqlObj insertKeyToDatabase:@"clientID" value:strClientID];
            [sqlObj insertKeyToDatabase:@"userid" value:strUserID];
            [sqlObj insertKeyToDatabase:@"ScanLBL" value:strScanLBL];
            [sqlObj insertKeyToDatabase:@"ScanPH" value:strScanPH];
            [sqlObj insertKeyToDatabase:@"PoleID" value:strPoleID];
            [sqlObj insertKeyToDatabase:@"MeasurmentUnit" value:strMeasurmentUnit];
            [sqlObj insertKeyToDatabase:@"OtherData" value:strOtherData];
            
            [self AlertForLogin] ;
            
        }];
        
        [alert addAction:yesButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
    else if ([[responseDict objectForKey:@"status"] isEqualToString:@"error"]) {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:[responseDict objectForKey:@"msg"]                                      message:@""
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
            
        }];
        
        [alert addAction:yesButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    } else if ([[responseDict objectForKey:@"status"] isEqualToString:@"0"]) {
        
        NSString *strMsg = [responseDict objectForKey:@"msg"];
        
        if (strMsg == nil || [strMsg  isEqualToString: @""]) {
            strMsg = @"Oops! Something went wrong. Try again later";
        }
        
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:LocalizedString(strMsg)
                                      message:@""
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
            sqlObj = [[SQLManager alloc] init] ;
            [sqlObj createAndOpenDatabase] ;
            
            [sqlObj DeleteData] ;
            
            UIStoryboard *story = [UIStoryboard storyboardWithName:@"SecurityCodeViewController" bundle:nil];
            
            SecurityCodeViewController *SecurityCodeViewController = [story instantiateViewControllerWithIdentifier:@"SecurityCodeViewController"];
            
            UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:SecurityCodeViewController];
            
            [[UIApplication sharedApplication].keyWindow setRootViewController:navController];
        }];
        
        [alert addAction:yesButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    } else {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:LocalizedString(@"Oops! Something went wrong. Try again later")
                                      message:@""
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
        }];
        [alert addAction:yesButton];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

-(void)AlertForLogin
{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Login"
                                                                   message:nil
                                                            preferredStyle:UIAlertControllerStyleAlert];
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        // optionally configure the text field
        
        textField.placeholder = @"Username" ;
        textField.borderStyle = UITextBorderStyleRoundedRect ;
        textField.font = [UIFont systemFontOfSize:16.0] ;
        textField.keyboardType = UIKeyboardTypeAlphabet;
        textField.returnKeyType = UIReturnKeySend ;
        textField.delegate = self ;
        
    }];
    
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textFieldPass) {
        // optionally configure the text field
        
        textFieldPass.placeholder = @"Password" ;
        textFieldPass.secureTextEntry = YES;
        textFieldPass.borderStyle = UITextBorderStyleRoundedRect ;
        textFieldPass.font = [UIFont systemFontOfSize:16.0] ;
        textFieldPass.keyboardType = UIKeyboardTypeAlphabet;
        textFieldPass.returnKeyType = UIReturnKeySend ;
        textFieldPass.delegate = self ;
        
    }];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Login"
                                                       style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction *action) {
        NSArray * textfields = alert.textFields;
        UITextField * namefield = textfields[0];
        UITextField * passfield = textfields[1];
        
        strUsername = namefield.text ;
        strPassword = passfield.text ;
        
        [self loginAction];
        
    }];
    
    
    [alert addAction:okAction];
    
    [self presentViewController:alert animated:YES completion:nil];
    
    
}

-(void)loginAction
{
    if ([strUsername isEqualToString:@""] || [strPassword isEqualToString:@""])
    {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:@"Please Enter Valid Username and Password."                                     message:@""
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
            [self AlertForLogin] ;
            
        }];
        
        [alert addAction:yesButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
    else
    {
        [self loginRequest];
    }
}


#pragma mark - Data Post

-(void)loginRequest
{
    if ([globalObject checkInternetConnection]) {
        [globalObject showActivityIndicator];
        
        strRequestFor = @"Login" ;
        
        NSString *str = userLoginURL ;
        NSString *urlString = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *url = [NSURL URLWithString:urlString];
        
        ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
        
        [request setRequestMethod:@"POST"] ;
        [request setDelegate:self] ;
        
        //------- Post Data
        
        // ---------- Initialize SqlObject
        
        sqlObj = [[SQLManager alloc] init] ;
        
        [sqlObj createAndOpenDatabase] ;
        
        NSLog(@"%@",[[sqlObj getAllUserInfo] valueForKey:@"clientID"]);
        
        NSLog(@"%@",[[sqlObj getAllUserInfo] valueForKey:@"userid"]);
        
        NSLog(@"%@",[[UIDevice currentDevice] identifierForVendor]);
        
        NSLog(@"%f",locationManager.location.coordinate.latitude);
        
        NSLog(@"%f",locationManager.location.coordinate.longitude);
        
        // 65D78ADF-32BC-4A73-B7BC-C9ADBC4C9F55
        
        NSString *strLat = [NSString stringWithFormat:@"%f",locationManager.location.coordinate.latitude];
        
        NSString *strLong = [NSString stringWithFormat:@"%f",locationManager.location.coordinate.longitude];
        
        [request setPostValue:[[sqlObj getAllUserInfo] valueForKey:@"clientID"] forKey:@"client_id"] ;
        
        [request setPostValue:[[sqlObj getAllUserInfo] valueForKey:@"userid"] forKey:@"userid"] ;
        
        [request setPostValue:strUsername forKey:@"username"] ;
        
        [request setPostValue:strPassword forKey:@"password"] ;
        
        [request setPostValue:strLat forKey:@"lat"] ;
        
        [request setPostValue:strLong forKey:@"long"] ;
        
        // [request setPostValue:[[[UIDevice currentDevice] identifierForVendor] UUIDString] forKey:@"DeviceID"] ;
        
        [request setPostValue:@"IOS" forKey:@"source"] ;
        
        [request setTimeOutSeconds:180] ;
        
        //-------
        
        [request startAsynchronous] ;
    } else {
        [globalObject removeActivityIndicator];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)eventof:(NSString *)strEventName {
    dispatch_async(dispatch_get_main_queue(), ^{
    NSString *strClientID = [[sqlObj getAllUserInfo] valueForKey:@"clientID"];
    NSString *strUserID = [[sqlObj getAllUserInfo] valueForKey:@"userid"];
    
    [FIRAnalytics logEventWithName:kFIREventSelectContent
                        parameters:@{
                                     kFIRParameterItemName:[NSString stringWithFormat:@"%@_%@_%@",strClientID, strUserID, strEventName],
                                     }];
    });
}

@end
