//
//  PoleOptionController.m
//  SLCScanner
//
//  Created by INT MAC 2015 on 08/06/18.
//  Copyright © 2018 INT. All rights reserved.

#import "PoleOptionController.h"

#import "LocalizeHelper.h"
#import "UIImageView+WebCache.h"
#import "PoleDataViewController.h"
#import "EditDataSaveViewController.h"
#import "ScanPoleIDViewController.h"
#import "SecurityCodeViewController.h"

#import <CoreLocation/CoreLocation.h>
#import <FirebaseAnalytics/FirebaseAnalytics.h>

@interface PoleOptionCell : UITableViewCell

@property (nonatomic, retain) IBOutlet UIButton      *btnCheck;
@property (nonatomic, retain) IBOutlet UILabel       *lblNm;
@property (nonatomic, retain) IBOutlet UITextField   *txtField;

@end

@implementation PoleOptionCell

@end

@interface PoleOptionController () <UIImagePickerControllerDelegate,UINavigationControllerDelegate,CLLocationManagerDelegate,UIGestureRecognizerDelegate, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate>
{
    NSString *strRequestFor ;
    
    NSString *strUsername ;
    
    NSString *strPassword ;
    
    CLLocationManager *locationManager;
    
    NSString *strIDForUpdateData ;
    
    NSString *ImageSelect ;
    
    IBOutlet UILabel *lblNotes;
    IBOutlet UILabel *lblDevices;
    
    IBOutlet UITextView *txtNotes;
    
    IBOutlet UIButton *btnAddPhoto;
    IBOutlet UIButton *btnSkip;
    
    NSString *strPoleID ;
    
    IBOutlet UIImageView *imgCamera;
    
    IBOutlet UITableView *tblView;
    
    NSMutableArray *arrTblData;
    NSMutableArray *arrSelected;
    NSMutableArray *arrTitle;
    NSMutableArray *arrTmp;
}
@end

@implementation PoleOptionController
@synthesize PoleImg ;
@synthesize strLat,strLong,strMacID,strSlcID, strAddress ;
@synthesize sqlObj, globalObject ;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self eventof:@"Notes"];
    
    //_arrOptionsData = [[NSMutableArray alloc] init];
    
    tblView.rowHeight = UITableViewAutomaticDimension;
    
    arrTblData      = [[NSMutableArray alloc] init];
    arrSelected     = [[NSMutableArray alloc] init];
    arrTitle        = [[NSMutableArray alloc] init];
    arrTmp          = [[NSMutableArray alloc] init];
    
    globalObject = [[GlobalAssistant alloc] init] ;
    
    //Lat long code
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self ;
    [locationManager requestWhenInUseAuthorization] ;
    locationManager.distanceFilter = kCLDistanceFilterNone; // whenever we move
    locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation ;
    
    [locationManager startUpdatingLocation];
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDetected)];
    singleTap.numberOfTapsRequired = 1;
    
    /*  if ([strPreviousEntry isEqualToString:@"Yes"])
     {
     [self getOtherData] ;
     }*/
    
    [globalObject buttonWithCornerRadious:22.0 btnView:_SaveImgBtn];
    [globalObject buttonWithCornerRadious:22.0 btnView:btnSkip];
    
    UIImage* imgback = [UIImage imageNamed:@"left_arrow"];
    CGRect frameimgBk = CGRectMake(0,0,25,25);
    
    UIButton *btnBack = [[UIButton alloc] initWithFrame:frameimgBk];
    [btnBack setBackgroundImage:imgback forState:UIControlStateNormal];
    [btnBack addTarget:self action:@selector(btnBackClick)
      forControlEvents:UIControlEventTouchUpInside];
    [btnBack setShowsTouchWhenHighlighted:YES];
    
    UIBarButtonItem *bkBtn =[[UIBarButtonItem alloc] initWithCustomView:btnBack];
    self.navigationItem.leftBarButtonItems = @[bkBtn];
    
    sqlObj = [[SQLManager alloc] init] ;
    
    [sqlObj createAndOpenDatabase] ;
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    txtNotes.text = LocalizedString(@"Enter notes here");
    self.title = LocalizedString(@"NOTES");
    [btnSkip setTitle:LocalizedString(@"SKIP") forState:UIControlStateNormal];
    lblNotes.text = LocalizedString(@"Enter Notes");
    lblDevices.text = LocalizedString(@"Devices");
    
    if (_isFrmEditView) {
        if (_isEditable) {
            [_SaveImgBtn setTitle:LocalizedString(@"NEXT") forState:UIControlStateNormal];
        } else {
            [_SaveImgBtn setTitle:LocalizedString(@"NEXT") forState:UIControlStateNormal];
        }
    } else {
        [_SaveImgBtn setTitle:LocalizedString(@"NEXT") forState:UIControlStateNormal];
    }
    
    if (_isFrmEditView) {
        arrTblData = _arrOptionsData;
        [btnSkip setHidden:true];
        if (!_isEditable) {
            [txtNotes setEditable:false];
            [_SaveImgBtn setHidden:true];
        }else{
            [txtNotes setEditable:true];
            [_SaveImgBtn setHidden:false];
            dispatch_async(dispatch_get_main_queue(), ^(void){
                _SaveImgBtn.frame = CGRectMake((self.view.frame.size.width/2.0)-_SaveImgBtn.frame.size.width/2.0,   _SaveImgBtn.frame.origin.y, _SaveImgBtn.frame.size.width, _SaveImgBtn.frame.size.height);
            });
        }
        txtNotes.text = _strNotes;
        for (int i=0; i<arrTblData.count; i++) {
            NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithDictionary:arrTblData[i]];
            NSString *strKeytext = dict[[NSString stringWithFormat:@"%@_text",dict[@"key"]]];
            [arrTitle addObject:strKeytext];
            if ([[NSString stringWithFormat:@"%@",dict[dict[@"key"]]] isEqualToString:@"1"]) {
                [arrSelected addObject:[NSString stringWithFormat:@"%d",i]];
            }
        }
        [tblView reloadData];
    } else {
        [self getDataFrmServer];
        [_SaveImgBtn setHidden:false];
    }
}

-(void)btnBackClick{
    /*NSArray *arrCount = [[NSArray alloc] initWithArray:self.navigationController.viewControllers];
     for (int i=0; i<arrCount.count; i++) {
     if ([arrCount[i] isKindOfClass:[EditDataSaveViewController class]]) {
     EditDataSaveViewController *editSve = arrCount[i];
     editSve.strNotes        = txtNotes.text;
     }
     }*/
    [self eventof:@"NotesBack"];
    if (_isEditable) {
        [self SaveImgBtn:_SaveImgBtn];
    } else {
        [self performSelector:@selector(popVC) withObject:self afterDelay:0.0];
    }
}
-(void)popVC {
    [self.navigationController popViewControllerAnimated:true];
}
- (IBAction)SaveImgBtn:(UIButton *)sender
{
    [self eventof:@"NotesSave"];
    if (sender.tag == 105) {
        NSMutableArray *arrTmp = [[NSMutableArray alloc] init];
        NSMutableArray *arrEditable = [[NSMutableArray alloc] init];
        for (int i =0; i<arrTblData.count; i++) {
            NSMutableDictionary *dictRealData = [[NSMutableDictionary alloc]initWithDictionary:arrTblData[i]];
            NSMutableDictionary *dictTmp = [[NSMutableDictionary alloc] init];
            if (_isEditable) {
                dictRealData[dictRealData[@"key"]] = @"0";
                dictRealData[[NSString stringWithFormat:@"%@_text",dictRealData[@"key"]]] = @"";
            } else {
                dictTmp[dictRealData[@"key"]] = @"0";
                dictTmp[[NSString stringWithFormat:@"%@_text",dictRealData[@"key"]]] = @"";
            }
            if (_isEditable) {
                [arrEditable addObject:dictRealData];
            } else {
                [arrTmp addObject:dictTmp];
            }
        }
        if ([[[sqlObj getAllUserInfo] valueForKey:@"PoleID"] isEqualToString:@"No"]) {
            if ([[[sqlObj getAllUserInfo] valueForKey:@"OtherData"] isEqualToString:@"No"]) {
                [self PostSLCData];
            }
            else
            {
                PoleDataViewController *PoleDataView = [[PoleDataViewController alloc] initWithNibName:@"PoleDataViewController" bundle:nil] ;
                
                PoleDataView.strMacID   = strMacID ;
                PoleDataView.strSlcID   = strSlcID ;
                PoleDataView.strLat     = strLat ;
                PoleDataView.strLong    = strLong ;
                PoleDataView.strAddress = strAddress ;
                PoleDataView.strPoleID  = @"" ;
                PoleDataView.strNotes   = txtNotes.text;
                if (_isEditable) {
                    PoleDataView.arrPoleOpt = arrEditable ;
                } else {
                    PoleDataView.arrPoleOpt = arrTmp ;
                }
                
                /*if ([ImageSelect isEqualToString:@"YES"])
                 {*/
                PoleDataView.PoleImg = PoleImg ;
                //}
                
                [self.navigationController pushViewController:PoleDataView animated:YES] ;
            }
        } else {
            NSMutableArray *arrEditable = [[NSMutableArray alloc] init];
            for (int i =0; i<arrTblData.count; i++) {
                NSMutableDictionary *dictRealData = [[NSMutableDictionary alloc]initWithDictionary:arrTblData[i]];
                NSMutableDictionary *dictTmp = [[NSMutableDictionary alloc] init];
                
                PoleOptionCell *cell = (PoleOptionCell *)[tblView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
                
                if ([arrSelected containsObject:[NSString stringWithFormat:@"%d",i]]) {
                    if (_isEditable) {
                        NSString *strKey = dictRealData[@"key"];
                        [dictRealData setValue:@"1" forKey:strKey];
                        [dictRealData setValue:arrTitle[i] forKey:[NSString stringWithFormat:@"%@_text",strKey]];
                    } else {
                        NSString *strKey = dictRealData[@"key"];
                        dictTmp[strKey] = @"1";
                        dictTmp[[NSString stringWithFormat:@"%@_text",strKey]] = arrTitle[i];
                    }
                } else {
                    if (_isEditable) {
                        dictRealData[dictRealData[@"key"]] = @"0";
                        dictRealData[[NSString stringWithFormat:@"%@_text",dictRealData[@"key"]]] = @"";
                    } else {
                        dictTmp[dictRealData[@"key"]] = @"0";
                        dictTmp[[NSString stringWithFormat:@"%@_text",dictRealData[@"key"]]] = @"";
                    }
                }
                if (_isEditable) {
                    [arrEditable addObject:dictRealData];
                } else {
                    [arrTmp addObject:dictTmp];
                }
            }
            arrTblData = arrEditable;
            if (_isFrmEditView) {
                if (_isEditable) {
                    NSArray *arrCount = [[NSArray alloc] initWithArray:self.navigationController.viewControllers];
                    for (int i=0; i<arrCount.count; i++) {
                        if ([arrCount[i] isKindOfClass:[EditDataSaveViewController class]]) {
                            EditDataSaveViewController *editSve = arrCount[i];
                            editSve.isFrmOptUpdate  = true;
                            editSve.arrOptionsData  = arrTblData;
                            editSve.strNotes        = [txtNotes.text isEqualToString:LocalizedString(@"Enter notes here")] ? @"" : txtNotes.text;
                            [self.navigationController popViewControllerAnimated:true];
                        }
                    }
                }
            } else {
                UIStoryboard *story = [UIStoryboard storyboardWithName:@"ScanPoleIDViewController" bundle:nil];
                
                ScanPoleIDViewController *ScanPoleIDViewController = [story instantiateViewControllerWithIdentifier:@"ScanPoleIDViewController"];
                
                ScanPoleIDViewController.strMacID = strMacID ;
                
                ScanPoleIDViewController.strSlcID = strSlcID ;
                
                ScanPoleIDViewController.strLat = strLat ;
                
                ScanPoleIDViewController.strLong = strLong ;
                
                ScanPoleIDViewController.strAddress = strAddress ;
                
                ScanPoleIDViewController.strNotes   = [txtNotes.text isEqualToString:LocalizedString(@"Enter notes here")] ? @"" : txtNotes.text;
                
                ScanPoleIDViewController.arrPoleOpt = arrTmp ;
                
                /*if ([ImageSelect isEqualToString:@"YES"])
                 {*/
                ScanPoleIDViewController.PoleImg = PoleImg ;
                //}
                
                [self.navigationController pushViewController:ScanPoleIDViewController animated:YES];
            }
        }
    } else {
        
        NSMutableArray *arrEditable = [[NSMutableArray alloc] init];
        for (int i =0; i<arrTblData.count; i++) {
            NSMutableDictionary *dictRealData = [[NSMutableDictionary alloc]initWithDictionary:arrTblData[i]];
            NSMutableDictionary *dictTmp = [[NSMutableDictionary alloc] init];
            
            PoleOptionCell *cell = (PoleOptionCell *)[tblView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
            
            if ([arrSelected containsObject:[NSString stringWithFormat:@"%d",i]]) {
                if (_isEditable) {
                    NSString *strKey = dictRealData[@"key"];
                    [dictRealData setValue:@"1" forKey:strKey];
                    [dictRealData setValue:arrTitle[i] forKey:[NSString stringWithFormat:@"%@_text",strKey]];
                } else {
                    NSString *strKey = dictRealData[@"key"];
                    dictTmp[strKey] = @"1";
                    dictTmp[[NSString stringWithFormat:@"%@_text",strKey]] = arrTitle[i];
                }
            } else {
                if (_isEditable) {
                    dictRealData[dictRealData[@"key"]] = @"0";
                    dictRealData[[NSString stringWithFormat:@"%@_text",dictRealData[@"key"]]] = @"";
                } else {
                    dictTmp[dictRealData[@"key"]] = @"0";
                    dictTmp[[NSString stringWithFormat:@"%@_text",dictRealData[@"key"]]] = @"";
                }
            }
            if (_isEditable) {
                [arrEditable addObject:dictRealData];
            } else {
                [arrTmp addObject:dictTmp];
            }
        }
        
        arrTblData = arrEditable;
        sqlObj = [[SQLManager alloc] init] ;
        [sqlObj createAndOpenDatabase] ;
        
        if (_isFrmEditView) {
            if (_isEditable) {
                NSArray *arrCount = [[NSArray alloc] initWithArray:self.navigationController.viewControllers];
                for (int i=0; i<arrCount.count; i++) {
                    if ([arrCount[i] isKindOfClass:[EditDataSaveViewController class]]) {
                        EditDataSaveViewController *editSve = arrCount[i];
                        editSve.isFrmOptUpdate  = true;
                        editSve.arrOptionsData  = arrTblData;
                        editSve.strNotes        = [txtNotes.text isEqualToString:LocalizedString(@"Enter notes here")] ? @"" : txtNotes.text;
                        [self.navigationController popViewControllerAnimated:true];
                    }
                }
            }
        } else {
            
            if ([[[sqlObj getAllUserInfo] valueForKey:@"PoleID"] isEqualToString:@"No"])
            {
                if ([[[sqlObj getAllUserInfo] valueForKey:@"OtherData"] isEqualToString:@"No"])
                {
                    [self PostSLCData];
                }
                else
                {
                    PoleDataViewController *PoleDataView = [[PoleDataViewController alloc] initWithNibName:@"PoleDataViewController" bundle:nil] ;
                    
                    PoleDataView.strMacID   = strMacID ;
                    PoleDataView.strSlcID   = strSlcID ;
                    PoleDataView.strLat     = strLat ;
                    PoleDataView.strLong    = strLong ;
                    PoleDataView.strAddress = strAddress ;
                    PoleDataView.strPoleID  = @"" ;
                    PoleDataView.strNotes   = [txtNotes.text isEqualToString:LocalizedString(@"Enter notes here")] ? @"" : txtNotes.text;
                    if (_isEditable) {
                        PoleDataView.arrPoleOpt = arrEditable;
                    } else {
                        PoleDataView.arrPoleOpt = arrTmp ;
                    }
                    PoleDataView.PoleImg = PoleImg ;
                    
                    [self.navigationController pushViewController:PoleDataView animated:YES] ;
                }
            }
            else
            {
                NSMutableArray *arrEditable = [[NSMutableArray alloc] init];
                for (int i =0; i<arrTblData.count; i++) {
                    NSMutableDictionary *dictRealData = [[NSMutableDictionary alloc]initWithDictionary:arrTblData[i]];
                    NSMutableDictionary *dictTmp = [[NSMutableDictionary alloc] init];
                    
                    PoleOptionCell *cell = (PoleOptionCell *)[tblView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
                    
                    if ([arrSelected containsObject:[NSString stringWithFormat:@"%d",i]]) {
                        if (_isEditable) {
                            NSString *strKey = dictRealData[@"key"];
                            [dictRealData setValue:@"1" forKey:strKey];
                            [dictRealData setValue:arrTitle[i] forKey:[NSString stringWithFormat:@"%@_text",strKey]];
                        } else {
                            NSString *strKey = dictRealData[@"key"];
                            dictTmp[strKey] = @"1";
                            dictTmp[[NSString stringWithFormat:@"%@_text",strKey]] = arrTitle[i];
                        }
                    } else {
                        if (_isEditable) {
                            dictRealData[dictRealData[@"key"]] = @"0";
                            dictRealData[[NSString stringWithFormat:@"%@_text",dictRealData[@"key"]]] = @"";
                        } else {
                            dictTmp[dictRealData[@"key"]] = @"0";
                            dictTmp[[NSString stringWithFormat:@"%@_text",dictRealData[@"key"]]] = @"";
                        }
                    }
                    if (_isEditable) {
                        [arrEditable addObject:dictRealData];
                    } else {
                        [arrTmp addObject:dictTmp];
                    }
                }
                
                
                UIStoryboard *story = [UIStoryboard storyboardWithName:@"ScanPoleIDViewController" bundle:nil];
                
                ScanPoleIDViewController *ScanPoleIDViewController = [story instantiateViewControllerWithIdentifier:@"ScanPoleIDViewController"];
                
                ScanPoleIDViewController.strMacID = strMacID ;
                
                ScanPoleIDViewController.strSlcID = strSlcID ;
                
                ScanPoleIDViewController.strLat = strLat ;
                
                ScanPoleIDViewController.strLong = strLong ;
                
                ScanPoleIDViewController.strAddress = strAddress ;
                
                ScanPoleIDViewController.strNotes   = [txtNotes.text isEqualToString:LocalizedString(@"Enter notes here")] ? @"" : txtNotes.text;
                
                ScanPoleIDViewController.arrPoleOpt = arrTmp ;
                
                /*if ([ImageSelect isEqualToString:@"YES"])
                 {*/
                ScanPoleIDViewController.PoleImg = PoleImg ;
                //}
                
                [self.navigationController pushViewController:ScanPoleIDViewController animated:YES];
            }
        }
    }
}

#pragma mark - Data Post

-(void)getDataFrmServer
{
    // --------- Initialize GlobalAssistant
    if ([globalObject checkInternetConnection]) {
        globalObject = [[GlobalAssistant alloc] init] ;
        
        [globalObject showActivityIndicator];
        
        strRequestFor = @"OptionData" ;
        
        NSString *str = getOptionDataURL ;
        NSString *urlString = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *url = [NSURL URLWithString:urlString];
        
        ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
        
        [request setRequestMethod:@"POST"] ;
        [request setPostValue:[[sqlObj getAllUserInfo] valueForKey:@"clientID"] forKey:@"client_id"] ;
        [request setPostValue:[[sqlObj getAllUserInfo] valueForKey:@"userid"] forKey:@"user_id"] ;
        [request setDelegate:self] ;
        
        //------- Post Data
        
        // ---------- Initialize SqlObject
        
        /*sqlObj = [[SQLManager alloc] init] ;
         
         [sqlObj createAndOpenDatabase] ;
         
         NSLog(@"%@",[[sqlObj getAllUserInfo] valueForKey:@"clientID"]);
         
         NSLog(@"%@",[[sqlObj getAllUserInfo] valueForKey:@"userid"]);
         
         [request setPostValue:[[sqlObj getAllUserInfo] valueForKey:@"clientID"] forKey:@"client_id"] ;
         
         [request setPostValue:[[sqlObj getAllUserInfo] valueForKey:@"userid"] forKey:@"user_id"] ;
         
         [request setPostValue:[[sqlObj getAllUserInfo] valueForKey:@"MeasurmentUnit"] forKey:@"measurement_units"] ;
         
         [request setPostValue:strMacID forKey:@"mac_address"] ;
         
         [request setPostValue:strSlcID forKey:@"slc_id"] ;
         
         [request setPostValue:strLat forKey:@"lat"] ;
         
         [request setPostValue:strLong forKey:@"lng"] ;
         
         // [request setPostValue:strIDForUpdateData forKey:@"SLCID"] ;
         
         [request setPostValue:strAddress forKey:@"address"] ;
         
         [request setPostValue:@"NO" forKey:@"copyasset"] ;
         
         [request setPostValue:@"IOS" forKey:@"source"] ;
         
         if ([ImageSelect isEqualToString:@"YES"])
         {
         NSData *imgData = [[NSData alloc] initWithData:UIImageJPEGRepresentation((PoleImg.image), 0.5)];
         
         [request setPostFormat:ASIMultipartFormDataPostFormat];
         
         [request addRequestHeader:@"Content-Type" value:@"multipart/form-data"];
         
         [request setData:imgData withFileName:@"abc.jpg" andContentType:@"multipart/form-data" forKey:@"pole_image"];
         }
         */
        [request setTimeOutSeconds:180] ;
        
        //-------
        
        [request startAsynchronous] ;
    } else {
        [globalObject removeActivityIndicator];
    }
}


-(void)PostSLCData
{
    if ([globalObject checkInternetConnection]) {
        [globalObject showActivityIndicatorForSave];
        
        strRequestFor = @"PostOtherData" ;
        
        NSString *str = PostOtherDataURL ;
        NSString *urlString = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *url = [NSURL URLWithString:urlString];
        
        ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
        
        [request setRequestMethod:@"POST"] ;
        [request setDelegate:self] ;
        
        //------- Post Data
        
        // ---------- Initialize SqlObject
        
        sqlObj = [[SQLManager alloc] init] ;
        
        [sqlObj createAndOpenDatabase] ;
        
        NSLog(@"%@",[[sqlObj getAllUserInfo] valueForKey:@"clientID"]);
        
        NSLog(@"%@",[[sqlObj getAllUserInfo] valueForKey:@"userid"]);
        
        [request setPostValue:[[sqlObj getAllUserInfo] valueForKey:@"clientID"] forKey:@"client_id"] ;
        
        [request setPostValue:[[sqlObj getAllUserInfo] valueForKey:@"userid"] forKey:@"user_id"] ;
        
        [request setPostValue:[[sqlObj getAllUserInfo] valueForKey:@"MeasurmentUnit"] forKey:@"measurement_units"] ;
        
        [request setPostValue:strMacID forKey:@"mac_address"] ;
        
        [request setPostValue:strSlcID forKey:@"slc_id"] ;
        
        [request setPostValue:strLat forKey:@"lat"] ;
        
        [request setPostValue:strLong forKey:@"lng"] ;
        
        NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
        
        [request setPostValue:version forKey:@"Version"] ;
        
        [request setPostValue:strAddress forKey:@"address"] ;
        
        //  [request setPostValue:strIDForUpdateData forKey:@"SLCID"] ;
        
        [request setPostValue:@"No" forKey:@"copyasset"] ;
        
        [request setPostValue:@"IOS" forKey:@"source"] ;
        
        if (PoleImg)
        {
            NSData *imgData = [[NSData alloc] initWithData:UIImageJPEGRepresentation((PoleImg), 0.1)];
            
            [request setPostFormat:ASIMultipartFormDataPostFormat];
            
            [request addRequestHeader:@"Content-Type" value:@"multipart/form-data"];
            
            [request setData:imgData withFileName:@"abc.jpg" andContentType:@"multipart/form-data" forKey:@"pole_image"];
        }
        
        [request setPostValue:[txtNotes.text isEqualToString:LocalizedString(@"Enter notes here")] ? @"" : txtNotes.text forKey:@"notes"];
        
        for (int i =0; i<arrTmp.count; i++) {
            NSMutableDictionary *dictTmp = [[NSMutableDictionary alloc] initWithDictionary:arrTmp[i]];
            NSArray *arrCount = [[NSArray alloc] init];
            arrCount = [dictTmp allKeys];
            for (int j =0; j<arrCount.count; j++) {
                [request setPostValue:dictTmp[arrCount[j]] forKey:arrCount[j]];
            }
        }
        
        
        [request setTimeOutSeconds:180] ;
        
        //-------
        
        [request startAsynchronous] ;
    } else {
        [globalObject removeActivityIndicator];
    }
}

-(void)requestFailed:(ASIHTTPRequest *)request
{
    NSError *error = [request error];
    NSLog(@"error : %@" , error.localizedDescription);
    [globalObject removeActivityIndicator];
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:error.localizedDescription
                                  message:@""
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"OK"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action)
                                {
    }];
    
    [alert addAction:yesButton];
    
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)requestFinished:(ASIHTTPRequest *)request
{
    NSString *responseString = [request responseString];
    NSMutableDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:[responseString dataUsingEncoding:NSUTF8StringEncoding] options:0 error:nil];
    NSLog(@"Responce Dictionary : %@", responseDict);
    
    [globalObject removeActivityIndicator];
    
    if ([strRequestFor isEqualToString:@"PostOtherData"]) {
        if ([[responseDict objectForKey:@"status"] isEqualToString:@"success"]) {
            UIAlertController * alert=   [UIAlertController
                                          alertControllerWithTitle:[responseDict objectForKey:@"msg"]                                      message:@""
                                          preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction * ListButton = [UIAlertAction
                                          actionWithTitle:LocalizedString(@"List")
                                          style:UIAlertActionStyleDefault
                                          handler:^(UIAlertAction * action)
                                          {
                [self.tabBarController setSelectedIndex:0];
                
                [self.navigationController popToRootViewControllerAnimated:YES];
            }];
            
            UIAlertAction* NewButton = [UIAlertAction
                                        actionWithTitle:LocalizedString(@"New Scan")
                                        style:UIAlertActionStyleDefault
                                        handler:^(UIAlertAction * action)
                                        {
                [self.navigationController popToRootViewControllerAnimated:YES];
            }];
            
            [alert addAction:ListButton];
            
            [alert addAction:NewButton];
            
            [self presentViewController:alert animated:YES completion:nil];
            
        } else if ([[responseDict objectForKey:@"status"] isEqualToString:@"error"]) {
            UIAlertController * alert=   [UIAlertController
                                          alertControllerWithTitle:[responseDict objectForKey:@"msg"]                                      message:@""
                                          preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* yesButton = [UIAlertAction
                                        actionWithTitle:@"OK"
                                        style:UIAlertActionStyleDefault
                                        handler:^(UIAlertAction * action)
                                        {
            }];
            
            [alert addAction:yesButton];
            
            [self presentViewController:alert animated:YES completion:nil];
        }  else if ([[responseDict objectForKey:@"status"] isEqualToString:@"0"]) {
            
            NSString *strMsg = [responseDict objectForKey:@"msg"];
            
            if (strMsg == nil || [strMsg  isEqualToString: @""]) {
                strMsg = @"Oops! Something went wrong. Try again later";
            }
            
            UIAlertController * alert=   [UIAlertController
                                          alertControllerWithTitle:LocalizedString(strMsg)
                                          message:@""
                                          preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* yesButton = [UIAlertAction
                                        actionWithTitle:@"OK"
                                        style:UIAlertActionStyleDefault
                                        handler:^(UIAlertAction * action)
                                        {
                sqlObj = [[SQLManager alloc] init] ;
                [sqlObj createAndOpenDatabase] ;
                
                [sqlObj DeleteData] ;
                
                UIStoryboard *story = [UIStoryboard storyboardWithName:@"SecurityCodeViewController" bundle:nil];
                
                SecurityCodeViewController *SecurityCodeViewController = [story instantiateViewControllerWithIdentifier:@"SecurityCodeViewController"];
                
                UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:SecurityCodeViewController];
                
                [[UIApplication sharedApplication].keyWindow setRootViewController:navController];
            }];
            
            [alert addAction:yesButton];
            
            [self presentViewController:alert animated:YES completion:nil];
        } else {
            UIAlertController * alert=   [UIAlertController
                                          alertControllerWithTitle:LocalizedString(@"Oops! Something went wrong. Try again later")
                                          message:@""
                                          preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* yesButton = [UIAlertAction
                                        actionWithTitle:@"OK"
                                        style:UIAlertActionStyleDefault
                                        handler:^(UIAlertAction * action) {
            }];
            [alert addAction:yesButton];
            [self presentViewController:alert animated:YES completion:nil];
        }
    } else if ([strRequestFor isEqualToString:@"OptionData"]) {
        if ([[responseDict objectForKey:@"status"] isEqualToString:@"0"]) {
            
            NSString *strMsg = [responseDict objectForKey:@"msg"];
            
            if (strMsg == nil || [strMsg  isEqualToString: @""]) {
                strMsg = @"Oops! Something went wrong. Try again later";
            }
            
            UIAlertController * alert=   [UIAlertController
                                          alertControllerWithTitle:LocalizedString(strMsg)
                                          message:@""
                                          preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* yesButton = [UIAlertAction
                                        actionWithTitle:@"OK"
                                        style:UIAlertActionStyleDefault
                                        handler:^(UIAlertAction * action)
                                        {
                sqlObj = [[SQLManager alloc] init] ;
                [sqlObj createAndOpenDatabase] ;
                
                [sqlObj DeleteData] ;
                
                UIStoryboard *story = [UIStoryboard storyboardWithName:@"SecurityCodeViewController" bundle:nil];
                
                SecurityCodeViewController *SecurityCodeViewController = [story instantiateViewControllerWithIdentifier:@"SecurityCodeViewController"];
                
                UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:SecurityCodeViewController];
                
                [[UIApplication sharedApplication].keyWindow setRootViewController:navController];
            }];
            
            [alert addAction:yesButton];
            
            [self presentViewController:alert animated:YES completion:nil];
        } else {
            globalObject = [[GlobalAssistant alloc] init] ;
            
            [globalObject removeActivityIndicator];
            
            arrTblData = responseDict[@"data"];
            
            for (int i=0; i<arrTblData.count; i++) {
                [arrTitle addObject:@""];
            }
            
            [tblView reloadData];
        }
    } 
}
-(void)AlertForLogin
{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Login"
                                                                   message:nil
                                                            preferredStyle:UIAlertControllerStyleAlert];
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        // optionally configure the text field
        
        textField.placeholder = @"Username" ;
        textField.borderStyle = UITextBorderStyleRoundedRect ;
        textField.font = [UIFont systemFontOfSize:16.0] ;
        textField.keyboardType = UIKeyboardTypeAlphabet;
        textField.returnKeyType = UIReturnKeySend ;
        textField.delegate = self ;
        
    }];
    
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textFieldPass) {
        // optionally configure the text field
        
        textFieldPass.placeholder = @"Password" ;
        textFieldPass.secureTextEntry = YES;
        textFieldPass.borderStyle = UITextBorderStyleRoundedRect ;
        textFieldPass.font = [UIFont systemFontOfSize:16.0] ;
        textFieldPass.keyboardType = UIKeyboardTypeAlphabet;
        textFieldPass.returnKeyType = UIReturnKeySend ;
        textFieldPass.delegate = self ;
        
    }];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Login"
                                                       style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction *action) {
        NSArray * textfields = alert.textFields;
        UITextField * namefield = textfields[0];
        UITextField * passfield = textfields[1];
        
        strUsername = namefield.text ;
        strPassword = passfield.text ;
        
        [self loginAction];
        
    }];
    
    
    [alert addAction:okAction];
    
    [self presentViewController:alert animated:YES completion:nil];
    
    
}

-(void)loginAction
{
    if ([strUsername isEqualToString:@""] || [strPassword isEqualToString:@""])
    {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:@"Please Enter Valid Username and Password."                                     message:@""
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
            [self AlertForLogin] ;
            
        }];
        
        [alert addAction:yesButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
    else
    {
        [self loginRequest];
    }
}


#pragma mark - Data Post

-(void)loginRequest
{
    if ([globalObject checkInternetConnection]) {
        [globalObject showActivityIndicator];
        
        strRequestFor = @"Login" ;
        
        NSString *str = userLoginURL ;
        NSString *urlString = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *url = [NSURL URLWithString:urlString];
        
        ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
        
        [request setRequestMethod:@"POST"] ;
        [request setDelegate:self] ;
        
        //------- Post Data
        
        // ---------- Initialize SqlObject
        
        sqlObj = [[SQLManager alloc] init] ;
        
        [sqlObj createAndOpenDatabase] ;
        
        NSLog(@"%@",[[sqlObj getAllUserInfo] valueForKey:@"clientID"]);
        
        NSLog(@"%@",[[sqlObj getAllUserInfo] valueForKey:@"userid"]);
        
        NSLog(@"%@",[[UIDevice currentDevice] identifierForVendor]);
        
        NSLog(@"%f",locationManager.location.coordinate.latitude);
        
        NSLog(@"%f",locationManager.location.coordinate.longitude);
        
        // 65D78ADF-32BC-4A73-B7BC-C9ADBC4C9F55
        
        NSString *strLat = [NSString stringWithFormat:@"%f",locationManager.location.coordinate.latitude];
        
        NSString *strLong = [NSString stringWithFormat:@"%f",locationManager.location.coordinate.longitude];
        
        [request setPostValue:[[sqlObj getAllUserInfo] valueForKey:@"clientID"] forKey:@"client_id"] ;
        
        [request setPostValue:[[sqlObj getAllUserInfo] valueForKey:@"userid"] forKey:@"userid"] ;
        
        [request setPostValue:strUsername forKey:@"username"] ;
        
        [request setPostValue:strPassword forKey:@"password"] ;
        
        [request setPostValue:strLat forKey:@"lat"] ;
        
        [request setPostValue:strLong forKey:@"long"] ;
        
        // [request setPostValue:[[[UIDevice currentDevice] identifierForVendor] UUIDString] forKey:@"DeviceID"] ;
        
        [request setPostValue:@"IOS" forKey:@"source"] ;
        
        [request setTimeOutSeconds:180] ;
        
        //-------
        
        [request startAsynchronous] ;
    } else {
        [globalObject removeActivityIndicator];
    }
}

-(void)btnCheck_click:(UIButton *)sender {
    sender.selected = !sender.selected;
    if (sender.selected) {
        [arrSelected addObject:[NSString stringWithFormat:@"%ld",sender.tag]];
    } else {
        [arrSelected removeObject:[NSString stringWithFormat:@"%ld",sender.tag]];
    }
    
    PoleOptionCell *cell = [tblView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:sender.tag inSection:0]];
    
    if ([arrSelected containsObject:[NSString stringWithFormat:@"%ld",sender.tag]]){
        cell.txtField.hidden = false;
    } else {
        cell.txtField.hidden = true;
    }
}

#pragma mark- TableView Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return  arrTblData.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    PoleOptionCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell = [[PoleOptionCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    
    cell.lblNm.text = arrTblData[indexPath.row][@"value"];
    
    cell.txtField.tag = indexPath.row;
    cell.txtField.delegate = self;
    cell.txtField.hidden   = true;
    cell.txtField.text = arrTitle[indexPath.row];
    cell.txtField.tag = indexPath.row;
    cell.txtField.placeholder = LocalizedString(@"Make");
    
    cell.btnCheck.tag = indexPath.row;
    [cell.btnCheck addTarget:self action:@selector(btnCheck_click:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.btnCheck.selected = false;
    
    if ([arrSelected containsObject:[NSString stringWithFormat:@"%ld",(long)indexPath.row]]){
        cell.btnCheck.selected = true;
        cell.txtField.hidden   = false;
    }
    
    if (_isFrmEditView) {
        if (_isEditable) {
            
        } else {
            [cell.lblNm setUserInteractionEnabled:false];
            [cell.btnCheck setUserInteractionEnabled:false];
            [cell.txtField setUserInteractionEnabled:false];
        }
    }
    
    return  cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return UITableViewAutomaticDimension;
}
-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return UITableViewAutomaticDimension;
}

#pragma mark - textview Delegate
- (BOOL)textView:(UITextView *)txtView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if( [text rangeOfCharacterFromSet:[NSCharacterSet newlineCharacterSet]].location == NSNotFound ) {
        return YES;
    }
    [txtView resignFirstResponder];
    return NO;
}
- (BOOL)textViewShouldEndEditing:(UITextView *)textView {
    if ([textView.text isEqualToString:@""]){
        textView.text = LocalizedString(@"Enter notes here");
    }
    return true;
}
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    if ([textView.text isEqualToString:LocalizedString(@"Enter notes here")]) {
        textView.text = @"";
    }
    
    return YES;
}

#pragma mark - textField Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return true;
}
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    if ([arrSelected containsObject:[NSString stringWithFormat:@"%ld",(long)textField.tag]]){
        arrTitle[textField.tag] = textField.text;
    }
    return true;
}

-(void)eventof:(NSString *)strEventName {
    dispatch_async(dispatch_get_main_queue(), ^{
        NSString *strClientID = [[sqlObj getAllUserInfo] valueForKey:@"clientID"];
        NSString *strUserID = [[sqlObj getAllUserInfo] valueForKey:@"userid"];
        
        [FIRAnalytics logEventWithName:kFIREventSelectContent
                            parameters:@{
                                kFIRParameterItemName:[NSString stringWithFormat:@"%@_%@_%@",strClientID, strUserID, strEventName],
                            }];
    });
}

@end
