//
//  MapViewController.m
//  SLCScanner
//
//  Created by INT MAC 2015 on 23/02/18.
//  Copyright © 2018 INT. All rights reserved.
//

#import "MapViewController.h"

#import "AppDelegate.h"
#import "TableViewCell.h"
#import "LocalizeHelper.h"
#import "PoleDataViewController.h"
#import "ScanPoleIDViewController.h"
#import "SecurityCodeViewController.h"

#import <GoogleMaps/GoogleMaps.h>
#import <FirebaseAnalytics/FirebaseAnalytics.h>

@interface MapViewController () <GMSMapViewDelegate> {
    CLLocationManager   *locationManager;
    
    NSString            *strlat ;
    NSString            *strlong ;
    NSString            *strRequestFor ;
    
    IBOutlet UILabel    *lblSatelite;
    IBOutlet UILabel    *lblAccuracy;
    IBOutlet UILabel    *lblLat;
    IBOutlet UILabel    *lblLong;
    
    IBOutlet UIButton   *btnAccept;
    IBOutlet UIButton   *btnSatelliteInfo;
    IBOutlet UIButton   *btnAccuracyInfo;
    
    IBOutlet UIImageView *imgGlobe;
    
    MKPointAnnotation   *annotation ;
    
    AppDelegate         *appDelegate;
    IBOutlet GMSMapView *googlMapView;
    GMSMarker           *marker;
    
    MKTileOverlayRenderer   *tileRender;
    MKTileOverlay           *overLay;
    MKMapRect               boundingMapRect;
    
    int                 currentMapTypeIndex;
}

@end

@implementation MapViewController
@synthesize strSLCID, mapView, lblSLCID, strMacAddress, lblMacID ;
@synthesize sqlObj, globalObject ;


#pragma mark :- viewDidLoad
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self eventof:@"Location"];
    
    //googlMapView = [GMSMapView mapWithFrame:self.view.bounds camera:nil];
    //[self.view addSubview:googlMapView];
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    mapView.hidden      = true;
    googlMapView.hidden = true;
//    googlMapView.isMyLocationEnabled = true;
    
    // --------- Initialize GlobalAssistant
    
    globalObject = [[GlobalAssistant alloc] init] ;
    
    //--------------------------------
    
    self.mapTypes = @[@"Satellite",@"Standard", @"Hybrid"];
    
    //--------------------------------
    
    /*   myLabel.hidden = NO;
     
     myLabel.alpha = 0.8f;
     
     [UIView animateWithDuration:5.0 delay:2.0 options:0 animations:^{
     
     myLabel.alpha = 0.0f;
     } completion:^(BOOL finished) {
     
     myLabel.hidden = YES;
     }];*/
    
    //--------------------------------
    
    UIImage* imageAlert = [UIImage imageNamed:@"Map.png"];
    CGRect frameimg = CGRectMake(0,0,25,25);
    
    UIButton *alertButton = [[UIButton alloc] initWithFrame:frameimg];
    [alertButton setBackgroundImage:imageAlert forState:UIControlStateNormal];
    [alertButton addTarget:self action:@selector(AlertAction)
          forControlEvents:UIControlEventTouchUpInside];
    [alertButton setShowsTouchWhenHighlighted:YES];
    
    UIImage* imageinfo = [UIImage imageNamed:@"info_white"];
    CGRect infoimg = CGRectMake(0,0,25,25);
    
    UIButton *infoButton = [[UIButton alloc] initWithFrame:infoimg];
    [infoButton setBackgroundImage:imageinfo forState:UIControlStateNormal];
    [infoButton addTarget:self action:@selector(infoClick)
         forControlEvents:UIControlEventTouchUpInside];
    [infoButton setShowsTouchWhenHighlighted:YES];
    
    UIImage* imgback = [UIImage imageNamed:@"left_arrow"];
    CGRect frameimgBk = CGRectMake(0,0,25,25);
    UIBarButtonItem *notificationbutton =[[UIBarButtonItem alloc] initWithCustomView:alertButton];
    self.navigationItem.rightBarButtonItem = notificationbutton;
    UIButton *btnBack = [[UIButton alloc] initWithFrame:frameimgBk];
    [btnBack setBackgroundImage:imgback forState:UIControlStateNormal];
    [btnBack addTarget:self action:@selector(btnBackClick)
      forControlEvents:UIControlEventTouchUpInside];
    [btnBack setShowsTouchWhenHighlighted:YES];
    
    UIBarButtonItem *bkBtn =[[UIBarButtonItem alloc] initWithCustomView:btnBack];
    self.navigationItem.leftBarButtonItems = @[bkBtn];
    
    //UIBarButtonItem *notificationbutton =[[UIBarButtonItem alloc] initWithCustomView:alertButton];
    //UIBarButtonItem *infoActionButton =[[UIBarButtonItem alloc] initWithCustomView:infoButton];
    //self.navigationItem.rightBarButtonItems = [[NSArray alloc] initWithObjects: infoActionButton, nil];
    
    [globalObject buttonWithCornerRadious:24.0 btnView:btnAccept];
}
-(void)btnBackClick{
    [self eventof:@"LocationBack"];
    [self.navigationController popViewControllerAnimated:true];
}
-(void)viewWillAppear:(BOOL)animated
{
    [googlMapView clear];
    mapView.delegate = self;
    currentMapTypeIndex = appDelegate.strSelectedMap;
    if ((currentMapTypeIndex != 6) && (currentMapTypeIndex > 2)) {
        mapView.hidden      = true;
        googlMapView.hidden = false;
        [self mapSelectionType];
    } else {
        mapView.hidden      = false;
        googlMapView.hidden = true;
        [self mapSelectionType];
    }
    [self PinMarker] ;
    [self localizationSetup];
}

-(void)localizationSetup {
    
    UILabel  *myLabel= [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width /2 - 155 , mapView.frame.origin.y + 20 , 310 , 50)];
    
    myLabel.text = LocalizedString(@"Please hold the pin and move it to your exact location");
    
    myLabel.numberOfLines = 0 ;
    
    myLabel.textAlignment = NSTextAlignmentCenter ;
    
    myLabel.textColor = [UIColor blackColor];
    
    myLabel.layer.masksToBounds = YES;
    
    myLabel.layer.cornerRadius = 8.0 ;
    
    myLabel.backgroundColor = [UIColor whiteColor];
    
    [self.view addSubview:myLabel];
    
    lblSLCID.text = [NSString stringWithFormat:@"%@ : %@", LocalizedString(@"SLC ID"), strSLCID];
    
    NSString *strMACID = [[self->sqlObj getAllUserInfo] valueForKey:@"ScanPH"];
    
    lblMacID.text = [NSString stringWithFormat:@"%@ : %@", LocalizedString(strMACID), strMacAddress];
    
    self.title = LocalizedString(@"LOCATION");
    [btnAccept setTitle:LocalizedString(@"ACCEPT") forState:UIControlStateNormal];
    
}

#pragma mark - Map Delegate

- (MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id<MKOverlay>)overlay {
    return tileRender;
}
-(void)setupOSMMap {
    if (currentMapTypeIndex == 6) {
        overLay = [[MKTileOverlay alloc] initWithURLTemplate:@"https://tile.openstreetmap.org/{z}/{x}/{y}.png"];//];
        [overLay setCanReplaceMapContent:true];
        [mapView addOverlay:overLay];
        tileRender = [[MKTileOverlayRenderer alloc] initWithTileOverlay:overLay];
        [self.mapView reloadInputViews];
    } else {
        [self.mapView removeOverlay:overLay];
        [overLay setCanReplaceMapContent:false];
        [tileRender setNeedsDisplay];
        [self.mapView reloadInputViews];
        [mapView setNeedsDisplay];
    }
}

- (void)mapSelectionType
{
    NSLog(@"Dropdown view did hide");
    
    switch (currentMapTypeIndex) {
        case 0:
            self.mapView.mapType = MKMapTypeSatellite ;
            break;
        case 1:
            self.mapView.mapType = MKMapTypeStandard ;
            break;
        case 2:
            self.mapView.mapType = MKMapTypeHybrid ;
            break;
        case 3:
            [googlMapView setMapType:kGMSTypeSatellite];
            break;
        case 4:
            [googlMapView setMapType:kGMSTypeTerrain];
            break;
        case 5:
            [googlMapView setMapType:kGMSTypeHybrid];
            break;
        case 6:
            [self.mapView reloadInputViews];
            break;
        default:
            break;
    }
}


#pragma mark - Mapview Method

-(void)PinMarker {
    [self setupOSMMap];
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self ;
    [locationManager requestWhenInUseAuthorization] ;
    locationManager.distanceFilter = kCLDistanceFilterNone; // whenever we move
    locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation ;
    
    [locationManager startUpdatingLocation];
    
    strlat = [NSString stringWithFormat:@"%f",locationManager.location.coordinate.latitude];
    
    strlong = [NSString stringWithFormat:@"%f",locationManager.location.coordinate.longitude];
    
    if (currentMapTypeIndex != 6 && currentMapTypeIndex > 2) {
        marker = [[GMSMarker alloc] init];
        marker.position = CLLocationCoordinate2DMake(locationManager.location.coordinate.latitude, locationManager.location.coordinate.longitude);
        marker.icon = [UIImage imageNamed:@"pin"];
        [marker setDraggable:true];
        GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:locationManager.location.coordinate.latitude
                                                                longitude:locationManager.location.coordinate.longitude
                                                                     zoom:16];
        [googlMapView animateToCameraPosition:camera];
        marker.map = googlMapView;
    } else {
        [mapView removeAnnotations:mapView.annotations];
        
        MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(locationManager.location.coordinate, 800, 800);
        [mapView setRegion:[mapView regionThatFits:region] animated:YES];
        
        annotation = [[MKPointAnnotation alloc] init] ;
        
        annotation.coordinate = locationManager.location.coordinate ;
        
        [mapView addAnnotation:annotation ];
        
        /*NSLog(@"%f",locationManager.location.horizontalAccuracy);
         NSLog(@"%f",locationManager.location.verticalAccuracy);
         NSLog(@"%f",locationManager.location.altitude);
         NSLog(@"%f",locationManager.location.coordinate);*/
    }
    [self AddressDataPost] ;
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations {
    if (manager.location.horizontalAccuracy < 0)
    {
        lblSatelite.text = LocalizedString(@"No Signal");
    }
    else if (manager.location.horizontalAccuracy > 163)
    {
        lblSatelite.text = LocalizedString(@"Poor");
    }
    else if (manager.location.horizontalAccuracy > 48)
    {
        lblSatelite.text = LocalizedString(@"Average");
    }
    else
    {
        lblSatelite.text = LocalizedString(@"Strong");
    }
    lblLat.text     = [NSString stringWithFormat:@"%.4f",manager.location.coordinate.latitude];
    lblLong.text    = [NSString stringWithFormat:@"%.4f",manager.location.coordinate.longitude];
    lblAccuracy.text = [NSString stringWithFormat:@"%.0f M",manager.location.horizontalAccuracy];
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation
{
    MKPinAnnotationView *pav = nil;
    
    if(annotation == mapView.userLocation)
    {
        return nil;
    }
    
    static NSString *reuseId = @"pin";
    pav = (MKPinAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:reuseId];
    if (pav == nil)
    {
        pav = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:reuseId];
        [pav setPinTintColor:[UIColor redColor]];
        UIImageView *iconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Pole_Blue.png"]];
        pav.leftCalloutAccessoryView = iconView;
        pav.canShowCallout = YES;
        pav.draggable = YES ;
        pav.animatesDrop = YES ;
        pav.selected = YES ;
    }
    else
    {
        pav.annotation = annotation;
    }
    
    return pav;
}


- (void)mapView:(MKMapView *)mapView
 annotationView:(MKAnnotationView *)annotationView
didChangeDragState:(MKAnnotationViewDragState)newState
   fromOldState:(MKAnnotationViewDragState)oldState
{
    if (newState == MKAnnotationViewDragStateEnding)
    {
        CLLocationCoordinate2D droppedAt = annotationView.annotation.coordinate;
        NSLog(@"Pin dropped at %f,%f", droppedAt.latitude, droppedAt.longitude);
        
        strlat = [NSString stringWithFormat:@"%f",droppedAt.latitude];
        
        strlong = [NSString stringWithFormat:@"%f",droppedAt.longitude];
        
        [self AddressDataPost] ;
    }
}

- (void)mapView:(GMSMapView *)mapView didEndDraggingMarker:(GMSMarker *)marker {
    strlat = [NSString stringWithFormat:@"%f",marker.position.latitude];
    strlong = [NSString stringWithFormat:@"%f",marker.position.longitude];
    NSLog(@"%f",marker.position.latitude);
    NSLog(@"%f",marker.position.longitude);
    [self AddressDataPost] ;
}

#pragma mark - Button Action
-(IBAction)btnSatelliteInfo_Click:(UIButton *)sender {
    
    [self eventof:@"LocationStrength"];
    
    //NSString *strSatellite  = @"Satellites : ";
    NSString *strSatelliteMsg = LocalizedString(@"GPS gets a signal from GPS satellites to calculate your position. Here the value is calculated based on the number of satellites connected to and the strength of the signal from these satellites.");
    
    NSMutableAttributedString *finalStr = [[NSMutableAttributedString alloc] init];
    //[finalStr appendAttributedString:[self convertTobold:strSatellite]];
    [finalStr appendAttributedString:[self convertToNormal:strSatelliteMsg]];
    
    UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:LocalizedString(@"Satellites") message:@"" preferredStyle:UIAlertControllerStyleAlert];
    //[alertVC setValue:[self convertTobold:@"Satellites \n"] forKey:@"attributedTitle"];
    [alertVC setValue:finalStr forKey:@"attributedMessage"];
    
    UIAlertAction *defaultAction = [UIAlertAction actionWithTitle: @"OK" style: UIAlertActionStyleDefault handler: nil];
    [alertVC addAction: defaultAction];
    
    [self presentViewController:alertVC animated:true completion:nil];
}
-(IBAction)btnAccuracyInfo_Click:(UIButton *)sender {
    [self eventof:@"LocationAccuracy"];
    
    //NSString *strAccuracy  = @"Accuracy : ";
    NSString *strAccuracyMsg = LocalizedString(@"Accuracy refers to the degree of closeness to the actual position. Lower the number, better the accuracy!");
    NSMutableAttributedString *finalStr = [[NSMutableAttributedString alloc] init];
    
    //[finalStr appendAttributedString:[self convertTobold:strAccuracy]];
    [finalStr appendAttributedString:[self convertToNormal:strAccuracyMsg]];
    
    UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:LocalizedString(@"Accuracy") message:@"" preferredStyle:UIAlertControllerStyleAlert];
    //[alertVC setValue:[self convertTobold:@"Information \n"] forKey:@"attributedTitle"];
    [alertVC setValue:finalStr forKey:@"attributedMessage"];
    
    UIAlertAction *defaultAction = [UIAlertAction actionWithTitle: @"OK" style: UIAlertActionStyleDefault handler: nil];
    [alertVC addAction: defaultAction];
    
    [self presentViewController:alertVC animated:true completion:nil];
    
}
- (IBAction)AcceptButton:(id)sender
{
    
    [self eventof:@"LocationAccept"];
    
    UIStoryboard *story = [UIStoryboard storyboardWithName:@"AddressViewController" bundle:nil];
    
    AddressViewController *AddressViewController = [story instantiateViewControllerWithIdentifier:@"AddressViewController"];
    
    AddressViewController.strMacID = strMacAddress ;
    
    AddressViewController.strSlcID = strSLCID ;
    
    AddressViewController.strLat = strlat ;
    
    AddressViewController.strLong = strlong ;
    
    [self.navigationController pushViewController:AddressViewController animated:YES];
}

#pragma mark - DROPDOWN VIEW

- (void)showDropDownViewFromDirection:(LMDropdownViewDirection)direction
{
    // Init dropdown view
    if (!self.dropdownView) {
        self.dropdownView = [LMDropdownView dropdownView];
        self.dropdownView.delegate = self;
        
        // Customize Dropdown style
        self.dropdownView.closedScale = 0.85;
        self.dropdownView.blurRadius = 5;
        self.dropdownView.blackMaskAlpha = 0.5;
        self.dropdownView.animationDuration = 0.5;
        self.dropdownView.animationBounceHeight = 20;
    }
    self.dropdownView.direction = direction;
    
    // Show/hide dropdown view
    if ([self.dropdownView isOpen]) {
        [self.dropdownView hide];
    }
    else {
        switch (direction) {
            case LMDropdownViewDirectionTop: {
                self.dropdownView.contentBackgroundColor = [UIColor whiteColor];
                
                [self.dropdownView showFromNavigationController:self.navigationController
                                                withContentView:self.menuTableView];
                break;
            }
            case LMDropdownViewDirectionBottom: {
                self.dropdownView.contentBackgroundColor = [UIColor whiteColor];
                
                /*   CGPoint origin = CGPointMake(0, CGRectGetHeight(self.navigationController.view.bounds) - CGRectGetHeight(self.moreBottomView.bounds));
                 [self.dropdownView showInView:self.navigationController.view
                 withContentView:self.moreBottomView
                 atOrigin:origin];*/
                break;
            }
            default:
                break;
        }
    }
}

- (void)dropdownViewWillShow:(LMDropdownView *)dropdownView
{
    NSLog(@"Dropdown view will show");
}

- (void)dropdownViewDidShow:(LMDropdownView *)dropdownView
{
    NSLog(@"Dropdown view did show");
}

- (void)dropdownViewWillHide:(LMDropdownView *)dropdownView
{
    NSLog(@"Dropdown view will hide");
}

- (void)dropdownViewDidHide:(LMDropdownView *)dropdownView
{
    NSLog(@"Dropdown view did hide");
    
    switch (currentMapTypeIndex) {
        case 0:
            self.mapView.mapType = MKMapTypeSatellite ;
            break;
        case 1:
            self.mapView.mapType = MKMapTypeStandard ;
            break;
        case 2:
            self.mapView.mapType = MKMapTypeHybrid ;
            break;
        default:
            break;
    }
}


#pragma mark - MENU TABLE VIEW

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.mapTypes count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (!cell) {
        cell = [[TableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    }
    
    cell.menuItemLabel.text = [self.mapTypes objectAtIndex:indexPath.row];
    cell.selectedMarkView.hidden = (indexPath.row != currentMapTypeIndex);
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    currentMapTypeIndex = (int)indexPath.row;
    
    [self.dropdownView hide];
}


#pragma mark - MAP VIEW DELEGATE
-(void)infoClick {
    
    
    
    /*Satellites: The GPS gets a signal from each GPS satellite to calculate your position. The satellites transmit the exact time the signals are sent.
     
     
     Accuracy: The accuracy refers to the degree of closeness to the actual position.*/
    
    UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:@"Information" message:@"" preferredStyle:UIAlertControllerStyleAlert];
    
    /*NSString *strSatellite  = @"Satellites :";
     NSString *strSatelliteMsg = @"The GPS gets a signal from each GPS satellite to calculate your position. The satellites transmit the exact time the signals are sent. \n\n";
     
     NSString *strAccuracy  = @"Accuracy :";
     NSString *strAccuracyMsg = @"The accuracy refers to the degree of closeness to the actual position.";*/
    
    NSString *strSatellite  = @"Satellites : ";
    NSString *strSatelliteMsg = @"GPS gets a signal from GPS satellites to calculate your position. Here the value is calculated based on the number of satellites connected to and the strength of the signal from these satellites.";
    
    NSString *strAccuracy  = @"Accuracy : ";
    NSString *strAccuracyMsg = @"Accuracy refers to the degree of closeness to the actual position. Lower the number, better the accuracy!";
    
    NSMutableAttributedString *finalStr = [[NSMutableAttributedString alloc] init];
    [finalStr appendAttributedString:[self convertTobold:strSatellite]];
    [finalStr appendAttributedString:[self convertToNormal:strSatelliteMsg]];
    
    [finalStr appendAttributedString:[self convertTobold:strAccuracy]];
    [finalStr appendAttributedString:[self convertToNormal:strAccuracyMsg]];
    
    
    [alertVC setValue:[self convertTobold:@"Information \n"] forKey:@"attributedTitle"];
    [alertVC setValue:finalStr forKey:@"attributedMessage"];
    
    UIAlertAction *defaultAction = [UIAlertAction actionWithTitle: @"OK" style: UIAlertActionStyleDefault handler: nil];
    [alertVC addAction: defaultAction];
    
    [self presentViewController:alertVC animated:true completion:nil];
}

-(NSMutableAttributedString *)convertTobold:(NSString *)strNormal {
    NSMutableAttributedString *hogan = [[NSMutableAttributedString alloc] initWithString:strNormal];
    [hogan addAttribute:NSFontAttributeName
                  value:[UIFont boldSystemFontOfSize:16.0]
                  range:NSMakeRange(0, strNormal.length)];
    return hogan;
}

-(NSMutableAttributedString *)convertToNormal:(NSString *)strNormal {
    NSMutableAttributedString *hogan = [[NSMutableAttributedString alloc] initWithString:strNormal];
    [hogan addAttribute:NSFontAttributeName
                  value:[UIFont systemFontOfSize:15.0]
                  range:NSMakeRange(0, strNormal.length)];
    return hogan;
}

-(void)AlertAction {
    [self eventof:@"LocationGlob"];
    [self.tabBarController setSelectedIndex:3];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    self.menuTableView.frame = CGRectMake(CGRectGetMinX(self.menuTableView.frame),
                                          CGRectGetMinY(self.menuTableView.frame),
                                          CGRectGetWidth(self.view.bounds),
                                          MIN(CGRectGetHeight(self.view.bounds) - 50, self.mapTypes.count * 50));
}


#pragma mark - Data Post For Address

-(void)AddressDataPost
{
    //[globalObject showActivityIndicator];
    if ([globalObject checkInternetConnection]) {
        strRequestFor = @"Address" ;
        
        NSString *str =  addressURL;
        NSString *urlString = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *url = [NSURL URLWithString:urlString];
        
        ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
        
        [request setRequestMethod:@"POST"] ;
        [request setDelegate:self] ;
        
        //------- Post Data
        
        sqlObj = [[SQLManager alloc] init] ;
        
        [sqlObj createAndOpenDatabase] ;
        
        [request setPostValue:strlat forKey:@"lat"] ;
        
        [request setPostValue:strlong forKey:@"lng"] ;
        
        [request setPostValue:[[sqlObj getAllUserInfo] valueForKey:@"clientID"] forKey:@"client_id"] ;
        
        [request setPostValue:[[sqlObj getAllUserInfo] valueForKey:@"userid"] forKey:@"user_id"] ;
        
        [request startAsynchronous] ;
    } else {
        [globalObject removeActivityIndicator];
    }
}

-(void)requestFailed:(ASIHTTPRequest *)request {
    NSError *error = [request error];
    NSLog(@"error : %@" , error.localizedDescription);
    [globalObject removeActivityIndicator];
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:error.localizedDescription
                                  message:@""
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"OK"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action)
                                {
                                }];
    
    [alert addAction:yesButton];
    
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)requestFinished:(ASIHTTPRequest *)request {
    NSString *responseString = [request responseString];
    NSMutableDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:[responseString dataUsingEncoding:NSUTF8StringEncoding] options:0 error:nil];
    NSLog(@"Responce Dictionary : %@", responseDict);
    
    // --------- Initialize GlobalAssistant
    
    globalObject = [[GlobalAssistant alloc] init] ;
    
    [globalObject removeActivityIndicator];
    
    if ([[responseDict objectForKey:@"status"] isEqualToString:@"success"]) {
        annotation.title    = [responseDict objectForKey:@"shortaddress"] ;
        marker.title        = [responseDict objectForKey:@"shortaddress"];
        [googlMapView setSelectedMarker:marker];
    } else if ([[responseDict objectForKey:@"status"] isEqualToString:@"0"]) {
        
        NSString *strMsg = [responseDict objectForKey:@"msg"];
        
        if (strMsg == nil || [strMsg  isEqualToString: @""]) {
            strMsg = @"Oops! Something went wrong. Try again later";
        }
        
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:LocalizedString(strMsg)
                                      message:@""
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
            sqlObj = [[SQLManager alloc] init] ;
            [sqlObj createAndOpenDatabase] ;
            
            [sqlObj DeleteData] ;
            
            UIStoryboard *story = [UIStoryboard storyboardWithName:@"SecurityCodeViewController" bundle:nil];
            
            SecurityCodeViewController *SecurityCodeViewController = [story instantiateViewControllerWithIdentifier:@"SecurityCodeViewController"];
            
            UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:SecurityCodeViewController];
            
            [[UIApplication sharedApplication].keyWindow setRootViewController:navController];
        }];
        
        [alert addAction:yesButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    } else {
        annotation.title    = [responseDict objectForKey:@"msg"] ;
        marker.title        = [responseDict objectForKey:@"msg"];
        [googlMapView setSelectedMarker:marker];
    }
}

-(void)eventof:(NSString *)strEventName {
    dispatch_async(dispatch_get_main_queue(), ^{
    NSString *strClientID = [[sqlObj getAllUserInfo] valueForKey:@"clientID"];
    NSString *strUserID = [[sqlObj getAllUserInfo] valueForKey:@"userid"];
    
    [FIRAnalytics logEventWithName:kFIREventSelectContent
                        parameters:@{
                                     kFIRParameterItemName:[NSString stringWithFormat:@"%@_%@_%@",strClientID, strUserID, strEventName],
                                     }];
    });
}

@end
