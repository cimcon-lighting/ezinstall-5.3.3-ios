//
//  PoleDataViewController.m
//  SLCScanner
//
//  Created by INT MAC 2015 on 27/02/18.
//  Copyright © 2018 INT. All rights reserved.
//

#import "PoleDataViewController.h"

#import "ViewController.h"
#import "LocalizeHelper.h"
#import "PoleViewController.h"
#import "SecurityCodeViewController.h"
#import <FirebaseAnalytics/FirebaseAnalytics.h>

@interface PoleDataViewController () <UITextFieldDelegate>
{
    UIButton *Databutton ;
    
    UIToolbar *toolbar ;
    
    //--------------------------------------
    
    NSString *strSelectedTime ;
    
    //--------------------------------------
    
    int selectedIndex ;
    int tagValue ;
    
    NSString *StrRequestFor ;
    NSString *StrNewAddedData ;
    
    NSString *StrForSkipClick ;
    
    //-------------------------------------------
    
    NSString *strUsername ;
    
    NSString *strPassword ;
    
    CLLocationManager *locationManager;
    
    NSString *strIDForUpdateData ;
    
}
@end

@implementation PoleDataViewController


@synthesize strSlcID, strMacID, strLong, strLat, strPoleID, strAddress, strPreviousEntry ;
@synthesize lblSelectDate ;
@synthesize btnSelectDate, btnSave ;
@synthesize globalObject, sqlObj, datePicker, PoleImg, dataSegmentObj, lblDataCopy ;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // --------- Initialize GlobalAssistant
    
    [self eventof:@"PoleDetailsSave"];
    
    globalObject = [[GlobalAssistant alloc] init] ;
    
    // ---------- Initialize SqlObject
    
    sqlObj = [[SQLManager alloc] init] ;
    
    [sqlObj createAndOpenDatabase] ;
    
    strSelectedTime = @"" ;
    
    //Lat long code
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self ;
    [locationManager requestWhenInUseAuthorization] ;
    locationManager.distanceFilter = kCLDistanceFilterNone; // whenever we move
    locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation ;
    
    [locationManager startUpdatingLocation];
    
    //--------------------------------
    
    dataSegmentObj.selectedSegmentIndex = 1 ;
    NSLog(@"%@",[[UIDevice currentDevice] systemVersion]);
    NSString *ver = [[UIDevice currentDevice] systemVersion];
    float ver_float = [ver floatValue];
    if (ver_float > 13.0) {
        [dataSegmentObj setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]} forState:UIControlStateSelected];
        UIColor *clr = [[UIColor alloc] initWithRed:1.0/255.0 green:49.0/255.0 blue:123.0/255.0 alpha:1.0];
        [dataSegmentObj setTitleTextAttributes:@{NSForegroundColorAttributeName : clr} forState:UIControlStateNormal];
    }
    dataSegmentObj.layer.cornerRadius = 8.0;
    dataSegmentObj.layer.borderColor = [UIColor colorWithRed:1.0/255.0 green:49.0/255.0 blue:123.0/255.0 alpha:1.0].CGColor ;
    dataSegmentObj.layer.borderWidth = 1.0f;
    dataSegmentObj.layer.masksToBounds = YES;
    
    UIFont *font = [UIFont boldSystemFontOfSize:16.0f];
    NSDictionary *attributes = [NSDictionary dictionaryWithObject:font
                                                           forKey:NSFontAttributeName];
    [dataSegmentObj setTitleTextAttributes:attributes
                                  forState:UIControlStateNormal];
    
    [dataSegmentObj addTarget:self action:@selector(SegmentChangeViewValueChanged:) forControlEvents:UIControlEventValueChanged];
    
    strPreviousEntry = @"No";
    
    [globalObject buttonWithCornerRadious:22.0 btnView:btnSave];
    
}

-(void)viewWillAppear:(BOOL)animated {
    [self getOtherData] ;
    [self localizationSetup];
}
-(void)localizationSetup {
    
    CGRect frameimg = CGRectMake(0,0,30,25);
    UIButton *alertButton = [[UIButton alloc] initWithFrame:frameimg];
    [alertButton setTitle:LocalizedString(@"SKIP") forState:UIControlStateNormal];
    [alertButton addTarget:self action:@selector(AlertAction)
          forControlEvents:UIControlEventTouchUpInside];
    [alertButton setShowsTouchWhenHighlighted:YES];
    
    UIBarButtonItem *notificationbutton =[[UIBarButtonItem alloc] initWithCustomView:alertButton];
    self.navigationItem.rightBarButtonItem = notificationbutton;
    
    UIImage* imgback = [UIImage imageNamed:@"left_arrow"];
    CGRect frameimgBk = CGRectMake(0,0,25,25);
    
    UIButton *btnBack = [[UIButton alloc] initWithFrame:frameimgBk];
    [btnBack setBackgroundImage:imgback forState:UIControlStateNormal];
    [btnBack addTarget:self action:@selector(btnBackClick)
      forControlEvents:UIControlEventTouchUpInside];
    [btnBack setShowsTouchWhenHighlighted:YES];
    
    UIBarButtonItem *bkBtn =[[UIBarButtonItem alloc] initWithCustomView:btnBack];
    self.navigationItem.leftBarButtonItems = @[bkBtn];
    
    lblDataCopy.text = LocalizedString(@"Copy Pole Data from Last Entry");
    
    self.title = LocalizedString(@"POLE DETAILS");
    lblSelectDate.text = LocalizedString(@"Date Of Installation");
    [btnSave setTitle:LocalizedString(@"SAVE") forState:UIControlStateNormal];
}
-(void)btnBackClick{
    [self eventof:@"PoleDetailsSaveBack"];
    [self.navigationController popViewControllerAnimated:true];
}
-(void)viewWillDisappear:(BOOL)animated
{
    [self removeOldUIForUpdate] ;
    
}

-(void)removeOldUIForUpdate
{
    for (UIView *i in _containerView.subviews)
    {
        if([i isKindOfClass:[UILabel class]])
        {
            UILabel *newLbl = (UILabel *)i;
            if(newLbl.tag == 2000)
            {
                newLbl.text = @"" ;
                [newLbl removeFromSuperview];
            }
        }
        else if([i isKindOfClass:[UIButton class]])
        {
            UIButton *newbtn = (UIButton *)i;
            if(newbtn.tag >= 1000)
            {
                [newbtn setTitle:@"" forState:UIControlStateNormal ];
                [newbtn removeFromSuperview];
            }
        }
    }
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
}

#pragma mark -  Button Action

- (IBAction)SelectDateButton:(id)sender
{
    [self eventof:@"PoleDetailsSaveDate"];
    datePicker = [[UIDatePicker alloc] init];
    datePicker.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    datePicker.datePickerMode = UIDatePickerModeDate ;
    if (@available(iOS 13.4, *)) {
        datePicker.preferredDatePickerStyle = UIDatePickerStyleWheels;
    }
    [datePicker setMaximumDate:[NSDate date]];
    
    datePicker.frame =  CGRectMake(0.0, self.view.frame.size.height - 250, self.view.frame.size.width, 250);
    datePicker.backgroundColor = [UIColor whiteColor];
    
    if ([strPreviousEntry isEqualToString:@"Yes"])
    {
        NSDateFormatter *formatter=[[NSDateFormatter alloc]init];
        [formatter setDateFormat:@"M-d-yyyy"];
        datePicker.date = [formatter dateFromString:strSelectedTime] ;
    }
    
    //  datePicker.minimumDate = [NSDate date];
    
    //*******************
    
    toolbar= [[UIToolbar alloc] initWithFrame:CGRectMake(0,self.view.frame.size.height - 294,self.view.frame.size.width,44)];
    toolbar.barStyle = UIBarStyleBlackOpaque;
    
    
    UIButton* SendButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 0 , 30)];
    [SendButton setTitle:LocalizedString(@"Set") forState:UIControlStateNormal];
    [SendButton addTarget:self action:@selector(SetButton) forControlEvents:UIControlEventTouchDown];
    
    UIBarButtonItem *SendB =[[UIBarButtonItem alloc]initWithCustomView:SendButton];
    
    UIButton* CancelButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 0 , 30)];
    [CancelButton setTitle:LocalizedString(@"Cancel") forState:UIControlStateNormal] ;
    [CancelButton addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchDown];
    UIBarButtonItem *CancelB =[[UIBarButtonItem alloc]initWithCustomView:CancelButton];
    
    [toolbar setItems:[NSArray arrayWithObjects: CancelB,SendB, nil]];
    
    [self.view addSubview:toolbar];
    [self.view addSubview:datePicker];
}

-(void)dismiss{
    
    [datePicker removeFromSuperview];
    [toolbar removeFromSuperview];
    
}

-(void)SetButton{
    
    [datePicker removeFromSuperview];
    [toolbar removeFromSuperview];
    
    NSDateFormatter *formatter=[[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"M-d-yyyy"];
    strSelectedTime = [NSString stringWithFormat:@"%@",[formatter stringFromDate:datePicker.date]] ;
    
    [btnSelectDate setTitle:strSelectedTime forState:UIControlStateNormal] ;
    
}

- (IBAction)SaveButton:(id)sender
{
    [self eventof:@"PoleDetailsSave"];
    
    StrForSkipClick = @"No" ;
    
    NSLog(@"%@",SelectedDataForPostDictionary);
    
    for (UIView *i in _containerView.subviews)  {
         if([i isKindOfClass:[UIButton class]]) {
            UIButton *newbtn = (UIButton *)i;
            if(newbtn.tag >= 1000) {
                int newTagValue = (int)newbtn.tag - 1000;
                NSDictionary *newDataDict = [DataArray objectAtIndex:newTagValue];
                if ([[newDataDict valueForKey:@"isRequire"] isEqualToString:@"1"] && [newbtn.titleLabel.text containsString:@"Select"])  {
                    NSString *strMsg = [NSString stringWithFormat:@"%@ is required", [newDataDict valueForKey:@"AttributeName"]];
                    [globalObject showAlertWithTitle:strMsg message:@""];
                    StrForSkipClick = @"Yes" ;
                    return;
                    break;
                }
            }
        }
    }
    
    [self PostOtherData];
    
}

-(void)AlertAction
{
    StrForSkipClick = @"Yes" ;
    
    [self eventof:@"PoleDetailsSaveSkip"];
    
    [self PostOtherData];
}

-(IBAction)SegmentChangeViewValueChanged:(UISegmentedControl *)SControl
{
    if (SControl.selectedSegmentIndex==0)
    {
        strPreviousEntry = @"Yes" ;
        [self eventof:@"PoleDetailsSaveYes"];
        [self removeOldUIForUpdate] ;
        
        [self getOtherData] ;
    }
    else
    {
        strPreviousEntry = @"No" ;
        [self eventof:@"PoleDetailsSaveNo"];
        [self removeOldUIForUpdate] ;
        
        [self getOtherData] ;
    }
}

#pragma mark - Data Get

-(void)getOtherData
{
    //---------------------- Request for Getting Client data List  ----------------------//
    if ([globalObject checkInternetConnection]) {
        [globalObject showActivityIndicator];
        
        // ---------- Initialize SqlObject
        
        sqlObj = [[SQLManager alloc] init] ;
        
        [sqlObj createAndOpenDatabase] ;
        
        NSString *strURL = [NSString stringWithFormat:@"%@/%@/%@/%@",DynamicUIdataURL,[[sqlObj getAllUserInfo] valueForKey:@"clientID"],[[sqlObj getAllUserInfo] valueForKey:@"MeasurmentUnit"],strPreviousEntry] ;
        
        NSURLConnection_Class *object = [[NSURLConnection_Class alloc] init] ;
        [object RequestForURL:strURL];
        object.delegate = self ;
    } else {
        [globalObject removeActivityIndicator];
    }
    
}

#pragma mark - Custom Delegate [ NSURLConnection ]

-(void)connectionFailWithError:(NSError *)error
{
    NSLog(@"Error: %@",error.localizedDescription) ;
    [globalObject showAlertWithTitle:@"Error" message:LocalizedString(@"No Internet Connection")];
    [globalObject removeActivityIndicator] ;
}

-(void)connectionFinishWithResponse:(NSString *)responseString
{
    NSMutableDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:[responseString dataUsingEncoding:NSUTF8StringEncoding] options:0 error:nil] ;
    
    NSLog(@"%@",responseDict);
    
    DataArray = [[NSMutableArray alloc] init] ;
    
    SelectedDataForPostDictionary = [[NSMutableDictionary alloc] init] ;
    
    DataDictionary = [[NSMutableDictionary alloc] initWithDictionary:responseDict] ;
    
    DataArray = [DataDictionary valueForKey:@"data"] ;
    
    self.containerView.frame = CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, self.containerView.frame.size.height) ;
    
    if([[responseDict valueForKey:@"status"] isEqualToString:@"success"])
    {
        CGRect LabelFrame;
        
        CGRect buttonFrame;
        
        if ([[responseDict objectForKey:@"slccount"] isEqualToString:@"No"])
        {
            lblDataCopy.hidden = YES ;
            
            dataSegmentObj.hidden = YES ;
            
            buttonFrame = CGRectMake(self.containerView.frame.size.width - 164.0f, 15.0, 156 , 34.0f);
            LabelFrame = CGRectMake(7.0f, 15.0, self.containerView.frame.size.width -172.0f, 34.0f);

            
        } else {
            buttonFrame = CGRectMake(self.containerView.frame.size.width - 164.0f, 100.0f, 156 , 34.0f);
            LabelFrame = CGRectMake(7.0f, 100.0f, self.containerView.frame.size.width -172.0f, 34.0f);
        }
        
        
        
        for (int index = 0; index <DataArray.count; index++)
        {
            NSString *strAttKey = [[DataArray objectAtIndex:index] valueForKey:@"AttributeName"];
            NSString *strAttKeyBlnk = [[DataArray objectAtIndex:index] valueForKey:@"AttrKey"];
            if ((![strAttKey.lowercaseString isEqualToString:@"pole image"]
                && ![strAttKey.lowercaseString isEqualToString:@"imagem do pólo"]
                && ![strAttKey.lowercaseString isEqualToString:@"imagen de poste"])
                
                && (![strAttKey.lowercaseString isEqualToString:@"pole id"]
                    && ![strAttKey.lowercaseString isEqualToString:@"polo id"]
                    && ![strAttKey.lowercaseString isEqualToString:@"pólo id"])
                
            && (![strAttKey.lowercaseString isEqualToString:@"notes"]
                && ![strAttKey.lowercaseString isEqualToString:@"notas"]
                && ![strAttKey.lowercaseString isEqualToString:@"notas"]))
            {
                Databutton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
                [Databutton setFrame:buttonFrame];
                [Databutton addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
                [Databutton setTag:index+1000];
                Databutton.titleLabel.font = btnSelectDate.titleLabel.font ;
                Databutton.layer.cornerRadius = 5 ;
                Databutton.layer.borderWidth = 1 ;
                [Databutton setBackgroundColor:btnSelectDate.backgroundColor] ;
                [Databutton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal] ;
                
                if ([strPreviousEntry isEqualToString:@"Yes"])
                {
                    NSString *title = [NSString stringWithFormat:@"%@",[[DataArray objectAtIndex:index] valueForKey:@"Selected"]];
                    [Databutton setTitle:title forState:UIControlStateNormal];
                }
                else
                {
                    NSString *title = [NSString stringWithFormat:@"%@",[[DataArray objectAtIndex:index] valueForKey:@"BtnText"]];
                    [Databutton setTitle:title forState:UIControlStateNormal];
                }
                
                [self.containerView addSubview:Databutton];
                buttonFrame.origin.y+=buttonFrame.size.height+15.0f;
                
                //**********************************************************
                
                UILabel *label = [[UILabel alloc] initWithFrame:LabelFrame] ;
                label.text = [[DataArray objectAtIndex:index] valueForKey:@"AttributeName"] ;
                label.font = lblSelectDate.font ;
                label.textColor = lblSelectDate.textColor ;
                label.tag = 2000 ;
                [self.containerView addSubview:label];
                
                LabelFrame.origin.y+=LabelFrame.size.height+15.0f;
                
                [SelectedDataForPostDictionary setObject:[[DataArray objectAtIndex:index] valueForKey:@"Selected"] forKey:[[DataArray objectAtIndex:index] valueForKey:@"AttrKey"]] ;
                
                NSLog(@"%@",SelectedDataForPostDictionary);
            } else {
                [SelectedDataForPostDictionary setObject:@"" forKey:strAttKeyBlnk];
            }
            
        }
        
        
        lblSelectDate.frame = CGRectMake(7.0f, LabelFrame.origin.y , self.containerView.frame.size.width -172.0f, 34.0f) ;
        
        btnSelectDate.frame = CGRectMake(self.containerView.frame.size.width - 164.0f, buttonFrame.origin.y, 156, 34.0f) ;
        
        btnSave.frame = CGRectMake(self.containerView.frame.size.width/2 - 75, lblSelectDate.frame.origin.y + 70 , 150, 46) ;
        
        if ([strPreviousEntry isEqualToString:@"Yes"])
        {
            strSelectedTime = [responseDict valueForKey:@"date_of_installation"] ;
            
            [btnSelectDate setTitle:strSelectedTime forState:UIControlStateNormal];
        }
        
        //**********************************************************
        
        self.containerView.frame = CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, btnSave.frame.origin.y + 70);
        
        [_scroll setContentSize:CGSizeMake([[UIScreen mainScreen]bounds].size.width, _containerView.frame.size.height)] ;
        
        [_scroll scrollRectToVisible:CGRectMake(0,  0 , [[UIScreen mainScreen] bounds].size.width , 0) animated:YES] ;
        
        [self.scroll addSubview:_containerView];
        
    }
    else if ([[responseDict objectForKey:@"status"] isEqualToString:@"error"]) {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:[responseDict objectForKey:@"msg"]
                                      message:@""
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
            //Handel your yes please button action here
            
        }];
        
        [alert addAction:yesButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    } else if ([[responseDict objectForKey:@"status"] isEqualToString:@"0"]) {
        
        NSString *strMsg = [responseDict objectForKey:@"msg"];
        
        if (strMsg == nil || [strMsg  isEqualToString: @""]) {
            strMsg = @"Oops! Something went wrong. Try again later";
        }
        
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:LocalizedString(strMsg)
                                      message:@""
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
            sqlObj = [[SQLManager alloc] init] ;
            [sqlObj createAndOpenDatabase] ;
            
            [sqlObj DeleteData] ;
            
            UIStoryboard *story = [UIStoryboard storyboardWithName:@"SecurityCodeViewController" bundle:nil];
            
            SecurityCodeViewController *SecurityCodeViewController = [story instantiateViewControllerWithIdentifier:@"SecurityCodeViewController"];
            
            UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:SecurityCodeViewController];
            
            [[UIApplication sharedApplication].keyWindow setRootViewController:navController];
        }];
        
        [alert addAction:yesButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    } else {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:LocalizedString(@"Oops! Something went wrong. Try again later")
                                      message:@""
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
        }];
        [alert addAction:yesButton];
        [self presentViewController:alert animated:YES completion:nil];
    }
    
    [globalObject removeActivityIndicator] ;
    
}

- (void)buttonPressed:(UIButton *)button
{
    
    [self.view endEditing:YES];
    
    NSMutableArray *valuesArray = [[NSMutableArray alloc] init] ;
    
    NSLog(@"Button Index: %ld",(long)button.tag) ;
    
    selectedIndex = (int)button.tag ;
    
    tagValue = (int)button.tag - 1000 ;
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle: [[DataArray objectAtIndex:tagValue] valueForKey:@"Note"]
                                                                         message:nil
                                                                  preferredStyle:UIAlertControllerStyleActionSheet];
    
    valuesArray = [[DataArray objectAtIndex:tagValue] valueForKey:@"Values"] ;
    
    NSString *PostDataKey = [[DataArray objectAtIndex:tagValue] valueForKey:@"AttrKey"] ;
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:LocalizedString(@"Cancel") style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        // Cancel button tappped.
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    
//    if ([[[DataArray objectAtIndex:tagValue] valueForKey:@"isRequire"] isEqualToString:@"1"]) {
    if ([[[DataArray objectAtIndex:tagValue] valueForKey:@"ispicklist"] isEqualToString:@"0"]) {
        /*UIAlertAction* online = [UIAlertAction
                                 actionWithTitle:LocalizedString(@"Other")
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
            
        }];*/
        if ([[[self->DataArray objectAtIndex:self->tagValue] valueForKey:@"Selected"] isEqualToString:@"None"]) {
            [self addOtherData:PostDataKey
                  keyboardType:[[self->DataArray objectAtIndex:self->tagValue] valueForKey:@"type"]
                 selectedValue:@""
                       heading:[[self->DataArray objectAtIndex:self->tagValue] valueForKey:@"AttributeName"]];
        } else {
            [self addOtherData:PostDataKey
                  keyboardType:[[self->DataArray objectAtIndex:self->tagValue] valueForKey:@"type"]
                 selectedValue:[[self->DataArray objectAtIndex:self->tagValue] valueForKey:@"Selected"]
                       heading:[[self->DataArray objectAtIndex:self->tagValue] valueForKey:@"AttributeName"]];
        }
        //[online setValue:[UIColor blackColor] forKey:@"titleTextColor"];
        //[actionSheet addAction:online];
    }
    
    
    for (NSString *title in valuesArray) {
        
        if ([title isEqualToString:button.titleLabel.text])
        {
            [actionSheet addAction:[UIAlertAction actionWithTitle:title style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
                
                [button setTitle:title forState:UIControlStateNormal] ;
                
                [SelectedDataForPostDictionary setObject:title forKey:PostDataKey] ;
                
                [self dismissViewControllerAnimated:YES completion:^{
                }];
            }]];
        }
        else
        {
            
            [actionSheet addAction:[UIAlertAction actionWithTitle:title style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                
                [button setTitle:title forState:UIControlStateNormal] ;
                
                [SelectedDataForPostDictionary setObject:title forKey:PostDataKey] ;
                
                [self dismissViewControllerAnimated:YES completion:^{
                }];
            }]];
        }
        
    }
    
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
        [actionSheet setModalPresentationStyle:UIModalPresentationPopover];
        
        UIPopoverPresentationController *popPresenter = [actionSheet
                                                         popoverPresentationController];
        popPresenter.sourceView = button;
        popPresenter.sourceRect = button.bounds;
        
    }
    
    [self presentViewController:actionSheet animated:YES completion:nil];
    
}


-(void)addOtherData: (NSString *)Key keyboardType: (NSString *)keyBoardType selectedValue:(NSString *)selectedString heading:(NSString *)strHeading
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:[NSString stringWithFormat:@"Add %@",strHeading]
                                                                   message:nil
                                                            preferredStyle:UIAlertControllerStyleAlert];
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        // optionally configure the text field
        textField.placeholder = @"Enter Here" ;
        textField.text = selectedString ;
        textField.borderStyle = UITextBorderStyleRoundedRect;
        if ([keyBoardType isEqualToString:@"1"]){
            textField.keyboardType = UIKeyboardTypeDefault;
        } else {
            textField.keyboardType = UIKeyboardTypeNumberPad;
        }
        textField.returnKeyType = UIReturnKeySend ;
        textField.delegate = self ;
        
    }];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Add"
                                                       style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction *action) {
        UITextField *textField = [alert.textFields firstObject];
        
        StrRequestFor = Key ;
        
        StrNewAddedData = textField.text ;
        
        [self sendRequestForAddNewData];
        
        //---------------------------------------
        
    }];
    
    UIAlertAction *Cancel = [UIAlertAction actionWithTitle:LocalizedString(@"Cancel")
                                                     style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction *action) {
        
    }];
    
    
    
    [alert addAction:Cancel];
    
    [alert addAction:okAction];
    
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)sendRequestForAddNewData
{
    if ([globalObject checkInternetConnection]) {
        [globalObject showActivityIndicator];
        
        NSString *str = addotherdataURL ;
        NSString *urlString = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *url = [NSURL URLWithString:urlString];
        
        ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
        
        [request setRequestMethod:@"POST"] ;
        [request setDelegate:self] ;
        
        sqlObj = [[SQLManager alloc] init] ;
        
        [sqlObj createAndOpenDatabase] ;
        
        [request setPostValue:[[sqlObj getAllUserInfo] valueForKey:@"clientID"] forKey:@"client_id"] ;
        [request setPostValue:StrNewAddedData forKey:@"name"] ;
        [request setPostValue:[[sqlObj getAllUserInfo] valueForKey:@"userid"] forKey:@"user_id"] ;
        [request setPostValue:StrRequestFor forKey:@"tag"] ;
        [request setPostValue:[[sqlObj getAllUserInfo] valueForKey:@"MeasurmentUnit"] forKey:@"measurement_units"] ;
        
        //-------
        
        [request startAsynchronous] ;
    } else {
        [globalObject removeActivityIndicator];
    }
}

#pragma mark - Data Post

-(void)PostOtherData
{
    if ([globalObject checkInternetConnection]) {
        [globalObject showActivityIndicatorForSave];
        
        StrRequestFor = @"PostOtherData" ;
        
        NSString *str = PostOtherDataURL ;
        NSString *urlString = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *url = [NSURL URLWithString:urlString];
        
        ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
        
        [request setRequestMethod:@"POST"] ;
        [request setDelegate:self] ;
        
        //------- Post Data
        
        NSError *error;
        
        NSData *jsonData2 = [NSJSONSerialization dataWithJSONObject:SelectedDataForPostDictionary options:NSJSONWritingPrettyPrinted error:&error];
        
        NSString *jsonString = [[NSString alloc] initWithData:jsonData2 encoding:NSUTF8StringEncoding];
        
        NSLog(@"jsonData as string:\n%@", jsonString);
        
        // ---------- Initialize SqlObject
        
        sqlObj = [[SQLManager alloc] init] ;
        
        [sqlObj createAndOpenDatabase] ;
        
        [request setPostValue:[[sqlObj getAllUserInfo] valueForKey:@"clientID"] forKey:@"client_id"] ;
        
        [request setPostValue:[[sqlObj getAllUserInfo] valueForKey:@"userid"] forKey:@"user_id"] ;
        
        [request setPostValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"SELECTDODETYPE"] forKey:@"node_type"] ;
        
        [request setPostValue:strMacID forKey:@"mac_address"] ;
        
        [request setPostValue:strSlcID forKey:@"slc_id"] ;
        
        [request setPostValue:strLat forKey:@"lat"] ;
        
        [request setPostValue:strLong forKey:@"lng"] ;
        
        [request setPostValue:strPoleID forKey:@"pole_id"] ;
        
        [request setPostValue:[[sqlObj getAllUserInfo] valueForKey:@"MeasurmentUnit"] forKey:@"measurement_units"] ;
        
        [request setPostValue:strSelectedTime forKey:@"date_of_installation"] ;
        
        [request setPostValue:@"IOS" forKey:@"source"] ;
        
        [request setPostValue:strAddress forKey:@"address"] ;
        
        [request setPostValue:StrForSkipClick forKey:@"skip"] ;
        
        [request setPostValue:_strNotes forKey:@"notes"];
        
        NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
        
        [request setPostValue:version forKey:@"Version"] ;
        
        for (int i =0; i<_arrPoleOpt.count; i++) {
            NSMutableDictionary *dictTmp = [[NSMutableDictionary alloc] initWithDictionary:_arrPoleOpt[i]];
            NSArray *arrCount = [[NSArray alloc] init];
            arrCount = [dictTmp allKeys];
            for (int j =0; j<arrCount.count; j++) {
                [request setPostValue:dictTmp[arrCount[j]] forKey:arrCount[j]];
            }
        }
        
        //  [request setPostValue:strIDForUpdateData forKey:@"SLCID"] ;
        
        [request setPostValue:strPreviousEntry forKey:@"copyasset"] ;
        
        if(!error)
        {
            [request setPostValue:jsonString forKey:@"Assets"] ;
        }
        
        if (PoleImg)
        {
            NSData *imgData = [[NSData alloc] initWithData:UIImageJPEGRepresentation((PoleImg), 0.1)];
            
            [request setPostFormat:ASIMultipartFormDataPostFormat];
            
            [request addRequestHeader:@"Content-Type" value:@"multipart/form-data"];
            
            [request setData:imgData withFileName:@"abc.jpg" andContentType:@"multipart/form-data" forKey:@"pole_image"];
        }
        
        [request setTimeOutSeconds:180] ;
        
        //-------
        
        [request startAsynchronous] ;
    } else {
        [globalObject removeActivityIndicator];
    }
}

-(void)requestFailed:(ASIHTTPRequest *)request
{
    NSError *error = [request error];
    NSLog(@"error : %@" , error.localizedDescription);
    [globalObject removeActivityIndicator];
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:error.localizedDescription
                                  message:@""
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"OK"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action)
                                {
    }];
    
    [alert addAction:yesButton];
    
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)requestFinished:(ASIHTTPRequest *)request
{
    
    [globalObject removeActivityIndicator];
    
    NSString *responseString = [request responseString];
    NSMutableDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:[responseString dataUsingEncoding:NSUTF8StringEncoding] options:0 error:nil];
    NSLog(@"Responce Dictionary : %@", responseDict);
    
    
    if ([[responseDict objectForKey:@"status"] isEqualToString:@"success"])
    {
        if ([StrRequestFor isEqualToString:@"PostOtherData"])
        {
            UIAlertController * alert=   [UIAlertController
                                          alertControllerWithTitle:[responseDict objectForKey:@"msg"]                                      message:@""
                                          preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction * ListButton = [UIAlertAction
                                          actionWithTitle:LocalizedString(@"List")
                                          style:UIAlertActionStyleDefault
                                          handler:^(UIAlertAction * action)
                                          {
                [self.tabBarController setSelectedIndex:0];
                
                [self.navigationController popToRootViewControllerAnimated:YES];
            }];
            
            UIAlertAction* NewButton = [UIAlertAction
                                        actionWithTitle:LocalizedString(@"New Scan")
                                        style:UIAlertActionStyleDefault
                                        handler:^(UIAlertAction * action)
                                        {
                [self.navigationController popToRootViewControllerAnimated:YES];
            }];
            
            [alert addAction:ListButton];
            
            [alert addAction:NewButton];
            
            [self presentViewController:alert animated:YES completion:nil];
            
        }
        else if ([StrRequestFor isEqualToString:@"Login"])
        {
            [sqlObj insertKeyToDatabase:@"user_logged_in" value:@"yes"];
            
            [sqlObj insertKeyToDatabase:@"token" value:[responseDict objectForKey:@"token"]];
            
            [self newAssetData] ;
            
        } else if ([[responseDict objectForKey:@"status"] isEqualToString:@"0"]) {
            
            NSString *strMsg = [responseDict objectForKey:@"msg"];
            
            if (strMsg == nil || [strMsg  isEqualToString: @""]) {
                strMsg = @"Oops! Something went wrong. Try again later";
            }
            
            UIAlertController * alert=   [UIAlertController
                                          alertControllerWithTitle:LocalizedString(strMsg)
                                          message:@""
                                          preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* yesButton = [UIAlertAction
                                        actionWithTitle:@"OK"
                                        style:UIAlertActionStyleDefault
                                        handler:^(UIAlertAction * action)
                                        {
                sqlObj = [[SQLManager alloc] init] ;
                [sqlObj createAndOpenDatabase] ;
                
                [sqlObj DeleteData] ;
                
                UIStoryboard *story = [UIStoryboard storyboardWithName:@"SecurityCodeViewController" bundle:nil];
                
                SecurityCodeViewController *SecurityCodeViewController = [story instantiateViewControllerWithIdentifier:@"SecurityCodeViewController"];
                
                UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:SecurityCodeViewController];
                
                [[UIApplication sharedApplication].keyWindow setRootViewController:navController];
            }];
            
            [alert addAction:yesButton];
            
            [self presentViewController:alert animated:YES completion:nil];
        }
        else
        {
            NSMutableArray *mutableArray = [NSMutableArray arrayWithArray:DataArray];
            [mutableArray replaceObjectAtIndex:tagValue withObject:[responseDict objectForKey:@"data"]];
            DataArray = mutableArray;
            
            UIButton *button = (UIButton *)[self.view viewWithTag:selectedIndex] ;
            
            [button setTitle:StrNewAddedData forState:UIControlStateNormal] ;
            
            [SelectedDataForPostDictionary setObject:StrNewAddedData forKey:StrRequestFor] ;
            
            UIAlertController * alert=   [UIAlertController
                                          alertControllerWithTitle:[responseDict objectForKey:@"msg"]                                      message:@""
                                          preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* yesButton = [UIAlertAction
                                        actionWithTitle:@"OK"
                                        style:UIAlertActionStyleDefault
                                        handler:^(UIAlertAction * action)
                                        {
                
            }];
            
            [alert addAction:yesButton];
            
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
    else if ([[responseDict objectForKey:@"status"] isEqualToString:@"logout"])
    {
        //  strIDForUpdateData = [responseDict objectForKey:@"SLCID"] ;
        
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:[responseDict objectForKey:@"msg"]                                      message:@""
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
            sqlObj = [[SQLManager alloc] init] ;
            [sqlObj createAndOpenDatabase] ;
            
            NSString *strClientID = [[sqlObj getAllUserInfo] valueForKey:@"clientID"] ;
            NSString *strUserID = [[sqlObj getAllUserInfo] valueForKey:@"userid"] ;
            NSString *strScanLBL = [[sqlObj getAllUserInfo] valueForKey:@"ScanLBL"] ;
            NSString *strScanPH = [[sqlObj getAllUserInfo] valueForKey:@"ScanPH"] ;
            
            NSString *strPoleID = [[sqlObj getAllUserInfo] valueForKey:@"PoleID"] ;
            NSString *strMeasurmentUnit = [[sqlObj getAllUserInfo] valueForKey:@"MeasurmentUnit"] ;
            NSString *strOtherData = [[sqlObj getAllUserInfo] valueForKey:@"OtherData"] ;
            
            [sqlObj DeleteData] ;
            
            [sqlObj createAndOpenDatabase] ;
            
            [sqlObj insertKeyToDatabase:@"clientID" value:strClientID];
            [sqlObj insertKeyToDatabase:@"userid" value:strUserID];
            [sqlObj insertKeyToDatabase:@"ScanLBL" value:strScanLBL];
            [sqlObj insertKeyToDatabase:@"ScanPH" value:strScanPH];
            [sqlObj insertKeyToDatabase:@"PoleID" value:strPoleID];
            [sqlObj insertKeyToDatabase:@"MeasurmentUnit" value:strMeasurmentUnit];
            [sqlObj insertKeyToDatabase:@"OtherData" value:strOtherData];
            
            [self AlertForLogin] ;
            
        }];
        
        [alert addAction:yesButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    }  else if ([[responseDict objectForKey:@"status"] isEqualToString:@"error"]) {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:[responseDict objectForKey:@"msg"]                                      message:@""
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
            
        }];
        
        [alert addAction:yesButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    } else if ([[responseDict objectForKey:@"status"] isEqualToString:@"0"]) {
        
        NSString *strMsg = [responseDict objectForKey:@"msg"];
        
        if (strMsg == nil || [strMsg  isEqualToString: @""]) {
            strMsg = @"Oops! Something went wrong. Try again later";
        }
        
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:LocalizedString(strMsg)
                                      message:@""
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
            sqlObj = [[SQLManager alloc] init] ;
            [sqlObj createAndOpenDatabase] ;
            
            [sqlObj DeleteData] ;
            
            UIStoryboard *story = [UIStoryboard storyboardWithName:@"SecurityCodeViewController" bundle:nil];
            
            SecurityCodeViewController *SecurityCodeViewController = [story instantiateViewControllerWithIdentifier:@"SecurityCodeViewController"];
            
            UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:SecurityCodeViewController];
            
            [[UIApplication sharedApplication].keyWindow setRootViewController:navController];
        }];
        
        [alert addAction:yesButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    } else {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:LocalizedString(@"Oops! Something went wrong. Try again later")
                                      message:@""
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
        }];
        [alert addAction:yesButton];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

-(void)AlertForLogin
{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Login"
                                                                   message:nil
                                                            preferredStyle:UIAlertControllerStyleAlert];
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        // optionally configure the text field
        
        textField.placeholder = @"Username" ;
        textField.borderStyle = UITextBorderStyleRoundedRect ;
        textField.font = [UIFont systemFontOfSize:16.0] ;
        textField.keyboardType = UIKeyboardTypeAlphabet;
        textField.returnKeyType = UIReturnKeySend ;
        textField.delegate = self ;
        
    }];
    
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textFieldPass) {
        // optionally configure the text field
        
        textFieldPass.placeholder = @"Password" ;
        textFieldPass.secureTextEntry = YES;
        textFieldPass.borderStyle = UITextBorderStyleRoundedRect ;
        textFieldPass.font = [UIFont systemFontOfSize:16.0] ;
        textFieldPass.keyboardType = UIKeyboardTypeAlphabet;
        textFieldPass.returnKeyType = UIReturnKeySend ;
        textFieldPass.delegate = self ;
        
    }];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Login"
                                                       style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction *action) {
        NSArray * textfields = alert.textFields;
        UITextField * namefield = textfields[0];
        UITextField * passfield = textfields[1];
        
        strUsername = namefield.text ;
        strPassword = passfield.text ;
        
        [self loginAction];
        
    }];
    
    
    [alert addAction:okAction];
    
    [self presentViewController:alert animated:YES completion:nil];
    
    
}

-(void)loginAction
{
    if ([strUsername isEqualToString:@""])
    {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:LocalizedString(@"Please enter username")
                                      message:@""
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
            [self AlertForLogin] ;
            
        }];
        
        [alert addAction:yesButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    } else if ([strPassword isEqualToString:@""]) {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:LocalizedString(@"Please enter password")
                                      message:@""
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
            [self AlertForLogin] ;
            
        }];
        
        [alert addAction:yesButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
    else
    {
        [self loginRequest];
    }
}


#pragma mark - Data Post

-(void)loginRequest
{
    if ([globalObject checkInternetConnection]) {
        [globalObject showActivityIndicator];
        
        StrRequestFor = @"Login" ;
        
        NSString *str = userLoginURL ;
        NSString *urlString = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *url = [NSURL URLWithString:urlString];
        
        ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
        
        [request setRequestMethod:@"POST"] ;
        [request setDelegate:self] ;
        
        //------- Post Data
        
        // ---------- Initialize SqlObject
        
        sqlObj = [[SQLManager alloc] init] ;
        
        [sqlObj createAndOpenDatabase] ;
        
        NSLog(@"%@",[[sqlObj getAllUserInfo] valueForKey:@"clientID"]);
        
        NSLog(@"%@",[[sqlObj getAllUserInfo] valueForKey:@"userid"]);
        
        NSLog(@"%@",[[UIDevice currentDevice] identifierForVendor]);
        
        NSLog(@"%f",locationManager.location.coordinate.latitude);
        
        NSLog(@"%f",locationManager.location.coordinate.longitude);
        
        // 65D78ADF-32BC-4A73-B7BC-C9ADBC4C9F55
        
        NSString *strLat = [NSString stringWithFormat:@"%f",locationManager.location.coordinate.latitude];
        
        NSString *strLong = [NSString stringWithFormat:@"%f",locationManager.location.coordinate.longitude];
        
        [request setPostValue:[[sqlObj getAllUserInfo] valueForKey:@"clientID"] forKey:@"client_id"] ;
        
        [request setPostValue:[[sqlObj getAllUserInfo] valueForKey:@"userid"] forKey:@"userid"] ;
        
        [request setPostValue:strUsername forKey:@"username"] ;
        
        [request setPostValue:strPassword forKey:@"password"] ;
        
        [request setPostValue:strLat forKey:@"lat"] ;
        
        [request setPostValue:strLong forKey:@"long"] ;
        
        // [request setPostValue:[[[UIDevice currentDevice] identifierForVendor] UUIDString] forKey:@"DeviceID"] ;
        
        [request setPostValue:@"IOS" forKey:@"source"] ;
        
        [request setTimeOutSeconds:180] ;
        
        //-------
        
        [request startAsynchronous] ;
    } else {
        [globalObject removeActivityIndicator];
    }
}

-(void)newAssetData
{
    [SelectedDataForPostDictionary removeAllObjects];
    
    DataArray = [[NSMutableArray alloc] init] ;
    
    SelectedDataForPostDictionary = [[NSMutableDictionary alloc] init] ;
    
    [self getOtherData] ;
    
    for (UIView *i in _containerView.subviews)
    {
        if([i isKindOfClass:[UILabel class]])
        {
            UILabel *newLbl = (UILabel *)i;
            if(newLbl.tag == 2000)
            {
                newLbl.text = @"" ;
                [newLbl removeFromSuperview];
            }
        }
        else if([i isKindOfClass:[UIButton class]])
        {
            UIButton *newbtn = (UIButton *)i;
            if(newbtn.tag >= 1000)
            {
                [newbtn setTitle:@"" forState:UIControlStateNormal ];
                [newbtn removeFromSuperview];
            }
        }
    }
}

#pragma mark - TextField Delegate

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}

-(BOOL)textFieldShouldReturn:(UITextField*)textField
{
    [textField resignFirstResponder];
    
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    if([textField.text length]==0)
    {
        if([string isEqualToString:@" "])
        {
            return NO;
        }
    }
    
    return YES;
}
/*
 NSUInteger index = dataSegmentObj.selectedSegmentIndex ;
 
 if (index == 0)
 {
 //  [self PostSLCData];
 
 PoleImageViewController.strPreviousEntry = @"Yes" ;
 }
 else
 {
 PoleImageViewController.strPreviousEntry = @"No" ;
 }
 */
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)eventof:(NSString *)strEventName {
    dispatch_async(dispatch_get_main_queue(), ^{
    NSString *strClientID = [[sqlObj getAllUserInfo] valueForKey:@"clientID"];
    NSString *strUserID = [[sqlObj getAllUserInfo] valueForKey:@"userid"];
    
    [FIRAnalytics logEventWithName:kFIREventSelectContent
                        parameters:@{
                                     kFIRParameterItemName:[NSString stringWithFormat:@"%@_%@_%@",strClientID, strUserID, strEventName],
                                     }];
    });
}
@end
