//
//  MapViewController.h
//  SLCScanner
//
//  Created by INT MAC 2015 on 23/02/18.
//  Copyright © 2018 INT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>

#import "SQLManager.h"
#import "LMDropdownView.h"
#import "GlobalAssistant.h"
#import "AddressViewController.h"

@interface MapViewController : UIViewController <CLLocationManagerDelegate,MKMapViewDelegate,UITableViewDataSource, UITableViewDelegate, LMDropdownViewDelegate>

//------------assign property--------------//

// --------- Initialize GlobalAssistant

@property (strong , nonatomic) GlobalAssistant *globalObject ;

//-------- SqlManager Obj -------//

@property (strong , nonatomic) SQLManager *sqlObj ;

@property (strong, nonatomic) NSString *strSLCID ;

@property (strong, nonatomic) NSString *strMacAddress ;

@property (strong, nonatomic) IBOutlet MKMapView *mapView;

@property (strong, nonatomic) IBOutlet UILabel *lblSLCID ;

@property (strong, nonatomic) IBOutlet UILabel *lblMacID ;

@property (strong, nonatomic) NSArray *mapTypes;
@property (strong, nonatomic) IBOutlet UITableView *menuTableView;
@property (strong, nonatomic) LMDropdownView *dropdownView;

- (IBAction)AcceptButton:(id)sender;

@end
