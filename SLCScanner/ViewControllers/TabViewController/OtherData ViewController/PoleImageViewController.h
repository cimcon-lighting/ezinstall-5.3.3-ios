//
//  PoleImageViewController.h
//  SLCScanner
//
//  Created by INT MAC 2015 on 08/06/18.
//  Copyright © 2018 INT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SQLManager.h"
#import "GlobalAssistant.h"
#import "ASIFormDataRequest.h"
#import "NSURLConnection_Class.h"


@interface PoleImageViewController : UIViewController <ASIHTTPRequestDelegate,NSURLConnection_CustomDelegate>

//------------assign property--------------//

// --------- Initialize GlobalAssistant

@property (strong , nonatomic) GlobalAssistant *globalObject ;

//-------- SqlManager Obj -------//

@property (strong , nonatomic) SQLManager *sqlObj ;

//------------------------------------------

@property (strong , nonatomic) NSString *strMacID ;

@property (strong , nonatomic) NSString *strSlcID ;

@property (strong , nonatomic) NSString *strLat ;

@property (strong , nonatomic)  NSString *strLong ;

@property (strong , nonatomic)  NSString *strAddress ;

//------------------------------------------

@property (strong, nonatomic) IBOutlet UIImageView *PoleImg ;

@property (strong, nonatomic) IBOutlet UIButton *ChooseImgBtn ;

@property (retain, nonatomic) IBOutlet UIButton *SaveImgBtn ;

- (IBAction)ChooseImgBtn:(id)sender;

- (IBAction)SaveImgBtn:(id)sender;

@end
