//
//  AddressViewController.h
//  SLCScanner
//
//  Created by INT MAC 2015 on 30/04/18.
//  Copyright © 2018 INT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SQLManager.h"
#import "GlobalAssistant.h"
#import "ASIFormDataRequest.h"
#import "NSURLConnection_Class.h"

@interface AddressViewController : UIViewController <ASIHTTPRequestDelegate,NSURLConnection_CustomDelegate>

@property (strong, nonatomic) IBOutlet UIButton *btnEdit ;

@property (strong, nonatomic) IBOutlet UIButton *btnConfirm ;

//------------assign property--------------//

// --------- Initialize GlobalAssistant

@property (strong , nonatomic) GlobalAssistant *globalObject ;

//-------- SqlManager Obj -------//

@property (strong , nonatomic) SQLManager *sqlObj ;

@property (strong , nonatomic) NSString *strMacID ;

@property (strong , nonatomic) NSString *strSlcID ;

@property (strong , nonatomic) NSString *strLat ;

@property (strong , nonatomic)  NSString *strLong ;

@property (strong, nonatomic) IBOutlet UITextView *txtView;

- (IBAction)OKButton:(id)sender;
- (IBAction)CancelButton:(id)sender;

@end
