//
//  ScanPoleIDViewController.m
//  SLCScanner
//
//  Created by INT MAC 2015 on 26/02/18.
//  Copyright © 2018 INT. All rights reserved.
//

#import "ScanPoleIDViewController.h"

#import "LocalizeHelper.h"
#import "PoleDataViewController.h"
#import "SecurityCodeViewController.h"
#import <FirebaseAnalytics/FirebaseAnalytics.h>

@interface ScanPoleIDViewController ()
{
    NSString *strNoneSelection ;
    
    //-------------------------------------------
    
    NSString *StrRequestFor ;
    
    
    NSString *strUsername ;
    
    NSString *strPassword ;
    
    CLLocationManager *locationManager;
    
    NSString *strIDForUpdateData ;
    
    IBOutlet UIButton *btnOK;
    IBOutlet UIButton *btnCancel;
    
    IBOutlet UILabel *lblTitlePole;
}

@end

@implementation ScanPoleIDViewController
@synthesize txtPoleID, globalObject, btnNoneSelection, sqlObj ;
@synthesize strLat,strLong,strMacID,strSlcID, strAddress, PoleImg, lblNone ;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self eventof:@"PoleID"];
    
    globalObject = [[GlobalAssistant alloc] init] ;
    
    strNoneSelection = @"No" ;
    
    [btnNoneSelection setImage:[UIImage imageNamed:@"No_Tick.png"] forState:UIControlStateNormal];
    
    sqlObj = [[SQLManager alloc] init] ;
    
    [sqlObj createAndOpenDatabase] ;
    
    /*if ([[[sqlObj getAllUserInfo] valueForKey:@"assetsView"] isEqualToString:@"No"])
    {
        btnNoneSelection.hidden = YES ;
        lblNone.hidden = YES ;
    }*/
    
    [globalObject buttonWithCornerRadious:22.0 btnView:btnOK];
    [globalObject buttonWithCornerRadious:22.0 btnView:btnCancel];
    
    UIImage* imgback = [UIImage imageNamed:@"left_arrow"];
    CGRect frameimgBk = CGRectMake(0,0,25,25);
    
    UIButton *btnBack = [[UIButton alloc] initWithFrame:frameimgBk];
    [btnBack setBackgroundImage:imgback forState:UIControlStateNormal];
    [btnBack addTarget:self action:@selector(btnBackClick)
      forControlEvents:UIControlEventTouchUpInside];
    [btnBack setShowsTouchWhenHighlighted:YES];
    
    UIBarButtonItem *bkBtn =[[UIBarButtonItem alloc] initWithCustomView:btnBack];
    self.navigationItem.leftBarButtonItems = @[bkBtn];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    lblTitlePole.text   = LocalizedString(@"Enter Pole ID");
    self.title          = LocalizedString(@"POLE ID");
    [btnCancel setTitle:LocalizedString(@"Cancel") forState:UIControlStateNormal];
    lblNone.text        = LocalizedString(@"None");
    txtPoleID.placeholder = LocalizedString(@"Enter Pole ID");
    
    /*if ([[[sqlObj getAllUserInfo] valueForKey:@"slc_pole_id"] isEqualToString:@"Yes"])  {
        [btnNoneSelection setHidden:false];
        [lblNone setHighlighted:false];
    } else {
        [btnNoneSelection setHidden:true];
        [lblNone setHighlighted:true];
    }*/
}
-(void)btnBackClick{
    [self eventof:@"PoleIDBack"];
    [self.navigationController popViewControllerAnimated:true];
}
#pragma mark - Button Action

- (IBAction)OKButton:(id)sender
{
    if ([txtPoleID.text isEqualToString:@""] && [strNoneSelection isEqualToString:@"No"]){
        [globalObject showAlertWithTitle:LocalizedString(@"Please Enter Valid Pole ID") message:@""];
    } else {
        sqlObj = [[SQLManager alloc] init] ;
        
        [sqlObj createAndOpenDatabase] ;
        
        if ([strNoneSelection isEqualToString:@"Yes"])
        {
            if ([[[sqlObj getAllUserInfo] valueForKey:@"OtherData"] isEqualToString:@"No"])
            {
                [self PostSLCData];
            }
            else
            {
                PoleDataViewController *PoleDataView = [[PoleDataViewController alloc] initWithNibName:@"PoleDataViewController" bundle:nil] ;
                
                PoleDataView.strMacID = strMacID ;
                PoleDataView.strSlcID = strSlcID ;
                PoleDataView.strLat = strLat ;
                PoleDataView.strLong = strLong ;
                PoleDataView.strPoleID = @"" ;
                PoleDataView.strAddress = strAddress ;
                PoleDataView.arrPoleOpt = _arrPoleOpt;
                PoleDataView.strNotes = _strNotes;
                PoleDataView.PoleImg = PoleImg ;
                
                [self.navigationController pushViewController:PoleDataView animated:YES] ;
            }
        }
        else
        {
            if ([txtPoleID.text isEqualToString:@""])
            {
                UIAlertController * alert=   [UIAlertController
                                              alertControllerWithTitle:LocalizedString(@"Please Enter Valid Pole ID")                                     message:@""
                                              preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* yesButton = [UIAlertAction
                                            actionWithTitle:@"OK"
                                            style:UIAlertActionStyleDefault
                                            handler:^(UIAlertAction * action)
                                            {
                                                //Handel your yes please button action here
                                                
                                            }];
                
                [alert addAction:yesButton];
                
                [self presentViewController:alert animated:YES completion:nil];
            }
            else
            {
                [self PostForCheckPoleID] ;
            }
        }
    }
}

- (IBAction)CancelButton:(id)sender
{
    [self eventof:@"PoleIDCancel"];
    txtPoleID.text = @"" ;
}

- (IBAction)NoneSelectButton:(id)sender
{
    txtPoleID.text = @"" ;
    
    [self eventof:@"PoleIDNone"];
    
    if ([strNoneSelection isEqualToString:@"No"])
    {
        strNoneSelection = @"Yes" ;
        
        txtPoleID.enabled = NO ;
        
        [btnNoneSelection setImage:[UIImage imageNamed:@"Tick_Box.png"] forState:UIControlStateNormal];
    }
    else
    {
        strNoneSelection = @"No" ;
        
        txtPoleID.enabled = YES ;
        
        [btnNoneSelection setImage:[UIImage imageNamed:@"No_Tick.png"] forState:UIControlStateNormal];
        
    }
    
}

#pragma mark - TextField Delegate

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    if(textField == txtPoleID)
    {
        if([textField.text length]==0)
        {
            if([string isEqualToString:@" "])
            {
                return NO;
            }
        }
    }
    
    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField*)textField
{
    [textField resignFirstResponder];
    
    return YES;
}

#pragma mark - Data Post

-(void)PostForCheckPoleID
{
    // [globalObject showActivityIndicator];
    if ([globalObject checkInternetConnection]) {
        StrRequestFor = @"checkpoleid" ;
        
        NSString *str = checkpoleidURL ;
        NSString *urlString = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *url = [NSURL URLWithString:urlString];
        
        ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
        
        [request setRequestMethod:@"POST"] ;
        [request setDelegate:self] ;
        
        //------- Post Data
        
        // ---------- Initialize SqlObject
        
        sqlObj = [[SQLManager alloc] init] ;
        
        [sqlObj createAndOpenDatabase] ;
        
        NSLog(@"%@",[[sqlObj getAllUserInfo] valueForKey:@"clientID"]);
        
        NSLog(@"%@",[[sqlObj getAllUserInfo] valueForKey:@"userid"]);
        
        [request setPostValue:[[sqlObj getAllUserInfo] valueForKey:@"clientID"] forKey:@"client_id"] ;
        
        [request setPostValue:[[sqlObj getAllUserInfo] valueForKey:@"userid"] forKey:@"user_id"] ;
        
        [request setPostValue:txtPoleID.text forKey:@"pole_id"] ;
        
        
        
        [request setTimeOutSeconds:180] ;
        
        //-------
        
        [request startAsynchronous] ;
    }else {
        [globalObject removeActivityIndicator];
    }
}


-(void)PostSLCData
{
    if ([globalObject checkInternetConnection]) {
        [globalObject showActivityIndicatorForSave];
        
        StrRequestFor = @"PostOtherData" ;
        
        NSString *str = PostOtherDataURL ;
        NSString *urlString = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *url = [NSURL URLWithString:urlString];
        
        ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
        
        [request setRequestMethod:@"POST"] ;
        [request setDelegate:self] ;
        
        //------- Post Data
        
        // ---------- Initialize SqlObject
        
        sqlObj = [[SQLManager alloc] init] ;
        
        [sqlObj createAndOpenDatabase] ;
        
        NSLog(@"%@",[[sqlObj getAllUserInfo] valueForKey:@"clientID"]);
        
        NSLog(@"%@",[[sqlObj getAllUserInfo] valueForKey:@"userid"]);
        
        [request setPostValue:[[sqlObj getAllUserInfo] valueForKey:@"clientID"] forKey:@"client_id"] ;
        
        [request setPostValue:[[sqlObj getAllUserInfo] valueForKey:@"userid"] forKey:@"user_id"] ;
        
        [request setPostValue:[[sqlObj getAllUserInfo] valueForKey:@"MeasurmentUnit"] forKey:@"measurement_units"] ;
        
        [request setPostValue:strMacID forKey:@"mac_address"] ;
        
        [request setPostValue:strSlcID forKey:@"slc_id"] ;
        
        [request setPostValue:strLat forKey:@"lat"] ;
        
        [request setPostValue:strLong forKey:@"lng"] ;
        
        [request setPostValue:strAddress forKey:@"address"] ;
        
        NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
        
        [request setPostValue:version forKey:@"Version"] ;
        
        //  [request setPostValue:strIDForUpdateData forKey:@"SLCID"] ;
        
        [request setPostValue:@"No" forKey:@"copyasset"] ;
        
        if ([strNoneSelection isEqualToString:@"No"])
        {
            [request setPostValue:txtPoleID.text forKey:@"pole_id"] ;
        }
        
        [request setPostValue:@"IOS" forKey:@"source"] ;
        
        if (PoleImg)
        {
            NSData *imgData = [[NSData alloc] initWithData:UIImageJPEGRepresentation((PoleImg), 0.1)];
            
            [request setPostFormat:ASIMultipartFormDataPostFormat];
            
            [request addRequestHeader:@"Content-Type" value:@"multipart/form-data"];
            
            [request setData:imgData withFileName:@"abc.jpg" andContentType:@"multipart/form-data" forKey:@"pole_image"];
        }
        
        [request setPostValue:_strNotes forKey:@"notes"];
        
        for (int i =0; i<_arrPoleOpt.count; i++) {
            NSMutableDictionary *dictTmp = [[NSMutableDictionary alloc] initWithDictionary:_arrPoleOpt[i]];
            NSArray *arrCount = [[NSArray alloc] init];
            arrCount = [dictTmp allKeys];
            for (int j =0; j<arrCount.count; j++) {
                [request setPostValue:dictTmp[arrCount[j]] forKey:arrCount[j]];
            }
        }
        
        
        [request setTimeOutSeconds:180] ;
        
        //-------
        
        [request startAsynchronous] ;
    } else {
        [globalObject removeActivityIndicator];
    }
}

-(void)requestFailed:(ASIHTTPRequest *)request
{
    NSError *error = [request error];
    NSLog(@"error : %@" , error.localizedDescription);
    [globalObject removeActivityIndicator];
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:error.localizedDescription
                                  message:@""
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"OK"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action)
                                {
                                }];
    
    [alert addAction:yesButton];
    
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)requestFinished:(ASIHTTPRequest *)request
{
    NSString *responseString = [request responseString];
    NSMutableDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:[responseString dataUsingEncoding:NSUTF8StringEncoding] options:0 error:nil];
    NSLog(@"Responce Dictionary : %@", responseDict);
    
    [globalObject removeActivityIndicator];
    
    
    if ([[responseDict objectForKey:@"status"] isEqualToString:@"success"])
    {
        if ([StrRequestFor isEqualToString:@"PostOtherData"])
        {
            
            UIAlertController * alert=   [UIAlertController
                                          alertControllerWithTitle:[responseDict objectForKey:@"msg"]                                      message:@""
                                          preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction * ListButton = [UIAlertAction
                                          actionWithTitle:LocalizedString(@"List")
                                          style:UIAlertActionStyleDefault
                                          handler:^(UIAlertAction * action)
                                          {
                                              [self.tabBarController setSelectedIndex:0];
                                              
                                              [self.navigationController popToRootViewControllerAnimated:YES];
                                          }];
            
            
            
            UIAlertAction* NewButton = [UIAlertAction
                                        actionWithTitle:LocalizedString(@"New Scan")
                                        style:UIAlertActionStyleDefault
                                        handler:^(UIAlertAction * action)
                                        {
                                            [self.navigationController popToRootViewControllerAnimated:YES];
                                        }];
            
            [alert addAction:ListButton];
            
            [alert addAction:NewButton];
            
            [self presentViewController:alert animated:YES completion:nil];
        }
        else if ([StrRequestFor isEqualToString:@"checkpoleid"])
        {
            
            if ([[[sqlObj getAllUserInfo] valueForKey:@"assetsView"] isEqualToString:@"No"])
            {
                [self PostSLCData];
            }
            else
            {
                if ([[[sqlObj getAllUserInfo] valueForKey:@"OtherData"] isEqualToString:@"No"])
                {
                    [self PostSLCData];
                }
                else
                {
                    PoleDataViewController *PoleDataView = [[PoleDataViewController alloc] initWithNibName:@"PoleDataViewController" bundle:nil] ;
                    
                    PoleDataView.strMacID = strMacID ;
                    PoleDataView.strSlcID = strSlcID ;
                    PoleDataView.strLat = strLat ;
                    PoleDataView.strLong = strLong ;
                    PoleDataView.strPoleID = txtPoleID.text ;
                    PoleDataView.strAddress = strAddress ;
                    PoleDataView.arrPoleOpt = _arrPoleOpt;
                    PoleDataView.strNotes = _strNotes;
                    PoleDataView.PoleImg = PoleImg ;
                    
                    [self.navigationController pushViewController:PoleDataView animated:YES] ;
                }
            }
        }
        else if ([StrRequestFor isEqualToString:@"Login"])
        {
            [sqlObj insertKeyToDatabase:@"user_logged_in" value:@"yes"];
            
            [sqlObj insertKeyToDatabase:@"token" value:[responseDict objectForKey:@"token"]];
            
        }
    }
    else if ([[responseDict objectForKey:@"status"] isEqualToString:@"logout"])
    {
        // strIDForUpdateData = [responseDict objectForKey:@"SLCID"] ;
        
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:[responseDict objectForKey:@"msg"]                                      message:@""
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
                                        sqlObj = [[SQLManager alloc] init] ;
                                        [sqlObj createAndOpenDatabase] ;
                                        
                                        NSString *strClientID = [[sqlObj getAllUserInfo] valueForKey:@"clientID"] ;
                                        NSString *strUserID = [[sqlObj getAllUserInfo] valueForKey:@"userid"] ;
                                        NSString *strScanLBL = [[sqlObj getAllUserInfo] valueForKey:@"ScanLBL"] ;
                                        NSString *strScanPH = [[sqlObj getAllUserInfo] valueForKey:@"ScanPH"] ;
                                        
                                        NSString *strPoleID = [[sqlObj getAllUserInfo] valueForKey:@"PoleID"] ;
                                        NSString *strMeasurmentUnit = [[sqlObj getAllUserInfo] valueForKey:@"MeasurmentUnit"] ;
                                        NSString *strOtherData = [[sqlObj getAllUserInfo] valueForKey:@"OtherData"] ;
                                        
                                        [sqlObj DeleteData] ;
                                        
                                        [sqlObj createAndOpenDatabase] ;
                                        
                                        [sqlObj insertKeyToDatabase:@"clientID" value:strClientID];
                                        [sqlObj insertKeyToDatabase:@"userid" value:strUserID];
                                        [sqlObj insertKeyToDatabase:@"ScanLBL" value:strScanLBL];
                                        [sqlObj insertKeyToDatabase:@"ScanPH" value:strScanPH];
                                        [sqlObj insertKeyToDatabase:@"PoleID" value:strPoleID];
                                        [sqlObj insertKeyToDatabase:@"MeasurmentUnit" value:strMeasurmentUnit];
                                        [sqlObj insertKeyToDatabase:@"OtherData" value:strOtherData];
                                        
                                        [self AlertForLogin] ;
                                        
                                    }];
        
        [alert addAction:yesButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    }  else if ([[responseDict objectForKey:@"status"] isEqualToString:@"error"]) {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:[responseDict objectForKey:@"msg"]                                      message:@""
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
                                        
                                    }];
        
        [alert addAction:yesButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    } else if ([[responseDict objectForKey:@"status"] isEqualToString:@"0"]) {
        
        NSString *strMsg = [responseDict objectForKey:@"msg"];
        
        if (strMsg == nil || [strMsg  isEqualToString: @""]) {
            strMsg = @"Oops! Something went wrong. Try again later";
        }
        
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:LocalizedString(strMsg)
                                      message:@""
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
            sqlObj = [[SQLManager alloc] init] ;
            [sqlObj createAndOpenDatabase] ;
            
            [sqlObj DeleteData] ;
            
            UIStoryboard *story = [UIStoryboard storyboardWithName:@"SecurityCodeViewController" bundle:nil];
            
            SecurityCodeViewController *SecurityCodeViewController = [story instantiateViewControllerWithIdentifier:@"SecurityCodeViewController"];
            
            UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:SecurityCodeViewController];
            
            [[UIApplication sharedApplication].keyWindow setRootViewController:navController];
        }];
        
        [alert addAction:yesButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    } else {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:LocalizedString(@"Oops! Something went wrong. Try again later")
                                      message:@""
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
                                    }];
        [alert addAction:yesButton];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

-(void)AlertForLogin
{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Login"
                                                                   message:nil
                                                            preferredStyle:UIAlertControllerStyleAlert];
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        // optionally configure the text field
        
        textField.placeholder = @"Username" ;
        textField.borderStyle = UITextBorderStyleRoundedRect ;
        textField.font = [UIFont systemFontOfSize:16.0] ;
        textField.keyboardType = UIKeyboardTypeAlphabet;
        textField.returnKeyType = UIReturnKeySend ;
        textField.delegate = self ;
        
    }];
    
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textFieldPass) {
        // optionally configure the text field
        
        textFieldPass.placeholder = @"Password" ;
        textFieldPass.secureTextEntry = YES;
        textFieldPass.borderStyle = UITextBorderStyleRoundedRect ;
        textFieldPass.font = [UIFont systemFontOfSize:16.0] ;
        textFieldPass.keyboardType = UIKeyboardTypeAlphabet;
        textFieldPass.returnKeyType = UIReturnKeySend ;
        textFieldPass.delegate = self ;
        
    }];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Login"
                                                       style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction *action) {
                                                         NSArray * textfields = alert.textFields;
                                                         UITextField * namefield = textfields[0];
                                                         UITextField * passfield = textfields[1];
                                                         
                                                         strUsername = namefield.text ;
                                                         strPassword = passfield.text ;
                                                         
                                                         [self loginAction];
                                                         
                                                     }];
    
    
    [alert addAction:okAction];
    
    [self presentViewController:alert animated:YES completion:nil];
    
    
}

-(void)loginAction
{
    if ([strUsername isEqualToString:@""] || [strPassword isEqualToString:@""])
    {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:@"Please Enter Valid Username and Password."                                     message:@""
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
                                        [self AlertForLogin] ;
                                        
                                    }];
        
        [alert addAction:yesButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
    else
    {
        [self loginRequest];
    }
}


#pragma mark - Data Post

-(void)loginRequest
{
    if ([globalObject checkInternetConnection]) {
        [globalObject showActivityIndicator];
        
        StrRequestFor = @"Login" ;
        
        NSString *str = userLoginURL ;
        NSString *urlString = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *url = [NSURL URLWithString:urlString];
        
        ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
        
        [request setRequestMethod:@"POST"] ;
        [request setDelegate:self] ;
        
        //------- Post Data
        
        // ---------- Initialize SqlObject
        
        sqlObj = [[SQLManager alloc] init] ;
        
        [sqlObj createAndOpenDatabase] ;
        
        NSLog(@"%@",[[sqlObj getAllUserInfo] valueForKey:@"clientID"]);
        
        NSLog(@"%@",[[sqlObj getAllUserInfo] valueForKey:@"userid"]);
        
        NSLog(@"%@",[[UIDevice currentDevice] identifierForVendor]);
        
        NSLog(@"%f",locationManager.location.coordinate.latitude);
        
        NSLog(@"%f",locationManager.location.coordinate.longitude);
        
        // 65D78ADF-32BC-4A73-B7BC-C9ADBC4C9F55
        
        NSString *strLat = [NSString stringWithFormat:@"%f",locationManager.location.coordinate.latitude];
        
        NSString *strLong = [NSString stringWithFormat:@"%f",locationManager.location.coordinate.longitude];
        
        [request setPostValue:[[sqlObj getAllUserInfo] valueForKey:@"clientID"] forKey:@"client_id"] ;
        
        [request setPostValue:[[sqlObj getAllUserInfo] valueForKey:@"userid"] forKey:@"userid"] ;
        
        [request setPostValue:strUsername forKey:@"username"] ;
        
        [request setPostValue:strPassword forKey:@"password"] ;
        
        [request setPostValue:strLat forKey:@"lat"] ;
        
        [request setPostValue:strLong forKey:@"long"] ;
        
        // [request setPostValue:[[[UIDevice currentDevice] identifierForVendor] UUIDString] forKey:@"DeviceID"] ;
        
        [request setPostValue:@"IOS" forKey:@"source"] ;
        
        [request setTimeOutSeconds:180] ;
        
        //-------
        
        [request startAsynchronous] ;
    } else {
        [globalObject removeActivityIndicator];
    }
}

/*
 
 - (IBAction)ImageScanButton:(id)sender
 {
 
 }
 
 - (IBAction)VoiceRecButton:(id)sender
 {
 if (audioEngine.isRunning) {
 [globalObject showActivityIndicator];
 recognitionTask =[speechRecognizer recognitionTaskWithRequest:recognitionRequest resultHandler:^(SFSpeechRecognitionResult * _Nullable result, NSError * _Nullable error) {
 [globalObject removeActivityIndicator];
 if (result != nil) {
 NSString *transcriptText = result.bestTranscription.formattedString;
 txtPoleID.text = transcriptText;
 }
 else {
 [audioEngine stop];;
 recognitionTask = nil;
 recognitionRequest = nil;
 }
 }];
 // make sure you release tap on bus else your app will crash the second time you record.
 [inputNode removeTapOnBus:0];
 
 [audioEngine stop];
 [recognitionRequest endAudio];
 [speakButton setImage:[UIImage imageNamed:@"voice_contest"] forState:UIControlStateNormal];
 animationImageView.hidden = YES;
 
 }
 else {
 [self startRecording];
 }
 }
 
 #pragma mark - Voice SCAN
 
 - (void)viewDidAppear:(BOOL)animated {
 [super viewDidAppear:animated];
 
 audioEngine = [[AVAudioEngine alloc] init];
 
 NSLocale *local =[[NSLocale alloc] initWithLocaleIdentifier:@"en-US"];
 speechRecognizer = [[SFSpeechRecognizer alloc] initWithLocale:local];
 
 
 //  for (NSLocale *locate in [SFSpeechRecognizer supportedLocales]) {
 //      NSLog(@"%@", [locate localizedStringForCountryCode:locate.countryCode]);
 //  }
 
 // Check Authorization Status
 // Make sure you add "Privacy - Microphone Usage Description" key and reason in Info.plist to request micro permison
 // And "NSSpeechRecognitionUsageDescription" key for requesting Speech recognize permison
 [SFSpeechRecognizer requestAuthorization:^(SFSpeechRecognizerAuthorizationStatus status) {
 
 dispatch_async(dispatch_get_main_queue(), ^{
 switch (status) {
 case SFSpeechRecognizerAuthorizationStatusAuthorized: {
 speakButton.enabled = YES;
 break;
 }
 case SFSpeechRecognizerAuthorizationStatusDenied: {
 speakButton.enabled = NO;
 //  resultTextView.text = @"User denied access to speech recognition";
 }
 case SFSpeechRecognizerAuthorizationStatusRestricted: {
 speakButton.enabled = NO;
 //  resultTextView.text = @"User denied access to speech recognition";
 }
 case SFSpeechRecognizerAuthorizationStatusNotDetermined: {
 speakButton.enabled = NO;
 //  resultTextView.text = @"User denied access to speech recognition";
 }
 }
 });
 
 }];
 }
 
 // Transcript from a file
 - (void)transcriptExampleFromAFile {
 NSURL *url = [[NSBundle mainBundle] URLForResource:@"checkFile" withExtension:@"m4a"];
 urlRequest = [[SFSpeechURLRecognitionRequest alloc] initWithURL:url];
 recognitionTask = [speechRecognizer recognitionTaskWithRequest:urlRequest resultHandler:^(SFSpeechRecognitionResult * _Nullable result, NSError * _Nullable error) {
 if (result != nil) {
 NSString *text = result.bestTranscription.formattedString;
 txtPoleID.text = text;
 }
 else {
 NSLog(@"Error, %@", error.description);
 }
 }];
 }
 
 // recording
 - (void)startRecording {
 
 NSURL *url = [[NSBundle mainBundle] URLForResource:@"recording_animate" withExtension:@"gif"];
 animationImageView.image = [UIImage animatedImageWithAnimatedGIFURL:url];
 animationImageView.hidden = NO;
 [speakButton setImage:[UIImage imageNamed:@"voice_contest_recording"] forState:UIControlStateNormal];
 
 if (recognitionTask) {
 [recognitionTask cancel];
 recognitionTask = nil;
 }
 
 AVAudioSession *session = [AVAudioSession sharedInstance];
 [session setCategory:AVAudioSessionCategoryRecord mode:AVAudioSessionModeMeasurement options:AVAudioSessionCategoryOptionDefaultToSpeaker error:nil];
 [session setActive:TRUE withOptions:AVAudioSessionSetActiveOptionNotifyOthersOnDeactivation error:nil];
 
 inputNode = audioEngine.inputNode;
 
 recognitionRequest = [[SFSpeechAudioBufferRecognitionRequest alloc] init];
 recognitionRequest.shouldReportPartialResults = NO;
 //  recognitionRequest.detectMultipleUtterances = YES;
 
 AVAudioFormat *format = [inputNode outputFormatForBus:0];
 
 [inputNode installTapOnBus:0 bufferSize:1024 format:format block:^(AVAudioPCMBuffer * _Nonnull buffer, AVAudioTime * _Nonnull when) {
 [recognitionRequest appendAudioPCMBuffer:buffer];
 }];
 [audioEngine prepare];
 NSError *error1;
 [audioEngine startAndReturnError:&error1];
 NSLog(@"%@", error1.description);
 
 }
 */

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)eventof:(NSString *)strEventName {
    NSString *strClientID = [[sqlObj getAllUserInfo] valueForKey:@"clientID"];
    NSString *strUserID = [[sqlObj getAllUserInfo] valueForKey:@"userid"];
    
    [FIRAnalytics logEventWithName:kFIREventSelectContent
                        parameters:@{
                                     kFIRParameterItemName:[NSString stringWithFormat:@"%@_%@_%@",strClientID, strUserID, strEventName],
                                     }];
}

@end
