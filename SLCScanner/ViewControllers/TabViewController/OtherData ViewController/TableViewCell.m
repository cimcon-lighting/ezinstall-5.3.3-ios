//
//  TableViewCell.m
//  SLCScanner
//
//  Created by INT MAC 2015 on 26/02/18.
//  Copyright © 2018 INT. All rights reserved.
//

#import "TableViewCell.h"

@implementation TableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
   
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated
{
    [super setHighlighted:highlighted animated:animated];
    /*
    if (highlighted) {
        self.menuItemLabel.backgroundColor = [UIColor colorWithRed:81.0/255 green:168.0/255 blue:101.0/255 alpha:1];
    }
    else {
        self.menuItemLabel.backgroundColor = [UIColor colorWithRed:40.0/255 green:196.0/255 blue:80.0/255 alpha:1];
    }*/
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
