//
//  TableViewCell.h
//  SLCScanner
//
//  Created by INT MAC 2015 on 26/02/18.
//  Copyright © 2018 INT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *menuItemLabel;
@property (strong, nonatomic) IBOutlet UIImageView *selectedMarkView;
@property (strong, nonatomic) IBOutlet UIImageView *MapTypeImage;

@end
