//
//  AddressViewController.m
//  SLCScanner
//
//  Created by INT MAC 2015 on 30/04/18.
//  Copyright © 2018 INT. All rights reserved.
//

#import "AddressViewController.h"

#import "LocalizeHelper.h"
#import "PoleOptionController.h"
#import "PoleDataViewController.h"
#import "PoleImageViewController.h"
#import "ScanPoleIDViewController.h"
#import "SecurityCodeViewController.h"

#import <CoreLocation/CoreLocation.h>
#import <FirebaseAnalytics/FirebaseAnalytics.h>

@interface AddressViewController () <CLLocationManagerDelegate, UITextViewDelegate>
{
    NSString *strRequestFor ;
    
    //-------------------------------------------
    
    NSString *strUsername ;
    
    NSString *strPassword ;
    
    CLLocationManager *locationManager;
    
    NSString *strIDForUpdateData ;
    
    IBOutlet UILabel *lbladdess;
}
@end

@implementation AddressViewController
@synthesize strLat, strLong, strMacID, strSlcID ;
@synthesize globalObject, sqlObj ;
@synthesize btnConfirm, btnEdit ;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self eventof:@"Address"];
    
    self.title = @"Address" ;
    
    //Lat long code
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self ;
    [locationManager requestWhenInUseAuthorization] ;
    locationManager.distanceFilter = kCLDistanceFilterNone; // whenever we move
    locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation ;
    
    [locationManager startUpdatingLocation];
    
    globalObject = [[GlobalAssistant alloc] init] ;
    [globalObject buttonWithCornerRadious:22.0 btnView:btnEdit];
    [globalObject buttonWithCornerRadious:22.0 btnView:btnConfirm];
    
    UIImage* imgback = [UIImage imageNamed:@"left_arrow"];
    CGRect frameimgBk = CGRectMake(0,0,25,25);
    
    UIButton *btnBack = [[UIButton alloc] initWithFrame:frameimgBk];
    [btnBack setBackgroundImage:imgback forState:UIControlStateNormal];
    [btnBack addTarget:self action:@selector(btnBackClick)
      forControlEvents:UIControlEventTouchUpInside];
    [btnBack setShowsTouchWhenHighlighted:YES];
    
    UIBarButtonItem *bkBtn =[[UIBarButtonItem alloc] initWithCustomView:btnBack];
    self.navigationItem.leftBarButtonItems = @[bkBtn];
}

-(void)viewWillAppear:(BOOL)animated {
    [self AddressDataPost];
    [self localizationSetup];
}

-(void)btnBackClick{
    [self.navigationController popViewControllerAnimated:true];
}

-(void)localizationSetup {
    self.title          = LocalizedString(@"Address");
    lbladdess.text      = LocalizedString(@"Enter Address");
    [btnConfirm setTitle:LocalizedString(@"Confirm") forState:UIControlStateNormal];
    [btnEdit setTitle:LocalizedString(@"Edit") forState:UIControlStateNormal];
    _txtView.text       = LocalizedString(@"Enter Address");
}

- (IBAction)OKButton:(id)sender
{
    [self eventof:@"AddressConfirm"];
    
    if ([_txtView.text isEqualToString:@""] ||  [_txtView.text isEqualToString:LocalizedString(@"Enter Address")] || [_txtView.text length]==0)
    {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:LocalizedString(@"Please Enter Valid Address")                                     message:@""
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
                                        //Handel your yes please button action here
                                        
                                    }];
        
        [alert addAction:yesButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
    else
    {
        
        sqlObj = [[SQLManager alloc] init] ;
        
        [sqlObj createAndOpenDatabase] ;
        
        if ([[[sqlObj getAllUserInfo] valueForKey:@"ImageView"] isEqualToString:@"No"])
        {
            
            UIStoryboard *story = [UIStoryboard storyboardWithName:@"PoleOptionController" bundle:nil];
            
            PoleOptionController *PoleDataView = [story instantiateViewControllerWithIdentifier:@"PoleOptionController"];
            
            PoleDataView.isFrmEditView = false;
            PoleDataView.strMacID = strMacID ;
            PoleDataView.strSlcID = strSlcID ;
            PoleDataView.strLat = strLat ;
            PoleDataView.strLong = strLong ;
            PoleDataView.strAddress = _txtView.text ;
            PoleDataView.strPoleID = @"" ;
            
            /*if ([ImageSelect isEqualToString:@"YES"])
            {
                PoleDataView.PoleImg = PoleImg.image ;
            }*/
            
            [self.navigationController pushViewController:PoleDataView animated:YES] ;
            /*UIStoryboard *story = [UIStoryboard storyboardWithName:@"ScanPoleIDViewController" bundle:nil];
            
            ScanPoleIDViewController *ScanPoleIDViewController = [story instantiateViewControllerWithIdentifier:@"ScanPoleIDViewController"];
            
            ScanPoleIDViewController.strMacID = strMacID ;
            
            ScanPoleIDViewController.strSlcID = strSlcID ;
            
            ScanPoleIDViewController.strLat = strLat ;
            
            ScanPoleIDViewController.strLong = strLong ;
            
            ScanPoleIDViewController.strAddress = _txtView.text ;
            
            [self.navigationController pushViewController:ScanPoleIDViewController animated:YES];*/
        }
        else
        {
            UIStoryboard *story = [UIStoryboard storyboardWithName:@"PoleImageViewController" bundle:nil];
            
            PoleImageViewController *PoleImageViewController = [story instantiateViewControllerWithIdentifier:@"PoleImageViewController"];
            
            PoleImageViewController.strMacID = strMacID ;
            
            PoleImageViewController.strSlcID = strSlcID ;
            
            PoleImageViewController.strLat = strLat ;
            
            PoleImageViewController.strLong = strLong ;
            
            PoleImageViewController.strAddress = _txtView.text ;
            
            [self.navigationController pushViewController:PoleImageViewController animated:YES];
        }
        
    }
}

- (IBAction)CancelButton:(id)sender
{
    [_txtView becomeFirstResponder];
}

#pragma mark - Data Post For Address

-(void)AddressDataPost
{
    if ([globalObject checkInternetConnection]) {
        [globalObject showActivityIndicator];
        
        strRequestFor = @"Address" ;
        
        NSString *str =  addressURL;
        NSString *urlString = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *url = [NSURL URLWithString:urlString];
        
        ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
        
        [request setRequestMethod:@"POST"] ;
        [request setDelegate:self] ;
        
        //------- Post Data
        
        sqlObj = [[SQLManager alloc] init] ;
        
        [sqlObj createAndOpenDatabase] ;
        
        [request setPostValue:strLat forKey:@"lat"] ;
        
        [request setPostValue:strLong forKey:@"lng"] ;
        
        [request setPostValue:[[sqlObj getAllUserInfo] valueForKey:@"clientID"] forKey:@"client_id"] ;
        
        [request setPostValue:[[sqlObj getAllUserInfo] valueForKey:@"userid"] forKey:@"user_id"] ;
        
        //-------
        
        [request startAsynchronous] ;
    } else {
        [globalObject removeActivityIndicator];
    }
}

-(void)requestFailed:(ASIHTTPRequest *)request
{
    NSError *error = [request error];
    NSLog(@"error : %@" , error.localizedDescription);
    [globalObject removeActivityIndicator];
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:error.localizedDescription
                                  message:@""
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"OK"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action)
                                {
                                }];
    
    [alert addAction:yesButton];
    
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)requestFinished:(ASIHTTPRequest *)request
{
    NSString *responseString = [request responseString];
    NSMutableDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:[responseString dataUsingEncoding:NSUTF8StringEncoding] options:0 error:nil];
    NSLog(@"Responce Dictionary : %@", responseDict);
    
    // --------- Initialize GlobalAssistant
    
    globalObject = [[GlobalAssistant alloc] init] ;
    
    [globalObject removeActivityIndicator];
    
    
    if ([[responseDict objectForKey:@"status"] isEqualToString:@"success"])
    {
        
        if ([strRequestFor isEqualToString:@"Address"])
        {
            _txtView.text = [responseDict objectForKey:@"address"] ;
            
        }
        
        
        [sqlObj updateDATA:[[responseDict objectForKey:@"setting"] valueForKey:@"client_slc_pole_image_view"] :@"ImageView"] ;
        
        [sqlObj updateDATA:[[responseDict objectForKey:@"setting"] valueForKey:@"client_slc_pole_assets_view"] :@"assetsView"] ;
        
        /*  else if ([strRequestFor isEqualToString:@"DataPost"])
         {
         UIAlertController * alert=   [UIAlertController
         alertControllerWithTitle:[responseDict objectForKey:@"msg"]                                      message:@""
         preferredStyle:UIAlertControllerStyleAlert];
         
         UIAlertAction * ListButton = [UIAlertAction
         actionWithTitle:@"List"
         style:UIAlertActionStyleDefault
         handler:^(UIAlertAction * action)
         {
         [self.tabBarController setSelectedIndex:0];
         
         [self.navigationController popToRootViewControllerAnimated:YES];
         }];
         
         
         
         UIAlertAction* NewButton = [UIAlertAction
         actionWithTitle:@"New Scan"
         style:UIAlertActionStyleDefault
         handler:^(UIAlertAction * action)
         {
         [self.navigationController popToRootViewControllerAnimated:YES];
         }];
         
         [alert addAction:ListButton];
         
         [alert addAction:NewButton];
         
         [self presentViewController:alert animated:YES completion:nil];
         }
         else  if ([strRequestFor isEqualToString:@"Login"])
         {
         [sqlObj insertKeyToDatabase:@"user_logged_in" value:@"yes"];
         
         [sqlObj insertKeyToDatabase:@"token" value:[responseDict objectForKey:@"token"]];
         
         }*/
    } else if ([[responseDict objectForKey:@"status"] isEqualToString:@"error"]) {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:[responseDict objectForKey:@"msg"]                                      message:@""
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
                                        
                                    }];
        
        [alert addAction:yesButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    } else if ([[responseDict objectForKey:@"status"] isEqualToString:@"0"]) {
        
        NSString *strMsg = [responseDict objectForKey:@"msg"];
        
        if (strMsg == nil || [strMsg  isEqualToString: @""]) {
            strMsg = @"Oops! Something went wrong. Try again later";
        }
        
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:LocalizedString(strMsg)
                                      message:@""
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
            sqlObj = [[SQLManager alloc] init] ;
            [sqlObj createAndOpenDatabase] ;
            
            [sqlObj DeleteData] ;
            
            UIStoryboard *story = [UIStoryboard storyboardWithName:@"SecurityCodeViewController" bundle:nil];
            
            SecurityCodeViewController *SecurityCodeViewController = [story instantiateViewControllerWithIdentifier:@"SecurityCodeViewController"];
            
            UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:SecurityCodeViewController];
            
            [[UIApplication sharedApplication].keyWindow setRootViewController:navController];
        }];
        
        [alert addAction:yesButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    } else {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:LocalizedString(@"Oops! Something went wrong. Try again later")
                                      message:@""
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
                                    }];
        [alert addAction:yesButton];
        [self presentViewController:alert animated:YES completion:nil];
    }
}
/*
 #pragma mark - Data Post
 
 -(void)PostSLCData
 {
 // --------- Initialize GlobalAssistant
 
 globalObject = [[GlobalAssistant alloc] init] ;
 
 [globalObject showActivityIndicator];
 
 strRequestFor = @"DataPost" ;
 
 NSString *str = PostOtherDataURL ;
 NSString *urlString = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
 NSURL *url = [NSURL URLWithString:urlString];
 
 ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
 
 [request setRequestMethod:@"POST"] ;
 [request setDelegate:self] ;
 
 //------- Post Data
 
 // ---------- Initialize SqlObject
 
 sqlObj = [[SQLManager alloc] init] ;
 
 [sqlObj createAndOpenDatabase] ;
 
 NSLog(@"%@",[[sqlObj getAllUserInfo] valueForKey:@"clientID"]);
 
 NSLog(@"%@",[[sqlObj getAllUserInfo] valueForKey:@"userid"]);
 
 [request setPostValue:[[sqlObj getAllUserInfo] valueForKey:@"clientID"] forKey:@"client_id"] ;
 
 [request setPostValue:[[sqlObj getAllUserInfo] valueForKey:@"userid"] forKey:@"user_id"] ;
 
 [request setPostValue:strMacID forKey:@"mac_address"] ;
 
 [request setPostValue:strSlcID forKey:@"slc_id"] ;
 
 [request setPostValue:strLat forKey:@"lat"] ;
 
 [request setPostValue:strLong forKey:@"lng"] ;
 
 [request setPostValue:strIDForUpdateData forKey:@"SLCID"] ;
 
 [request setPostValue:_txtView.text forKey:@"address"] ;
 
 [request setPostValue:@"IOS" forKey:@"source"] ;
 
 [request setPostValue:@"Yes" forKey:@"copyasset"] ;
 
 [request setTimeOutSeconds:180] ;
 
 //-------
 
 [request startAsynchronous] ;
 }
 
 -(void)AlertForLogin
 {
 
 UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Login"
 message:nil
 preferredStyle:UIAlertControllerStyleAlert];
 [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
 // optionally configure the text field
 
 textField.placeholder = @"Username" ;
 textField.borderStyle = UITextBorderStyleRoundedRect ;
 textField.font = [UIFont systemFontOfSize:16.0] ;
 textField.keyboardType = UIKeyboardTypeAlphabet;
 textField.returnKeyType = UIReturnKeySend ;
 textField.delegate = self ;
 
 }];
 
 [alert addTextFieldWithConfigurationHandler:^(UITextField *textFieldPass) {
 // optionally configure the text field
 
 textFieldPass.placeholder = @"Password" ;
 textFieldPass.secureTextEntry = YES;
 textFieldPass.borderStyle = UITextBorderStyleRoundedRect ;
 textFieldPass.font = [UIFont systemFontOfSize:16.0] ;
 textFieldPass.keyboardType = UIKeyboardTypeAlphabet;
 textFieldPass.returnKeyType = UIReturnKeySend ;
 textFieldPass.delegate = self ;
 
 }];
 
 UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Login"
 style:UIAlertActionStyleDefault
 handler:^(UIAlertAction *action) {
 NSArray * textfields = alert.textFields;
 UITextField * namefield = textfields[0];
 UITextField * passfield = textfields[1];
 
 strUsername = namefield.text ;
 strPassword = passfield.text ;
 
 [self loginAction];
 
 }];
 
 
 [alert addAction:okAction];
 
 [self presentViewController:alert animated:YES completion:nil];
 
 
 }
 
 -(void)loginAction
 {
 if ([strUsername isEqualToString:@""] || [strPassword isEqualToString:@""])
 {
 UIAlertController * alert=   [UIAlertController
 alertControllerWithTitle:@"Please Enter Valid Username and Password."                                     message:@""
 preferredStyle:UIAlertControllerStyleAlert];
 
 UIAlertAction* yesButton = [UIAlertAction
 actionWithTitle:@"OK"
 style:UIAlertActionStyleDefault
 handler:^(UIAlertAction * action)
 {
 [self AlertForLogin] ;
 
 }];
 
 [alert addAction:yesButton];
 
 [self presentViewController:alert animated:YES completion:nil];
 }
 else
 {
 [self loginRequest];
 }
 }
 
 
 #pragma mark - Data Post
 
 -(void)loginRequest
 {
 [globalObject showActivityIndicator];
 
 strRequestFor = @"Login" ;
 
 NSString *str = userLoginURL ;
 NSString *urlString = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
 NSURL *url = [NSURL URLWithString:urlString];
 
 ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
 
 [request setRequestMethod:@"POST"] ;
 [request setDelegate:self] ;
 
 //------- Post Data
 
 // ---------- Initialize SqlObject
 
 sqlObj = [[SQLManager alloc] init] ;
 
 [sqlObj createAndOpenDatabase] ;
 
 NSLog(@"%@",[[sqlObj getAllUserInfo] valueForKey:@"clientID"]);
 
 NSLog(@"%@",[[sqlObj getAllUserInfo] valueForKey:@"userid"]);
 
 NSLog(@"%@",[[UIDevice currentDevice] identifierForVendor]);
 
 NSLog(@"%f",locationManager.location.coordinate.latitude);
 
 NSLog(@"%f",locationManager.location.coordinate.longitude);
 
 // 65D78ADF-32BC-4A73-B7BC-C9ADBC4C9F55
 
 NSString *strLat = [NSString stringWithFormat:@"%f",locationManager.location.coordinate.latitude];
 
 NSString *strLong = [NSString stringWithFormat:@"%f",locationManager.location.coordinate.longitude];
 
 [request setPostValue:[[sqlObj getAllUserInfo] valueForKey:@"clientID"] forKey:@"client_id"] ;
 
 [request setPostValue:[[sqlObj getAllUserInfo] valueForKey:@"userid"] forKey:@"userid"] ;
 
 [request setPostValue:strUsername forKey:@"username"] ;
 
 [request setPostValue:strPassword forKey:@"password"] ;
 
 [request setPostValue:strLat forKey:@"lat"] ;
 
 [request setPostValue:strLong forKey:@"long"] ;
 
 // [request setPostValue:[[[UIDevice currentDevice] identifierForVendor] UUIDString] forKey:@"DeviceID"] ;
 
 [request setPostValue:@"IOS" forKey:@"source"] ;
 
 [request setTimeOutSeconds:180] ;
 
 //-------
 
 [request startAsynchronous] ;
 }
 */
#pragma mark - TextField Delegate

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}
-(void)textViewDidBeginEditing:(UITextView *)textView {
    if ([textView.text isEqualToString:@"Enter Address"]) {
        textView.text = @"";
    }
}
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([textView.text length]==0)
    {
        if([text isEqualToString:@" "])
        {
            return NO;
        }
    }
    
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)eventof:(NSString *)strEventName {
    dispatch_async(dispatch_get_main_queue(), ^{
    NSString *strClientID = [[sqlObj getAllUserInfo] valueForKey:@"clientID"];
    NSString *strUserID = [[sqlObj getAllUserInfo] valueForKey:@"userid"];
    
    [FIRAnalytics logEventWithName:kFIREventSelectContent
                        parameters:@{
                                     kFIRParameterItemName:[NSString stringWithFormat:@"%@_%@_%@",strClientID, strUserID, strEventName],
                                     }];
    });
}

@end
