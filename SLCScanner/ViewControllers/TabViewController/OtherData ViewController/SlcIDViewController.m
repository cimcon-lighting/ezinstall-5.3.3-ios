//
//  SlcIDViewController.m
//  SLCScanner
//
//  Created by INT MAC 2015 on 07/03/18.
//  Copyright © 2018 INT. All rights reserved.
//

#import "SlcIDViewController.h"

#import "API.h"
#import "LocalizeHelper.h"
#import "MapViewController.h"
#import "SecurityCodeViewController.h"

@interface SlcIDViewController () <CLLocationManagerDelegate,UITextFieldDelegate>
{
    UIButton *alertButton  ;
    NSString *SLCscannedCode ;
    UIView *SLClaserView ;
    
    NSString *strFinamSLCAdd ;
    
    
    IBOutlet UILabel *lblNodeHeading;
    IBOutlet UILabel *lblCustomerName;
    
    IBOutlet UIView *vwCustomerName;
    
    NSString *strRequestFor ;
    
    //-------------------------------------------
    
    NSString *strUsername ;
    
    NSString *strPassword ;
    
    NSMutableArray *arrNodetype ;
    
    CLLocationManager *locationManager;
    
    NSString *strIDForUpdateData ;
}

@property (nonatomic, weak) IBOutlet UIButton *btnNodetype;
@property (nonatomic, strong) ZXCapture *SLCcapture;
@property (nonatomic, weak) IBOutlet UIView *nodeTypeView;
@property (nonatomic, weak) IBOutlet UIView *SLCscanRectView;
@property (nonatomic, weak) IBOutlet UILabel *decodedLabel;

@end

@implementation SlcIDViewController
{
    CGAffineTransform _captureSizeTransform;
}
@synthesize strMacAddress , globalObject, sqlObj ;


- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = [NSString stringWithFormat:@"%@",LocalizedString(@"SCAN SLC ID")];
    
    SLCscannedCode = @"" ;
    
    // --------- Initialize GlobalAssistant
    
    globalObject = [[GlobalAssistant alloc] init] ;
    
    sqlObj          = [[SQLManager alloc] init] ;
    [sqlObj createAndOpenDatabase] ;
    
    //Lat long code
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self ;
    [locationManager requestWhenInUseAuthorization] ;
    locationManager.distanceFilter = kCLDistanceFilterNone; // whenever we move
    locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation ;
    
    [locationManager startUpdatingLocation];
    
    
    //--------------------------------
    
    self.SLCcapture = [[ZXCapture alloc] init];
    self.SLCcapture.camera = self.SLCcapture.back;
    self.SLCcapture.focusMode = AVCaptureFocusModeContinuousAutoFocus;
    
    [self.view.layer addSublayer:self.SLCcapture.layer];
    
    [self.view bringSubviewToFront:self.SLCscanRectView];
    [self.view bringSubviewToFront:self.nodeTypeView];
    [self.view bringSubviewToFront:self.decodedLabel];
    [self.view bringSubviewToFront:vwCustomerName];
    
    //-----------------------------------------------
    
    //-----------------------------------------------
    
    CGRect frameimg = CGRectMake(0,0,25,25);
    
    UIButton *alertButton = [[UIButton alloc] initWithFrame:frameimg];
    [alertButton setImage:[UIImage imageNamed:@"finger"] forState:UIControlStateNormal];
    //[alertButton setTitle:@"ADD" forState:UIControlStateNormal];
    alertButton.titleLabel.font = [UIFont systemFontOfSize:14.0];
    [alertButton addTarget:self action:@selector(AlertAction)
          forControlEvents:UIControlEventTouchUpInside];
    [alertButton setShowsTouchWhenHighlighted:YES];
    
    UIImage* imgback = [UIImage imageNamed:@"left_arrow"];
    CGRect frameimgBk = CGRectMake(0,0,25,25);
    UIBarButtonItem *notificationbutton =[[UIBarButtonItem alloc] initWithCustomView:alertButton];
    self.navigationItem.rightBarButtonItem = notificationbutton;
    UIButton *btnBack = [[UIButton alloc] initWithFrame:frameimgBk];
    [btnBack setBackgroundImage:imgback forState:UIControlStateNormal];
    [btnBack addTarget:self action:@selector(btnBackClick)
      forControlEvents:UIControlEventTouchUpInside];
    [btnBack setShowsTouchWhenHighlighted:YES];
    
    UIBarButtonItem *bkBtn =[[UIBarButtonItem alloc] initWithCustomView:btnBack];
    self.navigationItem.leftBarButtonItems = @[bkBtn];
    
}


-(void)AlertAction
{
    [self.SLCcapture stop];
    
    [self AlertForConfirm] ;
}

-(void)redLineShow
{
    SLClaserView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.SLCscanRectView.frame.size.width, 2)];
    SLClaserView.backgroundColor = [UIColor redColor];
    //  laserView.layer.shadowColor = CFBridgingRetain([UIColor redColor]);
    SLClaserView.layer.shadowOffset = CGSizeMake(0.5, 0.5);
    SLClaserView.layer.shadowOpacity = 0.6;
    SLClaserView.layer.shadowRadius = 1.5;
    SLClaserView.alpha = 0.0;
    [self.SLCscanRectView addSubview:SLClaserView];
    
    // Add the line
    [UIView animateWithDuration:0.2 animations:^{
        SLClaserView.alpha = 1.0;
    }];
    
    [UIView animateWithDuration:4.0 delay:0.0 options:UIViewAnimationOptionAutoreverse | UIViewAnimationOptionRepeat | UIViewAnimationOptionCurveEaseInOut animations:^{
        SLClaserView.frame = CGRectMake(0, self.SLCscanRectView.frame.size.height, self.SLCscanRectView.frame.size.width, 2);
    } completion:nil];
    
}

-(void)viewDidDisappear:(BOOL)animated
{
    [SLClaserView removeFromSuperview];
    
    self.SLCcapture.delegate = nil ;
}



#pragma mark - View Controller Methods

- (void)dealloc {
    [self.SLCcapture.layer removeFromSuperlayer];
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    lblCustomerName.text = [[sqlObj getAllUserInfo] valueForKey:@"CUSTOMER_NAME"];
    
    [self cameraPermission];
    
    [self redLineShow];
    
    self.SLCcapture.delegate = self;
    
    [self.SLCcapture start];
    
    [self applyOrientation];
    [self localizationSetup];
}
-(void)cameraPermission {
    AVAuthorizationStatus status = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    
    if(status == AVAuthorizationStatusDenied) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:LocalizedString(@"Please enter the code manually by tapping on hand icon on top right corner") delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
    }
}
-(void)localizationSetup {
    self.title = [NSString stringWithFormat:@"%@",LocalizedString(@"SCAN SLC ID")];//LocalizedString(@"SCAN SLC ID");
    _decodedLabel.text = LocalizedString(@"Position Code in Box Above");
    
}
-(void)btnBackClick{
    [self.navigationController popViewControllerAnimated:true];
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
    return toInterfaceOrientation == UIInterfaceOrientationPortrait;
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
    [self applyOrientation];
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id <UIViewControllerTransitionCoordinator>)coordinator {
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    [coordinator animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext> context) {
    } completion:^(id<UIViewControllerTransitionCoordinatorContext> context)
     {
        [self applyOrientation];
    }];
}

- (IBAction)nodeType_Click:(UIButton *)sender {
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle: @"Select node type"
                                                                         message:nil
                                                                  preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:LocalizedString(@"Cancel") style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    
    for (NSMutableDictionary *dictData in arrNodetype) {
        
        NSString *strClientNme = dictData[@"clientType"];
        
        if ([strClientNme isEqualToString: _btnNodetype.titleLabel.text])
        {
            [actionSheet addAction:[UIAlertAction actionWithTitle:strClientNme style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
                [self dismissViewControllerAnimated:YES completion:^{
                }];
            }]];
        }
        else
        {
            [actionSheet addAction:[UIAlertAction actionWithTitle:strClientNme style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                
                
                [self->_btnNodetype setTitle:action.title forState:UIControlStateNormal];
                
                if (![action.title isEqualToString:@"Unknown"]) {
                    [[NSUserDefaults standardUserDefaults] setValue:action.title forKey:@"SELECTDODETYPE"];
                } else {
                    [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"SELECTDODETYPE"];
                }
                
                [self dismissViewControllerAnimated:YES completion:^{
                }];
            }]];
        }
        
    }
    
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
        [actionSheet setModalPresentationStyle:UIModalPresentationPopover];
        
        UIPopoverPresentationController *popPresenter = [actionSheet
                                                         popoverPresentationController];
        popPresenter.sourceView = _btnNodetype;
        popPresenter.sourceRect = _btnNodetype.bounds;
        
    }
    
    [self presentViewController:actionSheet animated:YES completion:nil];
    
}

#pragma mark - Private

- (void)applyOrientation {
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    float scanRectRotation;
    float captureRotation;
    
    switch (orientation) {
        case UIInterfaceOrientationPortrait:
            captureRotation = 0;
            scanRectRotation = 90;
            break;
        case UIInterfaceOrientationLandscapeLeft:
            captureRotation = 90;
            scanRectRotation = 180;
            break;
        case UIInterfaceOrientationLandscapeRight:
            captureRotation = 270;
            scanRectRotation = 0;
            break;
        case UIInterfaceOrientationPortraitUpsideDown:
            captureRotation = 180;
            scanRectRotation = 270;
            break;
        default:
            captureRotation = 0;
            scanRectRotation = 90;
            break;
    }
    [self applyRectOfInterest:orientation];
    CGAffineTransform transform = CGAffineTransformMakeRotation((CGFloat) (captureRotation / 180 * M_PI));
    [self.SLCcapture setTransform:transform];
    [self.SLCcapture setRotation:scanRectRotation];
    
    self.SLCcapture.layer.frame = CGRectMake(self.view.frame.origin.x, 0, self.view.frame.size.width, self.view.frame.size.height) ;
    
    // self.capture.layer.frame = self.view.frame;
}

- (void)applyRectOfInterest:(UIInterfaceOrientation)orientation {
    CGFloat scaleVideo, scaleVideoX, scaleVideoY;
    CGFloat videoSizeX, videoSizeY;
    CGRect transformedVideoRect = self.SLCscanRectView.frame;
    if([self.SLCcapture.sessionPreset isEqualToString:AVCaptureSessionPreset1920x1080]) {
        videoSizeX = 1080;
        videoSizeY = 1920;
    } else {
        videoSizeX = 720;
        videoSizeY = 1280;
    }
    if(UIInterfaceOrientationIsPortrait(orientation)) {
        scaleVideoX = self.view.frame.size.width / videoSizeX;
        scaleVideoY = self.view.frame.size.height / videoSizeY;
        scaleVideo = MAX(scaleVideoX, scaleVideoY);
        if(scaleVideoX > scaleVideoY) {
            transformedVideoRect.origin.y += (scaleVideo * videoSizeY - self.view.frame.size.height) / 2;
        } else {
            transformedVideoRect.origin.x += (scaleVideo * videoSizeX - self.view.frame.size.width) / 2;
        }
    } else {
        scaleVideoX = self.view.frame.size.width / videoSizeY;
        scaleVideoY = self.view.frame.size.height / videoSizeX;
        scaleVideo = MAX(scaleVideoX, scaleVideoY);
        if(scaleVideoX > scaleVideoY) {
            transformedVideoRect.origin.y += (scaleVideo * videoSizeX - self.view.frame.size.height) / 2;
        } else {
            transformedVideoRect.origin.x += (scaleVideo * videoSizeY - self.view.frame.size.width) / 2;
        }
    }
    _captureSizeTransform = CGAffineTransformMakeScale(1/scaleVideo, 1/scaleVideo);
    self.SLCcapture.scanRect = CGRectApplyAffineTransform(transformedVideoRect, _captureSizeTransform);
    
    
}

#pragma mark - Private Methods

- (NSString *)barcodeFormatToString:(ZXBarcodeFormat)format {
    switch (format) {
        case kBarcodeFormatAztec:
            return @"Aztec";
            
        case kBarcodeFormatCodabar:
            return @"CODABAR";
            
        case kBarcodeFormatCode39:
            return @"Code 39";
            
        case kBarcodeFormatCode93:
            return @"Code 93";
            
        case kBarcodeFormatCode128:
            return @"Code 128";
            
        case kBarcodeFormatDataMatrix:
            return @"Data Matrix";
            
        case kBarcodeFormatEan8:
            return @"EAN-8";
            
        case kBarcodeFormatEan13:
            return @"EAN-13";
            
        case kBarcodeFormatITF:
            return @"ITF";
            
        case kBarcodeFormatPDF417:
            return @"PDF417";
            
        case kBarcodeFormatQRCode:
            return @"QR Code";
            
        case kBarcodeFormatRSS14:
            return @"RSS 14";
            
        case kBarcodeFormatRSSExpanded:
            return @"RSS Expanded";
            
        case kBarcodeFormatUPCA:
            return @"UPCA";
            
        case kBarcodeFormatUPCE:
            return @"UPCE";
            
        case kBarcodeFormatUPCEANExtension:
            return @"UPC/EAN extension";
            
        default:
            return @"Unknown";
    }
}

#pragma mark - ZXCaptureDelegate Methods

- (void)captureResult:(ZXCapture *)capture result:(ZXResult *)result {
    if (!result) return;
    
    CGAffineTransform inverse = CGAffineTransformInvert(_captureSizeTransform);
    NSMutableArray *points = [[NSMutableArray alloc] init];
    NSString *location = @"";
    for (ZXResultPoint *resultPoint in result.resultPoints) {
        CGPoint cgPoint = CGPointMake(resultPoint.x, resultPoint.y);
        CGPoint transformedPoint = CGPointApplyAffineTransform(cgPoint, inverse);
        transformedPoint = [self.SLCscanRectView convertPoint:transformedPoint toView:self.SLCscanRectView.window];
        NSValue* windowPointValue = [NSValue valueWithCGPoint:transformedPoint];
        location = [NSString stringWithFormat:@"%@ (%f, %f)", location, transformedPoint.x, transformedPoint.y];
        [points addObject:windowPointValue];
    }
    
    
    // We got a result. Display information about the result onscreen.
    NSString *formatString = [self barcodeFormatToString:result.barcodeFormat];
    NSString *display = [NSString stringWithFormat:@"Scanned!\n\nFormat: %@\n\nContents:\n%@\nLocation: %@", formatString, result.text, location];
    NSLog(@"%@",display);
    
    // Vibrate
    AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
    
    SLCscannedCode = result.text ;
    
    [self.SLCcapture stop];
    
    [self AlertForConfirm] ;
    
    //  [self.decodedLabel performSelectorOnMainThread:@selector(setText:) withObject:display waitUntilDone:YES];
    
    /*
     dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
     [self.capture start];
     });*/
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)AlertForConfirm
{
    
    NSLog(@"%@",SLCscannedCode) ;
    
    // ---------- Initialize SqlObject
    
    sqlObj = [[SQLManager alloc] init] ;
    
    [sqlObj createAndOpenDatabase] ;
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:LocalizedString(@"SLC ID")
                                                                   message:nil
                                                            preferredStyle:UIAlertControllerStyleAlert];
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        // optionally configure the text field
        textField.text = SLCscannedCode ;
        textField.placeholder = LocalizedString(@"SLC ID") ;
        textField.borderStyle = UITextBorderStyleRoundedRect ;
        textField.font = [UIFont systemFontOfSize:16.0] ;
        textField.keyboardType = UIKeyboardTypeNumberPad;
        textField.returnKeyType = UIReturnKeySend ;
        textField.delegate = self ;
    }];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:LocalizedString(@"Confirm")
                                                       style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction *action) {
        UITextField *textField = [alert.textFields firstObject];
        
        //   securityCode = textField.text ;
        
        SLCscannedCode = textField.text ;
        
        strFinamSLCAdd = textField.text ;
        
        if ([textField.text isEqualToString:@""]) {
            [globalObject showAlertWithTitle:LocalizedString(@"Please enter SLC ID") message:@""];
        } else {
            /*if ([textField.text longLongValue] >= 4294967295) {
                [globalObject showAlertWithTitle:LocalizedString(@"Please enter valid SLC ID") message:@""];
            } else {*/
                [self LocationSettingCheck] ;
           // }
        }
        //---------------------------------------
        
    }];
    
    UIAlertAction *CancelAction = [UIAlertAction actionWithTitle:LocalizedString(@"Cancel")
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction *action) {
        [alertButton setTitle:LocalizedString(@"Stop") forState:UIControlStateNormal];
        
        [self.SLCcapture start];
        
    }];
    
    [alert addAction:CancelAction];
    [alert addAction:okAction];
    
    [self presentViewController:alert animated:YES completion:nil];
    
}

- (void)LocationSettingCheck
{
    
    if ([CLLocationManager locationServicesEnabled])
    {
        
        NSLog(@"Location Services Enabled");
        
        if ([CLLocationManager authorizationStatus]==kCLAuthorizationStatusDenied)
        {
            
            NSLog(@"Not Allow");
            
            UIAlertController * alert=   [UIAlertController
                                          alertControllerWithTitle:LocalizedString(@"Please allow SLC EZinstall to access your location.")
                                          message:LocalizedString(@"We will use it to pin the pole location.")
                                          preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* noButton = [UIAlertAction
                                       actionWithTitle:LocalizedString(@"Cancel")
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction * action)
                                       {
                
            }];
            
            [alert addAction:noButton];
            
            
            UIAlertAction* yesButton = [UIAlertAction
                                        actionWithTitle:LocalizedString(@"Setting")
                                        style:UIAlertActionStyleDefault
                                        handler:^(UIAlertAction * action)
                                        {
                if([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]]) {
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]options:[NSDictionary dictionary] completionHandler:^(BOOL success) {
                    }];
                }
                
            }];
            
            [alert addAction:yesButton];
            
            [self presentViewController:alert animated:YES completion:nil];
        }
        else
        {
            [self checkSLCid] ;
        }
    }
}



#pragma mark - Data get

-(void)checkMACidSLCid
{
    if ([globalObject checkInternetConnection]) {
        [globalObject showActivityIndicator];
        
        strRequestFor = @"checkSLCMACid" ;
        
        NSString *str =  checkmacandslcURL;
        NSString *urlString = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *url = [NSURL URLWithString:urlString];
        
        ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
        
        [request setRequestMethod:@"POST"] ;
        [request setDelegate:self] ;
        
        // ---------- Initialize SqlObject
        
        sqlObj = [[SQLManager alloc] init] ;
        
        [sqlObj createAndOpenDatabase] ;
        
        //------- Post Data
        
        // ---------- Initialize SqlObject
        
        [request setPostValue:[[sqlObj getAllUserInfo] valueForKey:@"clientID"] forKey:@"client_id"] ;
        
        [request setPostValue:[[sqlObj getAllUserInfo] valueForKey:@"userid"] forKey:@"user_id"] ;
        
        [request setPostValue:strFinamSLCAdd forKey:@"slc_id"] ;
        [request setPostValue:@"IOS" forKey:@"source"] ;
        [request setPostValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"SELECTDODETYPE"] forKey:@"node_type"] ;
        
        [request setPostValue:strMacAddress forKey:@"mac_address"] ;
        
        [request setTimeOutSeconds:180] ;
        
        //-------
        
        [request startAsynchronous] ;
    } else {
        [globalObject removeActivityIndicator];
    }
}

-(void)checkSLCid
{
    if ([globalObject checkInternetConnection]) {
        [globalObject showActivityIndicator];
        
        strRequestFor = @"checkslcid" ;
        
        NSString *str =  checkslcidURL;
        NSString *urlString = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *url = [NSURL URLWithString:urlString];
        
        ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
        
        [request setRequestMethod:@"POST"] ;
        [request setDelegate:self] ;
        
        // ---------- Initialize SqlObject
        
        sqlObj = [[SQLManager alloc] init] ;
        
        [sqlObj createAndOpenDatabase] ;
        
        //------- Post Data
        
        // ---------- Initialize SqlObject
        
        [request setPostValue:[[sqlObj getAllUserInfo] valueForKey:@"clientID"] forKey:@"client_id"] ;
        
        [request setPostValue:[[sqlObj getAllUserInfo] valueForKey:@"userid"] forKey:@"user_id"] ;
        
        [request setPostValue:strFinamSLCAdd forKey:@"slcid"] ;
        
        [request setTimeOutSeconds:180] ;
        
        //-------
        
        [request startAsynchronous] ;
    } else {
        [globalObject removeActivityIndicator];
    }
}

-(void)requestFailed:(ASIHTTPRequest *)request
{
    NSError *error = [request error];
    NSLog(@"error : %@" , error.localizedDescription);
    [globalObject removeActivityIndicator];
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:error.localizedDescription
                                  message:@""
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"OK"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action)
                                {
    }];
    
    [alert addAction:yesButton];
    
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)requestFinished:(ASIHTTPRequest *)request
{
    NSString *responseString = [request responseString];
    NSMutableDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:[responseString dataUsingEncoding:NSUTF8StringEncoding] options:0 error:nil];
    NSLog(@"Responce Dictionary : %@", responseDict);
    
    [globalObject removeActivityIndicator];
    
    if([[responseDict valueForKey:@"status"] isEqualToString:@"success"])
    {
        if ([strRequestFor isEqualToString:@"Login"])
        {
            [self.SLCcapture start];
            
            [sqlObj insertKeyToDatabase:@"user_logged_in" value:@"yes"];
            
            [sqlObj insertKeyToDatabase:@"token" value:[responseDict objectForKey:@"token"]];
            
        }
        else if ([strRequestFor isEqualToString:@"checkSLCMACid"])
        {
            
            UIStoryboard *story = [UIStoryboard storyboardWithName:@"MapViewController" bundle:nil];
            
            MapViewController *MapViewController = [story instantiateViewControllerWithIdentifier:@"MapViewController"];
            
            MapViewController.strSLCID = strFinamSLCAdd ;
            
            MapViewController.strMacAddress = strMacAddress ;
            
            [self.navigationController pushViewController:MapViewController animated:YES];
        } else {
            [self checkMACidSLCid];
        }
    } else if ([[responseDict objectForKey:@"status"] isEqualToString:@"continue"]) {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:[responseDict objectForKey:@"msg"]                                      message:@""
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:LocalizedString(@"No")
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
            
        }];
        
        UIAlertAction* nobutton = [UIAlertAction
                                   actionWithTitle:LocalizedString(@"Yes")
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action)
                                   {
            UIStoryboard *story = [UIStoryboard storyboardWithName:@"MapViewController" bundle:nil];
            
            MapViewController *MapViewController = [story instantiateViewControllerWithIdentifier:@"MapViewController"];
            
            MapViewController.strSLCID = strFinamSLCAdd ;
            
            MapViewController.strMacAddress = strMacAddress ;
            
            [self.navigationController pushViewController:MapViewController animated:YES];
        }];
        
        [alert addAction:yesButton];
        [alert addAction:nobutton];
        
        [self presentViewController:alert animated:YES completion:nil];
    } else if ([[responseDict objectForKey:@"status"] isEqualToString:@"error"]) {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:[responseDict objectForKey:@"msg"]                                      message:@""
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
            [self.SLCcapture start];
        }];
        
        [alert addAction:yesButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    } else if ([[responseDict objectForKey:@"status"] isEqualToString:@"0"]) {
        
        NSString *strMsg = [responseDict objectForKey:@"msg"];
        
        if (strMsg == nil || [strMsg  isEqualToString: @""]) {
            strMsg = @"Oops! Something went wrong. Try again later";
        }
        
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:LocalizedString(strMsg)
                                      message:@""
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
            sqlObj = [[SQLManager alloc] init] ;
            [sqlObj createAndOpenDatabase] ;
            
            [sqlObj DeleteData] ;
            
            UIStoryboard *story = [UIStoryboard storyboardWithName:@"SecurityCodeViewController" bundle:nil];
            
            SecurityCodeViewController *SecurityCodeViewController = [story instantiateViewControllerWithIdentifier:@"SecurityCodeViewController"];
            
            UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:SecurityCodeViewController];
            
            [[UIApplication sharedApplication].keyWindow setRootViewController:navController];
        }];
        
        [alert addAction:yesButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    } else {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:LocalizedString(@"Oops! Something went wrong. Try again later")
                                      message:@""
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
        }];
        [alert addAction:yesButton];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

-(void)AlertForLogin
{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:LocalizedString(@"Login")
                                                                   message:nil
                                                            preferredStyle:UIAlertControllerStyleAlert];
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        // optionally configure the text field
        
        textField.placeholder = LocalizedString(@"Username") ;
        textField.borderStyle = UITextBorderStyleRoundedRect ;
        textField.font = [UIFont systemFontOfSize:16.0] ;
        textField.keyboardType = UIKeyboardTypeAlphabet;
        textField.returnKeyType = UIReturnKeySend ;
        textField.delegate = self ;
        
    }];
    
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textFieldPass) {
        // optionally configure the text field
        
        textFieldPass.placeholder = LocalizedString(@"Password") ;
        textFieldPass.secureTextEntry = YES;
        textFieldPass.borderStyle = UITextBorderStyleRoundedRect ;
        textFieldPass.font = [UIFont systemFontOfSize:16.0] ;
        textFieldPass.keyboardType = UIKeyboardTypeAlphabet;
        textFieldPass.returnKeyType = UIReturnKeySend ;
        textFieldPass.delegate = self ;
        
    }];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:LocalizedString(@"Login")
                                                       style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction *action) {
        NSArray * textfields = alert.textFields;
        UITextField * namefield = textfields[0];
        UITextField * passfield = textfields[1];
        
        strUsername = namefield.text ;
        strPassword = passfield.text ;
        
        [self loginAction];
        
    }];
    
    
    [alert addAction:okAction];
    
    [self presentViewController:alert animated:YES completion:nil];
    
    
}

-(void)loginAction
{
    if ([strUsername isEqualToString:@""] || [strPassword isEqualToString:@""])
    {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:@"Please Enter Valid Username and Password."                                     message:@""
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
            [self AlertForLogin] ;
            
        }];
        
        [alert addAction:yesButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
    else
    {
        [self loginRequest];
    }
}


#pragma mark - Data Post

-(void)loginRequest
{
    if ([globalObject checkInternetConnection]) {
        [globalObject showActivityIndicator];
        
        strRequestFor = @"Login" ;
        
        NSString *str = userLoginURL ;
        NSString *urlString = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *url = [NSURL URLWithString:urlString];
        
        ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
        
        [request setRequestMethod:@"POST"] ;
        [request setDelegate:self] ;
        
        //------- Post Data
        
        // ---------- Initialize SqlObject
        
        sqlObj = [[SQLManager alloc] init] ;
        
        [sqlObj createAndOpenDatabase] ;
        
        NSLog(@"%@",[[sqlObj getAllUserInfo] valueForKey:@"clientID"]);
        
        NSLog(@"%@",[[sqlObj getAllUserInfo] valueForKey:@"userid"]);
        
        NSLog(@"%@",[[UIDevice currentDevice] identifierForVendor]);
        
        NSLog(@"%f",locationManager.location.coordinate.latitude);
        
        NSLog(@"%f",locationManager.location.coordinate.longitude);
        
        // 65D78ADF-32BC-4A73-B7BC-C9ADBC4C9F55
        
        NSString *strLat = [NSString stringWithFormat:@"%f",locationManager.location.coordinate.latitude];
        
        NSString *strLong = [NSString stringWithFormat:@"%f",locationManager.location.coordinate.longitude];
        
        [request setPostValue:[[sqlObj getAllUserInfo] valueForKey:@"clientID"] forKey:@"client_id"] ;
        
        [request setPostValue:[[sqlObj getAllUserInfo] valueForKey:@"userid"] forKey:@"userid"] ;
        
        [request setPostValue:strUsername forKey:@"username"] ;
        
        [request setPostValue:strPassword forKey:@"password"] ;
        
        [request setPostValue:strLat forKey:@"lat"] ;
        
        [request setPostValue:strLong forKey:@"long"] ;
        
        // [request setPostValue:[[[UIDevice currentDevice] identifierForVendor] UUIDString] forKey:@"DeviceID"] ;
        
        [request setPostValue:@"IOS" forKey:@"source"] ;
        
        [request setTimeOutSeconds:180] ;
        
        //-------
        
        [request startAsynchronous] ;
    } else {
        [globalObject removeActivityIndicator];
    }
}

#pragma mark - Textfield Delegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
   /* if ([textField.text longLongValue] >= 4294967295) {
        if([string isEqualToString:@""]) {
            return true;
        } else {
            return false;
        }
    } else {*/
        if([textField.text length]==0)
        {
            if([string isEqualToString:@" "])
            {
                return false;
            }
        }
        
        return true;
    //}
    
}
@end
