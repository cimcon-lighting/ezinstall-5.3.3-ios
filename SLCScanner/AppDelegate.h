//
//  AppDelegate.h
//  SLCScanner
//
//  Created by INT MAC 2015 on 23/02/18.
//  Copyright © 2018 INT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SQLManager.h"
#import "ScanViewController.h"

typedef enum  {
    kAppleStandard = 0,
    kAppleHybrid,
    kAppleSatelite,
} mapType ;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (assign) mapType mpType ;

@property (strong, nonatomic) UIWindow *window;

//-------- SqlManager Obj -------//

@property (strong , nonatomic) SQLManager *sqlObj;

@property (nonatomic, assign) int strSelectedMap ;

@property (strong , nonatomic) NSString *strEditMacID ;
@property (strong , nonatomic) NSString *strFrmExternal ;
@property (assign , nonatomic) int selectedTabIndex;

@property (strong , nonatomic) NSString *strEditSLCID ;

@end

