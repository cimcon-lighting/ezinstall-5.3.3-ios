//
//  AppDelegate.m
//  SLCScanner
//
//  Created by INT MAC 2015 on 23/02/18.
//  Copyright © 2018 INT. All rights reserved.
//

#import "AppDelegate.h"

#import "API.h"
#import "LocalizeHelper.h"
#import "ViewController.h"
#import "PoleViewController.h"

#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import <FirebaseAnalytics/FirebaseAnalytics.h>
#import <Firebase/Firebase.h>

@import GoogleMaps;

@interface AppDelegate ()

@end

@implementation AppDelegate
@synthesize sqlObj, strEditMacID , strEditSLCID, strFrmExternal;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    [Fabric with:@[[Crashlytics class]]];
    
    [FIRApp configure];
    
    
    strEditMacID        = @"" ;
    strFrmExternal      = @"";
    strEditSLCID        = @"" ;
    _strSelectedMap     = 0;
    
    [self sqlCheck] ;
    
   // [GMSServices provideAPIKey:@"AIzaSyA8NezlaD2DPEjhoTl8JPVIXA54e-4ByyI"];
    [GMSServices provideAPIKey:@"AIzaSyAossywG9Y1aP-DcX3Uh3hM9RFBn07KZoo"];
    
    NSString *language          = [[NSLocale preferredLanguages] objectAtIndex:0];
    NSDictionary *languageDic   = [NSLocale componentsFromLocaleIdentifier:language];
    NSString *strLanguage       = [languageDic objectForKey:@"kCFLocaleLanguageCodeKey"];
    if ([[NSUserDefaults standardUserDefaults] stringForKey:@"appLanguage"] != nil) {
        LocalizationSetLanguage([[NSUserDefaults standardUserDefaults] stringForKey:@"appLanguage"]);
    } else {
        LocalizationSetLanguage(strLanguage);
    }
    
    
    if ([self userAlreadyLoggedIn])
    {
        NSLog(@"Logged in") ;
        
        NSDate *startDate = TimeSlotValue ;
        NSDate *endDate = [NSDate date];
        
        // Way 2
        NSTimeInterval timeDifference = [endDate timeIntervalSinceDate:startDate];
        
        double minutes = timeDifference / 60;
        double hours = minutes / 60;
        double seconds = timeDifference;
        double days = minutes / 1440;
        
        NSLog(@" days = %.0f,hours = %.2f, minutes = %.0f,seconds = %.0f", days, hours, minutes, seconds);
        
        if (hours >= 24)
        {            
            NSString *strClientID = [[sqlObj getAllUserInfo] valueForKey:@"clientID"] ;
            NSString *strUserID = [[sqlObj getAllUserInfo] valueForKey:@"userid"] ;
            NSString *strScanLBL = [[sqlObj getAllUserInfo] valueForKey:@"ScanLBL"] ;
            NSString *strScanPH = [[sqlObj getAllUserInfo] valueForKey:@"ScanPH"] ;
            
            NSString *strPoleID = [[sqlObj getAllUserInfo] valueForKey:@"PoleID"] ;
            NSString *strMeasurmentUnit = [[sqlObj getAllUserInfo] valueForKey:@"MeasurmentUnit"] ;
            NSString *strOtherData = [[sqlObj getAllUserInfo] valueForKey:@"OtherData"] ;
            
            [sqlObj DeleteData] ;
            
            [sqlObj createAndOpenDatabase] ;
            
            [sqlObj insertKeyToDatabase:@"clientID" value:strClientID];
            [sqlObj insertKeyToDatabase:@"userid" value:strUserID];
            [sqlObj insertKeyToDatabase:@"ScanLBL" value:strScanLBL];
            [sqlObj insertKeyToDatabase:@"ScanPH" value:strScanPH];
            [sqlObj insertKeyToDatabase:@"PoleID" value:strPoleID];
            [sqlObj insertKeyToDatabase:@"MeasurmentUnit" value:strMeasurmentUnit];
            [sqlObj insertKeyToDatabase:@"OtherData" value:strOtherData];
            
            
            self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
            
            UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            
            ViewController *ViewController = [story instantiateViewControllerWithIdentifier:@"ViewController"];
            
            self.window.rootViewController = ViewController ;
            
            [self.window makeKeyAndVisible];
            
        }
        else
        {
        
            self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
            
            UIStoryboard *story = [UIStoryboard storyboardWithName:@"TabBarView" bundle:nil];
            
            PoleViewController *PoleViewController = [story instantiateViewControllerWithIdentifier:@"TabBarView"];
            
            [[UITabBarItem appearance] setTitleTextAttributes:@{ NSForegroundColorAttributeName : [UIColor whiteColor] }
                                                     forState:UIControlStateNormal];
            [[UITabBarItem appearance] setTitleTextAttributes:@{ NSForegroundColorAttributeName : [UIColor whiteColor] }
                                                     forState:UIControlStateSelected];
            
            self.window.rootViewController = PoleViewController ;
            
            [self.window makeKeyAndVisible];
        }
        
    }
    /*else if ([self userAddSecurityCode])
    {
        NSLog(@"Logged in") ;
        
        self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
        
        UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        ViewController *ViewController = [story instantiateViewControllerWithIdentifier:@"ViewController"];
        
        self.window.rootViewController = ViewController ;
        
        [self.window makeKeyAndVisible];
     
    }*/
   
    return YES;
}

#pragma mark - Create Local Sqlite

-(void)sqlCheck
{
    sqlObj = [[SQLManager alloc] init] ;
    [sqlObj createAndOpenDatabase] ;
}

#pragma mark - Check User Log in Status

-(BOOL)userAddSecurityCode
{
    NSString *clientID = [[sqlObj getAllUserInfo] valueForKey:@"clientID"] ;
    
    if ([clientID isEqualToString:@""] || clientID == nil)
    {
        return NO ;
    }
    else
    {
        return YES ;
    }
    
}

-(BOOL)userAlreadyLoggedIn
{
    NSString *userLoggedIn = [[sqlObj getAllUserInfo] valueForKey:@"user_logged_in"] ;
    
    if ([userLoggedIn isEqualToString:@"Yes"])
    {
        return YES ;
    }
    else
    {
        return NO ;
    }
    
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
    NSDate *now = [NSDate date];
    
    [[NSUserDefaults standardUserDefaults] setValue:now forKey:@"TimeSlot"];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    
    NSDate *startDate = TimeSlotValue ;
    NSDate *endDate = [NSDate date];
    
    // Way 2
    NSTimeInterval timeDifference = [endDate timeIntervalSinceDate:startDate];
    
    double minutes = timeDifference / 60;
    double hours = minutes / 60;
    double seconds = timeDifference;
    double days = minutes / 1440;
    
    NSLog(@" days = %.0f,hours = %.2f, minutes = %.0f,seconds = %.0f", days, hours, minutes, seconds);
    
    if (hours >= 24)
    {
        [self sqlCheck];
        
        NSString *strClientID = [[sqlObj getAllUserInfo] valueForKey:@"clientID"] ;
        NSString *strUserID = [[sqlObj getAllUserInfo] valueForKey:@"userid"] ;
        NSString *strScanLBL = [[sqlObj getAllUserInfo] valueForKey:@"ScanLBL"] ;
        NSString *strScanPH = [[sqlObj getAllUserInfo] valueForKey:@"ScanPH"] ;
        
        NSString *strPoleID = [[sqlObj getAllUserInfo] valueForKey:@"PoleID"] ;
        NSString *strMeasurmentUnit = [[sqlObj getAllUserInfo] valueForKey:@"MeasurmentUnit"] ;
        NSString *strOtherData = [[sqlObj getAllUserInfo] valueForKey:@"OtherData"] ;
        
        [sqlObj DeleteData] ;
        
        [sqlObj createAndOpenDatabase] ;
        
        [sqlObj insertKeyToDatabase:@"clientID" value:strClientID];
        [sqlObj insertKeyToDatabase:@"userid" value:strUserID];
        [sqlObj insertKeyToDatabase:@"ScanLBL" value:strScanLBL];
        [sqlObj insertKeyToDatabase:@"ScanPH" value:strScanPH];
        [sqlObj insertKeyToDatabase:@"PoleID" value:strPoleID];
        [sqlObj insertKeyToDatabase:@"MeasurmentUnit" value:strMeasurmentUnit];
        [sqlObj insertKeyToDatabase:@"OtherData" value:strOtherData];
        
        self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
        
        UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        ViewController *ViewController = [story instantiateViewControllerWithIdentifier:@"ViewController"];
        
        UINavigationController *navcontroller = [[UINavigationController alloc] initWithRootViewController:ViewController] ;
        
        self.window.rootViewController = navcontroller ;
        
        [self.window makeKeyAndVisible];
        
    }
    
}


- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.

}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


@end
