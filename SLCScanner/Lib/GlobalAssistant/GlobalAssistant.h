//
//  GlobalAssistant.h
//  WestfordAcademy
//
//  Created by INT MAC Mini on 07/11/14.
//  Copyright (c) 2014 INT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MRProgress.h"
//#import "AppDelegate.h"

@class AppDelegate ;

@interface GlobalAssistant : NSObject
{
    UIAlertView *alert ;
}
@property (strong , nonatomic) AppDelegate *appDelegate ;

@property (nonatomic, retain) NSMutableArray *arrMaps;

-(void)showAlertWithTitle:(NSString *)alertTitle message:(NSString *)message ;

-(void)showActivityIndicator ;
-(void)removeActivityIndicator ;
-(BOOL)checkInternetConnection;

-(void)showActivityIndicatorForSave ;
-(void)buttonWithCornerRadious:(float)radious btnView:(UIView*)vw;
@end

