//
//  GlobalAssistant.m
//  WestfordAcademy
//
//  Created by INT MAC Mini on 07/11/14.
//  Copyright (c) 2014 INT. All rights reserved.
//

#import "GlobalAssistant.h"

#import "AppDelegate.h"
#import "Reachability.h"
#import "LocalizeHelper.h"

@implementation GlobalAssistant

#pragma mark - UIALertView Method

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.arrMaps = [[NSMutableArray alloc] initWithObjects:
                        LocalizedString(@"Apple Satellite"),
                        LocalizedString(@"Apple Standard"),
                        LocalizedString(@"Apple Hybrid"),
                        LocalizedString(@"Google Map Satellite"),
                        LocalizedString(@"Google Map Standard"),
                        LocalizedString(@"Google Map Hybrid"),
                        LocalizedString(@"Open Street Standard"),nil];
    }
    return self;
}
-(BOOL)checkInternetConnection {
    if ([[Reachability reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)
    {
        alert = [[UIAlertView alloc] initWithTitle:@"No Internet Connection" message:@"" delegate:Nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return false;
    }
    else
    {
        return true;
        //connection available
    }
}
-(void)showAlertWithTitle:(NSString *)alertTitle message:(NSString *)message
{
    alert = [[UIAlertView alloc] initWithTitle:alertTitle message:message delegate:Nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}

-(void)buttonWithCornerRadious:(float)radious btnView:(UIView*)vw {
    vw.layer.cornerRadius   = radious;
    vw.layer.masksToBounds  = true;
}

#pragma mark - UIActivity Indicator

-(void)showActivityIndicator
{
     _appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    [MRProgressOverlayView showOverlayAddedTo:_appDelegate.window title:[NSString stringWithFormat:@"%@",LocalizedString(@"Please wait. This can take some time. Don't tap on the back button.")] mode:MRProgressOverlayViewModeIndeterminateSmallDefault animated:YES];
}

-(void)removeActivityIndicator
{
    _appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    [MRProgressOverlayView dismissOverlayForView:_appDelegate.window animated:YES];
}

-(void)showActivityIndicatorForSave
{
    _appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    [MRProgressOverlayView showOverlayAddedTo:_appDelegate.window title:[NSString stringWithFormat:@"%@...",LocalizedString(@"Saving")] mode:MRProgressOverlayViewModeIndeterminateSmallDefault animated:YES];
    
}

@end




