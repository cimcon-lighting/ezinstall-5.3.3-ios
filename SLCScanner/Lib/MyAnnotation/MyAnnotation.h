//
//  MyAnnotation.h
//  sampleMapDemo
//
//  Created by INT MAC Mini on 15/12/15.
//  Copyright © 2015 INT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
@interface MyAnnotation : MKAnnotationView
{
    CLLocationCoordinate2D _coordinate;
    NSString *_title;
    NSString *_subtitle;
    NSString *_strID;
}
@property (nonatomic, retain) NSString *sID;

- (id)initWithCoordinate:(CLLocationCoordinate2D)coordinate title:(NSString *)title subtitle:(NSString *)subtitle strid:(NSString *)strID;

@end
