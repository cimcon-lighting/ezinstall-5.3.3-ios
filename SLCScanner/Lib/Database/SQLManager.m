//
//  SQLManager.m
//  CL Panic
//
//  Created by INT MAC 2015 on 28/06/16.
//  Copyright © 2016 IntotalityInc. All rights reserved.
//

#import "SQLManager.h"

@implementation SQLManager

@synthesize DB ;

#pragma mark - Database Methods

-(void)createAndOpenDatabase
{
    //NSLog(@"called") ;
    
    [self openDB];
    [self createTable] ;
}

-(NSString *)documentsPath
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDir = [paths objectAtIndex:0];
    NSLog(@"%@",paths);
    return [documentDir stringByAppendingPathComponent:@"ClPanic.sqlite"];
}

-(void)openDB
{
    if (sqlite3_open([[self documentsPath]UTF8String],&DB) == SQLITE_OK)
    {
        // NSLog(@"database opened");
    }
    else
    {
        sqlite3_close(DB);
        NSAssert(0, @"database failed to open");
    }
}

-(void) createTable
{
    // Table - [ User Data Table ]
    
    NSString *tableName = @"user_data";
    NSString *field1 = @"id";
    NSString *field2 = @"key";
    NSString *field3 = @"value";
    
    NSString *sql_stat = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS '%@' ('%@' " "INTEGER PRIMARY KEY AUTOINCREMENT, '%@' TEXT,'%@' TEXT);",tableName, field1, field2,field3];
    
    if (sqlite3_exec(DB, [sql_stat UTF8String], NULL, NULL, &errorDB) == SQLITE_OK &&
        sqlite3_exec(DB, [sql_stat UTF8String], NULL, NULL, &errorDB) == SQLITE_OK)
    {
        // NSLog(@"Table1 Created");
    }
    else
    {
        sqlite3_close(DB);
        NSAssert(0, @"Tabled failed to create.");
    }
}

#pragma mark - Get Data from DB

-(NSMutableDictionary *)getAllUserInfo
{
    NSMutableDictionary *userInfoDict = [[NSMutableDictionary alloc] init] ;
    
    NSMutableArray *keyArray = [[NSMutableArray alloc] init];
    NSMutableArray *valueArray = [[NSMutableArray alloc] init];
    
    NSString *display = [NSString stringWithFormat:@"SELECT * FROM user_data"];
    
    sqlite3_stmt *statement ;
    
    if (sqlite3_prepare_v2(DB, [display UTF8String], -1, &statement, nil) == SQLITE_OK)
    {
        while (sqlite3_step(statement) == SQLITE_ROW)
        {
            char *field = (char *) sqlite3_column_text(statement, 1);
            NSString *fieldStr = [[NSString alloc] initWithUTF8String:field];
            
            char *field2 = (char *) sqlite3_column_text(statement, 2);
            NSString *fieldStr2 = [[NSString alloc] initWithUTF8String:field2];
            
            [keyArray addObject:fieldStr] ;
            [valueArray addObject:fieldStr2] ;
        }
    }
    
    for (int i = 0 ; i < keyArray.count ; i++)
    {
        [userInfoDict setValue:[valueArray objectAtIndex:i] forKey:[keyArray objectAtIndex:i]] ;
    }
    
    return userInfoDict ;
}

#pragma mark - Insert User Data to table

-(BOOL)insertKeyToDatabase:(NSString *)key value:(NSString *)value
{
    NSString *insertTag = [NSString stringWithFormat:@"INSERT INTO user_data ('key','value') VALUES ('%@','%@')", key , value];
    
    if (sqlite3_exec(DB, [insertTag UTF8String], NULL, NULL, &errorDB) == SQLITE_OK)
    {
        return YES ;
    }
    else
    {
        return NO ;
    }
}

-(void)updateDATA : (NSString *)value : (NSString *)key
{
    sqlite3_stmt *updateStmt = nil;
    
    if (sqlite3_open([[self documentsPath] UTF8String], &DB) == SQLITE_OK)
    {
        if (updateStmt == nil)
        {
            const char *sql = "UPDATE user_data SET value =? where key = ?";
            
            if (sqlite3_prepare_v2(DB, sql, -1, &updateStmt, NULL) != SQLITE_OK)
                NSAssert1(0, @"Error while creating add statement. '%s'",sqlite3_errmsg(DB));
        }
        
        sqlite3_bind_text(updateStmt, 2, [key UTF8String], -1, SQLITE_TRANSIENT);
        
        sqlite3_bind_text(updateStmt, 1, [value UTF8String], -1, SQLITE_TRANSIENT);
        
        
        
        if (SQLITE_DONE != sqlite3_step(updateStmt))
            NSAssert1(0, @"Error while updating data. '%s'", sqlite3_errmsg(DB));
        else
        {
            sqlite3_finalize(updateStmt);
            sqlite3_close(DB);
        }
    }
    else
    {
        
        sqlite3_finalize(updateStmt);
        sqlite3_close(DB);
    }
    
}


-(void)DeleteData
{
    sqlite3_stmt *deleteStmnt = nil;
    
    if(sqlite3_open([[self documentsPath] UTF8String],&DB) == SQLITE_OK)
    {
        if(deleteStmnt == nil)
        {
            const char *sql = "delete from user_data";
            if(sqlite3_prepare_v2(DB, sql, -1, &deleteStmnt, NULL) != SQLITE_OK)
                NSAssert1(0, @"Error while creating delete statement. '%s'", sqlite3_errmsg(DB));
        }
        
        
        if (SQLITE_DONE != sqlite3_step(deleteStmnt))
            NSAssert1(0, @"Error while deleting. '%s'", sqlite3_errmsg(DB));
        
        else{
            
            sqlite3_finalize(deleteStmnt);
            sqlite3_close(DB);
        }
    }
    else{
        
        sqlite3_finalize(deleteStmnt);
        sqlite3_close(DB);
    }
}



@end
