//
//  SQLManager.h
//  CL Panic
//
//  Created by INT MAC 2015 on 28/06/16.
//  Copyright © 2016 IntotalityInc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "sqlite3.h"

@interface SQLManager : NSObject

{
    char *errorDB ;
}

@property (assign , nonatomic) sqlite3 *DB ;

/* Open DB And Create table if not exist  */

-(void)createAndOpenDatabase ;

/* Get All User Info Details */

-(NSMutableDictionary *)getAllUserInfo ;

/* Insert User Data to "user_data" Table */

-(BOOL)insertKeyToDatabase:(NSString *)key value:(NSString *)value ;
-(void)updateDATA : (NSString *)value : (NSString *)key ;
-(void)DeleteData ;

@end
