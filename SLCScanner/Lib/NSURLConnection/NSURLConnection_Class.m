//
//  NSURLConnection_Class.m
//  DemoNSObject
//
//  Created by INT MAC Mini on 19/06/15.
//  Copyright (c) 2015 INT. All rights reserved.
//

#import "NSURLConnection_Class.h"

@implementation NSURLConnection_Class

-(void)RequestForURL:(NSString *)str
{

    NSURL *url = [NSURL URLWithString:str];
    NSMutableURLRequest *rqst = [[NSMutableURLRequest alloc]initWithURL:url];
    _conn = [[NSURLConnection alloc] initWithRequest:rqst delegate:self];
    if(_conn)
    {
        _responseData = [[NSMutableData alloc] init] ;
    }
    

    NSAssert(self.conn != nil, @"Failure to create URL connection.");
}

#pragma mark - NSURLConnection Delegate

-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    if (connection == _conn)
    {
        [_responseData setLength:0];
    }
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    if (connection == _conn)
    {
        [_responseData appendData:data];
    }
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    /// Delegate Method if Connection Fail
    
    if([_delegate respondsToSelector:@selector(connectionFailWithError:)])
    {
        [[self delegate] connectionFailWithError:error];
    }
    [_globalObject removeActivityIndicator] ;

}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    
    NSString *str = [[NSString alloc] initWithBytes:[_responseData mutableBytes] length:[_responseData length] encoding:NSUTF8StringEncoding];
    
    /// Delegate Method After connection DidFinish
    
    if([_delegate respondsToSelector:@selector(connectionFinishWithResponse:)])
    {
        [[self delegate] connectionFinishWithResponse:str];
    }
    
    [_globalObject removeActivityIndicator];

}

@end
