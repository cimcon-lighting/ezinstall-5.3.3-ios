//
//  NSURLConnection_Class.h
//  DemoNSObject
//
//  Created by INT MAC Mini on 19/06/15.
//  Copyright (c) 2015 INT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GlobalAssistant.h"

@protocol NSURLConnection_CustomDelegate < NSObject >

- (void)connectionFinishWithResponse:(NSString *)responseString ;
- (void)connectionFailWithError:(NSError *)error ;

@end

@interface NSURLConnection_Class : NSOperation < NSURLConnectionDelegate >

@property (strong , nonatomic) NSMutableData *responseData ;
@property (strong , nonatomic) NSURLConnection *conn ;

@property (strong , nonatomic) NSMutableArray *array ;

@property (weak) id < NSURLConnection_CustomDelegate > delegate ;


/// Global Assistant class object

@property (strong , nonatomic) GlobalAssistant *globalObject ;


-(void)RequestForURL:(NSString *)str ;

@end
